#pragma once


enum DataType : char
{
    DTAdd,
    DTSub,
    DTMul,
    DTDiv,
};

class DataChunk
{
public:
    DataType type;
    char d1;
};

