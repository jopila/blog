// if.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <algorithm>

#include "..\common\Random.h"
#include "..\common\Timing.h"

#include "DataChunk.h"


const size_t NUM_DATA = 10000;

// We'll just stick to the 'basic' 4 types for this one
void TestSmall()
{
    // Initialize with random types and random data
    std::vector<DataChunk> data;
    data.reserve(NUM_DATA);
    for(size_t i = 0; i < NUM_DATA; i++)
    {
        DataChunk newData;
        newData.type = (DataType)gRandom.GetInt(DTDiv);
        newData.d1 = gRandom.GetInt(CHAR_MIN, CHAR_MAX);
        if(newData.d1 == 0) newData.d1 = 1; // Prevent divide by zero :(
        data.push_back(newData);
    }

    // Do the test!
    Timer timer;
    timer.Start();
    static int rawr = 0;
    for(size_t i = 0; i < data.size(); i++)
    {
        DataChunk& d = data[i];

        if(d.type == DTAdd) rawr += d.d1;
        if(d.type == DTSub) rawr -= d.d1;
        if(d.type == DTMul) rawr *= d.d1;
        if(d.type == DTDiv) rawr /= d.d1;
    }
    double ifTime = timer.End();
    std::cout << "if Time: " << ifTime * 1000000 << std::endl;

    // Sort out our data chunks!
    std::map<DataType, std::vector<DataChunk>> buckets;
    for(size_t i = 0; i < data.size(); i++)
    {
        buckets[data[i].type].push_back(data[i]);
    }

    // And do the second test!
    std::vector<DataChunk> bucketAdd = buckets[DTAdd];
    std::vector<DataChunk> bucketSub = buckets[DTSub];
    std::vector<DataChunk> bucketMul = buckets[DTMul];
    std::vector<DataChunk> bucketDiv = buckets[DTDiv];

    timer.Start();
    for(size_t i = 0; i < bucketAdd.size(); i++) rawr += bucketAdd[i].d1;
    for(size_t i = 0; i < bucketSub.size(); i++) rawr -= bucketSub[i].d1;
    for(size_t i = 0; i < bucketMul.size(); i++) rawr *= bucketMul[i].d1;
    for(size_t i = 0; i < bucketDiv.size(); i++) rawr /= bucketDiv[i].d1;
    double bucketTime = timer.End();
    std::cout << "bucket Time: " << bucketTime * 1000000 << std::endl;

    // Test branch prediction!
    timer.Start();
    std::sort(data.begin(), data.end(), [](const DataChunk& d1, const DataChunk& d2)->bool {return d1.type < d2.type;});
    double sortTime = timer.End();
    std::cout << "sort Time: " << sortTime * 1000000 << std::endl;

    timer.Start();
    for(size_t i = 0; i < data.size(); i++)
    {
      DataChunk& d = data[i];

      if(d.type == DTAdd) rawr += d.d1;
      if(d.type == DTSub) rawr -= d.d1;
      if(d.type == DTMul) rawr *= d.d1;
      if(d.type == DTDiv) rawr /= d.d1;
    }
    double branchPredictionTime = timer.End();
    std::cout << "branch prediction Time: " << branchPredictionTime * 1000000 << std::endl;
}

void main()
{
    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    TestSmall();


    // Wait for input...
    int dummy;
    std::cin >> dummy;
}

