// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#include "Objbase.h"

#include <vector>
#include <chrono>

#include "..\PerfInjector\SharedMemoryInfo.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}


enum ProfileType
{
	Time,          // time - time to profile for (ms)
	FunctionCount, // count - time that function has to be called before capture ends
	FunctionTime,  // functionTime - min time function has to take for a capture (ms)
};

#pragma pack(push, 4)
struct NativeProfileSettings
{
	ProfileType type;
	int32 time;
	int32 functionCount;
	int32 functionTime;
  SAFEARRAY* functionModificationAddresses;
  uint64 detourSize;
};
#pragma pack(pop)

struct BufferData
{
	double secondsPerCycle;
	double failureSpan;
	uint64 failureEventsWritten;
	int32 waitingCount;
	int32 bufferSize;
	Timestamp* buffer;
};

extern "C"
{
  __declspec(dllexport) BufferData NativeProfile(NativeProfileSettings settings)
  {
		BufferData result = { 0.0, 0.0, 0, 0, 0, nullptr };

		HANDLE fileMappingHandle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, fileMapSize, TEXT("perfToolSharedMemory"));
		BOOL fInit = (GetLastError() != ERROR_ALREADY_EXISTS); 
		if(fileMappingHandle == NULL)
			return result;

		SharedMemoryInfo* sharedMemoryInfo = (SharedMemoryInfo*)MapViewOfFile(fileMappingHandle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
		if(sharedMemoryInfo == NULL)
			return result;

		uint64 functionModificationsCount = 0;
		uint64* functionModificationAddresses = nullptr;
		if(settings.functionModificationAddresses)
		{
			functionModificationsCount = settings.functionModificationAddresses->rgsabound->cElements;
			functionModificationAddresses = (uint64*)settings.functionModificationAddresses->pvData;
		}

		const int32 readBufferSize = 16*1024*1024;
		std::vector<Timestamp> buffer;
		buffer.reserve(readBufferSize);
		int depth = 0;

		std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
		uint64 readPosition = sharedMemoryInfo->currentBufferReadPosition;
		float timeTaken = 0.0f;
		float timeToStop = settings.time / 1000.0f;                // convert ms to s
		float functionHitchTime = settings.functionTime / 1000.0f; // ^
		int32 functionCountSoFar = 0;
		bool functionStarted = false;
		std::chrono::high_resolution_clock::time_point functionTimeStart;
		uint64 beginTsc = __rdtsc();
		while(1)
		{
			// If we've caught up to the writing, wait
			if(readPosition < sharedMemoryInfo->currentBufferReadPosition)
			{
				buffer.push_back(sharedMemoryInfo->buffer[readPosition++ % fileMapBufferSize]);
			}
			else
			{
				result.waitingCount++;
				YieldProcessor();
			}

			if(readPosition + fileMapBufferSize < sharedMemoryInfo->currentBufferWritePosition)
			{
				std::chrono::duration<double> span = std::chrono::high_resolution_clock::now() - start;
				result.failureSpan = span.count();
				uint64 startRead = readPosition - buffer.size();
				uint64 eventsWritten = sharedMemoryInfo->currentBufferWritePosition - startRead;
				result.failureEventsWritten = eventsWritten;
				break;
			}

			// Check if we can finish capturing
			auto isEndEventForFunction = [settings, functionModificationsCount, functionModificationAddresses](Timestamp& ts)
			{
				for(size_t i = 1; i < functionModificationsCount; i++)
					if(ts.address == functionModificationAddresses[i] + settings.detourSize)
						return true;
				return false;
			};

			if(settings.type == ProfileType::Time)
			{
				std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - start;
				timeTaken = span.count();
				if(timeTaken > timeToStop)
					break;
			}
			else if(settings.type == ProfileType::FunctionCount)
			{
				if(functionStarted == false && buffer.back().address == functionModificationAddresses[0] + settings.detourSize)
				{
					buffer.erase(buffer.begin(), buffer.end() - 1);
					functionStarted = true;
				}
				if(functionStarted == true && isEndEventForFunction(buffer.back()))
				{
					functionCountSoFar++;
					if(functionCountSoFar >= settings.functionCount)
						break;
				}
			}
			else if(settings.type == ProfileType::FunctionTime)
			{
				if(functionStarted == false && buffer.back().address == functionModificationAddresses[0] + settings.detourSize)
				{
					buffer.erase(buffer.begin(), buffer.end() - 1);
					functionStarted = true;
					functionTimeStart = std::chrono::high_resolution_clock::now();
				}
				if(functionStarted == true && isEndEventForFunction(buffer.back()))
				{
					std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - functionTimeStart;
					timeTaken = span.count();
					if(timeTaken > functionHitchTime)
						break;
					else
						functionStarted = false;
				}
			}
		}

		uint64 endTsc = __rdtsc();
		std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - start;
		timeTaken = span.count();
		result.secondsPerCycle = timeTaken / (endTsc - beginTsc);

		result.bufferSize = (int)buffer.size();
		int bytes = result.bufferSize * sizeof(Timestamp);
		result.buffer = (Timestamp*)CoTaskMemAlloc(bytes);
		memcpy(result.buffer, &buffer[0], bytes);

		return result;
  }
}


