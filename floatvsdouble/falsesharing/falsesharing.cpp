// falsesharing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <thread>

#include "..\common\Timing.h"

const int NUM_ITERATIONS = 10000000;

struct ThreadData
{
  volatile int* data;
  TimePoint startTime;
  TimePoint endTime;
};

void adder(ThreadData* data)
{
    data->startTime = Timer::CurrentTime();
    for(int i = 0; i < NUM_ITERATIONS; i++)
    {
        *data->data += *data->data;
    }
    data->endTime = Timer::CurrentTime();
}

void Test(int numThreads, int dataSize)
{
    TimePoint start = Timer::CurrentTime();

    // Create some data for our contention
    int* data = new int[numThreads * dataSize];
    for(int i = 0; i < numThreads; i++)
        data[i * dataSize] = 1;

    // Create the threads
    ThreadData* threadData = new ThreadData[numThreads];
    std::thread* threads = new std::thread[numThreads];
    for(int i = 0; i < numThreads; i++)
    {
        threadData[i].data = &data[i * dataSize];
        threads[i] = std::thread(adder, &threadData[i]);
    }

    // Wait for all the threads to finish
    for(int i = 0; i < numThreads; i++)
    {
        threads[i].join();
    }

    // Printy print print!
    std::cout << "Test(" << numThreads << ", " << dataSize << ")" << std::endl;
    for(int i = 0; i < numThreads; i++)
    {
        std::cout << "d" << numThreads << ".startTime = " << Timer::DurationBetweenPoints(start, threadData[i].startTime) * 1000000 << std::endl;
        std::cout << "d" << numThreads << ".endTime   = " << Timer::DurationBetweenPoints(start, threadData[i].endTime) * 1000000 << std::endl;
    }

    delete[] data;
}

int main()
{
    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    Test(1, 1);
    Test(2, 1);
    Test(4, 1);
    Test(8, 1);

    Test(2, 2);
    Test(2, 4);
    Test(2, 8);
    Test(2, 16);
    Test(2, 32);
    Test(2, 64);

    // Wait for input...
    int dummy;
    std::cin >> dummy;

    return 0;
}

