
#include "stdafx.h"

#include <algorithm>

#include "BasicControls.h"
#include "CompositeControls.h"

namespace gui
{
	ScrollBar::ScrollBar(float length, Type type, float pageSize, SpriteRenderer* spriteRenderer)
		: Control(type == Type::Vertical ? 16.0f : length, type == Type::Vertical ? length : 16.0f), type(type), pageSize(pageSize)
	{
		currentPage = 0;
		dragging = false;

		Interactable* scrollBarInteractable;
		ImageButton* scrollUpButton;
		ImageButton* scrollDownButton;

		if(type == Type::Vertical)
		{
			(masterPanel = new Panel(16.0f, length, Panel::Type::VerticalStack, "Scroll Bar V"))->AddChildren(
				(scrollUpButton = new ImageButton(16.0f, 16.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonV"), spriteRenderer->GetSpriteIndex("arrowUp"))),
				(scrollOverlay = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
					(new Image(1.0f, 1.0f, spriteRenderer->GetSpriteIndex("scrollBackgroundV"))),
					(scrollBarInteractable = new Interactable(1.0f, 1.0f)),
					(scrollButtonAlignment = new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::TopLeft))->SetChild(
						(scrollButton = new ImageButton(1.0f, 32.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonV"), spriteRenderer->GetSpriteIndex("scroll_buttonForegroundV"))))),
				(scrollDownButton = new ImageButton(16.0f, 16.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonV"), spriteRenderer->GetSpriteIndex("arrowDown"))));
		}
		else
		{
			(masterPanel = new Panel(length, 16.0f, Panel::Type::HorizontalStack, "Scroll Bar H"))->AddChildren(
				(scrollUpButton = new ImageButton(16.0f, 16.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonH"), spriteRenderer->GetSpriteIndex("arrowLeft"))),
				(scrollOverlay = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
					(new Image(1.0f, 1.0f, spriteRenderer->GetSpriteIndex("scrollBackgroundH"))),
					(scrollBarInteractable = new Interactable(1.0f, 1.0f)),
					(scrollButtonAlignment = new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::TopLeft))->SetChild(
						(scrollButton = new ImageButton(1.0f, 32.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonH"), spriteRenderer->GetSpriteIndex("scroll_buttonForegroundH"))))),
				(scrollDownButton = new ImageButton(16.0f, 16.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("scrollButtonH"), spriteRenderer->GetSpriteIndex("arrowRight"))));
		}

		scrollBarInteractable->onDrag = [=](void*, int32 currentX, int32 currentY, int32 offsetX, int32 offsetY, bool released)
		{
			if(!dragging)
			{
				if(type == Type::Vertical && (currentY < scrollButton->renderY || currentY >= scrollButton->renderY + scrollButton->renderH))
					return;
				if(type == Type::Horizontal && (currentX < scrollButton->renderX || currentX >= scrollButton->renderX + scrollButton->renderW))
					return;
				if(type == Type::Vertical)   dragStart = currentY - (int32)scrollButtonAlignment->childOffsetY;
				if(type == Type::Horizontal) dragStart = currentX - (int32)scrollButtonAlignment->childOffsetX;
			}
			dragging = !released;

			int32 current = type == Type::Vertical ? currentY : currentX;
			float scrollOverlayLength = type == Type::Vertical ? scrollOverlay->renderH : scrollOverlay->renderW;
			float scrollButtonLength = type == Type::Vertical ? scrollButton->renderH : scrollButton->renderW;

			float scrollRatio = Clamp(float(current - dragStart) / (scrollOverlayLength - scrollButtonLength), 0.0f, 1.0f);
			float rawOffset = scrollRatio * (contentFullSize - contentRenderSize);
			currentPage = int32(ceilf(rawOffset / pageSize));
			UpdatePageAndButton();
		};

		scrollBarInteractable->onLeftReleased = [=](void*, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)
		{
			int32 mouse = type == Type::Vertical ? mouseY : mouseX;
			float scrollButtonUnder = type == Type::Vertical ? scrollButton->renderY : scrollButton->renderX;
			float scrollButtonOver = type == Type::Vertical ? scrollButton->renderY + scrollButton->renderH : scrollButton->renderX + scrollButton->renderW;
			if(mouse < scrollButtonUnder)
				currentPage -= (int32)(contentRenderSize / pageSize);
			if(mouse >= scrollButtonOver)
				currentPage += (int32)(contentRenderSize / pageSize);
			UpdatePageAndButton();
		};

		scrollUpButton->onClicked   = [=]() { Scroll(-1); };
		scrollDownButton->onClicked = [=]() { Scroll(+1); };
	}

	void ScrollBar::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Control::Layout(parentX, parentY, parentW, parentH);
		masterPanel->Layout(parentX, parentY, renderW, renderH);

		UpdatePageAndButton();
	}

	void ScrollBar::UpdatePageAndButton()
	{
		float renderSize = type == Type::Vertical ? renderH : renderW;
		int32 scrollButtonSize = (int32)Clamp((renderSize - 32.0f) * (pageSize) / (contentFullSize / pageSize), 16.0f, renderSize - 32.0f);
		scrollButton->h = (float)scrollButtonSize;

		const int maxPage = std::max(0, int32(ceilf((contentFullSize - contentRenderSize) / pageSize)));
		currentPage = Clamp(currentPage, 0, maxPage);
		if(onPageChange)
			onPageChange(currentPage);

		// Move the scroll button
		if(contentFullSize == contentRenderSize)
		{
			scrollButtonAlignment->childOffsetY = 0.0f;
			scrollButtonAlignment->childOffsetX = 0.0f;
		}
		else
		{
			float scrollOverlayLength = type == Type::Vertical ? scrollOverlay->renderH : scrollOverlay->renderW;
			float scrollButtonLength = type == Type::Vertical ? scrollButton->renderH : scrollButton->renderW;

			float temp = (float)(currentPage * pageSize) / (contentFullSize - contentRenderSize) * (scrollOverlayLength - scrollButtonLength);
			temp = Clamp(temp, 0.0f, scrollOverlayLength - scrollButtonLength);

			if(type == Type::Vertical) scrollButtonAlignment->childOffsetY = temp;
			else                       scrollButtonAlignment->childOffsetX = temp;
		}
		scrollButtonAlignment->Layout(scrollOverlay->renderX, scrollOverlay->renderY, scrollOverlay->renderW, scrollOverlay->renderH);
	}

	ScrollView::ScrollView(float width, float height, float vPageSize, float hPageSize, SpriteRenderer* spriteRenderer, ColorOffset backgroundColor)
		: Panel(1.0f, 1.0f, Panel::Type::VerticalStack, "Scroll View"), vPageSize(vPageSize), hPageSize(hPageSize)
	{
		currentVPage = 0;
		currentHPage = 0;

		Interactable* interactable;
		AddChildren(
			(new Panel(1.0f, 1.0f, Panel::Type::HorizontalStack))->AddChildren(
				(new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
					(new Image(1.0f, 1.0f, spriteRenderer->GetSpriteIndex("white"), backgroundColor)),
					(interactable = new Interactable(1.0f, 1.0f)),
					(clip = new Clip(1.0f, 1.0f))->SetChild(
						(contentPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay)))),
				(VScrollBar = new ScrollBar(1.0f, ScrollBar::Type::Vertical, vPageSize, spriteRenderer))),
			(HScrollBar = new ScrollBar(1.0f, ScrollBar::Type::Horizontal, vPageSize, spriteRenderer)));

		VScrollBar->onPageChange = [=](int32 page)
		{
			currentVPage = page;
			RelayoutContent();
		};
		HScrollBar->onPageChange = [=](int32 page)
		{
			currentHPage = page;
			RelayoutContent();
		};
		interactable->onScroll = [=](void* data, int32 currentX, int32 currentY, int scrollBias)
		{
			(*VScrollBar).Scroll(-scrollBias * SCROLL_SPEED);
		};
	}

	void ScrollView::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Panel::Layout(parentX, parentY, parentW, parentH);

		VScrollBar->SetContentSizes(contentPanel->GetFullH(), contentPanel->GetRenderH());
		HScrollBar->SetContentSizes(contentPanel->GetFullW(), contentPanel->GetRenderW());

		Panel::Layout(parentX, parentY, parentW, parentH);
		RelayoutContent();
	}

	void ScrollView::RelayoutContent()
	{
		contentPanel->Layout(clip->renderX - hPageSize * currentHPage, clip->renderY - vPageSize * currentVPage, clip->renderW, clip->renderH);
	}

	ListView::ListView(float width, float height, GenericListViewData& data, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
		: Panel(width, height, Panel::Type::Overlay, "List View"), data(data), spriteRenderer(spriteRenderer), textRenderer(textRenderer)
	{
		const int32 rows = data.GetNumRows();
		const float rowSize = data.GetRowH();
		currentPage = 0;

		Interactable* interactable;
		AddChildren(
			(new Panel(1.0f, 1.0f, Panel::Type::HorizontalStack))->AddChildren(
				(new Panel(1.0f, 1.0f, Panel::Type::VerticalStack))->AddChildren(
					(headerPanelParent = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
						(data.GetHeaderPanel(this, spriteRenderer, textRenderer))),
					(listPanelParent = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
						(new Image(1.0f, 1.0f, spriteRenderer->GetSpriteIndex("white"))),
						(interactable = new Interactable(1.0f, 1.0f)),
						(listPanel = new Panel(1.0f, 1.0f, Panel::Type::VerticalStack)))),
				(VScrollBar = new ScrollBar(1.0f, ScrollBar::Type::Vertical, rowSize, spriteRenderer))));
		
		VScrollBar->onPageChange = [=](int32 page)
		{
			currentPage = page;
			Relayout();
		};
		interactable->onScroll = [=](void* data, int32 currentX, int32 currentY, int scrollBias)
		{
			(*VScrollBar).Scroll(-scrollBias * SCROLL_SPEED);
		};
	}

	void ListView::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Panel::Layout(parentX, parentY, parentW, parentH);

		Relayout();

		Panel::Layout(parentX, parentY, parentW, parentH);
	}

	void ListView::Update(const wnd::Input& input)
	{
		if(onNextUpdate)
			onNextUpdate();
		onNextUpdate = nullptr;
		Panel::Update(input);
	}

	void ListView::Relayout()
	{
		onNextUpdate = [this]()
		{
			this->VScrollBar->SetContentSizes(data.GetNumRows() * data.GetRowH(), listPanel->GetRenderH());

			Panel* headerPanel;
			headerPanelParent->ClearAndDeleteChildren();
			headerPanelParent->AddChildren(headerPanel = data.GetHeaderPanel(this, spriteRenderer, textRenderer));
			headerPanel->Layout(headerPanelParent->GetRenderX(), headerPanelParent->GetRenderY(), headerPanelParent->GetRenderW(), headerPanelParent->GetRenderH());

			listPanel->ClearAndDeleteChildren();
			float y = 0.0f;
			for(int32 i = currentPage; i < data.GetNumRows(); i++)
			{
				listPanel->AddChildren(data.GetRowPanel(this, i, spriteRenderer, textRenderer));
				y += data.GetRowH();
				if(y > listPanel->GetRenderH())
					break;
			}
			listPanel->Layout(listPanelParent->GetRenderX(), listPanelParent->GetRenderY(), listPanelParent->GetRenderW(), listPanelParent->GetRenderH());
		};
	}
}
