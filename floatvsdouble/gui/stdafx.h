// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define _ITERATOR_DEBUG_LEVEL 0
#include <string>
#include <vector>
//#include "..\utl\array.h"

typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned int uint32;
typedef int int32;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned char uint8;
typedef char int8;


template<typename T>
T Clamp(T value, T min, T max) { return value < min ? min : (value > max ? max : value); }
template<typename T>
T Min(T a, T b) { return a < b ? a : b; }
template<typename T>
T Max(T a, T b) { return a < b ? b : a; }

