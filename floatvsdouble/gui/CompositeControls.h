
#pragma once

#include "CoreControls.h"

namespace gui
{
	class CheckBox;
	class ImageButton;

	class ScrollBar : public Control
	{
	public:
		enum class Type
		{
			Horizontal,
			Vertical,
		};

		ScrollBar(float length, Type type, float pageSize, SpriteRenderer* spriteRenderer);
		virtual ~ScrollBar() { delete masterPanel; }

		void Layout(float parentX, float parentY, float parentW, float parentH);
		void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

		void Scroll(int32 pages) { currentPage += pages; UpdatePageAndButton(); }
		void SetContentSizes(float contentFullSize, float contentRenderSize) { this->contentFullSize = contentFullSize; this->contentRenderSize = contentRenderSize; UpdatePageAndButton(); }

		float pageSize;

		std::function<void(int32 page)> onPageChange;


	private:
		float contentFullSize;
		float contentRenderSize;
		Type type;
		Panel* masterPanel;
		Panel* scrollOverlay;
		Alignment* scrollButtonAlignment;
		ImageButton* scrollButton;

		int32 currentPage = 0;

		bool dragging;
		int32 dragStart;
		
		void UpdatePageAndButton();
	};

	class ScrollView : public Panel
	{
	public:
		ScrollView(float width, float height, float vPageSize, float hPageSize, SpriteRenderer* spriteRenderer, ColorOffset backgroundColor = ColorOffset(0.0f, 0.0f, 0.0f, 0.0f));

		void Layout(float parentX, float parentY, float parentW, float parentH);
		ScrollView* SetChild(Control* child) { contentPanel->ClearAndDeleteChildren(); contentPanel->AddChildren(child); return this; }

		void RelayoutContent();

	private:
		Clip* clip;
		Panel* contentPanel;
		ScrollBar* VScrollBar;
		ScrollBar* HScrollBar;
		
		float vPageSize;
		float hPageSize;
		int32 currentVPage;
		int32 currentHPage;
	};

	class GenericListViewData
	{
	public:
		virtual int32 GetNumRows() = 0;
		virtual float GetRowH() = 0;
		virtual Panel* GetHeaderPanel(ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) = 0;
		virtual Panel* GetRowPanel(ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) = 0;


	private:
		friend class ListView;

	protected:
		virtual void Relayout() {}
	};

	class ListView : public Panel
	{
	public:
		ListView(float width, float height, GenericListViewData& data, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

		void Layout(float parentX, float parentY, float parentW, float parentH);
		void Update(const wnd::Input& input);
		//void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

		void Relayout();

	private:
		ScrollBar* VScrollBar;
		int32 currentPage;
		Panel* headerPanelParent;
		Panel* listPanelParent;
		Panel* listPanel;
		GenericListViewData& data;
		std::vector<Panel*> rowPanels;
		SpriteRenderer* spriteRenderer;
		TextRenderer* textRenderer;
		std::function<void()> onNextUpdate;
	};
}