#pragma once

#include <functional>
#include <algorithm>

#include "..\Vulkan\Sprite.h"

class TextRenderer;
namespace wnd
{
	class Input;
}

namespace gui
{
	const int SCROLL_SPEED = 3;

	class Control
	{
	public:
		Control(float width, float height) : w(width), h(height) {}
		virtual ~Control() {}
		float w, h;
		virtual void Layout(float parentX, float parentY, float parentW, float parentH);
		virtual void Update(const wnd::Input& input) {}
		virtual void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) {}
		virtual float GetFullW() { return renderW; }
		virtual float GetFullH() { return renderH; }

		float GetRenderX() { return renderX; }
		float GetRenderY() { return renderY; }
		float GetRenderW() { return renderW; }
		float GetRenderH() { return renderH; }

	protected:
		friend class Tests;
		friend class Panel;
		friend class Button;
		friend class TextButton;
		friend class BorderImage;
		friend class ListView;
		friend class Clip;
		friend class VScrollBar;
		friend class ScrollBar;
		friend class ScrollView;
		friend class Alignment;
		friend class Splitter;
		float renderX, renderY;
		float renderW, renderH;
	};


	class Panel : public Control
	{
	public:
		enum class Type
		{
			Overlay,
			HorizontalStack,
			VerticalStack,
		};

		Panel(float width, float height, Type type, const char* name = nullptr);
		~Panel();

		template<typename... Args>
		Panel* AddChildren(Args&&... args) { (children.push_back(args), ...); return this; }
		void ClearChildren();
		void ClearAndDeleteChildren();
		void RemoveAndDeleteChild(Control* child);

		void Layout(float parentX, float parentY, float parentW, float parentH);
		void Update(const wnd::Input& input);
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

		Type type;
		float GetFullW() { return fullW; }
		float GetFullH() { return fullH; }

	private:
		std::vector<Control*> children;
		float fullW, fullH;
		const char* name;
	};

	class Interactable : public Control
	{
	public:
		Interactable(float width, float height, void* data = nullptr);
		~Interactable() { if(onDestroy) onDestroy(data); }
		void Update(const wnd::Input& input);

		std::function<void(void* data, bool hovered)> onHoverChange;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onLeftClick;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onLeftReleased;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onRightClick;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onRightReleased;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onMiddleClick;
		std::function<void(void* data, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY)> onMiddleReleased;
		std::function<void(void* data, int32 currentX, int32 currentY, int32 offsetX, int32 offsetY, bool released)> onDrag;
		std::function<void(void* data, int32 currentX, int32 currentY, int scrollBias)> onScroll;
		std::function<void(void* data)> onDestroy;

		void* data;

	private:
		bool hovered;
		bool dragging;
	};

	class Image : public Control
	{
	public:
		Image(float width, float height, int32 spriteIndex, ColorOffset colorOffset = ColorOffset(0.0f, 0.0f, 0.0f, 0.0f));
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

		int32 spriteIndex;
		ColorOffset colorOffset;
	};

	class BorderImage : public Image
	{
	public:
		BorderImage(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex, ColorOffset colorOffset = ColorOffset(0.0f, 0.0f, 0.0f, 0.0f));

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
		}
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	private:
		float borderSize;
	};

	class Text : public Control
	{
	public:
		Text(float width, float height, std::string text);
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

		std::string text;
	};

	class Alignment : public Control
	{
	public:
		enum class Type
		{
			TopLeft, //lolwhat
			Top,
			TopRight,
			Left,
			Center,
			Right,
			BottomLeft,
			Bottom,
			BottomRight,
		};

		Alignment(float width, float height, float childOffsetX, float childOffsetY, Type type)
			: Control(width, height), childOffsetX(childOffsetX), childOffsetY(childOffsetY), type(type) {}
		~Alignment() { delete child; }

		Alignment* SetChild(Control* child) { this->child = child; return this; }

		void Layout(float parentX, float parentY, float parentW, float parentH);
		void Update(const wnd::Input& input) { child->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { child->Render(spriteRenderer, textRenderer); }

		float childOffsetX;
		float childOffsetY;
	private:
		Control* child;
		Type type;
	};

	class Clip : public Control
	{
	public:
		Clip(float width, float height) : Control(width, height) {}
		virtual ~Clip() { delete child; }

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			child->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input);
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		Clip* SetChild(Control* child) { this->child = child; return this; }

		static void InitScreenSize(float w, float h);
		static void BeginManualClip(float renderX, float renderY, float renderW, float renderH, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		static void EndManualClip(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	private:
		Control* child;
	};
}
