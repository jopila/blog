
#pragma once

#include "CoreControls.h"
#include "..\Vulkan\Sprite.h"

namespace gui
{
	class Button :public Control
	{
	public:
		Button(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex);
		virtual ~Button() { delete masterPanel; }

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

		void SetColor(ColorOffset newColorOffset) { image->colorOffset = newColorOffset; }

		std::function<void(void)> onClicked;

	protected:
		Panel* masterPanel;
		BorderImage* image;
	};

	class TextButton : public Button
	{
	public:
		TextButton(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex, TextRenderer* textRenderer, std::string text);
	};

	class ImageButton : public Button
	{
	public:
		ImageButton(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndexBackground, int32 spriteIndexIcon);
	};

	class CheckBox : public Control
	{
	public:
		CheckBox(float width, float height, SpriteRenderer* spriteRenderer);
		virtual ~CheckBox() { delete masterPanel; }

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

		std::function<void(bool checked)> onCheckedChanged;
		void SetChecked(bool newChecked, bool triggerDelegates);
		bool IsChecked() { return checked; }

	private:
		Panel* masterPanel;
		Image* checkBoxImage;
		int32 checkBoxSpriteIndex;
		int32 checkBoxCheckedSpriteIndex;
		bool checked;
	};

	class Tabs : public Control
	{
	public:
		struct TabHandle
		{
		private:
			friend class Tabs;
			Panel* tabButtonPanel;
			Interactable* tabButtonInter;
			Control* tabControl;
			Text* tabNameText;
		};

		Tabs(float width, float height, SpriteRenderer* spriteRenderer);
		virtual ~Tabs();

		TabHandle* AddTab(Control* newTab, std::string name, bool closable, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		TabHandle* GetCurrentSelection();
		std::string GetTabName(TabHandle* handle);
		void SelectTab(TabHandle* handle);
		void RenameTab(TabHandle* handle, std::string newName);
		void CloseTab(TabHandle* handle);
		
		std::function<void(TabHandle*)> onTabClosed;
		std::function<void(TabHandle*)> onTabSelected;

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input);
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }
	private:
		Panel* masterPanel;
		Panel* tabPanel;
		Panel* tabDockPanel;
		Panel* currentTabPanel;
		TabHandle* currentSelection;
		std::vector<Control*> tabs;
		Control* currentTab;
		static constexpr float rowHeight = 18.0f;
		static constexpr float fontSize = 12.0f;

		std::function<void(void)> postUpdate;
		std::vector<TabHandle*> tabHandles;
	};

	class Splitter : public Control
	{
	public:
		enum class Type
		{
			Horizontal,
			Vertical,
		};

		Splitter(float width, float height, Type type, float startingRatio = 0.5f);
		virtual ~Splitter() { delete masterPanel; }

		Control* SetChildren(SpriteRenderer* spriteRenderer, Control* child1, Control* child2);

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

	private:
		float currentRatio;
		Panel* masterPanel;
		Type type;
	};

	class TextInput : public Control
	{
	public:
		TextInput(float width, float height, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		virtual ~TextInput() { delete masterPanel; }

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input);
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

		void SetText(std::string text);
		std::string GetText();
		std::function<void(std::string)> onTextChanged;

	private:
		Panel* masterPanel;
		Text* textControl;
		bool hasFocus;
		int32 position;
		int32 cursorCounter; // this is a hack until proper dt inputs can be added
		int32 cursorSpriteIndex;
	};

	class RadioSelection : public Control
	{
	public:
		RadioSelection(float width, std::vector<std::string> options, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		virtual ~RadioSelection() { delete masterPanel; }

		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

		void SetSelection(int32 text);
		int32 GetSelection();
		std::function<void(int32 index, std::string text)> onSelectionChanged;

	private:
		Panel* masterPanel;
		int32 selectedIndex;
		std::vector<Image*> radioImages;
		float rowHeight = 18.0f;
	};
}

