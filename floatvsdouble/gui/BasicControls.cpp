
#include "stdafx.h"

#include <algorithm>

#include "..\Vulkan\StackEvents.h"
#include "..\Vulkan\Window.h"
#include "..\Vulkan\TextRenderer.h"
#include "BasicControls.h"


namespace gui
{
	Button::Button(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex)
		: Control(width, height)
	{
		Interactable* inter;
		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
			(inter = new Interactable(1.0f, 1.0f)),
			(image = new BorderImage(1.0f, 1.0f, spriteRenderer, spriteIndex)));

		inter->onHoverChange = [=](void*, bool hovered)
		{
			image->colorOffset = hovered ? ColorOffset(-0.05f, -0.05f, -0.05f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
		};

		inter->onLeftReleased = [=](void*, int32 mouseX, int32 mouseY, int32 relMouseX, int32 relMouseY) { if(onClicked) onClicked(); };
	}

	TextButton::TextButton(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex, TextRenderer* textRenderer, std::string text)
		: Button(width, height, spriteRenderer, spriteIndex)
	{
		masterPanel->AddChildren(
			(new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::Left))->SetChild(
				(new Text(1.0f, 18.0f, text))));
	}

	ImageButton::ImageButton(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndexBackground, int32 spriteIndexIcon)
		: Button(width, height, spriteRenderer, spriteIndexBackground)
	{
		float w, h;
		spriteRenderer->GetSpriteDims(spriteIndexIcon, w, h);
		masterPanel->AddChildren(
			(new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::Center))->SetChild(
				(new Image(w, h, spriteIndexIcon))));
	}

	CheckBox::CheckBox(float width, float height, SpriteRenderer* spriteRenderer)
		: Control(width, height)
	{
		checked = false;

		checkBoxSpriteIndex        = spriteRenderer->GetSpriteIndex("checkBox");
		checkBoxCheckedSpriteIndex = spriteRenderer->GetSpriteIndex("checkBoxChecked");
		float checkBoxSpriteW, checkBoxSpriteH;
		spriteRenderer->GetSpriteDims(checkBoxCheckedSpriteIndex, checkBoxSpriteW, checkBoxSpriteH);
		Interactable* checkBoxInter;

		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
			(checkBoxInter = new Interactable(1.0f, 1.0f)),
			(new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::Center))->SetChild(
				(checkBoxImage = new Image(checkBoxSpriteW, checkBoxSpriteH, checkBoxSpriteIndex))));

		checkBoxInter->onLeftClick = [=](void*, int32, int32, int32, int32)
		{
			checked = !checked;
			checkBoxImage->spriteIndex = checked ? checkBoxCheckedSpriteIndex : checkBoxSpriteIndex;
			if(onCheckedChanged)
				onCheckedChanged(checked);
		};

		checkBoxInter->onHoverChange = [=](void*, bool hovered)
		{
			checkBoxImage->colorOffset = hovered ? ColorOffset(-0.05f, -0.05f, -0.05f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
		};
	}

	void CheckBox::SetChecked(bool newChecked, bool triggerDelegates)
	{
		if(checked != newChecked && triggerDelegates)
			onCheckedChanged(newChecked);
		checked = newChecked;
		checkBoxImage->spriteIndex = checked ? checkBoxCheckedSpriteIndex : checkBoxSpriteIndex;
	}

	Tabs::Tabs(float width, float height, SpriteRenderer* spriteRenderer)
		: Control(width, height), currentSelection(nullptr), currentTab(nullptr)
	{
		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::VerticalStack, "Tab Panel"))->AddChildren(
			(tabPanel = new Panel(1.0f, rowHeight+4.0f, Panel::Type::Overlay, "tabSelectorPanel"))->AddChildren(
				(new Image(1.0f, 1.0f, spriteRenderer->GetSpriteIndex("tabBackground"), ColorOffset(-0.1f, -0.1f, -0.1f, 0.0f))),
				(new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::Bottom))->SetChild(
					(tabDockPanel = new Panel(1.0f, rowHeight, Panel::Type::HorizontalStack)))),
			(new Image(1.0f, 1.01f, spriteRenderer->GetSpriteIndex("white"), ColorOffset(-0.2f, -0.2f, -0.2f, 0.0f))),
			(currentTabPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay)));
	}

	Tabs::~Tabs()
	{
		for(size_t i = 0; i < tabs.size(); i++)
			if(tabs[i] != currentTab)
				delete tabs[i];
		for(size_t i = 0; i < tabHandles.size(); i++)
			delete tabHandles[i];
		delete masterPanel;
	}

	Tabs::TabHandle* Tabs::AddTab(Control* newTab, std::string name, bool closable, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		tabs.push_back(newTab);
		Interactable* inter;
		BorderImage* image;
		tabHandles.push_back(new TabHandle());
		TabHandle* result = tabHandles.back();
		result->tabControl = newTab;

		float textWidth = textRenderer->GetWidth(name.c_str(), fontSize);
		(tabDockPanel)->AddChildren(
			(result->tabButtonPanel = new Panel(textWidth + 20.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
				(result->tabButtonInter = inter = new Interactable(1.0f, 1.0f)),
				(image = new BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("tab"))),
				(result->tabNameText = new Text(1.0f, 1.0f, name))));
		tabPanel->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY(), masterPanel->GetRenderW(), masterPanel->GetRenderH());

		// tab selection
		const ColorOffset normalColorOffset = ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
		const ColorOffset hoveredColorOffset = ColorOffset(0.05f, 0.05f, 0.05f, 0.0f);
		const ColorOffset selectedColorOffset = ColorOffset(-0.1f, -0.05f, 0.0f, 0.0f);
		const ColorOffset hoveredAndSelectedColorOffset = ColorOffset(-0.05f, 0.0f, 0.05f, 0.0f);

		inter->onHoverChange = [=](void*, bool hovered)
		{
			if(hovered) image->colorOffset = currentSelection->tabButtonInter == inter ? hoveredAndSelectedColorOffset : hoveredColorOffset;
			else        image->colorOffset = currentSelection->tabButtonInter == inter ? selectedColorOffset : normalColorOffset;
		};

		inter->onLeftReleased = [=](void*, int32, int32, int32, int32)
		{
			TabHandle* tempCurrentSelection = currentSelection;
			currentSelection = result;
			if(tempCurrentSelection)
				tempCurrentSelection->tabButtonInter->onHoverChange(tempCurrentSelection->tabButtonInter->data, false); // unhighlight the previous selection
			currentTab = newTab;

			image->colorOffset = selectedColorOffset;

			currentTabPanel->ClearChildren();
			currentTabPanel->AddChildren(newTab);
			newTab->Layout(currentTabPanel->GetRenderX(), currentTabPanel->GetRenderY(), currentTabPanel->GetRenderW(), currentTabPanel->GetRenderH());
			
			if(onTabSelected)
				onTabSelected(result);
		};

		if(closable)
		{
			inter->onMiddleReleased = [=](void*, int32, int32, int32, int32)
			{
				CloseTab(result);
			};
		}

		// Use the first added tab as the default
		if(tabs.size() == 1)
			inter->onLeftReleased(inter->data, 0, 0, 0, 0);

		return result;
	}

	Tabs::TabHandle* Tabs::GetCurrentSelection()
	{
		return currentSelection;
	}

	std::string Tabs::GetTabName(TabHandle* handle)
	{
		return handle->tabNameText->text;
	}

	void Tabs::SelectTab(TabHandle* handle)
	{
		handle->tabButtonInter->onLeftReleased(nullptr, 0, 0, 0, 0);
	}

	void Tabs::RenameTab(TabHandle* handle, std::string newName)
	{
		handle->tabNameText->text = newName;
	}

	void Tabs::CloseTab(TabHandle* handle)
	{
		postUpdate = [=]()
		{
			onTabClosed(handle);

			size_t closedTabHandleIndex = -1;
			for(size_t i = 0; i < tabHandles.size(); i++)
				if(tabHandles[i] == handle)
					closedTabHandleIndex = i;

			// Closing our current tab, find another one to open
			if(currentSelection == handle)
			{
				if(tabHandles.size() >= 2)
				{
					size_t nextTabIndex = closedTabHandleIndex + 1;
					if(nextTabIndex == tabHandles.size())
						nextTabIndex -= 2;
					tabHandles[nextTabIndex]->tabButtonInter->onLeftReleased(nullptr, 0, 0, 0, 0);
					currentSelection = tabHandles[nextTabIndex];
				}
				else
					currentSelection = nullptr;
			}

			// Remove and delete the tab controls
			tabDockPanel->RemoveAndDeleteChild(handle->tabButtonPanel);
			currentTabPanel->RemoveAndDeleteChild(handle->tabControl);
			tabHandles.erase(tabHandles.begin() + closedTabHandleIndex);
			delete handle;
			tabPanel->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY(), masterPanel->GetRenderW(), masterPanel->GetRenderH());
		};
	}

	void Tabs::Update(const wnd::Input& input)
	{
		masterPanel->Update(input);
		if(postUpdate)
		{
			postUpdate();
			postUpdate = nullptr;
		}
	}

	Splitter::Splitter(float width, float height, Type type, float startingRatio)
		: Control(width, height), type(type)
	{
		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay));
		currentRatio = startingRatio;
	}

	Control* Splitter::SetChildren(SpriteRenderer* spriteRenderer, Control* child1, Control* child2)
	{
		Interactable* inter;
		Panel* layoutPanel;
		Panel* firstPanel;
		Panel* secondPanel;
		(masterPanel)->AddChildren(
			(layoutPanel = new gui::Panel(1.0f, 1.0f, type == Type::Horizontal ? Panel::Type::HorizontalStack : Panel::Type::VerticalStack))->AddChildren(
				(firstPanel = new gui::Panel(type == Type::Horizontal ? currentRatio : 1.0f, type == Type::Horizontal ? 1.0f : currentRatio, gui::Panel::Type::Overlay))->AddChildren(
					(child1)),
				(new Panel(type == Type::Horizontal ? 6.0f : 1.0f, type == Type::Horizontal ? 1.0f : 6.0f, Panel::Type::Overlay))->AddChildren(
					(inter = new Interactable(1.0f, 1.0f)),
					(new Image(1.0f, 1.0f, type == Type::Horizontal ? spriteRenderer->GetSpriteIndex("splitter_horizontal") : spriteRenderer->GetSpriteIndex("splitter_vertical")))),
				(secondPanel = new gui::Panel(type == Type::Horizontal ? 1.0f - currentRatio : 1.0f, type == Type::Horizontal ? 1.0f : 1.0f - currentRatio, gui::Panel::Type::Overlay))->AddChildren(
					(child2))));

		inter->onHoverChange = [=](void*, bool hovered)
		{
			if(hovered)
				wnd::Window::SetCursor(type == Type::Horizontal ? wnd::Window::CursorType::ResizeHorizontal : wnd::Window::CursorType::ResizeVertical);
			else
				wnd::Window::SetCursor(wnd::Window::CursorType::Normal);
		};

		inter->onDrag = [=](void* data, int32 currentX, int32 currentY, int32 offsetX, int32 offsetY, bool released)
		{
			currentRatio += type == Type::Horizontal ? offsetX / renderW : offsetY / renderH;
			firstPanel->w = type == Type::Horizontal ? currentRatio : 1.0f;
			firstPanel->h = type == Type::Horizontal ? 1.0f : currentRatio;
			secondPanel->w = type == Type::Horizontal ? 1.0f - currentRatio : 1.0f;
			secondPanel->h = type == Type::Horizontal ? 1.0f : 1.0f - currentRatio;
			layoutPanel->Layout(masterPanel->renderX, masterPanel->renderY, masterPanel->renderW, masterPanel->renderH);
		};

		return this;
	}

	TextInput::TextInput(float width, float height, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
		: Control(width, height)
	{
		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::Overlay))->AddChildren(
			(new BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("planeBorder"))),
			(textControl = new Text(1.0f, 1.0f, "")));

		position = 0;
		cursorCounter = 0;
		cursorSpriteIndex = spriteRenderer->GetSpriteIndex("white");
	}
	
	void TextInput::Update(const wnd::Input& input)
	{
		if(input.IsMouseButtonTriggered(wnd::Mouse::Left))
			hasFocus = input.GetMouseX() >= renderX && input.GetMouseX() < renderX + renderW && input.GetMouseY() >= renderY && input.GetMouseY() < renderY + renderH;

		if(hasFocus)
		{
			std::string oldText = textControl->text;
			std::vector<char> inputText = input.GetCharInput();
			textControl->text.insert(position, inputText.data(), inputText.size());
			position += (int)inputText.size();
			if(input.IsKeyTriggered(wnd::Keyboard::Backspace) && position > 0)
				textControl->text.erase(textControl->text.begin() + position-- - 1);
			if(input.IsKeyTriggered(wnd::Keyboard::Delete) && position < textControl->text.size())
				textControl->text.erase(textControl->text.begin() + position);
			if(input.IsKeyTriggered(wnd::Keyboard::Left))
				position--;
			if(input.IsKeyTriggered(wnd::Keyboard::Right))
				position++;
			if(input.IsKeyTriggered(wnd::Keyboard::Home))
				position = (int32)textControl->text.size();
			if(input.IsKeyTriggered(wnd::Keyboard::End))
				position = 0;
			position = Clamp(position, 0, (int)textControl->text.size());

			if(oldText != textControl->text)
				onTextChanged(textControl->text);
		}
	}

	void TextInput::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		masterPanel->Render(spriteRenderer, textRenderer);
		if(hasFocus && cursorCounter < 20)
		{
			std::string textLeftOfCursor = textControl->text.substr(0, position);

			SpriteRenderInfo info;
			info.x = renderX + textRenderer->GetWidth(textLeftOfCursor.data(), 12.0f) + 0.5f + 6.0f; // I'm so sorry about that 6.0f...not sorry about the 0.5f that one's to get between characters
			info.y = renderY;
			info.w = 1;
			info.h = renderH - 2.0f;
			info.colorOffset = ColorOffset(-0.9f, -0.9f, -0.9f, 0.0f);
			info.spriteIndex = cursorSpriteIndex;
			spriteRenderer->RenderOne(info);
		}

		cursorCounter++;
		cursorCounter %= 40;
	}

	void TextInput::SetText(std::string text)
	{
		textControl->text = text;
		position = (int32)text.size();
		if(onTextChanged)
			onTextChanged(text);
	}

	std::string TextInput::GetText()
	{
		return textControl->text;
	}

	RadioSelection::RadioSelection(float width, std::vector<std::string> options, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
		: Control(width, 0.0f)
	{
		h = options.size() * rowHeight;
		int32 spriteIndexRadio = spriteRenderer->GetSpriteIndex("radio");
		int32 spriteIndexRadioSelected = spriteRenderer->GetSpriteIndex("radio_selected");
		selectedIndex = 0;
		float radioW, radioH;
		spriteRenderer->GetSpriteDims(spriteIndexRadio, radioW, radioH);

		(masterPanel = new Panel(1.0f, 1.0f, Panel::Type::VerticalStack));

		for(size_t i = 0; i < options.size(); i++)
		{
			//Panel* panel;
			Image* image;
			Interactable* inter;

			(masterPanel)->AddChildren(
				(new Panel(1.0f, rowHeight, Panel::Type::Overlay))->AddChildren(
					(inter = new Interactable(1.0f, 1.0f)),
					(new Panel(rowHeight, rowHeight, Panel::Type::HorizontalStack))->AddChildren(
						(new Alignment(1.0f, 1.0f, 0.0f, 0.0f, Alignment::Type::Center))->SetChild(
							(image = new Image(radioW, radioH, spriteIndexRadio))),
						(new Text(1.0f, 1.0f, options[i])))));

			inter->onHoverChange = [=](void*, bool hovered)
			{
				image->colorOffset = hovered ? ColorOffset(-0.05f, -0.05f, -0.05f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
			};
			inter->onLeftClick = [=](void*, int32, int32, int32, int32)
			{
				radioImages[selectedIndex]->spriteIndex = spriteIndexRadio;
				selectedIndex = (int32)i;
				radioImages[selectedIndex]->spriteIndex = spriteIndexRadioSelected;
				if(onSelectionChanged)
					onSelectionChanged(selectedIndex, options[i]);
			};

			radioImages.push_back(image);
		}
		radioImages[selectedIndex]->spriteIndex = spriteIndexRadioSelected;
	}
}
