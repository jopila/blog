#include "stdafx.h"
#include "CoreControls.h"

#include <algorithm>

#include "..\Vulkan\Gfx.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\StackEvents.h"


namespace gui
{
	struct ClipDims
	{
		float x;
		float y;
		float w;
		float h;
	};
	static std::vector<ClipDims> clipDims;
	static ClipDims currentClipDim;

	void Control::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		renderX = parentX;
		renderY = parentY;
		renderW = w <= 1.0f ? roundf(w * parentW) : w;
		renderH = h <= 1.0f ? roundf(h * parentH) : h;
	}

	Panel::Panel(float width, float height, Type type, const char* name)
		: Control(width, height), type(type), name(name)
	{
	}

	Panel::~Panel()
	{
		for(Control* child : children)
			delete child;
	}

	void Panel::ClearChildren()
	{
		children.clear();
	}

	void Panel::ClearAndDeleteChildren()
	{
		for(Control* control : children)
			delete control;
		children.clear();
	}

	void Panel::RemoveAndDeleteChild(Control* child)
	{
		for(size_t i = 0; i < children.size(); i++)
		{
			if(children[i] == child)
			{
				delete children[i];
				children.erase(children.begin() + i);
			}
		}
	}

	void Panel::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		if(name) StackEvents::Push(name);

		Control::Layout(parentX, parentY, parentW, parentH);
		fullW = 0.0f;
		fullH = 0.0f;

		switch(type)
		{
			case Type::Overlay:
			{

				for(Control* child : children)
				{
					child->Layout(renderX, renderY, renderW, renderH);
					fullW = std::max(fullW, child->GetFullW());
					fullH = std::max(fullH, child->GetFullH());
				}
			}
			break;

			case Type::HorizontalStack:
			{
				// To make ratio children use leftover space, we need to tally up explict space
				float explicitW = 0.0f;
				for(Control* child : children)
					if(child->w > 1.0f)
						explicitW += child->w;
				float layoutW = std::max(0.0f, renderW - explicitW);

				for(Control* child : children)
				{
					child->Layout(renderX + fullW, renderY, layoutW, renderH);
					fullW += child->GetFullW();
					fullH = std::max(fullH, child->GetFullH());
				}
			}
			break;

			case Type::VerticalStack:
			{
				// To make ratio children use leftover space, we need to tally up explict space
				float explicitH = 0.0f;
				for(Control* child : children)
					if(child->h > 1.0f)
						explicitH += child->h;
				float layoutH = std::max(0.0f, renderH - explicitH);

				for(Control* child : children)
				{
					child->Layout(renderX, renderY + fullH, renderW, layoutH);
					fullW = std::max(fullW, child->GetFullW());
					fullH += child->GetFullH();
				}
			}
			break;
		}
		if(w == 0.0f) renderW = fullW;
		if(h == 0.0f) renderH = fullH;

		if(name) StackEvents::Pop();
	}

	void Panel::Update(const wnd::Input& input)
	{
		if(name) StackEvents::Push(name);
		for(Control* child : children)
		{
			if(child->renderX > currentClipDim.x + currentClipDim.w ||
				 child->renderY > currentClipDim.y + currentClipDim.h ||
				 child->renderX + child->GetFullW() < currentClipDim.x ||
				 child->renderY + child->GetFullH() < currentClipDim.y)
				continue;
			child->Update(input);
		}
		if(name) StackEvents::Pop();
	}

	void Panel::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		if(name) StackEvents::Push(name);
		uint64 size = children.size();
		if(size)
		{
			for(Control* child : children)
			{
				if(child->renderX > currentClipDim.x + currentClipDim.w ||
					 child->renderY > currentClipDim.y + currentClipDim.h ||
					 child->renderX + child->GetFullW() < currentClipDim.x ||
					 child->renderY + child->GetFullH() < currentClipDim.y)
					continue;

				child->Render(spriteRenderer, textRenderer);
			}
		}
		if(name) StackEvents::Pop();
	}

	Interactable::Interactable(float width, float height, void* data)
		: Control(width, height), hovered(false), data(data)
	{
	}

	void Interactable::Update(const wnd::Input& input)
	{
		bool withinClip = 
			renderX < currentClipDim.x + currentClipDim.w &&
			renderY < currentClipDim.y + currentClipDim.h &&
			renderX + renderW > currentClipDim.x &&
			renderY + renderH > currentClipDim.y;

		float fMouseX = (float)input.GetMouseX();
		float fMouseY = (float)input.GetMouseY();
		bool newHovered = fMouseX >= renderX && fMouseX < renderX + renderW && fMouseY >= renderY && fMouseY < renderY + renderH;
		if(newHovered != hovered)
		{
			if(withinClip || (!withinClip && newHovered == false))
			{
				if(onHoverChange)
					onHoverChange(data, newHovered);
				hovered = newHovered;
			}
		}

		if(onDrag)
		{
			bool prevDragging = dragging;
			if(hovered && input.IsMouseButtonTriggered(wnd::Mouse::Left))
				dragging = true;
			if(dragging)
				onDrag(data, input.GetMouseX(), input.GetMouseY(), input.GetMouseOffsetX(), input.GetMouseOffsetY(), input.IsMouseButtonReleased(wnd::Mouse::Left));
			if(dragging && input.IsMouseButtonReleased(wnd::Mouse::Left))
				dragging = false;
		}

		int32 mouseX = input.GetMouseX();
		int32 mouseY = input.GetMouseY();
		int32 relMouseX = mouseX - (int32)renderX;
		int32 relMouseY = mouseY - (int32)renderY;
		if(hovered && input.IsMouseButtonTriggered(wnd::Mouse::Left) && onLeftClick)
			onLeftClick(data, mouseX, mouseY, relMouseX, relMouseY);
		if(hovered && input.IsMouseButtonReleased(wnd::Mouse::Left) && onLeftReleased)
			onLeftReleased(data, mouseX, mouseY, relMouseX, relMouseY);
		if(hovered && input.IsMouseButtonTriggered(wnd::Mouse::Right) && onRightClick)
			onRightClick(data, mouseX, mouseY, relMouseX, relMouseY);
		if(hovered && input.IsMouseButtonReleased(wnd::Mouse::Right) && onRightReleased)
			onRightReleased(data, mouseX, mouseY, relMouseX, relMouseY);
		if(hovered && input.IsMouseButtonTriggered(wnd::Mouse::Middle) && onMiddleClick)
			onMiddleClick(data, mouseX, mouseY, relMouseX, relMouseY);
		if(hovered && input.IsMouseButtonReleased(wnd::Mouse::Middle) && onMiddleReleased)
			onMiddleReleased(data, mouseX, mouseY, relMouseX, relMouseY);

		if(hovered && input.GetMouseScrollY() != 0 && onScroll)
			onScroll(data, mouseX, mouseY, input.GetMouseScrollY());
	}

	Image::Image(float width, float height, int32 spriteIndex, ColorOffset colorOffset)
		: Control(width, height), spriteIndex(spriteIndex), colorOffset(colorOffset)
	{
	}

	void Image::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		SpriteRenderInfo info;
		info.x = renderX;
		info.y = renderY;
		info.w = renderW;
		info.h = renderH;
		info.colorOffset = colorOffset;
		info.spriteIndex = spriteIndex;
		spriteRenderer->RenderOne(info);
	}

	BorderImage::BorderImage(float width, float height, SpriteRenderer* spriteRenderer, int32 spriteIndex, ColorOffset colorOffset)
		: Image(width, height, spriteIndex, colorOffset)
	{
	}

	void BorderImage::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		SpriteRenderInfo info;
		info.colorOffset = colorOffset;
		info.spriteIndex = spriteIndex;
		spriteRenderer->GetSpriteDims(spriteIndex+1, borderSize, borderSize);

		#define RENDER_ONE(xx, yy, ww, hh) \
			info.x = xx; \
			info.y = yy; \
			info.w = ww; \
			info.h = hh; \
			info.spriteIndex++; \
			spriteRenderer->RenderOne(info)

		RENDER_ONE(renderX,                        renderY, borderSize,                  borderSize);
		RENDER_ONE(renderX + borderSize,           renderY, renderW - 2.0f * borderSize, borderSize);
		RENDER_ONE(renderX + renderW - borderSize, renderY, borderSize,                  borderSize);

		RENDER_ONE(renderX,                        renderY + borderSize, borderSize,                  renderH - 2.0f * borderSize);
		RENDER_ONE(renderX + borderSize,           renderY + borderSize, renderW - 2.0f * borderSize, renderH - 2.0f * borderSize);
		RENDER_ONE(renderX + renderW - borderSize, renderY + borderSize, borderSize,                  renderH - 2.0f * borderSize);

		RENDER_ONE(renderX,                        renderY + renderH - borderSize, borderSize,                  borderSize);
		RENDER_ONE(renderX + borderSize,           renderY + renderH - borderSize, renderW - 2.0f * borderSize, borderSize);
		RENDER_ONE(renderX + renderW - borderSize, renderY + renderH - borderSize, borderSize,                  borderSize);
	}

	Text::Text(float width, float height, std::string text)
		: Control(width, height), text(text)
	{
	}

	void Text::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		textRenderer->RenderString(text.c_str(), 12.0f, renderX + 6.0f, renderY + 1.0f, 0xffffffff);
	}

	void Alignment::Layout(float parentX, float parentY, float parentW, float parentH)
	{
		StackEvents::Push("Alignment Layout");
		//we need the child's width/height first...
		Control::Layout(parentX, parentY, parentW, parentH);
		child->Layout(parentX, parentY, renderW, renderH);

		switch(type)
		{
			case Type::TopLeft:
				break;
			case Type::Top:
				parentX += (renderW - child->renderW) * 0.5f;
				break;
			case Type::TopRight:
				parentX += (renderW - child->renderW);
				break;
			case Type::Left:
				parentY += (renderH - child->renderH) * 0.5f;
				break;
			case Type::Center:
				parentX += (renderW - child->renderW) * 0.5f;
				parentY += (renderH - child->renderH) * 0.5f;
				break;
			case Type::Right:
				parentX += (renderW - child->renderW);
				parentY += (renderH - child->renderH) * 0.5f;
				break;
			case Type::BottomLeft:
				parentY += (renderH - child->renderH);
				break;
			case Type::Bottom:
				parentX += (renderW - child->renderW) * 0.5f;
				parentY += (renderH - child->renderH);
				break;
			case Type::BottomRight:
				parentX += (renderW - child->renderW);
				parentY += (renderH - child->renderH);
				break;
		}

		parentX += childOffsetX;
		parentY += childOffsetY;

		child->Layout(roundf(parentX), roundf(parentY), renderW, renderH);
		
		StackEvents::Pop();
	}

	void Clip::Update(const wnd::Input & input)
	{
		BeginManualClip(renderX, renderY, renderW, renderH, nullptr, nullptr);
		child->Update(input);
		EndManualClip(nullptr, nullptr);
	}

	void Clip::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		if(renderW == 0.0f || renderH == 0.0f)
			return;
		BeginManualClip(renderX, renderY, renderW, renderH, spriteRenderer, textRenderer);
		child->Render(spriteRenderer, textRenderer);
		EndManualClip(spriteRenderer, textRenderer);
	}

	void Clip::InitScreenSize(float w, float h)
	{
		clipDims.clear();
		clipDims.push_back({0.0f, 0.0f, w, h});
		currentClipDim = clipDims.back();
	}

	void Clip::BeginManualClip(float renderX, float renderY, float renderW, float renderH, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		float clipX, clipY, clipW, clipH;
		clipX = renderX > currentClipDim.x ? renderX : currentClipDim.x;
		clipY = renderY > currentClipDim.y ? renderY : currentClipDim.y;
		float right = renderX + renderW;
		float bottom = renderY + renderH;
		float currentRight = currentClipDim.x + currentClipDim.w;
		float currentBottom = currentClipDim.y + currentClipDim.h;
		right = right < currentRight ? right : currentRight;
		bottom = bottom < currentBottom ? bottom : currentBottom;
		clipW = Max(right - clipX, 0.0f);
		clipH = Max(bottom - clipY, 0.0f);

		if(spriteRenderer) spriteRenderer->PushScissor((int32)clipX, (int32)clipY, (int32)clipW, (int32)clipH);
		if(textRenderer)   textRenderer->PushScissor((int32)clipX, (int32)clipY, (int32)clipW, (int32)clipH);
		clipDims.push_back({clipX, clipY, clipW, clipH});
		currentClipDim = clipDims.back();
	}

	void Clip::EndManualClip(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	{
		clipDims.pop_back();
		currentClipDim = clipDims.back();
		if(spriteRenderer) spriteRenderer->PopScissor();
		if(textRenderer)   textRenderer->PopScissor();
	}
}
