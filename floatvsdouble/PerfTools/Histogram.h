#pragma once

#include "..\gui\BasicControls.h"


class EventData;
class SpriteRenderer;
class TextRenderer;


class Histogram : public gui::Control
{
public:
	Histogram(float width, float height, SpriteRenderer* spriteRenderer);
	virtual ~Histogram() { delete masterPanel; delete borderImageHelper; }

	void SetData(const std::vector<EventData*>* data) { this->data = data; }

	void Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Control::Layout(parentX, parentY, parentW, parentH);
		masterPanel->Layout(parentX, parentY, renderW, renderH);
	}
	void Update(const wnd::Input& input) { masterPanel->Update(input); }
	void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	float fontSize;

private:
	gui::Panel* masterPanel;
	gui::BorderImage* borderImageHelper;

	const std::vector<EventData*>* data;
};
