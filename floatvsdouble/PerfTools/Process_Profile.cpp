
#include "stdafx.h"

#include "Process.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <chrono>
#include <ctime>
#define INITGUID
#include <evntcons.h>
#include <algorithm>

#include "..\PerfInjector\SharedMemoryInfo.h"
#include "..\Vulkan\File.h"

struct CSwitch
{
	uint32 NewThreadId;
	uint32 OldThreadId;
	int8   NewThreadPriority;
	int8   OldThreadPriority;
	uint8  PreviousCState;
	int8   SpareByte;

	// Wait reason for the previous thread. The following are the possible values:
	//  0  Executive
	//  1  FreePage
	//  2  PageIn
	//  3  PoolAllocation
	//  4  DelayExecution
	//  5  Suspended
	//  6  UserRequest
	//  7  WrExecutive
	//  8  WrFreePage
	//  9  WrPageIn
	//  10 WrPoolAllocation
	//  11 WrDelayExecution
	//  12 WrSuspended
	//  13 WrUserRequest
	//  14 WrEventPair
	//  15 WrQueue
	//  16 WrLpcReceive
	//  17 WrLpcReply
	//  18 WrVirtualMemory
	//  19 WrPageOut
	//  20 WrRendezvous
	//  21 WrKeyedEvent
	//  22 WrTerminated
	//  23 WrProcessInSwap
	//  24 WrCpuRateControl
	//  25 WrCalloutStack
	//  26 WrKernel
	//  27 WrResource
	//  28 WrPushLock
	//  29 WrMutex
	//  30 WrQuantumEnd
	//  31 WrDispatchInt
	//  32 WrPreempted
	//  33 WrYieldExecution
	//  34 WrFastMutex
	//  35 WrGuardedMutex
	//  36 WrRundown
	//  37 MaximumWaitReason
	int8 OldThreadWaitReason;

	int8 OldThreadWaitMode; // 0: KernelMode, 1: UserMode

													// State of the previous thread. The following are the possible state values:
													// 0 Initialized
													// 1 Ready
													// 2 Running
													// 3 Standby
													// 4 Terminated
													// 5 Waiting
													// 6 Transition
													// 7 DeferredReady (added for Windows Server 2003)
	int8 OldThreadState;

	int8 OldThreadWaitIdealProcessor;
	uint32 NewThreadWaitTime;
	uint32 Reserved;

	static const uint8 OPCODE = 36;
};

struct ContextSwitchInfo
{
	int32 coreId;
	uint32 prevThreadId;
	uint32 newThreadId;
	uint64 time;
};

static void WINAPI EventRecordCallback(EVENT_RECORD* eventRecord)
{
	std::vector<ContextSwitchInfo>& constextSwitchInfos = *(std::vector<ContextSwitchInfo>*)eventRecord->UserContext;
	const uint8 opcode = eventRecord->EventHeader.EventDescriptor.Opcode;
	if (opcode == CSwitch::OPCODE && sizeof(CSwitch) == eventRecord->UserDataLength)
	{
		CSwitch* pSwitchEvent = (CSwitch*)eventRecord->UserData;

		ContextSwitchInfo info;
		info.coreId = eventRecord->BufferContext.ProcessorNumber;
		info.prevThreadId = pSwitchEvent->OldThreadId;
		info.newThreadId = pSwitchEvent->NewThreadId;
		info.time = eventRecord->EventHeader.TimeStamp.QuadPart;
		constextSwitchInfos.push_back(info);
	}
}

HANDLE ProcessStartedEvent;
static DWORD WINAPI Win32TracingThread(LPVOID Parameter)
{
	TRACEHANDLE ConsumerHandle = *(TRACEHANDLE*)Parameter;
	SetEvent(ProcessStartedEvent);
	ProcessTrace(&ConsumerHandle, 1, 0, 0);
	SetEvent(ProcessStartedEvent);
	return(0);
}

void CapturedProcess::OnUpdate(std::function<void(void)> onTakeCapture)
{
	if(name == "")
		return;

	switch(sharedMemoryInfo->command)
	{
		default:
		case COMMAND_NONE: break;
		case COMMAND_TAKE_CAPTURE:
		{
			onTakeCapture();
			sharedMemoryInfo->command = COMMAND_NONE;
			break;
		}
	}
}

void CapturedProcess::Profile(ProfileSettings settings, uti::String filename)
{
	const int32 readBufferSize = 16*1024*1024;
	std::vector<uti::String> eventStack;
	std::vector<Timestamp> buffer;
	buffer.reserve(readBufferSize);
	int depth = 0;

	// ETW CSwitch tracing
	//ULONG bufferSize = sizeof(EVENT_TRACE_PROPERTIES) + sizeof(KERNEL_LOGGER_NAME);
	//EVENT_TRACE_PROPERTIES *sessionProperties = (EVENT_TRACE_PROPERTIES*) new char[bufferSize];
	//ZeroMemory(sessionProperties, bufferSize);
	//sessionProperties->LoggerNameOffset = sizeof(EVENT_TRACE_PROPERTIES);
	//sessionProperties->EnableFlags = EVENT_TRACE_FLAG_CSWITCH;
	//sessionProperties->LogFileMode = EVENT_TRACE_REAL_TIME_MODE;
	//sessionProperties->Wnode.Flags = WNODE_FLAG_TRACED_GUID;
	//sessionProperties->Wnode.Guid = SystemTraceControlGuid;
	//sessionProperties->Wnode.BufferSize = bufferSize;
	//sessionProperties->Wnode.ClientContext = 3; // use rdtsc
	//TRACEHANDLE SessionHandle;
	//lstrcpyW((LPWSTR)((char*)sessionProperties + sessionProperties->LoggerNameOffset), KERNEL_LOGGER_NAME);
	//ControlTrace(0, KERNEL_LOGGER_NAME, sessionProperties, EVENT_TRACE_CONTROL_STOP); // Kill any other running capture...
	//ULONG Status = StartTrace(&SessionHandle, KERNEL_LOGGER_NAME, sessionProperties);

	//EVENT_TRACE_LOGFILE LogFile = {0};
	//LogFile.LoggerName = KERNEL_LOGGER_NAME;
	//LogFile.ProcessTraceMode = (PROCESS_TRACE_MODE_REAL_TIME | PROCESS_TRACE_MODE_EVENT_RECORD | PROCESS_TRACE_MODE_RAW_TIMESTAMP);
	//LogFile.EventRecordCallback = EventRecordCallback;
	//std::vector<ContextSwitchInfo> contextSwitchInfos;
	//LogFile.Context = &contextSwitchInfos;
	//TRACEHANDLE ConsumerHandle = OpenTrace(&LogFile);

	//ProcessStartedEvent = CreateEvent(nullptr, false, false, nullptr);
	//DWORD ThreadID;
	//HANDLE ThreadHandle = CreateThread(0, 0, Win32TracingThread, &ConsumerHandle, 0, &ThreadID);
	//CloseHandle(ThreadHandle);
	//WaitForSingleObject(ProcessStartedEvent, INFINITE);
	//CloseHandle(ProcessStartedEvent);

	struct ThreadInfo
	{
		ThreadInfo() : threadId(-1), prevTime(-1), prevCoreId(-1), contextSwitchInfosIndex(0) {}
		DWORD threadId;
		DWORD64 prevTime;
		DWORD prevCoreId;
		std::vector<CapturedFunctionInfo> stack;
		std::vector<ContextSwitchInfo> contextSwitchInfos;
		size_t contextSwitchInfosIndex;
	};
	std::map<DWORD, ThreadInfo> threadInfos;

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	uint64 readPosition = sharedMemoryInfo->currentBuffeWritePosition;
	float timeTaken = 0.0f;
	float timeToStop = settings.time / 1000.0f;                // convert ms to s
	float functionHitchTime = settings.functionTime / 1000.0f; // ^
	int32 functionCountSoFar = 0;
	bool functionStarted = false;
	std::chrono::high_resolution_clock::time_point functionTimeStart;
	FunctionInfo selectedFunctionInfo = addressToFunction[settings.selectedFunction];
	uint64 beginTsc = __rdtsc();
	while(1)
	{
		// If we've caught up to the writing, wait
		if(readPosition < sharedMemoryInfo->currentBuffeWritePosition - 1)
		{
			buffer.push_back(sharedMemoryInfo->buffer[readPosition++ % fileMapBufferSize]);
			threadInfos[buffer.back().threadId].threadId = buffer.back().threadId; // Collect all the different threads we have
		}

		if(readPosition + fileMapBufferSize < sharedMemoryInfo->currentBuffeWritePosition)
		{
			std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - start;
			uint64 startRead = readPosition - buffer.size();
			uint64 eventsWritten = sharedMemoryInfo->currentBuffeWritePosition - startRead;
			printf("Warning: Failed to keep up with event stream. Abandoning. Captured %llu/%llu events over %fs\n", buffer.size(), eventsWritten, span.count());
			break;
		}

		// Check if we can finish capturing
		auto isEndEventForFunction = [](Timestamp& ts, FunctionInfo& info)
		{
			for(size_t i = 1; i < info.modifications.size(); i++)
				if(ts.address == info.modifications[i].address - 2)
					return true;
			return false;
		};

		if(settings.type == ProfileSettings::Type::Time)
		{
			std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - start;
			timeTaken = span.count();
			if(timeTaken > timeToStop)
				goto doneCapturing;
		}
		else if(settings.type == ProfileSettings::Type::FunctionCount)
		{
			if(functionStarted == false && buffer.back().address == selectedFunctionInfo.modifications[0].address - 2)
			{
				buffer.erase(buffer.begin(), buffer.end() - 1);
				functionStarted = true;
			}
			if(functionStarted == true && isEndEventForFunction(buffer.back(), selectedFunctionInfo))
			{
				functionCountSoFar++;
				if(functionCountSoFar >= settings.functionCount)
					goto doneCapturing;
			}
		}
		else if(settings.type == ProfileSettings::Type::FunctionTime)
		{
			if(functionStarted == false && buffer.back().address == selectedFunctionInfo.modifications[0].address - 2)
			{
				buffer.erase(buffer.begin(), buffer.end() - 1);
				functionStarted = true;
				functionTimeStart = std::chrono::high_resolution_clock::now();
			}
			if(functionStarted == true && isEndEventForFunction(buffer.back(), selectedFunctionInfo))
			{
				std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - functionTimeStart;
				timeTaken = span.count();
				if(timeTaken > functionHitchTime)
					goto doneCapturing;
				else
					functionStarted = false;
			}
		}

		YieldProcessor();
	}
doneCapturing:
	uint64 endTsc = __rdtsc();
	std::chrono::duration<float> span = std::chrono::high_resolution_clock::now() - start;
	timeTaken = span.count();

	//CloseTrace(ConsumerHandle);
	//WaitForSingleObject(ProcessStartedEvent, INFINITE);
	//delete[] sessionProperties;

	/////////////////////////////////////////////////////////////////////////////
	// compile the data into our more useful capture file format
	FileWriter file(filename.cstr());

	// Calculate the real time for a tsc tick 
	double secondsPerCycle = timeTaken / (endTsc - beginTsc);
	file.WriteRaw((uint8*)&secondsPerCycle, sizeof(secondsPerCycle));

	// At least one processor had unsynchronized tsc counters, try to account for that
	std::vector<int64> tscOffsets = GetCoreTSCOffsets();
	for(uint32 i = 0; i < (uint32)tscOffsets.size(); i++)
	{
		file.WriteRaw((uint8*)"tsc ", 4);
		mWriteRaw(file, i); // core
		mWriteRaw(file, tscOffsets[i]);
	}

	// Ferry our context switch infos into their appropriate threads
	//for(size_t i = 0; i < contextSwitchInfos.size(); i++)
	//{
	//	if(threadInfos.find(contextSwitchInfos[i].prevThreadId) != threadInfos.end())
	//		threadInfos[contextSwitchInfos[i].prevThreadId].contextSwitchInfos.push_back(contextSwitchInfos[i]);
	//	if(threadInfos.find(contextSwitchInfos[i].newThreadId) != threadInfos.end())
	//		threadInfos[contextSwitchInfos[i].newThreadId].contextSwitchInfos.push_back(contextSwitchInfos[i]);
	//}

	for(size_t bufferIndex = 0; bufferIndex < buffer.size(); bufferIndex++)
	{
		Timestamp& timestamp = buffer[bufferIndex];
		CapturedFunctionInfo info = functionAddressMap[timestamp.address];
		ThreadInfo& threadInfo = threadInfos[timestamp.threadId];

		// track context switches
		while(threadInfo.contextSwitchInfosIndex < threadInfo.contextSwitchInfos.size())
		{
			ContextSwitchInfo contextSwitchInfo = threadInfo.contextSwitchInfos[threadInfo.contextSwitchInfosIndex];
			if(contextSwitchInfo.time < threadInfo.prevTime)
				threadInfo.contextSwitchInfosIndex++;
			else
			{
				if(contextSwitchInfo.time < timestamp.time)
				{
					file.WriteRaw((uint8*)"ctxs", 4);
					mWriteRaw(file, contextSwitchInfo.time);
					mWriteRaw(file, contextSwitchInfo.prevThreadId);
					mWriteRaw(file, contextSwitchInfo.newThreadId);
					mWriteRaw(file, contextSwitchInfo.coreId);
					threadInfo.contextSwitchInfosIndex++;
				}
				else
					break;
			}
		}
		threadInfo.prevCoreId = timestamp.coreId;
		threadInfo.prevTime = timestamp.time;

		if(info.begin)
		{
			file.WriteRaw((uint8*)"push", 4);
			mWriteRaw(file, timestamp.time);
			mWriteRaw(file, timestamp.coreId);
			mWriteRaw(file, timestamp.threadId);
			file.WriteRaw((uint8*)info.name.cstr(), info.name.length() + 1ll);
			//printf("push:\n  %s\n  time: %llu\n  core: %u\n  thread: %u\n\n", info.name.cstr(), timestamp.time, timestamp.coreId, timestamp.threadId);

			// Debug track the stack to see if some functions aren't capturing the end
			depth++;
			threadInfo.stack.push_back(info);
		}
		else
		{
			// Debug track the stack to see if some functions aren't capturing the end
			depth--;
			if(depth < 0)
				depth = 0;
			std::vector<CapturedFunctionInfo>& stack = threadInfo.stack;
			if(stack.size() && stack.back().name != info.name)
			{
				printf("Warning popped '%s' when '%s' was on the stack\n", info.name.cstr(), stack.back().name.cstr());
				continue;
			}
			if(stack.size())
				stack.pop_back();
			else
				continue;

			file.WriteRaw((uint8*)"pop ", 4);
			mWriteRaw(file, timestamp.time);
			mWriteRaw(file, timestamp.coreId);
			mWriteRaw(file, timestamp.threadId);
			//printf("pop:\n  %s\n  time: %llu\n  core: %u\n  thread: %u\n\n", info.name.cstr(), timestamp.time, timestamp.coreId, timestamp.threadId);
		}
	}
}

struct TimingInfo
{
	uint64* timings;
	int32 size;
	int32 start;
	DWORD coreId;
};


DWORD WINAPI TimingFunction(LPVOID lpParam)
{
	TimingInfo& info = *(TimingInfo*)lpParam;
	info.coreId = GetCurrentProcessorNumber();

	if(info.start == 0)
		info.timings[0] = __rdtsc();

	uint64* timings = info.timings;
	for(int i = info.start; i < info.size; i+=2)
	{
		while(timings[i-1] == 0)
			;
		timings[i] = __rdtsc();
	}

	return 0;
}

std::vector<int64> CapturedProcess::GetCoreTSCOffsets()
{
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	int numCPU = sysinfo.dwNumberOfProcessors;
	std::vector<int64> result;
	result.push_back(0);

	for(int i = 1; i < numCPU; i++)
	{
		const int timingThreadCount = 2;
		HANDLE threads[timingThreadCount];
		DWORD threadIds[timingThreadCount];

		const int timingsSize = 1024 * 8;
		uint64* timings = new uint64[timingsSize];
		TimingInfo timingInfo1 = {timings, timingsSize, 0};
		TimingInfo timingInfo2 = {timings, timingsSize, 1};
		memset(timings, 0, timingsSize * sizeof(uint64));

		threads[0] = CreateThread(nullptr, 0, TimingFunction, &timingInfo1, CREATE_SUSPENDED, &threadIds[0]);
		threads[1] = CreateThread(nullptr, 0, TimingFunction, &timingInfo2, CREATE_SUSPENDED, &threadIds[1]);
		if(threads[0] && threads[1])
		{
			SetThreadPriority(threads[0], THREAD_PRIORITY_ABOVE_NORMAL);
			SetThreadPriority(threads[1], THREAD_PRIORITY_ABOVE_NORMAL);
			SetThreadAffinityMask(threads[0], 1ll << 0);
			SetThreadAffinityMask(threads[1], 1ll << i);
			ResumeThread(threads[0]);
			ResumeThread(threads[1]);

			WaitForMultipleObjects(timingThreadCount, threads, TRUE, INFINITE);

			std::vector<int64> diffs;
			for(int i = 2; i < timingsSize - 2; i += 2)
			{
				int64 diff1 = timings[i + 1] - timings[i + 0]; // other core time minus first 0 core time
				int64 diff2 = timings[i + 1] - timings[i + 2]; // other core time minus second 0 core time
				diffs.push_back((diff1 + diff2) / 2);
			}
			std::sort(diffs.begin(), diffs.end());
			result.push_back(diffs[diffs.size() / 2]); // Use the median of our results
		}

		delete[] timings;
	}

	return result;
}
