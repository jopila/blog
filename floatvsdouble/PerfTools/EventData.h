#pragma once


class ExclusiveData
{
public:
	uti::String name;
	double totalDuration;
	std::map<uti::String, double> durationsPerParent;
};

class EventData
{
public:
	~EventData()
	{
		for(EventData* child : children) delete child;
		for(std::pair<uti::String, ExclusiveData*> child : excData) delete child.second;
	}
	static std::map<uint32, EventData*> ParseFile(const char* filename);
	uti::String name;
	uint64 startInt;
	uint64 endInt;
	double start;
	double end;
	double duration;
	std::vector<EventData*> children;
	std::map<uti::String, ExclusiveData*> excData;
	EventData* parent;

private:
	static void EndEvent(EventData* result, EventData*& currentParent, double secondsPerCycle);
};

uti::String FormatTime(double time);
