
#include "stdafx.h"

#include "EventData.h"

#include <stdio.h>
#include "..\Vulkan\File.h"

#define mReadRaw(file, var) fread(&(var), sizeof(var), 1, file)


void DebugPrintEventData(FileWriter& debugFile, EventData* data, int depth = 0)
{
	char spaces[100];
	for(int i = 0; i < 100; i++) spaces[i] = ' ';
	spaces[depth < 100 ? depth : 99] = '\0';

	char debugWriteBuf[10000];
	sprintf_s(debugWriteBuf, 10000, "%s%s: %f(%llu-%llu)\n", spaces, data->name.cstr(), data->duration, data->startInt, data->endInt);
	debugFile.WriteRaw((uint8*)debugWriteBuf, strlen(debugWriteBuf));

	for(size_t i = 0; i < data->children.size(); i++)
		DebugPrintEventData(debugFile, data->children[i], depth+2);
}

std::map<uint32, EventData*> EventData::ParseFile(const char* filename)
{
	FILE* file;
	int64 globalStart = 0;
	std::vector<int64> coreOffsets;
	double secondsPerCycle;
	std::map<uint32, EventData*> currentParent;
	std::map<uint32, EventData*> result;

	auto GetCoreOffset = [&](uint32 coreId)
	{
		if(coreId < coreOffsets.size())
			return coreOffsets[coreId];
		else
			printf("core doesn't exist %u?!\n", coreId);
		return 0ll;
	};

	if(!fopen_s(&file, filename, "rb"))
	{
		fread(&secondsPerCycle, sizeof(secondsPerCycle), 1, file);

		auto EnsureThreadExists = [&](int32 threadId)
		{
			if(result.find(threadId) == result.end())
			{
				char threadIdName[24];
				_itoa_s(threadId, threadIdName, 10);
				result[threadId] = new EventData();
				result[threadId]->name = threadIdName;
				result[threadId]->startInt = 0;
				result[threadId]->endInt = 0;
				result[threadId]->start = 0.0f;
				result[threadId]->end = 0.0f;
				result[threadId]->duration = 0.0f;
				result[threadId]->parent = nullptr;
				currentParent[threadId] = result[threadId];
			}
		};

		const int getNameBufSize = 1024 * 1024;
		char* getNameBuf = new char[getNameBufSize];

		while(!feof(file))
		{
			char tagBuf[5] = {0};
			fread(tagBuf, sizeof(char), 4, file);
			if(strcmp(tagBuf, "push") == 0)
			{
				EventData* newEvent = new EventData();

				// start time
				uint32 threadId;
				uint32 coreId;
				mReadRaw(file, newEvent->startInt);
				mReadRaw(file, coreId);
				mReadRaw(file, threadId);
				EnsureThreadExists(threadId);

				// name
				getNameBuf[getNameBufSize - 1] = 0;
				for(int i = 0; i < getNameBufSize - 1 && (getNameBuf[i] = fgetc(file)); i++)
					;
				newEvent->name = getNameBuf;

				// offset start times
				uint64 rawStartTime = newEvent->startInt;
				if(globalStart == 0)
					globalStart = newEvent->startInt;
				newEvent->startInt -= globalStart - GetCoreOffset(coreId);
				//printf("New event '%s', thread %ui, start %llu\n", newEvent->name.cstr(), threadId, newEvent->startInt);

				// link nodes
				newEvent->parent = currentParent[threadId];
				currentParent[threadId]->children.push_back(newEvent);
				currentParent[threadId] = newEvent;
			}
			else if(strcmp(tagBuf, "pop ") == 0)
			{
				uint64 time;
				uint32 coreId;
				uint32 threadId;
				mReadRaw(file, time);
				mReadRaw(file, coreId);
				mReadRaw(file, threadId);
				EnsureThreadExists(threadId);

				// we're starting mid way into a frame, chop off some dangling events.
				if(currentParent[threadId]->parent == nullptr)
				{
					printf("No parent on thread %ui, clearing children\n", threadId);
					globalStart = 0;
					currentParent[threadId] = result[threadId];
					for(size_t i = 0; i < result[threadId]->children.size(); i++)
						delete result[threadId]->children[i];
					result[threadId]->children.clear();
				}
				else
				{
					currentParent[threadId]->endInt = time - (globalStart - GetCoreOffset(coreId));
					//printf("Popped event '%s', thread %ui, start %llu\n", currentParent[threadId]->name.cstr(), threadId, currentParent[threadId]->endInt);
					EndEvent(result[threadId], currentParent[threadId], secondsPerCycle);
				}
			}
			else if(strcmp(tagBuf, "ctxs") == 0)
			{
				uint64 time;
				uint32 prevThreadId;
				uint32 newThreadId;
				uint32 coreId;
				mReadRaw(file, time);
				mReadRaw(file, prevThreadId);
				mReadRaw(file, newThreadId);
				mReadRaw(file, coreId);
			}
			else if(strcmp(tagBuf, "tsc ") == 0)
			{
				uint32 coreId;
				int64 offset;
				mReadRaw(file, coreId);
				mReadRaw(file, offset);
				if(coreOffsets.size() == coreId)
					coreOffsets.push_back(offset);
				else
					printf("Unexpected core Id %i (expecting %zi next)\n", coreId, coreOffsets.size());
			}
		}

		delete[] getNameBuf;

		for(auto it = result.begin(); it != result.end(); it++)
		{
			uint32 threadId = it->first;
			while(currentParent[threadId]->endInt == 0 && currentParent[threadId] != result[threadId])
			{
				if(currentParent[threadId]->children.size())
					break;
				EventData* temp = currentParent[threadId];
				currentParent[threadId]->parent->children.pop_back();
				currentParent[threadId] = currentParent[threadId]->parent;
				delete temp;
			}

			while(currentParent[threadId] && currentParent[threadId]->children.size())
			{
				currentParent[threadId]->endInt = currentParent[threadId]->children.back()->endInt;
				EndEvent(result[threadId], currentParent[threadId], secondsPerCycle);
			}
		}

		fclose(file);
	}

	return result;
}

void EventData::EndEvent(EventData* result, EventData*& currentParent, double secondsPerCycle)
{					
	currentParent->start = currentParent->startInt * secondsPerCycle;
	currentParent->end = currentParent->endInt * secondsPerCycle;
	currentParent->duration = currentParent->end - currentParent->start;

	// add to our exclusive data
	ExclusiveData*& excData = result->excData[currentParent->name];
	if(!excData)
		excData = new ExclusiveData();
	excData->name = currentParent->name;
	double selfTime = currentParent->duration;
	for(EventData* child : currentParent->children)
		selfTime -= child->duration;
	excData->totalDuration += selfTime;
	if(currentParent->parent)
		excData->durationsPerParent[currentParent->parent->name] += selfTime;

	// add a self child
	if(currentParent->children.size())
	{
		EventData* newEvent = new EventData();
		newEvent->name = "self";
		newEvent->duration = selfTime;
		newEvent->parent = currentParent;
		currentParent->children.push_back(newEvent);
	}

	// Next!
	currentParent = currentParent->parent;
}

uti::String FormatTime(double time)
{
	char buf[24];
	/**/ if (time > 1e10) sprintf_s(buf, ">1e10s");
	else if (time > 100) sprintf_s(buf, "%.0fs", time);
	else if (time > 10) sprintf_s(buf, "%.1fs", time);
	else if (time > 1) sprintf_s(buf, "%.2fs", time);
	else if (time > 0.1) sprintf_s(buf, "%.0fms", time * 1000.0);
	else if (time > 0.01) sprintf_s(buf, "%.1fms", time * 1000.0);
	else if (time > 0.001) sprintf_s(buf, "%.2fms", time * 1000.0);
	else if (time > 0.0001) sprintf_s(buf, "%.0fus", time * 1000000.0);
	else if (time > 0.00001) sprintf_s(buf, "%.1fus", time * 1000000.0);
	else if (time > 0.000001) sprintf_s(buf, "%.2fus", time * 1000000.0);
	else if (time > 0.0000001) sprintf_s(buf, "%.0fns", time * 1000000000.0);
	else if (time > 0.00000001) sprintf_s(buf, "%.1fns", time * 1000000000.0);
	else if (time > 0.000000001) sprintf_s(buf, "%.2fns", time * 1000000000.0);
	else if (time > 0.0000000001) sprintf_s(buf, "%.0fps", time * 1000000000000.0);
	else if (time > 0.00000000001) sprintf_s(buf, "%.1fps", time * 1000000000000.0);
	else if (time > 0.000000000001) sprintf_s(buf, "%.2fps", time * 1000000000000.0);
	else sprintf_s(buf, "0s");
	return buf;
}
