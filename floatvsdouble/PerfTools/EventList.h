#pragma once

#include "..\gui\CompositeControls.h"
#include "..\utility\Delegate.h"
class EventData;

class EventListData : public gui::GenericListViewData
{
public:
	EventListData(const std::map<uint32, EventData*>& data);

	virtual int32 GetNumRows() override { return (int32)rowData.size(); }
	virtual float GetRowH() override { return rowHeight; }
	virtual gui::Panel* GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;
	virtual gui::Panel* GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;

	std::vector<EventData*>* GetCurrentSelection() { return currentSelectionRow >= 0 ? &rowData[currentSelectionRow].events : nullptr; }
	uti::Delegate<std::vector<EventData*>*> OnNewSelection;

private:
	float rowHeight;

	struct RowData
	{
		uti::String name;
		double averageExclusive;
		double averageInclusive;
		int32 count;
		std::vector<EventData*> events;
	};
	std::vector<RowData> rowData;
	std::vector<int32> reorderIndices;
	gui::Interactable* currentSelectionInter;
	int32 currentSelectionRow;

	void SortByColumn(int32 column);
};
