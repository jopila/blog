#include "stdafx.h"

#include <map>
#include "..\gui\BasicControls.h"

#include "SelectedEventsList.h"
#include "EventData.h"


SelectedEventsListData::SelectedEventsListData()
{
	rowHeight = 18.0f;
}

void SelectedEventsListData::SetData(const std::vector<EventData*>& inData)
{
	data = inData;
	if(data.size() == 0)
		return;

	currentSelectionInter = nullptr;
	currentSelectionRow = -1;

	name = data[0]->name;

	std::sort(data.begin(), data.end(), [](EventData* a, EventData* b) { return a->duration > b->duration; });

	Relayout();
}

gui::Panel* SelectedEventsListData::GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Panel* headerRowPanel = new gui::Panel(1.0f, (float)rowHeight, gui::Panel::Type::HorizontalStack);

	std::string headerName = name.cstr() ? name.cstr() : "Nothing Selected";
	headerRowPanel->AddChildren(new gui::TextButton(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("ListViewHeader"), textRenderer, headerName));

	return headerRowPanel;
}

gui::Panel* SelectedEventsListData::GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Interactable* inter;
	gui::BorderImage *bg1;
	int32 itemSpriteIndex = spriteRenderer->GetSpriteIndex("item");

	std::string duration = FormatTime(data[row]->duration).cstr();

	gui::Panel* result;
	(result = new gui::Panel(1.0f, rowHeight, gui::Panel::Type::Overlay))->AddChildren(
		(inter = new gui::Interactable(1.0f, 1.0f)),
		(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
			(new gui::Clip(1.0f, 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg1 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, duration))))));

	// line selection
	inter->onHoverChange = [=](void*, bool hovered)
	{
		if(currentSelectionInter == inter)
			bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		else
			bg1->colorOffset = hovered ? ColorOffset(-0.1f, -0.05f, 0.0f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
	};

	inter->onLeftReleased = [=](void*, int32, int32, int32, int32)
	{
		gui::Interactable* tempCurrentSelection = currentSelectionInter;
		currentSelectionInter = inter;
		currentSelectionRow = row;
		if(tempCurrentSelection)
			tempCurrentSelection->onHoverChange(tempCurrentSelection->data, false); // unhighlight the previous selection
		bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		OnNewSelection.Broadcast(data[row]);
	};

	inter->onDestroy = [=](void*)
	{
		if(currentSelectionInter == inter)
			currentSelectionInter = nullptr;
	};

	if(row == currentSelectionRow)
	{
		currentSelectionInter = inter;
		inter->onHoverChange(inter->data, false);
	}

	return result;
}
