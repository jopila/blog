#pragma once

#include "..\gui\BasicControls.h"


class EventData;
class SpriteRenderer;
class TextRenderer;

static const float barHeight = 20.0f;

class Timeline : public gui::Control
{
public:
	Timeline(float width, float height, const std::map<uint32, EventData*>& data, const std::map<uti::String, ColorOffset>& functionColors, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
	virtual ~Timeline() { delete masterPanel; delete borderImageHelper; delete highlightBorderImageHelper; }

	void Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Control::Layout(parentX, parentY, parentW, parentH);
		masterPanel->Layout(parentX, parentY, renderW, renderH);
	}
	void Update(const wnd::Input& input);
	void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	void SetSelectedEvent(EventData* event);
	void SetSelectedEventName(uti::String name) { selectedEventName = name; }

private:
	class ThreadControl : public Control
	{
	public:
		ThreadControl(float width, float height, Timeline* parent, uint32 threadId, EventData* data, const std::map<uti::String, ColorOffset>& functionColors, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
		virtual ~ThreadControl() { delete masterPanel; }
		void Layout(float parentX, float parentY, float parentW, float parentH)
		{
			Control::Layout(parentX, parentY, parentW, parentH);
			masterPanel->Layout(parentX, parentY, renderW, renderH);
		}
		void Update(const wnd::Input& input) { masterPanel->Update(input); }
		void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	private:
		gui::Panel* masterPanel;
		int32 barSpriteIndex;
		const float buttonHeight = 16.0f;
		
		Timeline* parent;

		struct TimelineDataEvent
		{
			uti::String name;
			ColorOffset color;
			double start;
			double end;
		};

		struct TimelineDataRow
		{
			std::vector<TimelineDataEvent> events;
		};

		struct TimelineDataThread
		{
			uti::String name;
			std::vector<TimelineDataRow> rows;
			bool expanded;
			float GetHeight() const { return ((expanded ? rows.size() : 0) + 1) * barHeight; }
		};

		TimelineDataThread thread;

	};

	friend class ThreadControl;

	bool MoveThreadUp(ThreadControl* control);
	bool MoveThreadDown(ThreadControl* control);
	void MoveThreadTop(ThreadControl* control);
	void MoveThreadBottom(ThreadControl* control);
	void RelayoutThreads();
	void FixBounds();

	gui::Panel* masterPanel;
	gui::BorderImage* borderImageHelper;
	gui::BorderImage* highlightBorderImageHelper;
	gui::Panel* threadHolder;
	std::vector<Control*> threadControls;

	double startTime;
	double maxViewableTime;
	double currentViewableTime;
	double pixelsPerSecond;
	double secondsPerPixel;
	float timelineOffsetY;
	int32 hoverBoxSpriteIndex;
	int32 whiteSpriteIndex;
	int32 buttonBackground;
	int32 mouseX;
	int32 mouseY;
	std::vector<uti::String> hoveredDataInfo;
	uti::String selectedEventName;



	//void RenderThread(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer, const TimelineDataThread& threadData, float offset);
};
