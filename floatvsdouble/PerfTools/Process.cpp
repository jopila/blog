
#include "stdafx.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <winternl.h>
#include <TlHelp32.h>
#include <wtsapi32.h>
#include <comdef.h>
#include <Psapi.h>
#define _NO_CVCONST_H
#include <DbgHelp.h>
#define INITGUID
#include <set>

#include "Process.h"
#include "..\PerfInjector\SharedMemoryInfo.h"

typedef LONG (NTAPI *NtSuspendProcess)(IN HANDLE ProcessHandle);
typedef LONG (NTAPI *NtResumeProcess)(IN HANDLE ProcessHandle);

NtSuspendProcess pfnNtSuspendProcess;
NtResumeProcess pfnNtResumeProcess;

BOOL EnumSymbols(PSYMBOL_INFO symInfo, ULONG symbolSize, PVOID userContext)
{
	if(symInfo->Tag == SymTagFunction)
	{
		CapturedProcess& procInfo = *(CapturedProcess*)userContext;

		void* addr = (void*)symInfo->Address;
		if(procInfo.addressToFunction.find(addr) != procInfo.addressToFunction.end())
		{
			procInfo.addressToFunction[addr].name += symInfo->Name;
			return TRUE;
		}

		procInfo.addressToFunction[addr] = {symInfo->Name, (void*)symInfo->Address, symInfo->Size};
	}
	return TRUE;
}

std::vector<ProcessInfo> EnumerateRunningProcesses(std::vector<std::string> exceptions)
{
	std::vector<ProcessInfo> result;

	WTS_PROCESS_INFO* pWPIs = NULL;
	DWORD dwProcCount = 0;
	if(WTSEnumerateProcesses(WTS_CURRENT_SERVER_HANDLE, NULL, 1, &pWPIs, &dwProcCount))
	{
		for(DWORD i = 0; i < dwProcCount; i++)
		{
			// First see if this process is skip-worthy
			if(pWPIs[i].ProcessId == 0)
				continue;
			if(pWPIs[i].ProcessId == GetCurrentProcessId())
				continue;

			std::string str((const char*)(_bstr_t)pWPIs[i].pProcessName);

			bool skip = false;
			for(size_t i = 0; i < exceptions.size(); i++)
				if(str == exceptions[i])
					skip = true;
			if(skip)
				continue;

			// Not skip worthy, try to process it
			CapturedProcess tempInfo;
			tempInfo.id = pWPIs[i].ProcessId;

			// Just get the symbols for the application itself (to avoid extraneous windows dlls)...may need a way to opt out of this?
			std::string exeName = str;
			size_t index = exeName.find_last_of('.');
			if(index != std::string::npos)
				exeName = exeName.erase(index);
			char buf[100];
			sprintf_s(buf, "%s!*", exeName.c_str());
			tempInfo.name = exeName.c_str();

			if(tempInfo.LoadSymbols(buf))
				result.push_back({str.c_str(), pWPIs[i].ProcessId});
		}
		WTSFreeMemory(pWPIs);
	}

	return result;
}

BOOL GetSpecificSymbol(PSYMBOL_INFO symInfo, ULONG symbolSize, PVOID userContext)
{
	*(void**)userContext = (void*)symInfo->Address;
	return false;
}


bool CapturedProcess::LaunchExe(std::string exe, std::string workingDirectory, std::string arguments)
{
	PROCESS_INFORMATION pi;
	STARTUPINFOA si;
	ZeroMemory(&si, sizeof(STARTUPINFOA));
	si.cb = sizeof(STARTUPINFOA);
	std::string cmd = "\"" + exe + "\" " + arguments;

	if (!CreateProcessA(NULL, (char*)cmd.c_str(), NULL, NULL, false, CREATE_NEW_CONSOLE, NULL, workingDirectory.c_str(), &si, &pi))
	{
		printf("Failed to create process %s (%s)", cmd.c_str(), workingDirectory.c_str());
		return false;
	}
	id = pi.dwProcessId;
	WaitForInputIdle(pi.hProcess, INFINITE);
	SuspendThread(pi.hThread);

	char nameProc[1024];
	GetProcessImageFileNameA(pi.hProcess, nameProc, sizeof(nameProc) / sizeof(*nameProc));
	std::string exeName = nameProc;
	size_t index = exeName.find_last_of('.');
	if(index != std::string::npos)
		exeName = exeName.erase(index);
	index = exeName.find_last_of('\\');
	if(index != std::string::npos)
		exeName = exeName.erase(0, index+1);
	Initialize({exeName.c_str(), pi.dwProcessId});

	ResumeThread(pi.hThread);
	CloseHandle(pi.hProcess);

	return true;
}

bool CapturedProcess::Initialize(ProcessInfo info)
{
	std::string exeName = info.name.cstr();
	size_t index = exeName.find_last_of('.');
	if(index != std::string::npos)
		exeName = exeName.erase(index);
	exeName += "!*";
	id = info.id;
	name = info.name.cstr();

	bool ok = true;
	ok = ok && InjectDLL();
	ok = ok && InitializeSymbols();
	ok = ok && LoadSymbols("*!*");
	return ok;
}

bool CapturedProcess::InjectDLL()
{
	processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, id);

	// Create shared memory
	HANDLE fileMappingHandle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, fileMapSize, TEXT("perfToolSharedMemory"));
	BOOL bufferNeedsInit = (GetLastError() != ERROR_ALREADY_EXISTS); 
	if(fileMappingHandle == NULL)
	{
		printf("Failed to make file mapping (%i)", GetLastError());
		return false;
	}

	sharedMemoryInfo = (SharedMemoryInfo*)MapViewOfFile(fileMappingHandle, FILE_MAP_ALL_ACCESS, 0, 0, 0);
	if(sharedMemoryInfo == NULL)
	{
		printf("Failed to map view of file (%i)", GetLastError());
		return false;
	}

	if(bufferNeedsInit)
		memset(sharedMemoryInfo, 0, fileMapSize);

	// Inject a dll
	char fullPath[256];
	GetFullPathNameA("..\\x64\\Release\\PerfInjector.dll", 255, fullPath, nullptr);
	DWORD pathSize = (lstrlenA(fullPath) + 1) * sizeof(char);
	LPVOID pszLibFileRemote = VirtualAllocEx(processHandle, NULL, pathSize, MEM_COMMIT, PAGE_READWRITE);
	DWORD n = WriteProcessMemory(processHandle, pszLibFileRemote, fullPath, pathSize, NULL);
	PTHREAD_START_ROUTINE pfnThreadRtn = (PTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryA");
	HANDLE hThread = CreateRemoteThread(processHandle, NULL, 0, pfnThreadRtn, pszLibFileRemote, 0, NULL);

	// Wait for it to init
	while(sharedMemoryInfo->injectedProcessId != id)
		;

	return true;
}

bool CapturedProcess::LoadSymbols(std::string filter)
{
	HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, id);
	if(!processHandle)
		return false;
	
	SymSetOptions(SYMOPT_DEFERRED_LOADS);
	if(!SymInitialize(processHandle, 0, TRUE))
	{
		SymCleanup(processHandle);
		CloseHandle(processHandle);
		return false;
	}

	SymEnumSymbols(processHandle, 0, filter.c_str(), (PSYM_ENUMERATESYMBOLS_CALLBACK)EnumSymbols, this);

	SymCleanup(processHandle);
	CloseHandle(processHandle);

	return addressToFunction.size();
}

bool CapturedProcess::InitializeSymbols()
{
	// Get all the application's symbols
	if(!processHandle || !SymInitialize(processHandle, 0, TRUE))
	{
		printf("Error with SymInitialize...everything might not work!\n");
		return false;
	}

	functionTime = nullptr;
	SymEnumSymbols(processHandle, 0, "PerfInjector!PerfInjectorTime", (PSYM_ENUMERATESYMBOLS_CALLBACK)GetSpecificSymbol, &functionTime);
	if(functionTime == nullptr)
	{
		printf("Error getting timing function...everything might not work!\n");
		return false;
	}

	// Get modules and form our jump table
	HMODULE moduleHandles[2048];
	DWORD neededBytes;
	EnumProcessModules(processHandle, moduleHandles, sizeof(moduleHandles), &neededBytes);
	for(int i = 0; i < (neededBytes / sizeof(HMODULE)); i++)
	{
		ModuleInfo newModule;
		
		char moduleName[1024];
		GetModuleFileNameExA(processHandle, moduleHandles[i], moduleName, sizeof(moduleName));
		newModule.name = moduleName;

		MODULEINFO info;
		GetModuleInformation(processHandle, moduleHandles[i], &info, sizeof(info));
		newModule.begin = (uint64)info.lpBaseOfDll;
		newModule.end = newModule.begin + info.SizeOfImage;
		
		MEMORY_BASIC_INFORMATION memInfo;
		VirtualQueryEx(processHandle, (void*)newModule.end, &memInfo, sizeof(memInfo));
		
		// Attempt to make a jump table (first attempt is to use the end of the module memory, assuming null values at the end are unused)
		bool success = false;
		void* buf = (void*)0xaabbccddeeff99;
		ReadProcessMemory(processHandle, (void*)(newModule.end - 8), &buf, 8, nullptr);
		if(buf == nullptr)
		{
			newModule.jumpTable = newModule.end - 8;
			BOOL ok;
			DWORD prevAccess;
			ok = VirtualProtectEx(processHandle, (void*)newModule.jumpTable, sizeof(functionTime), PAGE_EXECUTE_READWRITE, &prevAccess);
			ok = ok && WriteProcessMemory(processHandle, (void*)newModule.jumpTable, &functionTime, sizeof(functionTime), nullptr);
			if(ok)
			{
				modules.push_back(newModule);
				success = true;
			}
		}
		// Second attempt, allocating memory after the end of the module
		if(!success)
		{
			bool success = false;
			void* jumpTable = (void*)(((newModule.end - 1) & ~0xffff) + 0x10000);
			if(jumpTable = (void*)VirtualAllocEx(processHandle, jumpTable, sizeof(void*), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE))
			{
				if(WriteProcessMemory(processHandle, jumpTable, &functionTime, sizeof(functionTime), nullptr))
				{
					success = true;
					newModule.jumpTable = (uint64)jumpTable;
					modules.push_back(newModule);
				}
			}
			if(!success)
				printf("Failed to allocate memory for a jumptable '%s'", newModule.name.cstr());
		}
	}

	pfnNtSuspendProcess = (NtSuspendProcess)GetProcAddress(GetModuleHandle(L"ntdll"), "NtSuspendProcess");
	pfnNtResumeProcess = (NtResumeProcess)GetProcAddress(GetModuleHandle(L"ntdll"), "NtResumeProcess");

	return true;
}

// https://stackoverflow.com/questions/3918375/how-to-get-thread-stack-information-on-windows
//typedef enum THREADINFOCLASS {
//	ThreadBasicInformation = 0,
//};

typedef LONG KPRIORITY;
typedef struct _THREAD_BASIC_INFORMATION
{
	NTSTATUS                ExitStatus;
	PVOID                   TebBaseAddress;
	CLIENT_ID               ClientId;
	KAFFINITY               AffinityMask;
	KPRIORITY               Priority;
	KPRIORITY               BasePriority;
} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

void CapturedProcess::StartDetouring()
{
	// Suspend the app whole-sale real quick
	pfnNtSuspendProcess(processHandle);

	// List out all our threads
	HANDLE hThreadSnap = INVALID_HANDLE_VALUE; 
	THREADENTRY32 te32; 
	hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, id); 
	if(hThreadSnap == INVALID_HANDLE_VALUE)
		printf("Error getting threads...other features may not work");

	te32.dwSize = sizeof(THREADENTRY32 ); 
	if(!Thread32First(hThreadSnap, &te32)) 
		printf("Error getting threads...other features may not work");

	threads.clear();
	do 
	{ 
		if(te32.th32OwnerProcessID == id)
		{
			threads.push_back({te32.th32ThreadID, nullptr});
			threads.back().handle = OpenThread(THREAD_ALL_ACCESS, true, threads.back().id);
		}
	} while(Thread32Next(hThreadSnap, &te32));

	for(size_t i = 0; i < threads.size(); i++)
	{
		const uint32 maxStackSize = 100;
		CONTEXT context = {0};
		context.ContextFlags = CONTEXT_ALL;
		STACKFRAME64 stackFrame;
		void* addr[maxStackSize];

		ZeroMemory(&stackFrame, sizeof(STACKFRAME64));
		ZeroMemory(&addr, sizeof(addr));
		if(!GetThreadContext(threads[i].handle, &context))
			continue;

		threads[i].rip = context.Rip;
		threads[i].rsp = context.Rsp;

		stackFrame.AddrPC.Offset = context.Rip;
		stackFrame.AddrPC.Mode = AddrModeFlat;
		stackFrame.AddrFrame.Offset = context.Rsp;
		stackFrame.AddrFrame.Mode = AddrModeFlat;
		stackFrame.AddrStack.Offset = context.Rsp;
		stackFrame.AddrStack.Mode = AddrModeFlat;

		typedef __kernel_entry NTSTATUS(NTAPI* NtQueryInformationThreadFunctionType) (
			IN HANDLE ThreadHandle,
			IN THREADINFOCLASS ThreadInformationClass,
			OUT PVOID ThreadInformation,
			IN ULONG ThreadInformationLength,
			OUT PULONG ReturnLength OPTIONAL
			);

		// Get the thread's stack info so we can read the whole stack
		THREAD_BASIC_INFORMATION basicInfo = {0};
		NT_TIB tib;
		HMODULE ntdll = LoadLibrary(L"ntdll.dll");
		NtQueryInformationThreadFunctionType NtQueryInformationThreadFunction = (NtQueryInformationThreadFunctionType)GetProcAddress(ntdll, "NtQueryInformationThread");
		NtQueryInformationThreadFunction(threads[i].handle, (THREADINFOCLASS)0, &basicInfo, sizeof(THREAD_BASIC_INFORMATION), nullptr);
		ReadProcessMemory(processHandle, basicInfo.TebBaseAddress, &tib, sizeof(NT_TIB), nullptr);
		threads[i].stackSize = (uint64)tib.StackBase - context.Rsp;
		threads[i].stackMemory = new uint64[threads[i].stackSize / sizeof(uint64) + 1];
		ReadProcessMemory(processHandle, (void*)context.Rsp, threads[i].stackMemory, threads[i].stackSize, nullptr);
	}
}

CapturedProcess::SafetyInfo CapturedProcess::StartFunctionDetour(FunctionInfo& functionInfo)
{
	SafetyInfo safety;

	for(size_t i = 0; i < threads.size(); i++)
	{
		ThreadInfo& info = threads[i];
		// And then go by each value on the stack and if it's within our function's address, it's probably a return address and we need to be careful
		uint64 functionSize = functionInfo.detouredCode.size() ? functionInfo.detouredCode.size() : functionInfo.size;
		uint64 stackElements = info.stackSize / sizeof(uint64);
		uint64* stackEnd = &info.stackMemory[stackElements];
		uint64 functionBegin = (uint64)functionInfo.offset;
		uint64 functionEnd = (uint64)functionInfo.offset + functionSize;
		for(uint64* it = &info.stackMemory[0]; it != stackEnd; it++)
		{
			if(*it >= functionBegin && *it < functionEnd)
				safety.addressesOnStackThatNeedsToChange.push_back(info.rsp + sizeof(uint64) * (it - &info.stackMemory[0]));
		}
		// Also if the instruction pointer is already in our function...
		if(info.rip >= (uint64)functionInfo.offset && info.rip < (uint64)functionInfo.offset + functionSize)
			safety.threadHandlesWithRipsThatNeedsToChange.push_back((uint64)info.handle);
	}

	return safety;
}

void CapturedProcess::FinishFunctionDetour(bool success, const SafetyInfo& safetyInfo)
{
	if(success)
	{
		std::function<int64(uint64)> CalculateOffset = [&](uint64 addr)
		{
			int64 offset = 0;
			for(int j = 0; j < safetyInfo.modifications.size(); j++)
			{
				uint64 detourAddr = safetyInfo.modifications[j].address;
				if(safetyInfo.modifications[j].isDetour)
				{
					if(addr < detourAddr)
						break;
					else if(addr >= detourAddr && addr < detourAddr + safetyInfo.modifications[j].size)
						printf("Error: Releasing a function while it's recording an event...this isn't handled yet");
					else
						offset += safetyInfo.ensureMode == SafetyInfo::Mode::Capture ? safetyInfo.modifications[j].size : -safetyInfo.modifications[j].size;
				}
				else if(safetyInfo.modifications[j].isRipPromotion)
				{
					if(addr < detourAddr)
						break;
					else if(addr >= detourAddr + safetyInfo.modifications[j].size)
						offset += safetyInfo.ensureMode == SafetyInfo::Mode::Capture ? safetyInfo.modifications[j].size : -safetyInfo.modifications[j].size;
				}
			}
			return offset;
		};

		for(size_t i = 0; i < safetyInfo.addressesOnStackThatNeedsToChange.size(); i++)
		{
			uint64 addr = safetyInfo.addressesOnStackThatNeedsToChange[i];
			uint64 stackValue;
			ReadProcessMemory(processHandle, (void*)addr, &stackValue, sizeof(stackValue), nullptr);
			int64 offset = CalculateOffset(stackValue);
			stackValue += offset;
			WriteProcessMemory(processHandle, (void*)addr, &stackValue, sizeof(stackValue), nullptr);
		}
		for(size_t i = 0; i < safetyInfo.threadHandlesWithRipsThatNeedsToChange.size(); i++)
		{
			CONTEXT context = {0};
			context.ContextFlags = CONTEXT_ALL;
			GetThreadContext((HANDLE)safetyInfo.threadHandlesWithRipsThatNeedsToChange[i], &context);
			int64 offset = CalculateOffset(context.Rip);
			context.Rip += offset;
			SetThreadContext((HANDLE)safetyInfo.threadHandlesWithRipsThatNeedsToChange[i], &context);
		}
	}
}

void CapturedProcess::FinishDetouring()
{
	for(size_t i = 0; i < threads.size(); i++)
	{
		CloseHandle(threads[i].handle);
		delete[] threads[i].stackMemory;
	}

	pfnNtResumeProcess(processHandle);
}

uint8 detourAsm[] = {0xff, 0x15, 0xcc, 0xcc, 0xcc, 0xcc, 0x90, 0x90};
//                   ^^^^^^^^^^ call [rip]
//                               ^^^^^^^^^^^^^^^^^^^^^^ rip offset
//                                                       ^^^^^^^^^^ padding for 8 byte alignment
int32* detourOffsetOffset = (int32*)(detourAsm + 2);
int32 detourCallInstructionSize = 6;


bool CapturedProcess::InjectFunction(FunctionInfo& functionInfo, SafetyInfo& safety, void* nextFunctionOffset)
{
	// Lazy failsafe to allow redundant captures
	if(functionInfo.detouredCode.size())
		return false;

	// Get our detour function's addresses
	uint64 jumpTable = 0;
	for(size_t i = 0; i < modules.size(); i++)
	{
		if((uint64)functionInfo.offset >= modules[i].begin && (uint64)functionInfo.offset < modules[i].end)
		{
			jumpTable = modules[i].jumpTable;
			break;
		}
	}
	if(jumpTable == 0)
	{
		printf("Module info not found for '%s' (%p)\n", functionInfo.name.cstr(), functionInfo.offset);
		return false;
	}

	// Inject timing code
	std::vector<uint8>& newCode = functionInfo.detouredCode;
	newCode.reserve(functionInfo.size + 8*sizeof(detourAsm));
	functionInfo.code.resize(functionInfo.size);
	ReadProcessMemory(processHandle, functionInfo.offset, functionInfo.code.data(), functionInfo.size, nullptr);

	struct InstructionMetaData
	{
		bool needsDetour;
		bool needsRipPromotion;
		int64 additionalOffset;

		//rip relative stuff
		int64 oldInstructionOffset;
		int64 ripRelativeOffsetOffset;
		RipRelativeSize ripRelativeSize;
		int64 newRipRelativeOffset;
	};


	// Start moving over the function
	std::vector<Instruction> instructions;
	std::vector<InstructionMetaData> metaData;
	std::map<uint64, uint64> offsetToInstructionIndex;
	uint64 readIndex = 0;
	uint64 additionalOffset = sizeof(detourAsm);
	while(readIndex < functionInfo.size)
	{
		Instruction instr;
		if(GetInstruction(functionInfo.code, readIndex, instr))
		{
			InstructionMetaData newMetaData;

			newMetaData.needsDetour = instr.detourWorthy;
			if(instr.jumpToRegisterAddress)
			{
				if(readIndex == functionInfo.size)
					newMetaData.needsDetour = true; // make a special case for what we're assuming to be tail end virtual calls
				else
					goto failure; // Otherwise, it's a jump table (probably), and we don't handle that.
			}

			if(newMetaData.needsDetour)
				additionalOffset += sizeof(detourAsm);
			newMetaData.needsRipPromotion = false;
			newMetaData.additionalOffset = additionalOffset;
			newMetaData.oldInstructionOffset = readIndex;
			offsetToInstructionIndex[readIndex] = instructions.size();
			newMetaData.ripRelativeOffsetOffset = instr.ripRelativeOffsetOffset;
			newMetaData.ripRelativeSize = instr.ripRelativeSize;
			newMetaData.newRipRelativeOffset = 0;
			metaData.push_back(newMetaData);
			instructions.push_back(instr);
		}
		else
		{
		failure:
			printf("Unrecognized instruction in '%s' (0x%p+0x%llx): {", functionInfo.name.cstr(), functionInfo.offset, readIndex - instr.bytes.size());
			for(int i = 0; i < instr.bytes.size(); i++)
				printf("%.2x ", instr.bytes[i]);
			printf("}\n");
			functionInfo.code.clear();
			functionInfo.detouredCode.clear();
			return false;
		}
	}

	bool needsAnotherPass = true;
	while(needsAnotherPass)
	{
		needsAnotherPass = false;
		uint64 newAdditionalOffset = sizeof(detourAsm);
		for(size_t i = 0; i < instructions.size(); i++)
		{
			if(metaData[i].needsDetour)
				newAdditionalOffset += sizeof(detourAsm);

			if(metaData[i].ripRelativeSize != relNone)
			{
				int64 rel;
				/**/ if(metaData[i].ripRelativeSize == rel8)
					rel = *(int8*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset];
				else if(metaData[i].ripRelativeSize == rel16)
					rel = *(int16*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset];
				else if(metaData[i].ripRelativeSize == rel32)
					rel = *(int32*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset];

				int64 adjustment = 0;
				int64 ripDestination = rel + metaData[i].oldInstructionOffset;
				/**/ if(ripDestination == 0 && instructions[i].bytes[0] == 0xe8)
					adjustment = 0 - metaData[i].additionalOffset;
				else if(ripDestination < 0)
					adjustment = -metaData[i].additionalOffset;
				else if(ripDestination >= (int64)functionInfo.size)
					adjustment = -metaData[i].additionalOffset;
				else
					adjustment = metaData[offsetToInstructionIndex[ripDestination]].additionalOffset - metaData[i].additionalOffset;

				rel += adjustment;

				bool rel8NeedsPromotion = metaData[i].ripRelativeSize == rel8 && (rel >= 0x80 || rel <= -0x80);
				bool rel16NeedsPromotion = metaData[i].ripRelativeSize == rel16 && (rel >= 0x8000 || rel <= -0x8000);
				if((rel8NeedsPromotion || rel16NeedsPromotion))
				{
					needsAnotherPass = needsAnotherPass || !metaData[i].needsRipPromotion;
					metaData[i].needsRipPromotion = true;
					/**/ if(rel8NeedsPromotion  && (instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)) newAdditionalOffset += 4;
					else if(rel8NeedsPromotion  && (instructions[i].bytes[0] == 0xeb))                                     newAdditionalOffset += 3;
					else if(rel16NeedsPromotion && (instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)) newAdditionalOffset += 1;
				}

				metaData[i].newRipRelativeOffset = rel;
			}
			metaData[i].additionalOffset = newAdditionalOffset;
		}
	}

	readIndex = 0;
	auto AddDetour = [&](bool begin)
	{
		uint64 rip = (uint64)functionInfo.offset + newCode.size() + detourCallInstructionSize;
		*detourOffsetOffset = (int32)(jumpTable - rip);
		newCode.resize(newCode.size() + sizeof(detourAsm));
		memcpy(&*(newCode.end() - sizeof(detourAsm)), detourAsm, sizeof(detourAsm));
		functionAddressMap[rip] = {functionInfo.name, begin};

		functionInfo.modifications.push_back({(uint64)functionInfo.offset + newCode.size(), sizeof(detourAsm), true, false});
		safety.modifications.push_back({(uint64)functionInfo.offset + readIndex, sizeof(detourAsm), true, false});
	};
	AddDetour(true);

	for(size_t i = 0; i < instructions.size(); i++)
	{
		if(metaData[i].needsDetour)
		{
			AddDetour(false);
		}

		if(metaData[i].needsRipPromotion)
		{
			// convert smaller conditional jump instructions into 32 bit conditional jumps
			bool successfulPromote = true;
			int32 instructionSizeOffset = 0;
			if(metaData[i].ripRelativeSize == rel8)
			{
				if(instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)
				{
					instructions[i].bytes = { 0x0f, (uint8)(instructions[i].bytes[0] + 0x10), 0, 0, 0, 0 };
					metaData[i].ripRelativeOffsetOffset = 2;
					instructionSizeOffset = 4;
				}
				else if(instructions[i].bytes[0] == 0xeb)
				{
					instructions[i].bytes = {0xe9, 0, 0, 0, 0 };
					instructionSizeOffset = 3;
				}
				else
					successfulPromote = false;
			}
			if(metaData[i].ripRelativeSize == rel16)
			{
				if(instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)
				{
					instructions[i].bytes = { 0x0f, (uint8)(instructions[i].bytes[2]), 0, 0, 0, 0 };
					metaData[i].ripRelativeOffsetOffset = 2;
					instructionSizeOffset = 1;
				}
				else
					successfulPromote = false;
			}

			if(!successfulPromote)
			{
				printf("Unhandled rip promotion in '%s' (0x%p+0x%llx): {", functionInfo.name.cstr(), functionInfo.offset, readIndex - instructions[i].bytes.size());
				for(int i = 0; i < instructions[i].bytes.size(); i++)
					printf("%.2x ", instructions[i].bytes[i]);
				printf("}\n");
				functionInfo.code.clear();
				functionInfo.detouredCode.clear();
				return false;
			}

			//This instruction is now at it's final form!
			metaData[i].ripRelativeSize = rel32;

			functionInfo.modifications.push_back({(uint64)functionInfo.offset + newCode.size() + instructions[i].bytes.size(), instructionSizeOffset, false, true});
			safety.modifications.push_back({(uint64)functionInfo.offset + readIndex, instructionSizeOffset, false, true});
		}

		if(metaData[i].ripRelativeSize != relNone)
		{
			if(metaData[i].ripRelativeSize == rel8)
				*(int8*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset] = (int8)metaData[i].newRipRelativeOffset;
			if(metaData[i].ripRelativeSize == rel16)
				*(int16*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset] = (int16)metaData[i].newRipRelativeOffset;
			if(metaData[i].ripRelativeSize == rel32)
				*(int32*)&instructions[i].bytes[metaData[i].ripRelativeOffsetOffset] = (int32)metaData[i].newRipRelativeOffset;
		}

		for(size_t j = 0; j < instructions[i].bytes.size(); j++)
			newCode.push_back(instructions[i].bytes[j]);
		readIndex += instructions[i].bytes.size();
	}

	// Safe guard...
	if((uint8*)functionInfo.offset + newCode.size() >= (uint8*)nextFunctionOffset)
	{
		printf("Not enough buffer to capture '%s' (%p), use a bigger padding (linker option /FUNCTIONPADMIN:%llu on msvc)\n", functionInfo.name.cstr(), functionInfo.offset, newCode.size() - functionInfo.size);
		functionInfo.code.clear();
		functionInfo.detouredCode.clear();
		return false;
	}

	// Finally commit our changes to the process
	WriteProcessMemory(processHandle, functionInfo.offset, newCode.data(), newCode.size(), nullptr);
	//printf("Successfully captured! '%s' (0x%p)\n", functionInfo.name.cstr(), functionInfo.offset);
	return true;
}

void CapturedProcess::UninjectFunction(FunctionInfo& functionInfo)
{
	WriteProcessMemory(processHandle, functionInfo.offset, functionInfo.code.data(), functionInfo.size, nullptr);
	functionInfo.code.clear();
	functionInfo.detouredCode.clear();
	functionInfo.modifications.clear();
}

void CapturedProcess::InjectSingleFunction(void* offset)
{
	// Lookup the function info first
	std::map<void*,FunctionInfo>::iterator functionItr = addressToFunction.find(offset);
	FunctionInfo& functionInfo = functionItr->second;

	// Make sure we can safely change this code
	StartDetouring();
	SafetyInfo safety = StartFunctionDetour(functionInfo);
	safety.ensureMode = SafetyInfo::Mode::Capture;

	bool success = false;
	if(++functionItr != addressToFunction.end())
		success = InjectFunction(functionInfo, safety, functionItr->second.offset);
	else
		printf("Can't capture the last function in our map due to laziness. (Haha. you unlucky bastard, how did you hit this?)");

	FinishFunctionDetour(success, safety);
	FinishDetouring();
}

void CapturedProcess::UninjectSingleFunction(void* offset)
{
	// Lookup the function info first
	FunctionInfo& functionInfo = addressToFunction[offset];

	StartDetouring();
	SafetyInfo safety = StartFunctionDetour(functionInfo);
	safety.ensureMode = SafetyInfo::Mode::Release;
	safety.modifications = functionInfo.modifications;

	UninjectFunction(functionInfo);

	FinishFunctionDetour(true, safety);
	FinishDetouring();
}

void CapturedProcess::InjectSeveralFunctions(std::vector<void*> offsets)
{
	StartDetouring();
	for(size_t i = 0; i < offsets.size(); i++)
	{
		std::map<void*,FunctionInfo>::iterator functionItr = addressToFunction.find(offsets[i]);
		FunctionInfo& functionInfo = functionItr->second;

		SafetyInfo safety = StartFunctionDetour(functionInfo);
		safety.ensureMode = SafetyInfo::Mode::Capture;

		bool success = false;
		if(++functionItr != addressToFunction.end())
			success = InjectFunction(functionInfo, safety, functionItr->second.offset);
		else
			printf("Can't capture the last function in our map due to laziness. (Haha. you unlucky bastard, how did you hit this?)");

		FinishFunctionDetour(success, safety);
	}

	FinishDetouring();
}

void CapturedProcess::UninjectSeveralFunctions(std::vector<void*> offsets)
{
	StartDetouring();
	for(size_t i = 0; i < offsets.size(); i++)
	{
		FunctionInfo& functionInfo = addressToFunction[offsets[i]];

		SafetyInfo safety = StartFunctionDetour(functionInfo);
		safety.ensureMode = SafetyInfo::Mode::Release;
		safety.modifications = functionInfo.modifications;

		UninjectFunction(functionInfo);

		FinishFunctionDetour(true, safety);
	}

	FinishDetouring();
}

std::vector<std::string> ReadFileLines(std::string filename);

void CapturedProcess::FindFunctionCalls(void* offset, std::vector<void*>& childOffsets)
{
	/*
	FunctionInfo& functionInfo = addressToFunction[offset];

	bool needToReadCode = false;
	if(functionInfo.code.size() == 0)
	{
		needToReadCode = true;
		functionInfo.code.resize(functionInfo.size);
		ReadProcessMemory(processHandle, functionInfo.offset, functionInfo.code.data(), functionInfo.size, nullptr);
	}
	std::vector<std::string> fileLines = ReadFileLines(functionInfo.filename.cstr());
	printf("Printing call sites for %s (%llx)\n", functionInfo.name.cstr(), (uint64)offset);

	uint64 readIndex = 0;
	while(readIndex < functionInfo.size)
	{
		Instruction instr;
		if(GetInstruction(functionInfo.code, readIndex, instr))
		{

			if(instr.callSite)
			{
				void* callAddress = (uint8*)functionInfo.offset + instr.callOffset;
				if(addressToFunction.find(callAddress) != addressToFunction.end())
				{
					printf("  Found child function '%s' (%p)\n", addressToFunction[callAddress].name.cstr(), callAddress);
				}
				else if(instr.callOffset != -1)
				{
					const int32 MAX_NAME_LENGTH = 2048;
					IMAGEHLP_SYMBOL* symbolInfo = (IMAGEHLP_SYMBOL*)new char[sizeof(IMAGEHLP_SYMBOL) + MAX_NAME_LENGTH];
					symbolInfo->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL);
					symbolInfo->MaxNameLength = MAX_NAME_LENGTH;
					SymGetSymFromAddr64(processHandle, (uint64)callAddress, 0, symbolInfo);
					printf("  Found uncapturable function '%s' (%p)\n", symbolInfo->Name, callAddress);
					delete[] symbolInfo;
				}
				else
				{
					DWORD bytesFromStart;
					IMAGEHLP_LINE lineInfo;
					lineInfo.SizeOfStruct = sizeof(IMAGEHLP_LINE);
					if(SymGetLineFromAddr(processHandle, (int64)offset + readIndex, &bytesFromStart, &lineInfo))
					{
						printf("  Unknown function call at %s\n", fileLines[lineInfo.LineNumber].c_str());
					}
					else
					{
						printf("  Completely unknown call at %llx\n", (int64)offset + readIndex);
					}
				}
			}
		}
	}

	if(needToReadCode)
		functionInfo.code.clear();
		*/
}

void CapturedProcess::GetFunctionSourceInfo(void* offset, std::vector<SourceInfo>& sourceInfo)
{
	FunctionInfo& functionInfo = addressToFunction[offset];

	std::map<uti::String, std::vector<std::string>> fileLines;

	bool needToReadCode = false;
	if(functionInfo.code.size() == 0)
	{
		needToReadCode = true;
		functionInfo.code.resize(functionInfo.size);
		ReadProcessMemory(processHandle, functionInfo.offset, functionInfo.code.data(), functionInfo.size, nullptr);
	}

	uint64 readIndex = 0;
	while(readIndex < functionInfo.size)
	{
		Instruction instr;
		if(GetInstruction(functionInfo.code, readIndex, instr))
		{
			int64 addr = (int64)offset + readIndex - instr.bytes.size();

			int32 inlineTrace = (int32)SymAddrIncludeInlineTrace(processHandle, addr);

			std::vector<SourceInfo>* ActiveInfo = &sourceInfo;
			int32 parentLine = 0;

			DWORD inlineContext, frameIndex;
			if(SymQueryInlineTrace(processHandle, addr, 0, addr, addr, &inlineContext, &frameIndex))
			{
				for(int32 i = 0; i <= inlineTrace; i++)
				{
					DWORD bytesFromStart;
					IMAGEHLP_LINE64 lineInfo;
					lineInfo.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
					if(SymGetLineFromInlineContext(processHandle, addr, inlineContext + inlineTrace - i, 0, &bytesFromStart, &lineInfo))
					{
						for(int j = 0; j < ActiveInfo->size(); j++)
						{
							SourceInfo& checkInfo = ActiveInfo->operator[](j);
							int32 lineNumber = (int32)lineInfo.LineNumber;
							if(checkInfo.filename == lineInfo.FileName
								&& lineNumber < checkInfo.lineEnd + 10
								&& lineNumber > checkInfo.lineStart - 10) // Same file and within 10 lines of the info, merge them
							{
								checkInfo.lineEnd = Max(checkInfo.lineEnd, lineNumber);
								checkInfo.lineStart = Min(checkInfo.lineStart, lineNumber);
								goto nextInlineContext;
							}
						}

						SourceInfo newSourceInfo;
						newSourceInfo.filename = lineInfo.FileName;
						newSourceInfo.lineStart = newSourceInfo.lineEnd = lineInfo.LineNumber;
						newSourceInfo.lineParent = parentLine;
						ActiveInfo->push_back(newSourceInfo);
					}
					else
					{
						SourceInfo newSourceInfo;
						newSourceInfo.filename = "[No File]";
						newSourceInfo.lineStart = newSourceInfo.lineEnd = -1;
						newSourceInfo.lineParent = parentLine;
						newSourceInfo.lines.push_back("[No Code]");
						ActiveInfo->push_back(newSourceInfo);
					}
				nextInlineContext:
					parentLine = ActiveInfo->back().lineEnd;
					ActiveInfo = &ActiveInfo->back().inlines;
				}
			}
		}
	}

	std::function<void(std::vector<SourceInfo>&)> readLines = [&](std::vector<SourceInfo>& sourceInfo)
	{
		for(int i = 0; i < sourceInfo.size(); i++)
		{
			if(fileLines.find(sourceInfo[i].filename) == fileLines.end())
				fileLines[sourceInfo[i].filename] = ReadFileLines(sourceInfo[i].filename.cstr());
			if(fileLines[sourceInfo[i].filename].size())
			{
				for(int j = sourceInfo[i].lineStart; j <= sourceInfo[i].lineEnd; j++)
				{
					sourceInfo[i].lines.push_back(fileLines[sourceInfo[i].filename][j - 1].c_str());
					sourceInfo[i].lines.back().Replace("\t", "  ");
				}
			}
			readLines(sourceInfo[i].inlines);
		}
	};
	readLines(sourceInfo);

	/*
	printf("Printing code (including inlines) for %s\n", functionInfo.name.cstr());
	for(int i = 0; i < sourceInfo.size(); i++)
	{
		printf("%s\n", sourceInfo[i].filename.cstr());
		for(int j = 0; j < sourceInfo[i].lines.size(); j++)
		{
			bool hasInlines = false;
			int32 offset = sourceInfo[i].lineStart;
			if(std::find_if(sourceInfo[i].inlines.begin(), sourceInfo[i].inlines.end(), [=](SourceInfo& info) {return info.lineParent == j+offset; }) != sourceInfo[i].inlines.end())
				hasInlines = true;
			printf("  %i:%s\t%s\n", sourceInfo[i].lineStart + j, hasInlines ? " +" : "", sourceInfo[i].lines[j].cstr());
		}
	}
	*/

	if(needToReadCode)
		functionInfo.code.clear();
}

int32 CapturedProcess::GetFunctionSizeBytes(void* offset)
{
	return (int32)addressToFunction[offset].size;
}

void CapturedProcess::Cleanup()
{
	if(processHandle != nullptr)
	{
		SymCleanup(processHandle);
		CloseHandle(processHandle);
	}
}





