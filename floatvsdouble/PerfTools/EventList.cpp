#include "stdafx.h"

#include <map>
#include "..\gui\BasicControls.h"

#include "EventList.h"
#include "EventData.h"

static float columnSizes[] = {1.0f, 60.0f, 60.0f, 60.0f};
static const char* const headerNames[] = {"Name", "avgEx", "avgIn", "count"};

EventListData::EventListData(const std::map<uint32, EventData*>& data)
{
	rowHeight = 18.0f;
	currentSelectionInter = nullptr;
	currentSelectionRow = -1;

	std::map<uti::String, RowData> eventNameToRowData;

	std::function<void(EventData*)> parse = [&](EventData* eventData)
	{
		if(eventData->name == "self")
			return;
		RowData& rowData = eventNameToRowData[eventData->name];
		if(!rowData.name.cstr())
			rowData.name = eventData->name;
		rowData.averageInclusive += eventData->duration;
		if(eventData->children.size() && eventData->children.back()->name == "self")
			rowData.averageExclusive += eventData->children.back()->duration;
		else
			rowData.averageExclusive += eventData->duration;
		rowData.count++;
		rowData.events.push_back(eventData);

		for(size_t i = 0; i < eventData->children.size(); i++)
			parse(eventData->children[i]);
	};
	for(auto it = data.begin(); it != data.end(); it++)
		parse(it->second);

	for(auto it = eventNameToRowData.begin(); it != eventNameToRowData.end(); it++)
	{
		it->second.averageExclusive /= it->second.count;
		it->second.averageInclusive /= it->second.count;
		rowData.push_back(it->second);
	}

	reorderIndices.reserve(rowData.size());
	for(int32 i = 0; i < rowData.size(); i++)
		reorderIndices.push_back(i);

	SortByColumn(1);
}

gui::Panel* EventListData::GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Panel* headerRowPanel = new gui::Panel(1.0f, (float)rowHeight, gui::Panel::Type::HorizontalStack);

	for(int32 c = 0; c < sizeof(columnSizes) / sizeof(float); c++)
	{
		gui::TextButton* button;
		headerRowPanel->AddChildren(button = new gui::TextButton(columnSizes[c], 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("ListViewHeader"), textRenderer, headerNames[c]));
		button->onClicked = [=](){ SortByColumn(c); listView->Relayout(); };
	}

	return headerRowPanel;
}

void EventListData::SortByColumn(int32 column)
{
	int32 numRows = (int32)rowData.size();
	for(int32 i = 0; i < numRows - 1; i++)
	for(int32 j = 0; j < numRows - i - 1; j++)
	{
		RowData& data = rowData[reorderIndices[j]];
		RowData& dataNext = rowData[reorderIndices[j+1]];
		switch(column)
		{
		case 0: if(data.name             > dataNext.name)             std::swap(reorderIndices[j], reorderIndices[j+1]); break;
		case 1: if(data.averageExclusive < dataNext.averageExclusive) std::swap(reorderIndices[j], reorderIndices[j+1]); break;
		case 2: if(data.averageInclusive < dataNext.averageInclusive) std::swap(reorderIndices[j], reorderIndices[j+1]); break;
		case 3: if(data.count            < dataNext.count)            std::swap(reorderIndices[j], reorderIndices[j+1]); break;
		}
	}
}

gui::Panel* EventListData::GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Interactable* inter;
	gui::BorderImage *bg1, *bg2, *bg3, *bg4;
	int32 itemSpriteIndex = spriteRenderer->GetSpriteIndex("item");

	row = reorderIndices[row];

	std::string averageExclusiveText = FormatTime(rowData[row].averageExclusive).cstr();
	std::string averageInclusiveText = FormatTime(rowData[row].averageInclusive).cstr();
	char countText[24];
	_itoa_s(rowData[row].count, countText, 10);
	
	gui::Panel* result;
	(result = new gui::Panel(1.0f, rowHeight, gui::Panel::Type::Overlay))->AddChildren(
		(inter = new gui::Interactable(1.0f, 1.0f)),
		(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
			(new gui::Clip(columnSizes[0], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg1 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, rowData[row].name.cstr())))),
			(new gui::Clip(columnSizes[1], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg2 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, averageExclusiveText)))),
			(new gui::Clip(columnSizes[2], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg3 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, averageInclusiveText)))),
			(new gui::Clip(columnSizes[3], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg4 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, countText))))));

	// line selection
	inter->onHoverChange = [=](void*, bool hovered)
	{
		if(currentSelectionInter == inter)
			bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		else
			bg1->colorOffset = hovered ? ColorOffset(-0.1f, -0.05f, 0.0f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
		bg4->colorOffset = bg3->colorOffset = bg2->colorOffset = bg1->colorOffset;
	};

	inter->onLeftReleased = [=](void*, int32, int32, int32, int32)
	{
		gui::Interactable* tempCurrentSelection = currentSelectionInter;
		currentSelectionInter = inter;
		currentSelectionRow = row;
		if(tempCurrentSelection)
			tempCurrentSelection->onHoverChange(tempCurrentSelection->data, false); // unhighlight the previous selection
		bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		bg4->colorOffset = bg3->colorOffset = bg2->colorOffset = bg1->colorOffset;
		OnNewSelection.Broadcast(&rowData[row].events);
	};

	inter->onDestroy = [=](void*)
	{
		if(currentSelectionInter == inter)
			currentSelectionInter = nullptr;
	};

	if(row == currentSelectionRow)
	{
		currentSelectionInter = inter;
		inter->onHoverChange(inter->data, false);
	}

	return result;
}
