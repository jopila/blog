#pragma once

#include "..\gui\CompositeControls.h"
#include "..\utility\Delegate.h"
class EventData;

class SelectedEventsListData : public gui::GenericListViewData
{
public:
	SelectedEventsListData();

	void SetData(const std::vector<EventData*>& data);
	virtual int32 GetNumRows() override { return (int32)data.size(); }
	virtual float GetRowH() override { return rowHeight; }
	virtual gui::Panel* GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;
	virtual gui::Panel* GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;

	uti::Delegate<EventData*> OnNewSelection;

private:
	float rowHeight;

	uti::String name;

	std::vector<EventData*> data;
	std::vector<int32> reorderIndices;
	gui::Interactable* currentSelectionInter;
	int32 currentSelectionRow;
};
