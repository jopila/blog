#pragma once

#include "..\gui\BasicControls.h"


class SpriteRenderer;
class TextRenderer;
class EventData;
class EventTreeData;
class EventListData;
class SelectedEventsListData;

class ReportUI : public gui::Control
{
public:
	ReportUI(float width, float height, uti::String filename, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
	~ReportUI();

	void Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Control::Layout(parentX, parentY, parentW, parentH);
		masterPanel->Layout(parentX, parentY, renderW, renderH);
	}
	void Update(const wnd::Input& input)                                    { masterPanel->Update(input); }
	void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

private:
	gui::Panel* masterPanel;
	std::map<uint32, EventData*> eventData;
	EventTreeData* eventTreeData;
	EventListData* eventListData;
	SelectedEventsListData* selectedEventsListData;
};
