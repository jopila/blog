#include "stdafx.h"

#include "SourceView.h"

#include "..\gui\CompositeControls.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"

const uint32 gFunctionColor = 0xffffffaa;
const uint32 gFunctionHoverColor = 0xffffff66;
const uint32 gWordColor = 0xffaaccff;
const uint32 gWordHoverColor = 0xff6688ff;
const uint32 gKeywordColor = 0xff44ffff;
const uint32 gCommentColor = 0xffffaaff;

class SourceViewInternal : public gui::Control
{
public:
	SourceViewInternal(float width, float height, SourceView* parentSourceView, SpriteRenderer* spriteRenderer) : Control(width, height), sourceView(parentSourceView)
	{
		borderImageHelper = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("planeBorder"));
		spriteIndexWhite = spriteRenderer->GetSpriteIndex("white");
		spriteIndexCollapsed = spriteRenderer->GetSpriteIndex("collapsed");
		spriteIndexExpanded = spriteRenderer->GetSpriteIndex("expanded");
		spriteRenderer->GetSpriteDims(spriteIndexCollapsed, spriteCollapsedExpandedSize, spriteCollapsedExpandedSize);
		fontSize = 12.0f;
	}
	virtual ~SourceViewInternal() { delete borderImageHelper; }
	

	void SetData(const CapturedProcess::SourceInfo& data, TextRenderer* textRenderer);

	void Layout(float parentX, float parentY, float parentW, float parentH) { h = Max(h, parentH); Control::Layout(parentX, parentY, parentW, parentH); }
	void Update(const wnd::Input& input);
	void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

private:
	SourceView* sourceView;

	float fontSize;
	gui::BorderImage* borderImageHelper;
	int32 spriteIndexWhite;
	int32 spriteIndexCollapsed;
	int32 spriteIndexExpanded;
	float spriteCollapsedExpandedSize;

	const float inlineOffset = 10.0f;

	struct ProcessedSourceLine
	{
		uti::String filename;
		int32 lineNumber;
		float headerSize;
		float lineSize;
		//uti::String line;
		bool hasChildren;
		bool expanded;
		bool hovered;
		int32 parentIndex;
		int32 depth;
		enum class LineDataType { None, Word, Keyword, Function, Comment };
		struct LineData { uti::String text; float xOffset; uint32 color; LineDataType type; };
		std::vector<LineData> lineData;
	};

	std::vector<ProcessedSourceLine> processedSource;

	float GetRenderHeight(int32 startIndex = 0);

	bool IsExpanded(ProcessedSourceLine& line)
	{
		return line.parentIndex == -1 || processedSource[line.parentIndex].expanded == true && IsExpanded(processedSource[line.parentIndex]);
	}
};

SourceView::SourceView(float width, float height, SpriteRenderer* spriteRenderer)
	: Panel(width, height, gui::Panel::Type::Overlay, "Source View")
{
	float fontSize = 12.0f;

	AddChildren(
		(scrollView = new gui::ScrollView(1.0f, 1.0f, fontSize * 1.1f, 8.0f, spriteRenderer))->SetChild(
			(sourceViewInternal = new SourceViewInternal(1.0f, 1.0f, this, spriteRenderer))));
}

void SourceView::SetData(const CapturedProcess::SourceInfo& data, TextRenderer* textRenderer)
{
	sourceViewInternal->SetData(data, textRenderer);
	RelayoutContent();
}

void SourceView::RelayoutContent()
{
	scrollView->Layout(renderX, renderY, renderW, renderH);
}

bool IsKeyword(const std::string& text)
{
	std::vector<std::string> keywords =
	{
		"alignas", "default", "register",
		"alignof", "delete", "reinterpret_cast",
		"and", "do", "requires",
		"and_eq", "double", "return",
		"asm", "dynamic_cast", "short",
		"atomic_cancel", "else", "signed",
		"atomic_commit", "enum", "sizeof",
		"atomic_noexcept", "explicit", "static",
		"auto", "export", "static_assert",
		"bitand", "extern", "static_cast",
		"bitor", "false", "struct",
		"bool", "float", "switch",
		"break", "for", "synchronized",
		"case", "friend", "template",
		"catch", "goto", "this",
		"char", "if", "thread_local",
		"char8_t", "inline", "throw",
		"char16_t", "int", "true",
		"char32_t", "long", "try",
		"class", "mutable", "typedef",
		"compl", "namespace", "typeid",
		"concept", "new", "typename",
		"const", "noexcept", "union",
		"consteval", "not", "unsigned",
		"constexpr", "not_eq", "using",
		"constinit", "nullptr", "virtual",
		"const_cast", "operator", "void",
		"continue", "or", "volatile",
		"co_await", "or_eq", "wchar_t",
		"co_return", "private", "while",
		"co_yield", "protected", "xor",
		"decltype", "public", "xor_eq",
		"reflexpr",
	};
	for(int32 i = 0; i < keywords.size(); i++)
		if(text == keywords[i])
			return true;
	return false;
}

void SourceViewInternal::SetData(const CapturedProcess::SourceInfo& data, TextRenderer* textRenderer)
{
	processedSource.clear();
	processedSource.reserve(10000);


	float yOffset = 0.0f;
	std::function<void(const CapturedProcess::SourceInfo&,int32,int32)> processSourceInfo = [this,textRenderer,&processSourceInfo,&yOffset](const CapturedProcess::SourceInfo& info, int32 parentIndex, int32 depth)
	{
		bool handlingMultiLineComment = false;
		for(int i = 0; i < info.lines.size(); i++)
		{
			int32 currentLineNumber = i + info.lineStart;

			ProcessedSourceLine processedSourceLine;
			if(i == 0) processedSourceLine.filename = info.filename;
			processedSourceLine.lineNumber = currentLineNumber;
			processedSourceLine.headerSize = i == 0 ? 2.0f + fontSize * 1.5f : 0.0f;
			processedSourceLine.lineSize = fontSize * 1.1f;
			processedSourceLine.hasChildren = false;
			processedSourceLine.expanded = false;
			processedSourceLine.hovered = false;
			processedSourceLine.parentIndex = parentIndex;
			processedSourceLine.depth = depth;

			const char* whitespace = " \t\r\n";
			const char* varInitialChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
			const char* varChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
			std::string line = info.lines[i].cstr();
			float lastXOffset = 0.0f;

			// Quick check for single line comment first
			size_t commentOffset;
			std::string commentLine;
			float commentXOffset = 0.0f;
			if((commentOffset = line.find("//")) != std::string::npos)
			{
				commentLine = line.substr(commentOffset);
				commentXOffset = textRenderer->GetWidth(commentLine.c_str(), fontSize);
				line = line.substr(0, commentOffset);
			}
			
			// Quick check for multi-line comments
			if(handlingMultiLineComment)
			{
				size_t commentEndOffset;
				if((commentEndOffset = line.find("*/")) != std::string::npos)
				{
					handlingMultiLineComment = false;
					std::string commentLine = line.substr(0, 2 + commentEndOffset);
					processedSourceLine.lineData.push_back({commentLine.c_str(), 0.0f, gCommentColor, ProcessedSourceLine::LineDataType::Comment});
					lastXOffset += textRenderer->GetWidth(commentLine.c_str(), fontSize);
					line = line.substr(2 + commentEndOffset);
				}
				else
				{
					processedSourceLine.lineData.push_back({line.c_str(), 0.0f, gCommentColor, ProcessedSourceLine::LineDataType::Comment});
					lastXOffset = textRenderer->GetWidth(line.c_str(), fontSize);
					goto end;
				}
			}

			// Now go through the line and find words
			size_t lastFinalizedOffset = 0;
			while(lastFinalizedOffset < line.size())
			{
				size_t nextWordOffset = line.find_first_of(varInitialChars, lastFinalizedOffset);
				if(nextWordOffset > lastFinalizedOffset)
				{
					std::string nonWordText = line.substr(lastFinalizedOffset, nextWordOffset - lastFinalizedOffset).c_str();
					std::string commentText = "";
					size_t commentOffset;
					// See if we picked up a multi-line comment first
					if((commentOffset = nonWordText.find("/*")) != std::string::npos)
					{
						commentText = nonWordText.substr(commentOffset, nextWordOffset - commentOffset);
						nonWordText = nonWordText.substr(0, commentOffset);
					}
					// Post non-comment text to the results
					processedSourceLine.lineData.push_back({nonWordText.c_str(), lastXOffset, 0xffffffff, ProcessedSourceLine::LineDataType::None});
					lastXOffset += textRenderer->GetWidth(nonWordText.c_str(), fontSize);

					// Multi-line comments are a bitch
					if(commentOffset != std::string::npos)
					{
						size_t endCommentOffset;
						if((endCommentOffset = line.find("*/", lastFinalizedOffset + commentOffset+2)) != std::string::npos)
						{
							std::string commentText = line.substr(lastFinalizedOffset + commentOffset, 2 + endCommentOffset - (lastFinalizedOffset + commentOffset));
							processedSourceLine.lineData.push_back({commentText.c_str(), lastXOffset, gCommentColor, ProcessedSourceLine::LineDataType::Comment});
							lastXOffset += textRenderer->GetWidth(commentText.c_str(), fontSize);
							lastFinalizedOffset = 2 + endCommentOffset;
							continue;
						}
						else
						{
							std::string commentText = line.substr(lastFinalizedOffset + commentOffset);
							processedSourceLine.lineData.push_back({commentText.c_str(), lastXOffset, gCommentColor, ProcessedSourceLine::LineDataType::Comment});
							lastXOffset += textRenderer->GetWidth(commentText.c_str(), fontSize);
							handlingMultiLineComment = true;
							break;
						}
					}

					//
					lastFinalizedOffset = nextWordOffset;
					if(lastFinalizedOffset == std::string::npos)
						break;
				}
				size_t endOfWordOffset = line.find_first_not_of(varChars, lastFinalizedOffset+1);
				if(endOfWordOffset > lastFinalizedOffset)
				{
					uti::String wordText = line.substr(lastFinalizedOffset, endOfWordOffset - lastFinalizedOffset).c_str();
					ProcessedSourceLine::LineDataType type = ProcessedSourceLine::LineDataType::Word;
					size_t tempOffset;
					// Check for special types of words
					if((tempOffset = line.find_first_not_of(whitespace, endOfWordOffset)) != std::string::npos ? line[tempOffset] == '(' : false)
						type = ProcessedSourceLine::LineDataType::Function;
					if(IsKeyword(wordText.cstr()))
						type = ProcessedSourceLine::LineDataType::Keyword;

					// Post the word to our results
					processedSourceLine.lineData.push_back({wordText, lastXOffset, type == ProcessedSourceLine::LineDataType::Keyword ? gKeywordColor : 0xffffffff, type});
					lastXOffset += textRenderer->GetWidth(wordText.cstr(), fontSize);
					lastFinalizedOffset = endOfWordOffset;
				}
			}
			if(commentLine.length())
				processedSourceLine.lineData.push_back({commentLine.c_str(), lastXOffset, gCommentColor, ProcessedSourceLine::LineDataType::Comment});

		end:
			processedSourceLine.lineData.push_back({"", lastXOffset + commentXOffset, 0xffffffff, ProcessedSourceLine::LineDataType::None});
			processedSource.push_back(processedSourceLine);

			std::vector<CapturedProcess::SourceInfo*> inlineInfos;
			std::vector<CapturedProcess::SourceInfo>::const_iterator inlineIterator = info.inlines.begin();
			int32 currentIndex = (int32)processedSource.size() - 1;
			volatile static bool foo = true;
			foo = !foo;
			while((inlineIterator = std::find_if(inlineIterator, info.inlines.end(), [=](const CapturedProcess::SourceInfo& info) { return info.lineParent == currentLineNumber; })) != info.inlines.end())
			{
				processedSource[currentIndex].hasChildren = true;
				processSourceInfo(*inlineIterator++, currentIndex, depth + 1);
			}
		}
	};
	processSourceInfo(data, -1, 0);

	h = GetRenderHeight();
}

void SourceViewInternal::Update(const wnd::Input& input)
{
	float x = renderX;
	float y = renderY;

	float yOffset = 0.0f;

	for(int32 i = 0; i < (int32)processedSource.size(); i++)
	{
		ProcessedSourceLine& source = processedSource[i];
		if(IsExpanded(source))
		{
			yOffset += source.headerSize;

			for(int32 j = 0; j < (int32)source.lineData.size() - 1; j++)
			{
				if(source.lineData[j].type == ProcessedSourceLine::LineDataType::None)
					continue;

				bool within = Within(x + 80.0f + fontSize + source.lineData[j].xOffset, y + yOffset + 2.0f, source.lineData[j+1].xOffset - source.lineData[j].xOffset + 2.0f, fontSize, (float)input.GetMouseX(), (float)input.GetMouseY());
				if(source.lineData[j].type == ProcessedSourceLine::LineDataType::Function)
					source.lineData[j].color = within ? gFunctionHoverColor : gFunctionColor;
				if(source.lineData[j].type == ProcessedSourceLine::LineDataType::Word)
					source.lineData[j].color = within ? gWordHoverColor : gWordColor;
				if(within && input.IsMouseButtonReleased(wnd::Mouse::Left) && sourceView->onWordClicked)
					sourceView->onWordClicked(source.lineData[j].text);
			}

			if(source.hasChildren)
			{
				bool within = Within(x + inlineOffset * source.depth + 1.0f, y + yOffset + 4.0f, spriteCollapsedExpandedSize, spriteCollapsedExpandedSize, (float)input.GetMouseX(), (float)input.GetMouseY());
				source.hovered = within;
				if(within && input.IsMouseButtonReleased(wnd::Mouse::Left))
				{
					source.expanded = !source.expanded;
					h = GetRenderHeight();
					sourceView->RelayoutContent();
				}
			}
			yOffset += source.lineSize;
		}
	}
}

void SourceViewInternal::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	if(processedSource.size() == 0)
		return;

	float x = renderX;
	float y = renderY;
	float w = renderW;
	float h = renderH;

	// Render backdrop
	SpriteRenderInfo renderInfo = {x, y, w, h, ColorOffset(0.0f, 0.0f, 0.0f, 0.0f), spriteIndexWhite};
	spriteRenderer->RenderOne(renderInfo);

	spriteRenderer->RenderOne({x + 80.0f, y, 1.0f, h, ColorOffset(-0.5f, -0.5f, -0.5f, 0.0f), spriteIndexWhite});
	
	float yOffset = 0.0f;

	for(int32 i = 0; i < (int32)processedSource.size(); i++)
	{
		ProcessedSourceLine& source = processedSource[i];
		if(IsExpanded(source))
		{
			if(source.filename.length())
			{
				spriteRenderer->RenderOne({renderX + inlineOffset * source.depth, y + yOffset + 3.0f, w, fontSize*1.5f - 1.0f, ColorOffset(-0.05f, -0.05f, 0.0f, 0.0f), spriteIndexWhite});
				textRenderer->RenderString(source.filename.cstr(), fontSize, x + inlineOffset * source.depth, y + yOffset + 2.0f, 0xffaaaaaa);

				float blockHeight = GetRenderHeight(i);
				spriteRenderer->RenderOne({x + inlineOffset * source.depth, y + yOffset + 2.0f, 1.0f, blockHeight, ColorOffset(-0.3f, -0.3f, -0.3f, 0.0f), spriteIndexWhite});
				spriteRenderer->RenderOne({x + inlineOffset * source.depth, y + yOffset + blockHeight + 2.0f, w, 1.0f, ColorOffset(-0.3f, -0.3f, -0.3f, 0.0f), spriteIndexWhite});

				yOffset += source.headerSize;
			}
			if(source.hasChildren)
			{
				spriteRenderer->RenderOne({x + inlineOffset * source.depth + 1.0f, y + yOffset + 4.0f, spriteCollapsedExpandedSize, spriteCollapsedExpandedSize,
					source.hovered ? ColorOffset(-0.4f, 0.0f, 0.0f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f),
					source.expanded ? spriteIndexExpanded : spriteIndexCollapsed});
			}

			//textRenderer->RenderString(source.line.cstr(), fontSize, x + 80.0f + fontSize, y + yOffset, 0xffffffff);
			for(int32 j = 0; j < source.lineData.size(); j++)
			{

				textRenderer->RenderString(source.lineData[j].text.cstr(), fontSize, x + 80.0f + fontSize + source.lineData[j].xOffset, y + yOffset, source.lineData[j].color);
			}
			char buf[24];
			_itoa_s(source.lineNumber, buf, 10);
			textRenderer->RenderString(buf, fontSize, x + fontSize + inlineOffset * source.depth, y + yOffset, 0xff80a0ff);
			
			yOffset += source.lineSize;
		}
	}

	textRenderer->RenderString("red",   fontSize, x + w - 100, y + fontSize * 0.5f, 0xffffff80);
	textRenderer->RenderString("green", fontSize, x + w - 100, y + fontSize * 1.5f, 0xffff80ff);
	textRenderer->RenderString("blue",  fontSize, x + w - 100, y + fontSize * 2.5f, 0xff80ffff);
	textRenderer->RenderString("red",   fontSize, x + w - 50, y + fontSize * 0.5f, 0xffffff00);
	textRenderer->RenderString("green", fontSize, x + w - 50, y + fontSize * 1.5f, 0xffff00ff);
	textRenderer->RenderString("blue",  fontSize, x + w - 50, y + fontSize * 2.5f, 0xff00ffff);
}

float SourceViewInternal::GetRenderHeight(int32 startIndex)
{
	int32 startDepth = processedSource[startIndex].depth;
	float renderHeight = 0.0f;
	for(int32 i = startIndex; i < (int32)processedSource.size(); i++)
	{
		ProcessedSourceLine& source = processedSource[i];
		if(source.depth < startDepth)
			break;
		if(IsExpanded(source))
		{
			renderHeight += source.headerSize;
			renderHeight += source.lineSize;
		}
	}
	return renderHeight;
}
