
#include "stdafx.h"

#include <map>

#include "EventTree.h"

#include "..\gui\BasicControls.h"

#include "EventData.h"

float columnSizes[] = {1.0f, 60.0f, 60.0f, 60.0f, 60.0f};

EventTreeData::EventTreeData(const std::map<uint32, EventData*>& data)
{
	rowHeight = 18.0f;
	currentSelectionInter = nullptr;
	currentSelectionRow = -1;

	treeData = new TreeData();
	treeData->name = "Application";
	treeData->average = 0.0f;
	for(auto it = data.begin(); it != data.end(); it++)
	{
		EventData* eventData = it->second;
		TreeData* threadTreeData = new TreeData();
		char buf[24];
		_itoa_s(it->first, buf, 10);
		threadTreeData->name = buf;
		treeData->children.push_back(threadTreeData);
		BuildTreeData(eventData, treeData->children.back());
	}
	BuildRowData(treeData, 0);
}


void EventTreeData::BuildTreeData(EventData* eventData, TreeData* treeData)
{
	for(EventData* child : eventData->children)
	{
		// find an already existing tree data for this child's event
		TreeData* currentTreeData = nullptr;
		for(int32 i = 0; i < treeData->children.size(); i++)
		{
			if(treeData->children[i]->name == child->name)
			{
				currentTreeData = treeData->children[i];
				break;
			}
		}

		// make a new one if we didn't find one
		if(currentTreeData == nullptr)
		{
			currentTreeData = new TreeData();
			currentTreeData->name = child->name;
			currentTreeData->average = 0.0f;
			currentTreeData->min = DBL_MAX;
			treeData->children.push_back(currentTreeData);
		}

		// update the tree data
		currentTreeData->average += child->duration;
		currentTreeData->min = Min(currentTreeData->min, child->duration);
		currentTreeData->max = Max(currentTreeData->max, child->duration);
		currentTreeData->events.push_back(child);

		// so many kids
		BuildTreeData(child, currentTreeData);
	}
}

void EventTreeData::BuildRowData(TreeData* treeData, int32 depth)
{
	for(TreeData* child : treeData->children)
	{
		RowData* newRowData = new RowData();
		newRowData->name = child->name;
		newRowData->average = FormatTime(child->average / child->events.size());
		newRowData->min = FormatTime(child->min);
		newRowData->max = FormatTime(child->max);
		char buf[100];
		sprintf_s(buf, "%i", (int32)child->events.size());
		newRowData->count = buf;
		newRowData->depth = depth;
		newRowData->expanded = false;
		newRowData->expandable = child->children.size();
		newRowData->events = &child->events;
		rowData.push_back(newRowData);
		BuildRowData(child, depth + 1);
	}
}

EventTreeData::~EventTreeData()
{
	delete treeData;
	for(RowData* row : rowData)
		delete row;
}

gui::Panel* EventTreeData::GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Panel* headerRowPanel = new gui::Panel(1.0f, (float)rowHeight, gui::Panel::Type::HorizontalStack);

	const char* const headerNames[] = {"Name", "avg", "min", "max", "count"};
	for(int32 c = 0; c < sizeof(columnSizes) / sizeof(float); c++)
	{
		headerRowPanel->AddChildren(new gui::TextButton(columnSizes[c], 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("ListViewHeader"), textRenderer, headerNames[c]));
	}

	return headerRowPanel;
}

gui::Panel* EventTreeData::GetRowPanel(gui::ListView* listView, int32 inRow, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Interactable* inter;
	gui::Interactable* expandButton;
	gui::Image* expandImage;
	gui::BorderImage *bg1, *bg2, *bg3, *bg4, *bg5;
	int32 collapsedSpriteIndex = spriteRenderer->GetSpriteIndex("collapsed");
	int32 expandedSpriteIndex = spriteRenderer->GetSpriteIndex("expanded");
	int32 itemSpriteIndex = spriteRenderer->GetSpriteIndex("item");

	int32 row = 0;
	for(size_t i = 0; i < inRow && row < (int32)rowData.size(); i++)
	{
		int32 newOffset = 1;
		if(!rowData[row]->expanded)
		{
			size_t start = row;
			while(row + newOffset < (int32)rowData.size() && rowData[row + newOffset]->depth > rowData[start]->depth)
				newOffset++;
		}
		row += newOffset;
	}

	if(row == rowData.size())
		return new gui::Panel(1.0f, 100.0f, gui::Panel::Type::Overlay);
	
	gui::Panel* result;
	(result = new gui::Panel(1.0f, rowHeight, gui::Panel::Type::Overlay))->AddChildren(
		(inter = new gui::Interactable(1.0f, 1.0f)),
		(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
			(new gui::Clip(columnSizes[0], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg1 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Alignment(1.0f, 1.0f, rowData[row]->depth * 12.0f, 0.0f, gui::Alignment::Type::TopLeft))->SetChild(
						(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
							(expandButton = new gui::Interactable(22.0f, 1.0f)),
							(new gui::Alignment(16.0f, 1.0f, 0.0f, 0.0f, gui::Alignment::Type::Center))->SetChild(
								(expandImage = new gui::Image(8.0f, 8.0f, rowData[row]->expanded ? expandedSpriteIndex : collapsedSpriteIndex))),
							(new gui::Alignment(1.0f, 1.0f, 16.0f, 0.0f, gui::Alignment::Type::Center))->SetChild(
								(new gui::Text(1.0f, 1.0f, rowData[row]->name.cstr()))))))),
			(new gui::Clip(columnSizes[1], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg2 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, rowData[row]->average.cstr())))),
			(new gui::Clip(columnSizes[2], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg3 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, rowData[row]->min.cstr())))),
			(new gui::Clip(columnSizes[3], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg4 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, rowData[row]->max.cstr())))),
			(new gui::Clip(columnSizes[4], 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(bg5 = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, itemSpriteIndex)),
					(new gui::Text(1.0f, 1.0f, rowData[row]->count.cstr()))))));

	if(!rowData[row]->expandable)
		expandImage->w = expandImage->h = 0.0f;

	// line selection
	inter->onHoverChange = [=](void*, bool hovered)
	{
		if(currentSelectionInter == inter)
			bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		else
			bg1->colorOffset = hovered ? ColorOffset(-0.1f, -0.05f, 0.0f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
		bg5->colorOffset = bg4->colorOffset = bg3->colorOffset = bg2->colorOffset = bg1->colorOffset;
	};

	inter->onLeftReleased = [=](void*, int32, int32, int32, int32)
	{
		gui::Interactable* tempCurrentSelection = currentSelectionInter;
		currentSelectionInter = inter;
		currentSelectionRow = row;
		if(tempCurrentSelection)
			tempCurrentSelection->onHoverChange(tempCurrentSelection->data, false); // unhighlight the previous selection
		bg1->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		bg5->colorOffset = bg4->colorOffset = bg3->colorOffset = bg2->colorOffset = bg1->colorOffset;
		OnNewSelection.Broadcast(rowData[row]->events);
	};

	inter->onDestroy = [=](void*)
	{
		if(currentSelectionInter == inter)
			currentSelectionInter = nullptr;
	};

	// expand buttons
	expandButton->onHoverChange = [=](void*, bool hovered)
	{
		if(hovered)
			expandImage->colorOffset = rowData[row]->expanded ? ColorOffset(-0.3f, 0.15f, 0.15f, 0.0f) : ColorOffset(-0.4f, 0.0f, 0.0f, 0.0f);
		else
			expandImage->colorOffset = ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);
	};

	expandButton->onLeftReleased = [=](void*, int32, int32, int32, int32)
	{
		rowData[row]->expanded = !rowData[row]->expanded;
		listView->Relayout();
	};

	if(row == currentSelectionRow)
	{
		currentSelectionInter = inter;
		inter->onHoverChange(inter->data, false);
	}

	return result;
}
