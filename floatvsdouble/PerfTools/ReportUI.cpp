
#include "stdafx.h"

#include "ReportUI.h"
#include "EventData.h"
#include "EventTree.h"
#include "EventList.h"
#include "Timeline.h"
#include "Histogram.h"
#include "SelectedEventsList.h"

#include "..\gui\CompositeControls.h"

ReportUI::ReportUI(float width, float height, uti::String filename, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	: Control(width, height)
{
	eventData = EventData::ParseFile(filename.cstr());

	// Generate colors
	auto RandColorComponent = []()
	{
		float result;
		result = (float)rand() / RAND_MAX * -0.5f;
		return result;
	};
	std::map<uti::String, ColorOffset> functionColors;
	for(auto it = eventData.begin(); it != eventData.end(); it++)
		for(std::pair<uti::String, ExclusiveData*> excData : it->second->excData)
			functionColors[excData.first] = ColorOffset(RandColorComponent(), RandColorComponent(), RandColorComponent(), 0.0f);

	eventTreeData = new EventTreeData(eventData);
	eventListData = new EventListData(eventData);
	selectedEventsListData = new SelectedEventsListData();
	gui::Tabs* eventTabs = new gui::Tabs(1.0f, 1.0f, spriteRenderer);
	gui::Tabs* selectedEventsTabs = new gui::Tabs(1.0f, 1.0f, spriteRenderer);
	gui::ListView* eventTree = new gui::ListView(1.0f, 1.0f, *eventTreeData, spriteRenderer, textRenderer);
	gui::ListView* eventList = new gui::ListView(1.0f, 1.0f, *eventListData, spriteRenderer, textRenderer);
	Timeline* timeline = new Timeline(1.0f, 1.0f, eventData, functionColors, spriteRenderer, textRenderer);
	Histogram* histogram = new Histogram(1.0f, 1.0f, spriteRenderer);
	gui::ListView* selectedEventsList = new gui::ListView(1.0f, 1.0f, *selectedEventsListData, spriteRenderer, textRenderer);

	(masterPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack, "Capture Report Overlay"))->AddChildren(
		(new gui::Splitter(1.0f, 1.0f, gui::Splitter::Type::Vertical))->SetChildren(spriteRenderer,
			(timeline),
			(new gui::Splitter(1.0f, 1.0f, gui::Splitter::Type::Horizontal, 0.6f))->SetChildren(spriteRenderer,
				(eventTabs),
				(selectedEventsTabs))));
	
	eventTabs->AddTab(eventTree, "Tree", false, spriteRenderer, textRenderer);
	eventTabs->AddTab(eventList, "List", false, spriteRenderer, textRenderer);

	selectedEventsTabs->AddTab(histogram, "Histogram", false, spriteRenderer, textRenderer);
	selectedEventsTabs->AddTab(selectedEventsList, "Events", false, spriteRenderer, textRenderer);


	eventTabs->onTabSelected = [=](gui::Tabs::TabHandle* handle)
	{
		std::string name = eventTabs->GetTabName(handle);
		std::vector<EventData*>* events = nullptr;
		if(name == "Tree")
			events = eventTreeData->GetCurrentSelection();
		else if(name == "List")
			events = eventListData->GetCurrentSelection();
		if(events)
		{
			histogram->SetData(events);
			selectedEventsListData->SetData(*events);
			selectedEventsList->Relayout();
			timeline->SetSelectedEventName(events ? (events->size() ? (*events)[0]->name : "") : "");
		}

	};

	eventTreeData->OnNewSelection.Add([=](std::vector<EventData*>* events)
	{
		histogram->SetData(events);
		selectedEventsListData->SetData(*events);
		selectedEventsList->Relayout();
		timeline->SetSelectedEventName(events ? (events->size() ? (*events)[0]->name : "") : "");
	});
	eventListData->OnNewSelection.Add([=](std::vector<EventData*>* events)
	{
		histogram->SetData(events);
		selectedEventsListData->SetData(*events);
		selectedEventsList->Relayout();
		timeline->SetSelectedEventName(events ? (events->size() ? (*events)[0]->name : "") : "");
	});
	selectedEventsListData->OnNewSelection.Add([=](EventData* event)
	{
			timeline->SetSelectedEvent(event);
	});
}

ReportUI::~ReportUI()
{
	delete masterPanel;
	delete eventTreeData;
	delete eventListData;
	delete selectedEventsListData;
	for(auto it = eventData.begin(); it != eventData.end(); it++)
		delete it->second;
}
