
#include "stdafx.h"

#include "Timeline.h"

#include "EventData.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\StackEvents.h"

Timeline::Timeline(float width, float height, const std::map<uint32, EventData*>& data, const std::map<uti::String, ColorOffset>& functionColors, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	: Control(width, height)
{
	// Determine the 'time-size' of our timeline
	startTime = DBL_MAX;
	double endTime = 0.0;
	for(auto it = data.begin(); it != data.end(); it++)
	{
		startTime = Min(startTime, it->second->start);
		endTime = Max(endTime, it->second->end);
	}
	maxViewableTime = endTime - startTime;
	currentViewableTime = maxViewableTime;

	// 
	timelineOffsetY = 0;
	
	// Image init
	int32 barSpriteIndex = spriteRenderer->GetSpriteIndex("itemSmall");
	hoverBoxSpriteIndex = spriteRenderer->GetSpriteIndex("planeBorder");
	whiteSpriteIndex = spriteRenderer->GetSpriteIndex("white");
	buttonBackground = spriteRenderer->GetSpriteIndex("tab");
	borderImageHelper = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, barSpriteIndex);
	highlightBorderImageHelper = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("glow"), ColorOffset(0.3f, 0.6f, 1.0f, 0.0f));

	// Gui
	gui::Interactable* inter;
	(masterPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay, "Timeline"))->AddChildren(
		(inter = new gui::Interactable(1.0f, 1.0f)),
		(threadHolder = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack)));

	for(auto it = data.begin(); it != data.end(); it++)
	{
		threadControls.push_back(new ThreadControl(1.0f, 0.0f, this, it->first, it->second, functionColors, spriteRenderer, textRenderer));
		threadHolder->AddChildren(threadControls.back());
		threadControls.push_back(new gui::Panel(1.0f, 6.0f, gui::Panel::Type::Overlay));
		threadHolder->AddChildren(threadControls.back());
	}

	// Zoom
	inter->onScroll = [=](void*, int32 currentX, int32 currentY, int scrollBias)
	{
		pixelsPerSecond = renderW / currentViewableTime;
		double oldCursorTime = (currentX - renderX) / pixelsPerSecond;

		while(scrollBias-- > 0)
			currentViewableTime *= 0.75f;
		scrollBias++;
		while(scrollBias++ < 0)
			currentViewableTime *= 1.0f / 0.75f;

		pixelsPerSecond = renderW / currentViewableTime;
		double newCursorTime = (currentX - renderX) / pixelsPerSecond;
		startTime += oldCursorTime - newCursorTime;

		FixBounds();
	};

	// click and drag scroll
	inter->onDrag = [=](void*, int32 currentX, int32 currentY, int32 offsetX, int32 offsetY, bool released)
	{
		// x
		pixelsPerSecond = renderW / currentViewableTime;
		startTime -= offsetX / pixelsPerSecond;

		FixBounds();

		// y
		timelineOffsetY -= offsetY;
		RelayoutThreads();
	};
}

void Timeline::Update(const wnd::Input& input)
{
	mouseX = input.GetMouseX();
	mouseY = input.GetMouseY();
	masterPanel->Update(input);
}

void Timeline::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	StackEvents ev("Timeline::Render");

	spriteRenderer->RenderOne({renderX, renderY, renderW, renderH, ColorOffset(0.0f, 0.0f, 0.0f, 0.0f), whiteSpriteIndex});

	if(maxViewableTime < 0.0) // If this is negative, we had no events and the math got wonky
		return;

	gui::Clip::BeginManualClip(renderX, renderY, renderW, renderH, spriteRenderer, textRenderer);
	pixelsPerSecond = renderW / currentViewableTime;
	secondsPerPixel = 1.0f / pixelsPerSecond;


	masterPanel->Render(spriteRenderer, textRenderer);

	// Time axis
	/*
	textRenderer->RenderString(FormatTime(startTime).cstr(), 12.0f, renderX, renderY, 0xffffffff);
	double tickIncrement = 1.0;
	double divisions[] = {2.0, 2.5, 2.0};
	int divisionIndex = 0;
	while(tickIncrement * 6 > currentViewableTime)
		tickIncrement /= divisions[divisionIndex++ % 3];
	int32 currentTick = 1;
	tickIncrement /= 10.0f;
	SpriteRenderInfo info;
	info.y = renderY;
	info.w = 1.0f;
	info.h = renderH;
	info.spriteIndex = whiteSpriteIndex;

	while(tickIncrement * currentTick < currentViewableTime)
	{
		float tickRenderX = (float)round(renderX + tickIncrement * currentTick * pixelsPerSecond);
		info.x = tickRenderX;
		if(currentTick % 10 == 0)
		{
			char buf[64];
			sprintf_s(buf, "+%s", FormatTime(tickIncrement * currentTick).cstr());
			textRenderer->RenderString(buf, 12.0f, tickRenderX, renderY, 0xffffffff);
			info.colorOffset = ColorOffset(-0.2f, -0.2f, -0.2f, 0.0f);
		}
		else
			info.colorOffset = ColorOffset(-0.1f, -0.1f, -0.1f, 0.0f);
		spriteRenderer->RenderOne(info);
		currentTick++;
	}
	*/


	// Hover text
	borderImageHelper->spriteIndex = hoverBoxSpriteIndex;
	if(hoveredDataInfo.size())
	{
		float cursorOffsetY = 16.0f;
		borderImageHelper->w = 200;
		borderImageHelper->h = barHeight * hoveredDataInfo.size();
		borderImageHelper->colorOffset = ColorOffset(-0.2f, -0.2f, -0.2f, 0.0f);
		borderImageHelper->Layout((float)mouseX, (float)mouseY + cursorOffsetY, renderW, renderH);
		borderImageHelper->Render(spriteRenderer, textRenderer);

		for(size_t i = 0; i < hoveredDataInfo.size(); i++)
			textRenderer->RenderString(hoveredDataInfo[i].cstr(), 12.0f, mouseX + 2.0f, mouseY + cursorOffsetY + 2.0f + i * barHeight, 0xffffffff);
	}
	hoveredDataInfo.clear();

	gui::Clip::EndManualClip(spriteRenderer, textRenderer);
}

void Timeline::SetSelectedEvent(EventData* event)
{
	SetSelectedEventName(event->name);
	currentViewableTime = event->duration * 1.2;
	startTime = event->start - event->duration * 0.1;
	FixBounds();
}

bool Timeline::MoveThreadUp(ThreadControl* control)
{
	size_t index = std::find(threadControls.begin(), threadControls.end(), control) - threadControls.begin();

	bool canMove = index > 1;
	if(canMove)
		std::swap(threadControls[index], threadControls[index-2]); // 2 because of the spacers

	RelayoutThreads();

	return canMove;
}

bool Timeline::MoveThreadDown(ThreadControl* control)
{
	size_t index = std::find(threadControls.begin(), threadControls.end(), control) - threadControls.begin();

	bool canMove = index < threadControls.size() - 2;
	if(canMove)
		std::swap(threadControls[index], threadControls[index+2]); // 2 because of the spacers

	RelayoutThreads();

	return canMove;
}

void Timeline::MoveThreadTop(ThreadControl* control)
{
	while(MoveThreadUp(control));
}

void Timeline::MoveThreadBottom(ThreadControl* control)
{
	while(MoveThreadDown(control));
}

void Timeline::RelayoutThreads()
{
	threadHolder->ClearChildren();
	for(size_t i = 0; i < threadControls.size(); i++)
		threadHolder->AddChildren(threadControls[i]);

	// Do a 'dummy' layout so the newly modified thread gives the right height for the official relayout's tally
	threadHolder->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY() - timelineOffsetY, masterPanel->GetRenderW(), masterPanel->GetRenderH());

	float renderHeight = 0.0f;
	for(size_t i = 0; i < threadControls.size(); i++)
		renderHeight += threadControls[i]->GetRenderH();
	if(timelineOffsetY + renderH > renderHeight)
		timelineOffsetY = renderHeight - renderH;
	timelineOffsetY = Max(0.0f, timelineOffsetY);

	threadHolder->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY() - timelineOffsetY, masterPanel->GetRenderW(), masterPanel->GetRenderH());
}

void Timeline::FixBounds()
{
	// Keep us in bounds
	startTime = Max(0.0, startTime);
	currentViewableTime = Min(currentViewableTime, maxViewableTime);
	if(startTime + currentViewableTime > maxViewableTime)
		startTime = maxViewableTime - currentViewableTime;
}

Timeline::ThreadControl::ThreadControl(float width, float height, Timeline* parent, uint32 threadId, EventData* data, const std::map<uti::String, ColorOffset>& functionColors, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
	: Control(width, height), parent(parent)
{
	uint32 buttonBackground = spriteRenderer->GetSpriteIndex("tab");
	uint32 arrowUpSpriteIndex = spriteRenderer->GetSpriteIndex("arrowUp");
	uint32 arrowUp2SpriteIndex = spriteRenderer->GetSpriteIndex("arrowUp2");
	uint32 arrowDownSpriteIndex = spriteRenderer->GetSpriteIndex("arrowDown");
	uint32 arrowDown2SpriteIndex = spriteRenderer->GetSpriteIndex("arrowDown2");
	uint32 whiteSpriteIndex = spriteRenderer->GetSpriteIndex("white");
	uint32 backdropSpriteIndex = spriteRenderer->GetSpriteIndex("planeBorder");
	barSpriteIndex = spriteRenderer->GetSpriteIndex("itemSmall");
	float nameWidth = textRenderer->GetWidth(data->name.cstr(), 12.0f) + 14.0f;

	gui::ImageButton* buttonUp;
	gui::ImageButton* buttonDown;
	gui::ImageButton* buttonTop;
	gui::ImageButton* buttonBottom;
	gui::TextButton* buttonTitle;

	(masterPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack, "Timeline Thread"))->AddChildren(
		(new gui::Panel(1.0f, buttonHeight, gui::Panel::Type::HorizontalStack, "Timeline Thread Buttons"))->AddChildren(
			(buttonUp = new gui::ImageButton(buttonHeight, buttonHeight, spriteRenderer, buttonBackground, arrowUpSpriteIndex)),
			(buttonDown = new gui::ImageButton(buttonHeight, buttonHeight, spriteRenderer, buttonBackground, arrowDownSpriteIndex)),
			(buttonTop = new gui::ImageButton(buttonHeight, buttonHeight, spriteRenderer, buttonBackground, arrowUp2SpriteIndex)),
			(buttonBottom = new gui::ImageButton(buttonHeight, buttonHeight, spriteRenderer, buttonBackground, arrowDown2SpriteIndex)),
			(buttonTitle = new gui::TextButton(nameWidth, buttonHeight, spriteRenderer, buttonBackground, textRenderer, data->name.cstr()))),
		(new gui::BorderImage(1.0f, 1.0f, spriteRenderer, backdropSpriteIndex, ColorOffset(-0.1f, -0.1f, -0.1f, 0.0f))),
		(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay, "Timeline Thread Scratch Area")));

	buttonUp->onClicked = [=]() { parent->MoveThreadUp(this); };
	buttonDown->onClicked = [=]() { parent->MoveThreadDown(this); };
	buttonTop->onClicked = [=]() { parent->MoveThreadTop(this); };
	buttonBottom->onClicked = [=]() { parent->MoveThreadBottom(this); };
	buttonTitle->onClicked = [=]()
	{
		thread.expanded = !thread.expanded;
		h = thread.GetHeight();
		parent->RelayoutThreads();
	};

	// Refactor our raw event data into easily renderable data
	std::function<void(EventData*, uint32)> BuildTimelineData = [&](EventData* eventData, uint32 row)
	{
		if(thread.rows.size() < row)
			thread.rows.push_back(TimelineDataRow());

		for(size_t i = 0; i < eventData->children.size(); i++)
		{
			EventData* child = eventData->children[i];
			if(child->name == "self")
				continue;

			TimelineDataEvent event;
			event.name = child->name;
			event.color = functionColors.at(event.name);
			event.start = child->start;
			event.end = child->end;
			thread.rows[row-1].events.push_back(event);
			BuildTimelineData(child, row+1);
		}

		if(thread.rows.back().events.size() == 0)
			thread.rows.pop_back();
	};
	char buf[24];
	_itoa_s(threadId, buf, 10);
	thread.name = buf;
	thread.expanded = true;
	BuildTimelineData(data, 1);

	h = thread.GetHeight();
}

void Timeline::ThreadControl::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	masterPanel->Render(spriteRenderer, textRenderer);

	if(!thread.expanded)
		return;

	// Events
	parent->borderImageHelper->spriteIndex = barSpriteIndex;
	float offset = buttonHeight + 1.0f;

	for(size_t rowIndex = 0; rowIndex < thread.rows.size(); rowIndex++)
	{
		const TimelineDataRow& row = thread.rows.at(rowIndex);
		for(size_t eventIndex = 0; eventIndex < row.events.size(); eventIndex++)
		{
			const TimelineDataEvent& event = row.events.at(eventIndex);

			if(event.end < parent->startTime)
				continue; // event is to the left the viewable area

			if(event.start > parent->startTime + parent->currentViewableTime)
				break; // event is past the viewable area

			double duration = event.end - event.start;
			// If an event is too small, try to find similar events and group them into one render
			if(duration * parent->pixelsPerSecond < 2.0)
			{
				double childStartTime = event.start;
				double endTime = event.end;
				bool combinedEvents = false;
				while(++eventIndex < row.events.size() - 1 && endTime + parent->secondsPerPixel > row.events.at(eventIndex).end)
				{
					endTime = row.events.at(eventIndex).end;
					combinedEvents = true;
				}
				eventIndex--;

				parent->borderImageHelper->w = Max(1.001f, float((endTime - childStartTime) * parent->pixelsPerSecond));
				parent->borderImageHelper->h = barHeight;
				float x = renderX + float((childStartTime - parent->startTime) * parent->pixelsPerSecond);
				float y = renderY + offset + barHeight * rowIndex;
				if(combinedEvents)
					parent->borderImageHelper->colorOffset = ColorOffset(-0.5f, -0.5f, -0.5f, 0.0f);
				else
					parent->borderImageHelper->colorOffset = event.color;
				parent->borderImageHelper->Layout(x, y, renderW, renderH);
				parent->borderImageHelper->Render(spriteRenderer, textRenderer);
			}
			else
			{
				parent->borderImageHelper->w = float(duration * parent->pixelsPerSecond);
				parent->borderImageHelper->h = barHeight;
				float x = renderX + float((event.start - parent->startTime) * parent->pixelsPerSecond);
				float y = renderY + offset + barHeight * rowIndex;
				parent->borderImageHelper->colorOffset = event.color;
				parent->borderImageHelper->Layout(x, y, renderW, renderH);
				parent->borderImageHelper->Render(spriteRenderer, textRenderer);
				if(parent->selectedEventName == event.name)
				{
					parent->highlightBorderImageHelper->w = parent->borderImageHelper->w;
					parent->highlightBorderImageHelper->h = parent->borderImageHelper->h;
					parent->highlightBorderImageHelper->Layout(x, y, renderW, renderH);
					parent->highlightBorderImageHelper->Render(spriteRenderer, textRenderer);
				}

				if(parent->borderImageHelper->w > 20.0f)
				{
					gui::Clip::BeginManualClip(x, y, parent->borderImageHelper->w, parent->borderImageHelper->h, spriteRenderer, textRenderer);
					textRenderer->RenderString(event.name.cstr(), 12.0f, Max(0.0f, x) + 2.0f, y + 2.0f, parent->selectedEventName == event.name ? 0xff88BBff : 0xffffffff);
					gui::Clip::EndManualClip(spriteRenderer, textRenderer);
				}

				if(parent->mouseX >= x && parent->mouseX < x + parent->borderImageHelper->w && parent->mouseY >= y && parent->mouseY < y + parent->borderImageHelper->h)
				{
					parent->hoveredDataInfo.push_back(event.name);
					char buf[64];
					sprintf_s(buf, "%s (%s-%s)", FormatTime(duration).cstr(), FormatTime(event.start).cstr(), FormatTime(event.end).cstr());
					parent->hoveredDataInfo.push_back(buf);
				}
			}
		}
	}
}
