#pragma once

#include <functional>
#include "..\utility\SmallArray.h"

struct SharedMemoryInfo; 

struct ProcessInfo
{
	uti::String name;
	uint32 id;
};

std::vector<ProcessInfo> EnumerateRunningProcesses(std::vector<std::string> exceptions);

struct CapturedProcess
{
	uti::String name;
	uint32 id;

	struct ModuleInfo
	{
		uti::String name;
		uint64 begin;
		uint64 end;
		uint64 jumpTable;
	};
	std::vector<ModuleInfo> modules;

	struct Modification
	{
		uint64 address;
		int32 size;
		bool isDetour;
		bool isRipPromotion;
	};

	struct FunctionInfo
	{
		uti::String name;
		void* offset;
		uint64 size;
		//uti::String filename;
		//int32 fileLineStart;
		//int32 fileLineEnd;
		std::vector<uint8> code;
		std::vector<uint8> detouredCode;
		std::vector<Modification> modifications;
	};
	//std::vector<FunctionInfo> functions;
	std::map<void*, FunctionInfo> addressToFunction;

	struct SourceInfo
	{
		uti::String filename;
		int32 lineStart;
		int32 lineEnd; // inclusive
		int32 lineParent;
		std::vector<uti::String> lines;
		std::vector<SourceInfo> inlines;
	};

	struct ProfileSettings
	{
		enum class Type
		{
			Time,          // time - time to profile for (ms)
			FunctionCount, // count - time that function has to be called before capture ends
			FunctionTime,  // functionTime - min time function has to take for a capture (ms)
		};
		Type type;
		int32 time;
		void* selectedFunction;
		int32 functionCount;
		int32 functionTime;
	};

	int32 currentModule;

	bool LaunchExe(std::string exe, std::string workingDirectory, std::string arguments);
	bool Initialize(ProcessInfo info);
	bool InjectDLL();
	bool LoadSymbols(std::string filter);
	bool InitializeSymbols();
	void InjectSingleFunction(void* offset);
	void UninjectSingleFunction(void* offset);
	void InjectSeveralFunctions(std::vector<void*> offsets);
	void UninjectSeveralFunctions(std::vector<void*> offsets);
	void FindFunctionCalls(void* offset, std::vector<void*>& childOffsets);
	void GetFunctionSourceInfo(void* offset, std::vector<SourceInfo>& sourceInfo);
	int32 GetFunctionSizeBytes(void* offset);
	void OnUpdate(std::function<void(void)> onTakeCapture);
	void Profile(ProfileSettings settings, uti::String filename);
	void Cleanup();

	uti::String GetFunctionNameFromOffset(void* offset) { return addressToFunction[offset].name; }
	bool IsFunctionCaptured(void* offset) { return addressToFunction[offset].code.size(); }

private:
	void* processHandle = nullptr;
	void* functionTime;
	//void* functionBaseThreadInitThunk;
	//void* functionBaseThreadInitThunkEnd;

	struct ThreadInfo
	{
		uint32 id;
		void* handle;
		uint64 stackSize;
		uint64* stackMemory;
		uint64 rip;
		uint64 rsp;
	};
	std::vector<ThreadInfo> threads;

	struct SafetyInfo
	{
		// Written by ensure function
		std::vector<uint64> addressesOnStackThatNeedsToChange;
		std::vector<uint64> threadHandlesWithRipsThatNeedsToChange;

		// Written by caller
		std::vector<Modification> modifications;
		enum class Mode { Capture, Release } ensureMode;
	};
	void StartDetouring();
	SafetyInfo StartFunctionDetour(FunctionInfo& functionInfo);
	bool InjectFunction(FunctionInfo& functionInfo, SafetyInfo& safety, void* nextFunctionOffset);
	void UninjectFunction(FunctionInfo& functionInfo);
	void FinishFunctionDetour(bool success, const SafetyInfo& safetyInfo);
	void FinishDetouring();

	SharedMemoryInfo* sharedMemoryInfo;
	struct CapturedFunctionInfo
	{
		uti::String name;
		bool begin;
	};
	std::map<uint64, CapturedFunctionInfo> functionAddressMap;

	std::vector<int64> GetCoreTSCOffsets();

	// Asm helpers
	bool IsRexPrefix(uint8 instr) { return instr >= 0x40 && instr <= 0x4f; }
	bool IsSegmentPrefix(uint8 instr) { return instr == 0x64 || instr == 0x65; }
	bool IsWaitPrefix(uint8 instr) { return instr == 0x9b; }
	bool IsLockPrefix(uint8 instr) { return instr == 0xf0; }
	bool IsRepPrefix(uint8 instr) { return instr == 0xf2 || instr == 0xf3; }
	bool IsOperandSizePrefix(uint8 instr) { return instr == 0x66; }
	bool IsUnsupportedOpcode(uint8 instr);
	bool IsUnsupported2ByteOpcode(uint8 instr, uint8 instr2);
	bool IsMemoryOpcode(uint8 instr);
	bool IsMemory2ByteOpcode(uint8 instr);
	int32 GetImmSizeForOpcode(uint8 instr, uint8 modrm, bool sizeOverride, bool rexSizeOverride);
	int32 GetImmSizeFor2ByteOpcode(uint8 instr, uint8 instr2, bool sizeOverride);

	enum RipRelativeSize { relNone, rel8, rel16, rel32 };
	struct Instruction
	{
		uti::SmallArray<uint8, 16> bytes;
		RipRelativeSize ripRelativeSize;
		uint64 ripRelativeOffsetOffset; // offset within the instruction for the rip relative offset // I have no idea what this means anymore // oh yeah...now I remember
		int32 stackOffset;
		bool prologInstruction;
		bool detourWorthy;
		bool jumpToRegisterAddress;
		bool callSite;
	};

	uint64 GetInstruction(const std::vector<uint8>& code, uint64& readIndex, Instruction& outInstruction); // returns size of the instruction (zero for failure)
};

