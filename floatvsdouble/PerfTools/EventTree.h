#pragma once

#include "..\gui\CompositeControls.h"
#include "..\utility\Delegate.h"
class EventData;

class EventTreeData : public gui::GenericListViewData
{
public:
	EventTreeData(const std::map<uint32, EventData*>& data);
	~EventTreeData();

	virtual int32 GetNumRows() override { return (int32)rowData.size(); }
	virtual float GetRowH() override { return rowHeight; }
	virtual gui::Panel* GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;
	virtual gui::Panel* GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override;
	
	std::vector<EventData*>* GetCurrentSelection() { return currentSelectionRow >= 0 ? rowData[currentSelectionRow]->events : nullptr; }
	uti::Delegate<std::vector<EventData*>*> OnNewSelection;

private:
	float rowHeight;

	struct TreeData
	{
		~TreeData() { for(TreeData* child : children) delete child; }
		uti::String name;
		double average;
		double min;
		double max;
		std::vector<TreeData*> children;
		std::vector<EventData*> events;
	};
	TreeData* treeData;

	struct RowData
	{
		uti::String name;
		uti::String average;
		uti::String min;
		uti::String max;
		uti::String count;
		int32 depth;
		bool expanded;
		bool expandable;
		std::vector<EventData*>* events;
	};
	std::vector<RowData*> rowData;
	gui::Interactable* currentSelectionInter;
	int32 currentSelectionRow;

	void BuildTreeData(EventData* data, TreeData* treeData);
	void BuildRowData(TreeData* treeData, int32 depth);
};
