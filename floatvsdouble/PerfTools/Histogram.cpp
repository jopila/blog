#include "stdafx.h"

#include "Histogram.h"

#include "EventData.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\StackEvents.h"

Histogram::Histogram(float width, float height, SpriteRenderer* spriteRenderer)
	: Control(width, height), data(nullptr)
{
	fontSize = 12.0f;
	masterPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay, "Histogram");

	borderImageHelper = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("planeBorder"));
}

void Histogram::Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	SpriteRenderInfo renderInfo = {renderX, renderY, renderW, renderH, ColorOffset(0.0f, 0.0f, 0.0f, 0.0f), spriteRenderer->GetSpriteIndex("white") };
	spriteRenderer->RenderOne(renderInfo);

	if(data == nullptr || data->size() <= 1)
		return;

	int32 numBuckets;
	numBuckets = Max(1, int32(renderH / 24.0f));

	// find our min/max
	double minDur = DBL_MAX;
	double maxDur = 0.0;
	for(EventData* ed : *data)
	{
		minDur = Min(minDur, ed->duration);
		maxDur = Max(maxDur, ed->duration);
	}
	double span = maxDur - minDur;

	// bucket the durations
	int32 biggestBucket = 0;
	std::vector<int32> buckets;
	buckets.resize(numBuckets, 0);

	if(span == 0.0)
		biggestBucket = 0;
	else
	{
		for(EventData* ed : *data)
		{
			int32 index = (int32)((ed->duration - minDur) / span * numBuckets);
			index = Min(index, numBuckets - 1);
			buckets[index]++;
			biggestBucket = Max(biggestBucket, buckets[index]);
		}
	}

	// Calculate the locations of the axis
	float histoXOffset = fontSize + textRenderer->GetWidth("1.23ms", fontSize);
	float histoYOffset = fontSize;
	float histoWidth = renderW - fontSize * 2.0f - textRenderer->GetWidth("1.23ms", fontSize);
	float histoHeight = renderH - 2.0f * fontSize;

	// Render backdrop
	renderInfo = {renderX + histoXOffset - 1.0f, renderY + histoYOffset, 1.0f, histoHeight, ColorOffset(-0.5f, -0.5f, -0.5f, 0.0f), spriteRenderer->GetSpriteIndex("white") };
	spriteRenderer->RenderOne(renderInfo);

	// Render each bucket as a bar
	for(int32 i = 0; i < numBuckets; i++)
	{
		float left = 0.0f;
		float right = histoWidth * buckets[i] / biggestBucket;
		float top = histoHeight * i / numBuckets;
		float bottom = histoHeight * (i + 1) / numBuckets;
		top = roundf(top);
		bottom = roundf(bottom);

		left += histoXOffset;
		right += histoXOffset;
		top += histoYOffset;
		bottom += histoYOffset;

		if(buckets[i] != 0)
		{
			borderImageHelper->w = right - left;
			borderImageHelper->h = bottom - top;
			borderImageHelper->colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
			borderImageHelper->Layout(renderX + left, renderY + top, renderW, renderH);
			borderImageHelper->Render(spriteRenderer, textRenderer);

			char buf[20];
			_itoa_s(buckets[i], buf, 10);
			textRenderer->RenderString(buf, fontSize, renderX + left + 2.0f, renderY + top + 2.0f, 0xffffffff);
		}

		uti::String axisLabel = FormatTime(minDur + span / numBuckets * i);
		textRenderer->RenderString(axisLabel.cstr(), fontSize, renderX + fontSize, renderY + top - fontSize * 0.5f, 0xffffffff);
	}

	uti::String axisLabel = FormatTime(maxDur);
	textRenderer->RenderString(axisLabel.cstr(), fontSize, renderX + fontSize, renderY + histoHeight + histoYOffset - fontSize * 0.5f, 0xffffffff);
}
