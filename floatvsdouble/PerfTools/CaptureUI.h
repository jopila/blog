#pragma once

#include "..\gui\BasicControls.h"
#include "Process.h"


class SpriteRenderer;
class TextRenderer;
class ProcessListViewData;
class FunctionListViewData;
class SourceView;
class FunctionList;

class CaptureUI : public gui::Control
{
public:
	CaptureUI(float width, float height);
	virtual ~CaptureUI();

	struct FunctionInfo
	{
		uti::String name;
		void* offset;
	};

	void OpenProcessSelection(std::vector<uti::String> names, std::function<void(uti::String)> selection, std::function<bool(std::string, std::string, std::string)> launchFunc, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	struct OpenFunctionSelectionDelegates
	{
		std::function<void(void*)> captureFunc;
		std::function<void(void*)> decaptureFunc;
		std::function<void(std::vector<void*>)> captureAllFunc;
		std::function<void(std::vector<void*>)> uncaptureAllFunc;
		std::function<bool(void*)> isFuncCaptured;
		std::function<int32(void*)> getFunctionSizeBytes;
		std::function<void()> profileFunc;
		std::function<CapturedProcess::SourceInfo(void*)> getSourceInfo;
	};
	void OpenFunctionSelection(std::vector<FunctionInfo>& inFunctions, OpenFunctionSelectionDelegates delegates, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);

	void Layout(float parentX, float parentY, float parentW, float parentH)
	{
		Control::Layout(parentX, parentY, parentW, parentH);
		masterPanel->Layout(parentX, parentY, renderW, renderH);
	}
	void Update(const wnd::Input& input);
	void Render(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) { masterPanel->Render(spriteRenderer, textRenderer); }

	float fontSize;

	CapturedProcess::ProfileSettings profileSettings;

private:
	std::vector<FunctionInfo> allFunctions;
	std::vector<FunctionInfo> trimmedFunctions;
	std::vector<FunctionInfo*> rowToFunction;

	gui::Panel* masterPanel;
	gui::Panel* relayoutPanel;
	ProcessListViewData* processData;
	FunctionListViewData* functionListData;
	gui::CheckBox* SameTargetCheckBox;
	uti::String processSelection;
	int32 captureSelection = -1;
	gui::Panel* functionListParent;
	gui::ListView* functionListView;
	gui::Panel* sourceViewParent;
	SourceView* sourceView;
	bool showGeneratedFunctions = false;
	bool showStdFunctions = false;
	bool showSmallFunctions = false;
	std::string filter;

	std::function<void(void)> postUpdate;
	std::function<int32(void*)> getFuncSizeBytes;

	void CreateTrimmedFunctionList();
	void CreateFunctionList(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer);
	void onCapture(int32 row, std::function<void(void*)> captureFunc);
	void onDecapture(int32 row, std::function<void(void*)> decaptureFunc);
	void onProfileClicked(std::function<void()> profileFunc);
};