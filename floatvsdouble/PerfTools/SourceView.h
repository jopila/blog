#pragma once

#include "..\gui\BasicControls.h"
#include "..\gui\CompositeControls.h"
#include "Process.h"


class SpriteRenderer;
class TextRenderer;
class SourceViewInternal;

class SourceView : public gui::Panel
{
public:
	SourceView(float width, float height, SpriteRenderer* spriteRenderer);

	void SetData(const CapturedProcess::SourceInfo& data, TextRenderer* textRenderer);

	std::function<void(uti::String)> onWordClicked;

private:
	gui::ScrollView* scrollView;
	SourceViewInternal* sourceViewInternal;

	friend class SourceViewInternal;
	void RelayoutContent();
};
