
#include "stdafx.h"

#include <chrono>
#include <thread>
#include <stdio.h>

#include "..\Vulkan\Gfx.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\StackEvents.h"

#include "..\gui\CoreControls.h"
#include "..\Vulkan\File.h"

#include "Process.h"
#include "CaptureUI.h"
#include "ReportUI.h"

int main()
{
	// Create our window
	int32 screenWidth = 1600;
	int32 screenHeight = 900;

	wnd::WindowHandle* window = wnd::WindowHandle::Create(screenWidth, screenHeight, "Injection Profiler");
	gfx::Gfx* gfx = new gfx::Gfx(window);
	gui::Clip::InitScreenSize((float)screenWidth, (float)screenHeight);

	// Create our renderers
	StackEvents::Push("Creating renderers");
	SpriteRenderer* guiSpriteRenderer = new SpriteRenderer(gfx->GetDevice(), "..\\vulkan\\Sprites\\GUI");
	TextRenderer* guiTextRenderer = new TextRenderer(gfx->GetDevice(), "Roboto-Regular");
	gfx->RegisterRenderer(guiSpriteRenderer);
	gfx->RegisterRenderer(guiTextRenderer);
	StackEvents::Pop();

	// Kick off with a list of running processes we can capture
	std::vector<std::string> exceptions = ReadFileLines("CaptureExceptions.ini");
	std::vector<ProcessInfo> infos = EnumerateRunningProcesses(exceptions);
	CapturedProcess attachedProcess;

	// Create our master control
	gui::Panel* globalPanel;
	gui::Button* openButton;
	gui::Button* saveButton;
	CaptureUI* captureUI;
	gui::Tabs* tabs;
	std::vector<gui::Tabs::TabHandle*> unsavedTabHandles;
	int capturesDone = 0;
	(globalPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack, "Global Overlay"))->AddChildren(
		(new gui::Alignment(1.0f, 20.0f, 1.0f, 1.0f, gui::Alignment::Type::TopLeft))->SetChild(
			(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
				(openButton = new gui::ImageButton(18.0f, 18.0f, guiSpriteRenderer, guiSpriteRenderer->GetSpriteIndex("button"), guiSpriteRenderer->GetSpriteIndex("openIcon"))),
				(saveButton = new gui::ImageButton(18.0f, 18.0f, guiSpriteRenderer, guiSpriteRenderer->GetSpriteIndex("button"), guiSpriteRenderer->GetSpriteIndex("saveIcon"))))),
		(tabs = new gui::Tabs(1.0f, 1.0f, guiSpriteRenderer)));
	globalPanel->Layout(0.0f, 0.0f, (float)screenWidth, (float)screenHeight);
	tabs->AddTab(captureUI = new CaptureUI(1.0f, 1.0f), "Capture", false, guiSpriteRenderer, guiTextRenderer);
	tabs->onTabClosed = [&](gui::Tabs::TabHandle* tabHandle)
	{
		std::vector<gui::Tabs::TabHandle*>::iterator it = std::find(unsavedTabHandles.begin(), unsavedTabHandles.end(), tabHandle);
		if(it != unsavedTabHandles.end())
			unsavedTabHandles.erase(it);
	};

	// Begin the capture UI with the process selection
	std::vector<uti::String> processNames;
	for(ProcessInfo& info : infos)
		processNames.push_back(info.name);

	std::function<void(void)> takeACapture = [&]()
	{
		char buf[10];
		_itoa_s(capturesDone++, buf, 10);
		std::string tabName = std::string("untitled") + buf + ".ipc";
		attachedProcess.Profile(captureUI->profileSettings, tabName.c_str());
		ReportUI* reportUI = new ReportUI(1.0f, 1.0f, tabName.c_str(), guiSpriteRenderer, guiTextRenderer);
		gui::Tabs::TabHandle* tab = tabs->AddTab(reportUI, tabName, true, guiSpriteRenderer, guiTextRenderer);
		unsavedTabHandles.push_back(tab);
		tabs->SelectTab(tab);
	};

	auto OpenFunctionSelection = [&]()
	{
		std::vector<CaptureUI::FunctionInfo> functions;
		for(std::pair<void*const,CapturedProcess::FunctionInfo>& pair : attachedProcess.addressToFunction)
			functions.push_back({pair.second.name, pair.second.offset});

		CaptureUI::OpenFunctionSelectionDelegates delegates;
		delegates.captureFunc      = [&](void* offset)               {attachedProcess.InjectSingleFunction(offset);};
		delegates.decaptureFunc    = [&](void* offset)               {attachedProcess.UninjectSingleFunction(offset);};
		delegates.captureAllFunc   = [&](std::vector<void*> offsets) {attachedProcess.InjectSeveralFunctions(offsets);};
		delegates.uncaptureAllFunc = [&](std::vector<void*> offsets) {attachedProcess.UninjectSeveralFunctions(offsets);};
		delegates.isFuncCaptured   = [&](void* offset)               {return attachedProcess.IsFunctionCaptured(offset);};
		delegates.profileFunc      = takeACapture;
		delegates.getSourceInfo    = [&](void* offset)
		{
			std::vector<CapturedProcess::SourceInfo> SourceInfo;
			attachedProcess.GetFunctionSourceInfo(offset, SourceInfo);
			if(SourceInfo.size() > 1)
				printf("Unexpected source info: has too many top level source codes\n");
			if(SourceInfo.size() == 0)
			{
				printf("Empty source info\n");
				return CapturedProcess::SourceInfo();
			}
			return SourceInfo[0];
		};
		delegates.getFunctionSizeBytes = [&](void* offset) {return attachedProcess.GetFunctionSizeBytes(offset);};

		captureUI->OpenFunctionSelection(functions, delegates, guiSpriteRenderer, guiTextRenderer);
	};

	auto launchFunction = [&](std::string exe, std::string workingDirectory, std::string arguments)
	{
		bool result = attachedProcess.LaunchExe(exe, workingDirectory, arguments);
		if(result)
			OpenFunctionSelection();
		return result;
	};

	captureUI->OpenProcessSelection(processNames, [&](uti::String name)
	{
		for(int32 i = 0; i < infos.size(); i++)
		{
			if(infos[i].name == name)
			{
				attachedProcess.Initialize(infos[i]);
			}
		}

		OpenFunctionSelection();
		
	}, launchFunction, guiSpriteRenderer, guiTextRenderer);

	openButton->onClicked = [=]()
	{
		std::string result = OpenFileDialog({{"Injection Profiler Capture", "*.ipc"}});
		if(!result.empty())
		{
			std::string filename = result.substr(result.find_last_of('\\') + 1);
			gui::Tabs::TabHandle* tab = tabs->AddTab(new ReportUI(1.0f, 1.0f, result.c_str(), guiSpriteRenderer, guiTextRenderer), filename, true, guiSpriteRenderer, guiTextRenderer);
			tabs->SelectTab(tab);
		}
	};

	saveButton->onClicked = [=,&unsavedTabHandles]()
	{
		int foundHandleIndex = -1;
		for(size_t i = 0; i < unsavedTabHandles.size(); i++)
			if(tabs->GetCurrentSelection() == unsavedTabHandles[i])
				foundHandleIndex = (int)i;

		if(foundHandleIndex != -1)
		{
			gui::Tabs::TabHandle* foundHandle = unsavedTabHandles[foundHandleIndex];
			std::string result = SaveFileDialog({{"Injection Profiler Capture", "*.ipc"}});
			if(!result.empty())
			{
				if(!EndsWith(result, ".ipc"))
					result += ".ipc";
				remove(result.c_str());
				rename(tabs->GetTabName(foundHandle).c_str(), result.c_str());
				std::string filename = result.substr(result.find_last_of('\\') + 1);
				tabs->RenameTab(foundHandle, filename);
			}
			unsavedTabHandles.erase(unsavedTabHandles.begin() + foundHandleIndex);
		}
	};

	// Make sure our UI adjusts when the window changes
	gfx->OnScreenResize = [=,&screenWidth,&screenHeight](int32 width, int32 height)
	{
		screenWidth = width;
		screenHeight = height;
		gui::Clip::InitScreenSize((float)screenWidth, (float)screenHeight);
		globalPanel->Layout(0.0f, 0.0f, (float)screenWidth, (float)screenHeight);
	};

	// MAIN LOOP!
	bool requestExit = false;
	while(!requestExit)
	{
		StackEvents::Push("Main Loop");
		std::this_thread::sleep_for(std::chrono::milliseconds(5));

		gfx->BeginFrame();
		window->PollEvents();

		attachedProcess.OnUpdate(takeACapture);
		globalPanel->Update(window->GetInput());
		globalPanel->Render(guiSpriteRenderer, guiTextRenderer);

		gfx->EndFrame();
		requestExit = window->ShouldCloseWindow() || window->GetInput().IsKeyTriggered(wnd::Keyboard::Escape);
		StackEvents::Pop();
	}

	// delete the temporary 'untitled' captures
	for(size_t i = 0; i < unsavedTabHandles.size(); i++)
		remove(tabs->GetTabName(unsavedTabHandles[i]).c_str());

	// Buncha cleanup!
	attachedProcess.Cleanup();
	delete globalPanel;
	gfx->UnregisterRenderer(guiTextRenderer);
	gfx->UnregisterRenderer(guiSpriteRenderer);
	delete guiSpriteRenderer;
	delete guiTextRenderer;
	delete gfx;
	wnd::WindowHandle::Destroy(window);

	return 0;
}
