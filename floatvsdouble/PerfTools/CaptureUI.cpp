#include "stdafx.h"
#include <algorithm>

#include "CaptureUI.h"
#include "SourceView.h"

#include "..\gui\CompositeControls.h"
#include "..\Vulkan\File.h"

const char* captureTargetIniFileName = "CaptureTarget.ini";


class ListViewData : public gui::GenericListViewData
{
public:
	virtual float GetRowH() override { return rowHeight; }
	virtual float GetColumnWidth(int32 column) { return 1.0f / numColumns; }
	virtual int32 GetNumColumns() = 0;
	virtual std::string GetHeader(int32 column) = 0;
	virtual std::string GetData(int32 row, int32 column) = 0;
	virtual bool IsLessThan(int32 rowA, int32 rowB, int32 column) = 0;

	bool addCheckboxes = false;
	virtual void Relayout() override;

	std::function<void(ListViewData* data, int32 row)> onSelected;
	std::function<bool(ListViewData* data, int32 row, bool checked)> onCheckedChanged;

protected:
	void SortByColumn(int32 column);

private:
	virtual gui::Panel* GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override final;
	virtual gui::Panel* GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer) override final;
	virtual bool ShouldRowBeChecked(int32 row) { return false; }

	std::vector<gui::Panel*> itemPanels;
	std::vector<int32> reorderIndices;
	std::vector<gui::CheckBox*> checkboxes;
	float rowHeight = 18.0f;
	int32 numColumns;
	int32 numRows;
	int32 currentSelectionRow = -1;
	gui::Interactable* currentSelectionInteractable = nullptr;
};


void ListViewData::SortByColumn(int32 column)
{
	for(int32 i = 0; i < numRows - 1; i++)
	{
		for(int32 j = 0; j < numRows - i - 1; j++)
		{
			if(!IsLessThan(reorderIndices[j], reorderIndices[j+1], column))
				std::swap(reorderIndices[j], reorderIndices[j+1]);
		}
	}
}

void ListViewData::Relayout()
{
	currentSelectionInteractable = nullptr;
	currentSelectionRow = -1;
	itemPanels.clear();
	reorderIndices.clear();
	checkboxes.clear();
}

gui::Panel * ListViewData::GetHeaderPanel(gui::ListView* listView, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	// Sneaky constructor...
	numColumns = GetNumColumns();
	numRows = GetNumRows();

	reorderIndices.reserve(numRows);
	for(int32 i = 0; i < numRows; i++)
		reorderIndices.push_back(i);

	// Actual header panel generation
	gui::Panel* headerRowPanel = new gui::Panel(1.0f, rowHeight, gui::Panel::Type::HorizontalStack);
	for(int32 c = 0; c < numColumns; c++)
	{
		gui::TextButton* button;
		headerRowPanel->AddChildren(button = new gui::TextButton(
			GetColumnWidth(c), 1.0f,
			spriteRenderer, spriteRenderer->GetSpriteIndex("ListViewHeader"),
			textRenderer, GetHeader(c)));

		button->onClicked = [=](){ SortByColumn(c); listView->Relayout(); };
	}

	return headerRowPanel;
}

gui::Panel* ListViewData::GetRowPanel(gui::ListView* listView, int32 row, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	gui::Panel* rowPanel;
	gui::Panel* itemPanel;
	gui::Interactable* inter;
	gui::CheckBox* checkBox;
	gui::BorderImage* rowImage;
	std::vector<gui::BorderImage*> rowImages;

	(rowPanel = new gui::Panel(1.0f, rowHeight, gui::Panel::Type::HorizontalStack));

	if(addCheckboxes)
	{
		(rowPanel)->AddChildren(
			(checkBox = addCheckboxes ? new gui::CheckBox(rowHeight, rowHeight, spriteRenderer) : nullptr));
		if(ShouldRowBeChecked(row))
			checkBox->SetChecked(true, false);
		checkboxes.push_back(checkBox);
		checkBox->onCheckedChanged = [=](bool checked) { onCheckedChanged(this, row, checked); };
	}

	(rowPanel)->AddChildren(
		(itemPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
			(inter = new gui::Interactable(1.0f, 1.0f))));

	for(int32 c = 0; c < numColumns; c++)
	{
		(itemPanel)->AddChildren(
			(new gui::Clip(1.0f, 1.0f))->SetChild(
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
					(rowImage = new gui::BorderImage(1.0f, 1.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("item"))),
					(new gui::Text(1.0f, 1.0f, GetData(row, c))))));
		rowImages.push_back(rowImage);
	}

	// line selection
	inter->onHoverChange = [=](void*, bool hovered)
	{
		ColorOffset colorOffset;
		if(currentSelectionInteractable == inter)
			colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		else
			colorOffset = hovered ? ColorOffset(-0.1f, -0.05f, 0.0f, 0.0f) : ColorOffset(0.0f, 0.0f, 0.0f, 0.0f);

		for(int32 c = 0; c < numColumns; c++)
			rowImages[c]->colorOffset = colorOffset;
	};

	inter->onLeftReleased = [=](void*, int32, int32, int32, int32)
	{
		gui::Interactable* tempCurrentSelection = currentSelectionInteractable;
		currentSelectionInteractable = inter;
		currentSelectionRow = row;
		if(tempCurrentSelection)
			tempCurrentSelection->onHoverChange(tempCurrentSelection->data, false); // unhighlight the previous selection

		ColorOffset colorOffset = ColorOffset(-0.2f, -0.1f, 0.1f, 0.0f);
		for(int32 c = 0; c < numColumns; c++)
			rowImages[c]->colorOffset = colorOffset;

		if(onSelected)
			onSelected(this, row);
	};

	inter->onDestroy = [=](void*)
	{
		currentSelectionInteractable = nullptr;
	};

	if(row == currentSelectionRow)
	{
		currentSelectionInteractable = inter;
		inter->onHoverChange(inter->data, false);
	}

	itemPanels.push_back(rowPanel);
	return rowPanel;
}

class ProcessListViewData final : public ListViewData
{
public:
	static const int32 columns = 1;
	std::vector<uti::String> processes;
	uti::String headerName;

	ProcessListViewData(std::vector<uti::String> processes, uti::String headerName)
		:processes(processes), headerName(headerName) {}

	virtual int32 GetNumRows() override    { return (int32)processes.size(); }
	virtual int32 GetNumColumns() override { return columns; }

	virtual std::string GetHeader(int32 column) override
	{
		return headerName.cstr();
	}

	virtual std::string GetData(int32 row, int32 column) override
	{
		return processes[row].cstr();
	}

	virtual bool IsLessThan(int32 rowA, int32 rowB, int32 column) override
	{
		return processes[rowA] < processes[rowB];
	}
};

class FunctionListViewData final : public ListViewData
{
public:
	std::vector<CaptureUI::FunctionInfo> functions;
	std::function<bool(void*)> isFuncCaptured;

	virtual int32 GetNumRows() override        { return (int32)functions.size(); }
	virtual int32 GetNumColumns() override     { return 1; }
	virtual bool ShouldRowBeChecked(int32 row) { return isFuncCaptured(functions[row].offset); }

	virtual std::string GetHeader(int32 column) override { return "functions"; }
	virtual std::string GetData(int32 row, int32 column) override { return functions[row].name.cstr(); }
	virtual bool IsLessThan(int32 rowA, int32 rowB, int32 column) override { return functions[rowA].name < functions[rowB].name; }
};

CaptureUI::CaptureUI(float width, float height)
	: Control(width, height)
{
	(masterPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
		(relayoutPanel = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay)));

	profileSettings.type = CapturedProcess::ProfileSettings::Type::Time;
	profileSettings.time = 200;
	profileSettings.functionCount = 5;
	profileSettings.functionTime = 100;
}

CaptureUI::~CaptureUI()
{
	delete masterPanel;
	delete processData;
	delete functionListData;
}

void CaptureUI::OpenProcessSelection(std::vector<uti::String> names, std::function<void(uti::String)> selection, std::function<bool(std::string, std::string, std::string)> launchFunc, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	// If there's an exception reading the capture target assume it doesn't exist and we need to show the target selection
	std::vector<std::string> targets;
	try
	{
		targets = ReadFileLines(captureTargetIniFileName);

		if(targets.size() != 1)
			throw std::exception();

		bool foundName = false;
		for(size_t i = 0; i < names.size(); i++)
			if(targets[0] == names[i].cstr())
				foundName = true;
		if(!foundName)
			throw std::exception();
	}
	catch(...)
	{
		gui::TextButton* attachButton;
		gui::ImageButton* exeButton;
		gui::ImageButton* workingDirectoryButton;
		gui::TextButton* launchButton;
		gui::Text* exeText;
		gui::Text* workingDirectoryText;
		gui::TextInput* argumentsTextInput;
		int32 buttonSpriteIndex = spriteRenderer->GetSpriteIndex("button");
		int32 ellipsisIndex = spriteRenderer->GetSpriteIndex("ellipsis");

		processData = new ProcessListViewData(names, "Processes");
		(relayoutPanel)->AddChildren(
			(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
				(new gui::ListView(0.5f, 1.0f, *processData, spriteRenderer, textRenderer)),
					(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack))->AddChildren(
						(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
							(SameTargetCheckBox = new gui::CheckBox(18.0f, 18.0f, spriteRenderer)),
							(new gui::Text(1.0f, 1.0f, "Use same target for future captures (delete CaptureTarget.ini to reset this)"))),
						(attachButton = new gui::TextButton(200.0f, 24.0f, spriteRenderer, buttonSpriteIndex, textRenderer, "Attach!")),
						(new gui::Panel(1.0f, 8.0f, gui::Panel::Type::Overlay)),
						(new gui::Text(1.0f, 18.0f, "Launch Settings:")),
						(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
							(exeButton = new gui::ImageButton(18.0f, 1.0f, spriteRenderer, buttonSpriteIndex, ellipsisIndex)),
							(exeText = new gui::Text(1.0f, 1.0f, "Select an executable"))),
						(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
							(workingDirectoryButton = new gui::ImageButton(18.0f, 1.0f, spriteRenderer, buttonSpriteIndex, ellipsisIndex)),
							(workingDirectoryText = new gui::Text(1.0f, 1.0f, "Select a working directory"))),
						(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
							(new gui::Text(100.0f, 18.0f, "Arguments: ")),
							(argumentsTextInput = new gui::TextInput(1.0f, 18.0f, spriteRenderer, textRenderer))),
						(launchButton = new gui::TextButton(200.0f, 24.0f, spriteRenderer, buttonSpriteIndex, textRenderer, "Launch!")))));
		relayoutPanel->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY(), masterPanel->GetRenderW(), masterPanel->GetRenderH());

		attachButton->onClicked = [=]()
		{
			if(SameTargetCheckBox->IsChecked())
			{
				FileWriter captureTargetIniFile = FileWriter(std::string(captureTargetIniFileName));
				captureTargetIniFile.WriteRaw((uint8*)processSelection.cstr(), processSelection.length());
			}

			if(selection && processSelection != "")
				selection(processSelection);
		};

		exeButton->onClicked = [=]()
		{
			std::string filename = OpenFileDialog({{"executable", "*.exe"}});
			exeText->text = filename;
			workingDirectoryText->text = filename.substr(0, filename.find_last_of('\\'));
		};

		workingDirectoryButton->onClicked = [=]()
		{
			std::string foldername = OpenFolderDialog();
			workingDirectoryText->text = foldername;
		};

		launchButton->onClicked = [=]()
		{
			launchFunc(exeText->text, workingDirectoryText->text, argumentsTextInput->GetText());
		};

		processData->onSelected = [=](ListViewData* data, int32 row)
		{
			ProcessListViewData* myData = (ProcessListViewData*)data;
			processSelection = myData->processes[row];
		};
		return;
	}

	selection(targets[0].c_str());
}

void CaptureUI::OpenFunctionSelection(std::vector<FunctionInfo>& inFunctions, OpenFunctionSelectionDelegates delegates, SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	postUpdate = [=]()
	{
		relayoutPanel->ClearAndDeleteChildren();
		delete processData;
		processData = nullptr;

		allFunctions = inFunctions;

		std::qsort(&allFunctions[0], allFunctions.size(), sizeof(allFunctions[0]), [](const void* pa, const void* pb)
		{
			FunctionInfo& a = *(FunctionInfo*)pa;
			FunctionInfo& b = *(FunctionInfo*)pb;
			if(a.name < b.name) return -1;
			if(a.name > b.name) return  1;
			return 0;
		});

		functionListData = new FunctionListViewData();
		functionListData->isFuncCaptured = delegates.isFuncCaptured;
		getFuncSizeBytes = delegates.getFunctionSizeBytes;
		functionListData->addCheckboxes = true;

		gui::CheckBox* showGeneratedFunctionsCheckBox;
		gui::CheckBox* showStdFunctionsCheckBox;
		gui::CheckBox* showSmallFunctionsCheckBox;
		gui::TextButton* profileButton;
		gui::TextButton* captureAllButton;
		gui::TextButton* uncaptureAllButton;
		gui::TextInput* functionFilterTextInput;
		gui::RadioSelection* captureSettingRadio;
		gui::Text* captureSettingTextDescription;
		gui::TextInput* captureSettingTextInput;

		(relayoutPanel)->AddChildren(
			(new gui::Splitter(1.0f, 1.0f, gui::Splitter::Type::Horizontal))->SetChildren(spriteRenderer,
				(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack))->AddChildren(
					(functionFilterTextInput = new gui::TextInput(1.0f, 18.0f, spriteRenderer, textRenderer)),
					(functionListParent = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
						(functionListView = new gui::ListView(1.0f, 1.0f, *functionListData, spriteRenderer, textRenderer)))),
				(new gui::Panel(1.0, 1.0f, gui::Panel::Type::VerticalStack))->AddChildren(
					(new gui::Panel(1.0f, 24.0f + 3 * 18.0f + 4.0f, gui::Panel::Type::HorizontalStack, "Capture Settings"))->AddChildren(
						(new gui::Panel(200.0f, 1.0f, gui::Panel::Type::VerticalStack))->AddChildren(
							(profileButton = new gui::TextButton(200.0f, 24.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("button"), textRenderer, "Profile!")),
							(captureSettingRadio = new gui::RadioSelection(1.0f, {"Timed", "Function Count", "Function Hitch"}, spriteRenderer, textRenderer))),
						(new gui::Panel(1.0f, 1.0f, gui::Panel::Type::VerticalStack))->AddChildren(
							(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
								(captureSettingTextDescription = new gui::Text(100.0f, 18.0f, "Time (ms):")),
								(captureSettingTextInput = new gui::TextInput(1.0f, 1.0f, spriteRenderer, textRenderer))))),
					(new gui::Alignment(1.0f, 21.0f, 0.0f, 0.0f, gui::Alignment::Type::Center))->SetChild(
						(new gui::Image(1.0f, 1.01f, spriteRenderer->GetSpriteIndex("white"), ColorOffset(-0.3f, -0.3f, -0.3f, 0.0f)))),
					(captureAllButton = new gui::TextButton(200.0f, 24.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("button"), textRenderer, "Capture all functions")),
					(uncaptureAllButton = new gui::TextButton(200.0f, 24.0f, spriteRenderer, spriteRenderer->GetSpriteIndex("button"), textRenderer, "Uncapture all functions")),
					(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
						(showGeneratedFunctionsCheckBox = new gui::CheckBox(18.0f, 18.0f, spriteRenderer)),
						(new gui::Text(1.0f, 1.0f, "Show generated functions"))),
					(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
						(showStdFunctionsCheckBox = new gui::CheckBox(18.0f, 18.0f, spriteRenderer)),
						(new gui::Text(1.0f, 1.0f, "Show std:: functions"))),
					(new gui::Panel(1.0f, 18.0f, gui::Panel::Type::HorizontalStack))->AddChildren(
						(showSmallFunctionsCheckBox = new gui::CheckBox(18.0f, 18.0f, spriteRenderer)),
						(new gui::Text(1.0f, 1.0f, "Show Small functions"))),
					(new gui::Alignment(1.0f, 21.0f, 0.0f, 0.0f, gui::Alignment::Type::Center))->SetChild(
						(new gui::Image(1.0f, 1.01f, spriteRenderer->GetSpriteIndex("white"), ColorOffset(-0.3f, -0.3f, -0.3f, 0.0f)))),
					(sourceViewParent = new gui::Panel(1.0f, 1.0f, gui::Panel::Type::Overlay))->AddChildren(
						(sourceView = new SourceView(1.0f, 1.0f, spriteRenderer))))));
		relayoutPanel->Layout(masterPanel->GetRenderX(), masterPanel->GetRenderY(), masterPanel->GetRenderW(), masterPanel->GetRenderH());
		CreateTrimmedFunctionList();
		CreateFunctionList(spriteRenderer, textRenderer);
		captureSettingTextInput->SetText("200");

		profileButton->onClicked = [=]()
		{
			onProfileClicked(delegates.profileFunc);
		};

		sourceView->onWordClicked = [=](uti::String word)
		{
			functionFilterTextInput->SetText(word.cstr());
		};

		functionListData->onSelected = [=](ListViewData*, int32 row)
		{
			CapturedProcess::SourceInfo sourceInfo = delegates.getSourceInfo(rowToFunction[row]->offset);
			profileSettings.selectedFunction = rowToFunction[row]->offset;
			sourceView->SetData(sourceInfo, textRenderer);
		};

		functionListData->onCheckedChanged = [=](ListViewData* data, int32 row, bool checked)
		{
			if(checked)
				onCapture(row, delegates.captureFunc);
			else
				onDecapture(row, delegates.decaptureFunc);
			return true;
		};

		captureAllButton->onClicked = [=]()
		{
			std::vector<void*> functionOffsets;
			for(size_t i = 0; i < functionListData->GetNumRows(); i++)
				functionOffsets.push_back(rowToFunction[i]->offset);
			delegates.captureAllFunc(functionOffsets);
			functionListView->Relayout();
		};

		uncaptureAllButton->onClicked = [=]()
		{
			std::vector<void*> functionOffsets;
			for(size_t i = 0; i < functionListData->GetNumRows(); i++)
				functionOffsets.push_back(rowToFunction[i]->offset);
			delegates.uncaptureAllFunc(functionOffsets);
			functionListView->Relayout();
		};

		showGeneratedFunctionsCheckBox->onCheckedChanged = [=](bool checked)
		{
			showGeneratedFunctions = checked;
			CreateTrimmedFunctionList();
			CreateFunctionList(spriteRenderer, textRenderer);
		};

		showStdFunctionsCheckBox->onCheckedChanged = [=](bool checked)
		{
			showStdFunctions = checked;
			CreateTrimmedFunctionList();
			CreateFunctionList(spriteRenderer, textRenderer);
		};

		showSmallFunctionsCheckBox->onCheckedChanged = [=](bool checked)
		{
			showSmallFunctions = checked;
			CreateTrimmedFunctionList();
			CreateFunctionList(spriteRenderer, textRenderer);
		};

		functionFilterTextInput->onTextChanged = [=](std::string newText)
		{
			filter = newText;
			CreateFunctionList(spriteRenderer, textRenderer);
		};

		captureSettingRadio->onSelectionChanged = [=](int32 index, std::string text)
		{
			char ch[24];
			profileSettings.type = (CapturedProcess::ProfileSettings::Type)index;
			switch(profileSettings.type)
			{
			case CapturedProcess::ProfileSettings::Type::Time:
				captureSettingTextDescription->text = "Time (ms):";
				_itoa_s(profileSettings.time, ch, 10);
				captureSettingTextInput->SetText(ch);
				break;
			case CapturedProcess::ProfileSettings::Type::FunctionCount:
				captureSettingTextDescription->text = "Call Count:";
				_itoa_s(profileSettings.functionCount, ch, 10);
				captureSettingTextInput->SetText(ch);
				break;
			case CapturedProcess::ProfileSettings::Type::FunctionTime:
				captureSettingTextDescription->text = "Min Time (ms):";
				_itoa_s(profileSettings.functionTime, ch, 10);
				captureSettingTextInput->SetText(ch);
				break;
			default: break;
			}
		};

		captureSettingTextInput->onTextChanged = [=](std::string str)
		{
			switch(profileSettings.type)
			{
			case CapturedProcess::ProfileSettings::Type::Time: profileSettings.time = atoi(str.c_str()); break;
			case CapturedProcess::ProfileSettings::Type::FunctionCount: profileSettings.functionCount = atoi(str.c_str()); break;
			case CapturedProcess::ProfileSettings::Type::FunctionTime: profileSettings.functionTime = atoi(str.c_str()); break;
			default: break;
			}
		};
	};
}

void CaptureUI::Update(const wnd::Input & input)
{
	masterPanel->Update(input);
	if(postUpdate)
	{
		postUpdate();
		postUpdate = nullptr;
	}
}

void CaptureUI::CreateTrimmedFunctionList()
{
	trimmedFunctions.clear();
	int32 passesGeneratedCheck = 0;
	int32 passesStdCheck = 0;
	int32 passesSmallCheck = 0;
	for(size_t i = 0; i < allFunctions.size(); i++)
	{
		uti::String& functionName = allFunctions[i].name;
		bool isGeneratedFunction = functionName.cstr()[0] == '`';
		bool isStdFunction = functionName.Find("std::") != functionName.length();
		bool isSmallFunction = getFuncSizeBytes(allFunctions[i].offset) < 100;

		bool passes = true;
		passes = passes && (!isGeneratedFunction || showGeneratedFunctions);
		passes = passes && (!isStdFunction || showStdFunctions);
		passes = passes && (!isSmallFunction || showSmallFunctions);

		passesGeneratedCheck += isGeneratedFunction;
		passesStdCheck += isStdFunction;
		passesSmallCheck += isSmallFunction;

		if(passes)
			trimmedFunctions.push_back(allFunctions[i]);
	}

	printf("Trimmed Functions (%i)\n  Generated: %i\n  Std: %i\n  Small: %i\n", (int32)allFunctions.size(), passesGeneratedCheck, passesStdCheck, passesSmallCheck);
}

void CaptureUI::CreateFunctionList(SpriteRenderer* spriteRenderer, TextRenderer* textRenderer)
{
	functionListData->functions.clear();
	rowToFunction.clear();
	int32 passesFilterCheck = 0;
	for(size_t i = 0; i < trimmedFunctions.size(); i++)
	{
		uti::String& functionName = trimmedFunctions[i].name;
		bool passesFilter = true;
		if(filter.length() > 2)
			passesFilter = functionName.FindIgnoreCase(filter.c_str()) != functionName.length();

		bool passes = true;
		passes = passes && (passesFilter || filter.size() == 0);

		passesFilterCheck += passesFilter;

		if(passes)
		{
			functionListData->functions.push_back(trimmedFunctions[i]);
			rowToFunction.push_back(&trimmedFunctions[i]);
		}
	}

	printf("Filter finished (%i)\n  Filtered: %i\n", (int32)trimmedFunctions.size(), passesFilterCheck);

	functionListParent->ClearAndDeleteChildren();
	functionListData->Relayout();
	(functionListParent)->AddChildren(
		(functionListView = new gui::ListView(1.0f, 1.0f, *functionListData, spriteRenderer, textRenderer)));
	functionListView->Layout(functionListParent->GetRenderX(), functionListParent->GetRenderY(), functionListParent->GetRenderW(), functionListParent->GetRenderH());
}

void CaptureUI::onCapture(int32 row, std::function<void(void*)> captureFunc)
{
	captureFunc(rowToFunction[row]->offset);
}

void CaptureUI::onDecapture(int32 row, std::function<void(void*)> decaptureFunc)
{
	decaptureFunc(rowToFunction[row]->offset);
}

void CaptureUI::onProfileClicked(std::function<void()> profileFunc)
{
	if(profileFunc) profileFunc();
}
