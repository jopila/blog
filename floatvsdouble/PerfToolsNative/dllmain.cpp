// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <winternl.h>
#include <TlHelp32.h>
#include <wtsapi32.h>
#include <comdef.h>
#include <Psapi.h>
#define _NO_CVCONST_H
#include <DbgHelp.h>
#define INITGUID
#include <string>
#include <vector>

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

struct NativeSymbolInfo
{
	std::string name;
	unsigned long long address;
	unsigned size;
};
std::vector<NativeSymbolInfo> nativeSymbolInfos;

BOOL EnumSymbols(PSYMBOL_INFO symInfo, ULONG symbolSize, PVOID userContext)
{
	if(symInfo->Tag == SymTagFunction)
	{
		void* addr = (void*)symInfo->Address;
		//if(procInfo.addressToFunction.find(addr) != procInfo.addressToFunction.end())
		//{
		//	procInfo.addressToFunction[addr].name += symInfo->Name;
		//	return TRUE;
		//}

		nativeSymbolInfos.push_back({symInfo->Name, symInfo->Address, symInfo->Size});
	}
	return TRUE;
}

__declspec(dllexport) bool __cdecl LoadSymbols(DWORD id, LPCSTR filter, char*** names, ULONG64** addresses, ULONG** sizes, int* count)
{
	HANDLE processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, id);
	if(!processHandle)
		return false;

	SymSetOptions(SYMOPT_DEFERRED_LOADS);
	if(!SymInitialize(processHandle, 0, TRUE))
	{
		SymCleanup(processHandle);
		CloseHandle(processHandle);
		return false;
	}

	SymEnumSymbols(processHandle, 0, filter, (PSYM_ENUMERATESYMBOLS_CALLBACK)EnumSymbols, nullptr);

	*count = (int)nativeSymbolInfos.size();
	*names = (char**)CoTaskMemAlloc(sizeof(char*) * *count);
	*addresses = (ULONG64*)CoTaskMemAlloc(sizeof(ULONG64) * *count);
	*sizes = (ULONG*)CoTaskMemAlloc(sizeof(ULONG) * *count);
	for(int i = 0; i < *count; i++)
	{
		(*names)[i] = (char*)CoTaskMemAlloc(nativeSymbolInfos[i].name.size() + 1);
		strcpy_s((*names)[i], nativeSymbolInfos[i].name.size() + 1, nativeSymbolInfos[i].name.c_str());
		(*addresses)[i] = nativeSymbolInfos[i].address;
		(*sizes)[i] = nativeSymbolInfos[i].size;
	}

	SymCleanup(processHandle);
	CloseHandle(processHandle);

	return *count > 0;
}