// dllmain.cpp : Defines the entry point for the DLL application.
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Psapi.h>
#include <intrin.h>

#include "SharedMemoryInfo.h"


extern "C"
{
	HANDLE fileMappingHandle = nullptr;
	SharedMemoryInfo* sharedMemoryInfo = nullptr;
	__declspec(dllexport) void PerfInjectorTime();
}

volatile bool running = true;
HANDLE keyInputThreadHandle;

DWORD WINAPI KeyInput(_In_ LPVOID lpParameter)
{
	while(running)
	{
		if(sharedMemoryInfo->command == COMMAND_NONE && (GetKeyState(VK_F8) & 0x8000) == 0x8000)
			sharedMemoryInfo->command = COMMAND_TAKE_CAPTURE;
		Sleep(10);
	}
	return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		{
			fileMappingHandle = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, fileMapSize, TEXT("perfToolSharedMemory"));
			BOOL fInit = (GetLastError() != ERROR_ALREADY_EXISTS); 
			if(fileMappingHandle == NULL)
				return FALSE; 

			sharedMemoryInfo = (SharedMemoryInfo*)MapViewOfFile(fileMappingHandle, FILE_MAP_WRITE, 0, 0, 0);
			if(sharedMemoryInfo == NULL)
				return FALSE;

			sharedMemoryInfo->currentBufferWritePosition = 0;
			sharedMemoryInfo->currentBufferReadPosition = 0;
			sharedMemoryInfo->injectedProcessId = GetCurrentProcessId();

			keyInputThreadHandle = CreateThread(NULL, 0, KeyInput, NULL, 0, NULL);

			break;
		}
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
		{
			running = false;
			WaitForSingleObject(keyInputThreadHandle, INFINITE);
			BOOL fIgnore = UnmapViewOfFile(sharedMemoryInfo);
			fIgnore = CloseHandle(fileMappingHandle);
			break;
		}
	}
	return TRUE;
}

