#pragma once

struct Timestamp
{
	DWORD64 address;
	DWORD64 time;
	DWORD coreId;
	DWORD threadId;
};

#pragma warning(push)
#pragma warning(disable : 4200)
struct SharedMemoryInfo
{
	volatile DWORD injectedProcessId;
	volatile DWORD command;
	volatile DWORD64 commandParameter;
	volatile DWORD64 currentBufferWritePosition;
	volatile DWORD64 currentBufferReadPosition;
	Timestamp buffer[0];
};
#pragma warning(pop)

const DWORD fileMapBufferSize = 1024*1024;
const DWORD fileMapSize = sizeof(SharedMemoryInfo) + fileMapBufferSize * sizeof(Timestamp);

const DWORD COMMAND_NONE = 0;
const DWORD COMMAND_TAKE_CAPTURE = 1;