
EXTERN sharedMemoryInfo : qword
EXTERN GetThreadId : PROC

.CODE


PerfInjectorTime PROC FRAME
	pushfq
	push rax
.pushreg rax
	push rbx
.pushreg rbx
	push rcx
.pushreg rcx
	push rdx
.pushreg rdx
.endprolog
	mov rcx, qword ptr [rsp+28h]

	; Timestamp& timestamp = sharedMemoryInfo->buffer[sharedMemoryInfo->currentBufferWritePosition++ % bufferSize];
	mov rbx, [sharedMemoryInfo]
	mov rax, 1
	lock xadd qword ptr [rbx+16], rax
	push rax ; save for later
	and rax, 0fffffh
	imul rax, 18h
	add rbx, 32
	add rbx, rax

	; timestamp.address = calleeAddress;
	mov qword ptr [rbx], rcx

	; timestamp.time = __rdtsc();
	rdtscp
	shl rdx, 20h
	or  rax, rdx
	mov qword ptr [rbx+8], rax
	; timestamp.coreId = __rdtscp()
	mov dword ptr [rbx+16], ecx

	; timestamp.threadId = GetCurrentThreadId();
	mov rax, qword ptr gs:[30h]
	mov eax, dword ptr [rax+48h]
	mov dword ptr [rbx+20], eax

	; sharedMemoryInfo->currentBufferReadPosition = sharedMemoryInfo->currentBufferWritePosition;
	mfence
	mov rbx, [sharedMemoryInfo]
	pop rcx
	inc rcx
tryagain:
	mov rax, rcx
	dec rax
	lock cmpxchg [rbx+24], rcx
	jnz tryagain

	pop rdx
	pop rcx
	pop rbx
	pop rax
	popfq
	ret
PerfInjectorTime ENDP

END
