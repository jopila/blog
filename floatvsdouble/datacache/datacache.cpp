// datacache.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "..\common\Random.h"
#include "..\common\Timing.h"

const int NUM_ITERATIONS = 100000;

double Test(int intsPerData)
{
    // Grab some memory to twiddle
    int* datas = new int[NUM_ITERATIONS * intsPerData];
    memset(datas, 1, sizeof(int) * NUM_ITERATIONS * intsPerData);

    Timer timer;
    timer.Start();

    // Twiddle some bits!
    for(int i = 0; i < NUM_ITERATIONS; i++)
    {
        datas[i * intsPerData] *= 42;
    }

    // Print and clean up, *yawn*
    double time = timer.End();
    std::cout << intsPerData << " intsPerData Time: " << time * 1000000 << std::endl;
    delete[] datas;
    return time;
}

int main()
{
    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    Test(1);
    Test(2);
    Test(3);
    Test(4);
    Test(6);
    Test(8);
    Test(12);
    Test(16);
    Test(24);
    Test(32);

    // Wait for input...
    int dummy;
    std::cin >> dummy;

    return 0;
}

