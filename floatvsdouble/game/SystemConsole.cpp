
#include "stdafx.h"

#include "SystemConsole.h"
#include "SystemGraphics.h"
#include "..\utility\Stats.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\File.h"

SystemConsole* sys_console;

const ColorOffset BUTTON_COLOR = ColorOffset(0.0f, 0.1f, 0.2f, -0.3f);
const ColorOffset BUTTON_BORDER_COLOR = ColorOffset(0.0f, 0.0f, 0.0f, -0.3f);
const float BUTTON_MIN_WIDTH = 100.0f;
const float BUTTON_HEIGHT = 20.0f;
const float BUTTON_TEXT_SIZE = 16.0f;
const uint32 BUTTON_TEXT_COLOR = 0x00000000;
const uint32 BUTTON_TEXT_AUTO_COMPLETE_COLOR = 0x00808080;

SystemConsole::SystemConsole()
{
  renderTimer = new SmoothTimer("machines.render");
  guiSpriteRenderer = new SpriteRenderer(sys_graphics->gfx->GetDevice(), "..\\vulkan\\Sprites\\GUI");
  guiTextRenderer = new TextRenderer(sys_graphics->gfx->GetDevice(), "Inconsolata-Regular");
  sys_graphics->gfx->RegisterRenderer(guiSpriteRenderer);
  sys_graphics->gfx->RegisterRenderer(guiTextRenderer);
  
  isActive = false;
}

SystemConsole::~SystemConsole()
{
  delete renderTimer;
  delete guiSpriteRenderer;
  delete guiTextRenderer;
}

void SystemConsole::Update()
{
  const wnd::Input& input = sys_graphics->window->GetInput();

  if(input.IsKeyTriggered(wnd::Keyboard::Grave))
  {
    text = "";
    historyIndex = 0;
    isActive = !isActive;
  }

  if(!isActive)
    return;

  // basic text input
  std::vector<char> chars = input.GetCharInput();
  for(char c : chars)
  {
    if(c == '.' || c == ' ')
    {
      // auto-complete
      Command* command = FindCommand(text);
      std::vector<std::string> commandCategories = Split(text, '.');
      bool isFinishedCommand = false;
      bool foundMatch = false;
      for(std::pair<std::string, Command*> command : command->subCommands)
      {
        if(command.first.rfind(commandCategories.back()) == 0)
        {
          std::string autoCompleteText = &command.first[commandCategories.back().size()];
          text += autoCompleteText;
          isFinishedCommand = dynamic_cast<Variable*>(command.second);
          foundMatch = true;
          break;
        }
      }
      // allow space to correctly auto complete even if it should be a period
      if(c == ' ')
        text += (!foundMatch || isFinishedCommand) ? ' ' : '.';
      else
        text += '.';
    }
    else if(c != '`')
      text += c;
  }
  if(input.IsKeyTriggered(wnd::Keyboard::Backspace))
    text.pop_back();

  // history retrieval
  if(input.IsKeyTriggered(wnd::Keyboard::Up))
  {
    historyIndex++;
    if(history.size() >= historyIndex)
      text = history[history.size() - historyIndex];
    else
      historyIndex--;
  }
  if(input.IsKeyTriggered(wnd::Keyboard::Down))
  {
    historyIndex--;
    if(historyIndex < 0)
      historyIndex = 0;
    text = historyIndex > 0 ? history[history.size() - historyIndex] : "";
  }

  // submit command
  if(input.IsKeyTriggered(wnd::Keyboard::Enter) || input.IsKeyTriggered(wnd::Keyboard::NumPadEnter))
  {
    std::vector<std::string> split = Split(text, ' ');
    std::string commandStr = split[0];
    std::vector<std::string> commandCategories = Split(commandStr, '.');

    Command* command = FindCommand(text);

    if(command)
    {
      Variable* varCommand;
      if(varCommand = dynamic_cast<Variable*>(command))
      {
        if(varCommand->type == typeid(bool).name() && split.size() == 1)
          varCommand->write(varCommand->read() == "1" ? "0" : "1");
        else
          varCommand->write(split[1]);

        history.push_back(text);
      }
      text = "";
      isActive = false;
    }
  }
}

void SystemConsole::Render()
{
  if(!isActive)
    return;

  renderTimer->Start();

  RenderButton(0.0f, 0.0f, (float)sys_graphics->screenWidth, BUTTON_HEIGHT);
  float inputTextWidth = guiTextRenderer->GetWidth(text.c_str(), BUTTON_TEXT_SIZE);
  guiTextRenderer->RenderString(text.c_str(), BUTTON_TEXT_SIZE, 0.0f, 0.0f, BUTTON_TEXT_COLOR);

  Command* command = FindCommand(text);

  if(Variable* var = dynamic_cast<Variable*>(command))
  {
    std::string varText = var->type + ": " + var->read();
    float buttonWidth = Max(guiTextRenderer->GetWidth(varText.c_str() + 4, BUTTON_TEXT_SIZE), BUTTON_MIN_WIDTH);
    RenderButton(0.0f, 20.0f, buttonWidth, BUTTON_HEIGHT);
    guiTextRenderer->RenderString(varText.c_str(), BUTTON_TEXT_SIZE, 0.0f, BUTTON_HEIGHT, BUTTON_TEXT_COLOR);
  }
  else
  {
    // Find a good width for our buttons
    float buttonWidth = 0;
    for(std::pair<std::string, Command*> it : command->subCommands)
      buttonWidth = Max(buttonWidth, guiTextRenderer->GetWidth(it.first.c_str(), BUTTON_TEXT_SIZE));
    buttonWidth += 4;
    buttonWidth = Max(buttonWidth, BUTTON_MIN_WIDTH);

    // render out our buttons
    float buttonY = BUTTON_HEIGHT;
    for(std::pair<std::string, Command*> command : command->subCommands)
    {
      RenderButton(0.0f, buttonY, buttonWidth, BUTTON_HEIGHT);
      guiTextRenderer->RenderString(command.first.c_str(), BUTTON_TEXT_SIZE, 0.0f, buttonY, BUTTON_TEXT_COLOR);
      buttonY += BUTTON_HEIGHT;
    }

    // auto-complete
    if(text.size())
    {
      std::vector<std::string> commandCategories = Split(text, '.');
      for(std::pair<std::string, Command*> command : command->subCommands)
      {
        if(command.first.rfind(commandCategories.back()) == 0)
        {
          std::string autoCompleteText = &command.first[commandCategories.back().size()];
          guiTextRenderer->RenderString(autoCompleteText.c_str(), BUTTON_TEXT_SIZE, inputTextWidth, 0.0f, BUTTON_TEXT_AUTO_COMPLETE_COLOR);
          break;
        }
      }
    }
  }

  // primitive cursor
  SpriteRenderInfo cursorRenderInfo;
  cursorRenderInfo.x = inputTextWidth + 2.0f;
  cursorRenderInfo.y = BUTTON_HEIGHT - 2.0f;
  cursorRenderInfo.w = guiTextRenderer->GetWidth(" ", BUTTON_TEXT_SIZE);
  cursorRenderInfo.h = 2.0f;
  cursorRenderInfo.spriteIndex = guiSpriteRenderer->GetSpriteIndex("white");
  cursorRenderInfo.colorOffset = ColorOffset(0.0f, 0.0f, 0.0f, -0.2f);
  guiSpriteRenderer->RenderOne(cursorRenderInfo);

  renderTimer->Stop();
}

void SystemConsole::RenderButton(float x, float y, float w, float h)
{
  SpriteRenderInfo info;
  info.spriteIndex = guiSpriteRenderer->GetSpriteIndex("black");
  info.colorOffset = BUTTON_BORDER_COLOR;
  info.x = x;
  info.y = y;
  info.w = w;
  info.h = 1;
  guiSpriteRenderer->RenderOne(info);
  info.y = y + h - 1;
  guiSpriteRenderer->RenderOne(info);
  info.x = x;
  info.y = y + 1;
  info.w = 1;
  info.h = h - 2;
  guiSpriteRenderer->RenderOne(info);
  info.x = x + w - 1;
  guiSpriteRenderer->RenderOne(info);

  info.colorOffset = BUTTON_COLOR;
  info.x = x + 1;
  info.y = y + 1;
  info.w = w - 2;
  info.h = h - 2;
  guiSpriteRenderer->RenderOne(info);
}

SystemConsole::Command* SystemConsole::FindCommand(std::string name)
{
  Command* command = &root;

  std::vector<std::string> split = Split(name, ' ');
  if(split.size())
  {
    std::string commandStr = split[0];
    std::vector<std::string> commandCategories = Split(commandStr, '.');

    for(size_t i = 0; i < commandCategories.size(); i++)
    {
      auto commandIterator = command->subCommands.find(commandCategories[i]);
      if(commandIterator == command->subCommands.end())
        break;
      command = commandIterator->second;
    }
  }

  return command;
}

SystemConsole::Variable* SystemConsole::CreateVariableCommand(std::string name)
{
  std::vector<std::string> sections = Split(name, '.');

  Command* command = &root;
  std::string cumulativeName;
  for(size_t i = 0; i < sections.size(); i++)
  {
    auto it = command->subCommands.find(sections[i]);
    cumulativeName += (i == 0 ? "" : ".") + sections[i];

    if(i + 1 < sections.size()) // deeper
    {
      if(it == command->subCommands.end())
      {
        it->second = new Command();
        command->subCommands[sections[i]] = it->second;
      }

      Assert(!dynamic_cast<Variable*>(it->second), ("Couldn't add category due to conflicting variable\n"
        "  new='" + name + "'\n"
        "  existing='" + cumulativeName + "'\n").c_str() );

      command = it->second;
    }
    else
    {
      Assert(it == command->subCommands.end(), ("Couldn't add variable due to existing commands\n"
        "  new='" + name + "'\n"
        "  existing='" + cumulativeName + "'\n").c_str() );

      Variable* newCommand = new Variable();
      command->subCommands[sections[i]] = newCommand;
      return newCommand;
    }
  }
}
