#pragma once

class Terrain;

class SystemGame
{
public:
  SystemGame();
  ~SystemGame();

	float dt;
  Terrain* terrain;
};

extern SystemGame* sys_game;
