
#include "stdafx.h"

#include "SystemGame.h"
#include "Terrain.h"

SystemGame* sys_game;

SystemGame::SystemGame()
{
  terrain = new Terrain();
}

SystemGame::~SystemGame()
{
  delete terrain;
}
