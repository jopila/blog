// game.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "stdafx.h"

#include "..\Vulkan\Input.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\Point3D.h"
#include "..\Vulkan\Obj.h"
#include "..\Vulkan\Billboard.h"
#include "..\utility\Mat4x4.h"
#include "..\utility\Stats.h"
#include <thread>

#include "Terrain.h"
#include "SystemGraphics.h"
#include "SystemConsole.h"
#include "SystemGame.h"
#include "SystemMachines.h"
#include "SystemPlayer.h"

int main()
{
	printf("Hello World!\n");

	sys_stats = new SystemStats();
	sys_graphics = new SystemGraphics();
  sys_console = new SystemConsole();
	sys_game = new SystemGame();
  sys_machines = new SystemMachines();
	sys_player = new SystemPlayer();
  sys_game->terrain->Update(sys_player->pos);


	// MAIN LOOP!
	bool requestExit = false;
	SmoothTimer* frameTimer = new SmoothTimer("frame");
	float prevFrameTime = 0.0f;
	bool freezeIntersection = false;
	Vec4 intersectionStart;
	Vec4 intersectionVector;
	while(!sys_graphics->requestingExit)
	{
		frameTimer->Start();

		sys_graphics->BeginFrame();

    if(!sys_console->isActive)
    {
      sys_player->Update();
      sys_game->terrain->Update(sys_player->pos); // TODO: Maybe this should be sys_game->Update/Render();?
      sys_machines->Update();
    }
    sys_machines->Render();
    sys_game->terrain->Render(sys_player->pos);
    sys_console->Update();
    sys_console->Render();

		if(!freezeIntersection)
		{
			intersectionStart = sys_player->pos;
			intersectionVector = sys_player->forward;
		}

		// RAY CAST
		Terrain::IntersectionData intersection = sys_game->terrain->FindIntersect(intersectionStart, intersectionVector, freezeIntersection ? sys_graphics->pointRenderer : nullptr);

    //sys_graphics->pointRenderer->RenderOne({
		//	intersection.x - intersection.left * 1.0f + intersection.right * 1.0f,
		//	intersection.y - intersection.bottom * 1.0f + intersection.top * 1.0f,
		//	intersection.z - intersection.back * 1.0f + intersection.front * 1.0f,
		//	1.0, 1.0, 0.0});
    //sys_graphics->pointRenderer->RenderOne({intersection.x, intersection.y, intersection.z, 1.0, 0.0, 1.0});
		if(freezeIntersection)
      sys_graphics->pointRenderer->RenderOne({intersectionStart.x, intersectionStart.y, intersectionStart.z, 0.0, 1.0, 0.0});

		// INPUT
		wnd::Input input = sys_graphics->window->GetInput();

		if(input.IsKeyTriggered(wnd::Keyboard::J))
      freezeIntersection = !freezeIntersection;

    sys_graphics->Update();

		char buf[60];
		sprintf_s(buf, "pos: (%0.2f, %0.2f, %0.2f)", sys_player->pos.x, sys_player->pos.y, sys_player->pos.z);
		sys_graphics->guiTextRenderer->RenderString(buf, 14, 0.0f, 0.0f, 0xffffffff);
    sprintf_s(buf, "Cube Pos: (%d, %d, %d)", intersection.wx, intersection.wy, intersection.wz);
    sys_graphics->guiTextRenderer->RenderString(buf, 14, 0.0f, 16.0f, 0xffffffff);
		sprintf_s(buf, "Frame Time!: %.3fms", prevFrameTime * 1000.0f);
    sys_graphics->guiTextRenderer->RenderString(buf, 14.0f, 0.0f, 32.0f, 0xffffffff);
		sprintf_s(buf, "T-Update: %.3fms", sys_stats->timers["terrain.update"]->GetTime() * 1000.0f);
    sys_graphics->guiTextRenderer->RenderString(buf, 14.0f, 0.0f, 48.0f, 0xffffffff);
		sprintf_s(buf, "T-Render: %.3fms", sys_stats->timers["terrain.render"]->GetTime() * 1000.0f);
    sys_graphics->guiTextRenderer->RenderString(buf, 14.0f, 0.0f, 64.0f, 0xffffffff);

    sys_graphics->EndFrame();

		frameTimer->Stop();
		prevFrameTime = frameTimer->GetTime();
		sys_game->dt = prevFrameTime;
	}



	delete frameTimer;

	delete sys_player;
  delete sys_machines;
	delete sys_game;
  delete sys_console;
  delete sys_graphics;
	delete sys_stats;
}
