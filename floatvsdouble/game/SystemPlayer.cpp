
#include "stdafx.h"

#include "SystemPlayer.h"

#include "SystemGraphics.h"
#include "SystemConsole.h"
#include "SystemGame.h"
#include "SystemMachines.h"
#include "Terrain.h"
#include "..\Vulkan\Window.h"
#include "..\Vulkan\Input.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\Billboard.h"
#include "..\Vulkan\Obj.h"
#include "..\Vulkan\Point3D.h"
#include "..\Vulkan\Cube.h"

SystemPlayer* sys_player;



// handle connections
bool connectionOneSelected;
ConnectionInfo connectionOne;
uint32 connectionOneX;
uint32 connectionOneY;
uint32 connectionOneZ;

SystemPlayer::SystemPlayer()
{
  pos = Vec4(130.0f, 20.0f, 0.0f, 1.0f);
  forward = Vec4(1.0f, 0.0f, 0.0f, 0.0f);
  up = Vec4(0.0f, 1.0f, 0.0f, 0.0f);
  yaw = 0.0f;
  pitch = 0.0f;
  height = 8.0f;
  fly = false;
  sys_console->AddVariable("player.fly", fly);
  sys_console->AddVariable("player.height", height);

  currentBuilding = 0;
  currentRotation = 0;
  connectionOneSelected = false;
}

void SystemPlayer::Update()
{
  // Movement
  forward.x = cosf(yaw) * cosf(pitch);
  forward.y = sinf(pitch);
  forward.z = sinf(yaw) * cosf(pitch);
  forward.w = 0.0f;

  wnd::Input input = sys_graphics->window->GetInput();

  if(input.IsKeyTriggered(wnd::Keyboard::Space)) height = height == 8.0f ? 4.0f : 8.0f;
  if(input.IsKeyTriggered(wnd::Keyboard::F)) fly = !fly;

  Vec4 right = Cross(forward, up);
  right.Normalize();
  float speed = 14.0f * sys_game->dt;
  if(input.IsKeyPressed(wnd::Keyboard::LShift)) speed *= 5.0f;
  if(input.IsKeyPressed(wnd::Keyboard::A)) pos -= right * speed;
  if(input.IsKeyPressed(wnd::Keyboard::D)) pos += right * speed;
  if(input.IsKeyPressed(wnd::Keyboard::W)) pos += forward * speed;
  if(input.IsKeyPressed(wnd::Keyboard::S)) pos -= forward * speed;
  if(input.IsKeyPressed(wnd::Keyboard::E)) pos.y += speed;
  if(input.IsKeyPressed(wnd::Keyboard::Q)) pos.y -= speed;

  pos.x = Clamp(pos.x, 0.0f, 4e9f);
  pos.y = Clamp(pos.y, 0.0f, 4e9f);
  pos.z = Clamp(pos.z, 0.0f, 4e9f);

  if(!fly)
    pos.y = sys_game->terrain->FindGroundLevel(pos.x, pos.y, pos.z) + height;

  yaw += input.GetMouseOffsetX() * 0.005f;
  pitch += input.GetMouseOffsetY() * -0.005f;
  pitch = Clamp(pitch, -PI * 0.45f, PI * 0.45f); // look at matrix collapses at views directly up or down


  // Building Selection
  currentBuilding += input.GetMouseScrollY();
  while(currentBuilding >= (signed)sys_machines->availableMachines.size())
    currentBuilding -= (int32)sys_machines->availableMachines.size();
  while(currentBuilding < 0)
    currentBuilding += (int32)sys_machines->availableMachines.size();
  if(input.IsKeyTriggered(wnd::Keyboard::R))
    currentRotation++;
  if(currentRotation >= 4)
    currentRotation = 0;

  std::string buildingName = sys_machines->availableMachines[currentBuilding].name;
  sys_graphics->guiTextRenderer->RenderString(buildingName.c_str(), 18.0f, 8.0f, 580.0f, 0x00000000);



  // Find the closest intersection between the terrain and machines
  Terrain::IntersectionData terrainIntersection = sys_game->terrain->FindIntersect(pos, forward, nullptr);
  Terrain::IntersectionData machineIntersection = sys_machines->FindIntersect(pos, forward);
  Vec4 tip = {terrainIntersection.x, terrainIntersection.y, terrainIntersection.z, 0.0f};
  Vec4 mip = {machineIntersection.x, machineIntersection.y, machineIntersection.z, 0.0f};
  Terrain::IntersectionData intersection = terrainIntersection;
  bool pointingAtMachine = machineIntersection.IsValid() && Distance(tip, pos) > Distance(mip, pos);
  if(pointingAtMachine)
    intersection = machineIntersection;

  // get cube position in front of the intersection
  uint32 wx = intersection.wx + (intersection.orientation == Orientation::right ? +1 : 0) + (intersection.orientation == Orientation::left   ? -1 : 0);
  uint32 wy = intersection.wy + (intersection.orientation == Orientation::top   ? +1 : 0) + (intersection.orientation == Orientation::bottom ? -1 : 0);
  uint32 wz = intersection.wz + (intersection.orientation == Orientation::front ? +1 : 0) + (intersection.orientation == Orientation::back   ? -1 : 0);

  // draw input/output helpers
  ConnectionInfo connectionInfo;
  bool pointingAtConnection = sys_machines->GetConnectionInfo(intersection.wx, intersection.wy, intersection.wz, connectionInfo);
  pointingAtConnection = pointingAtConnection && connectionInfo.orientation == intersection.orientation;
  if(pointingAtConnection && (!connectionOneSelected || connectionInfo.direction != connectionOne.direction))
  {
    uint32 cx = intersection.wx + (connectionInfo.orientation == Orientation::right ? +1 : 0) + (connectionInfo.orientation == Orientation::left   ? -1 : 0);
    uint32 cy = intersection.wy + (connectionInfo.orientation == Orientation::top   ? +1 : 0) + (connectionInfo.orientation == Orientation::bottom ? -1 : 0);
    uint32 cz = intersection.wz + (connectionInfo.orientation == Orientation::front ? +1 : 0) + (connectionInfo.orientation == Orientation::back   ? -1 : 0);

    int8 rotation;
    switch(connectionInfo.orientation)
    {
    case Orientation::right: rotation = 1; break;
    case Orientation::left:  rotation = 3; break;
    case Orientation::front: rotation = 0; break;
    case Orientation::back:  rotation = 2; break;
    }
    if(connectionInfo.direction == ConnectionDirection::out)
      rotation = (rotation + 2) % 4;

    BillboardRenderInfo info;
    sys_game->terrain->MakeBillboardRenderData(cx, cy-1, cz, Orientation::top, rotation, 0, info);
    sys_graphics->billboardRenderer->RenderOne(info);

    if(input.IsMouseButtonTriggered(wnd::Mouse::Left))
    {
      if(connectionOneSelected)
      {
        connectionOneSelected = false;
        sys_machines->AddBelt(connectionOneX, connectionOneY, connectionOneZ, connectionOne,
          intersection.wx, intersection.wy, intersection.wz, connectionInfo);
        //sys_machines->AddMachine(currentBuilding, currentRotation, connectionOneX, connectionOneY, connectionOneZ);
        //sys_machines->AddMachine(currentBuilding, currentRotation, cx, cy, cz);
      }
      else
      {
        connectionOneSelected = true;
        connectionOne = connectionInfo;
        connectionOneX = intersection.wx;
        connectionOneY = intersection.wy;
        connectionOneZ = intersection.wz;
      }
    }
  }

  if(connectionOneSelected)
  {
    Point3DRenderInfo info;
    info.x = (float)(connectionOneX + (connectionOne.orientation == Orientation::right ? +1 : 0) + (connectionOne.orientation == Orientation::left   ? -1 : 0));
    info.y = (float)(connectionOneY + (connectionOne.orientation == Orientation::top   ? +1 : 0) + (connectionOne.orientation == Orientation::bottom ? -1 : 0));
    info.z = (float)(connectionOneZ + (connectionOne.orientation == Orientation::front ? +1 : 0) + (connectionOne.orientation == Orientation::back   ? -1 : 0));
    info.r = 0.0f; info.g = 0.4f; info.b = 1.0f;
    sys_graphics->pointRenderer->RenderOne(info);
  }

  bool validLocation = true;
  if(currentBuilding != 0)
  {
    if(intersection.orientation != Orientation::top)
      validLocation = false;
    if(validLocation && !sys_machines->HasSpace(currentBuilding, currentRotation, intersection.wx, intersection.wy + 1, intersection.wz))
      validLocation = false;
  }

  SystemMachines::Data currentMachineData = sys_machines->availableMachines[currentBuilding];

  // Pointer
  BillboardRenderInfo info;
  sys_game->terrain->MakeBillboardRenderData(intersection.wx, intersection.wy, intersection.wz, intersection.orientation, 0, 2, info);
  sys_graphics->billboardRenderer->RenderOne(info);

  static bool renderMachine = true;
  if(input.IsKeyTriggered(wnd::Keyboard::M))
    renderMachine = !renderMachine;

  if(currentBuilding != 0 && validLocation)
  {
    if(renderMachine)
    {
      ObjRenderInfo info;
      sys_machines->MakeMachineRenderInfo(MachineInfo(wx, wy, wz, currentBuilding, currentRotation), info);
      sys_graphics->objRenderer->RenderOne(info);
    }
    else
    {
      std::vector<SystemMachines::Pos> positions;
      sys_machines->GetNeededPositions(currentBuilding, currentRotation, wx, wy, wz, positions);
      for(SystemMachines::Pos pos : positions)
      {
        Point3DRenderInfo info;
        info.x = (float)pos.x;
        info.y = (float)pos.y;
        info.z = (float)pos.z;
        info.r = info.g = info.b = 0.7f;
        sys_graphics->pointRenderer->RenderOne(info);
      }
    }

    if(input.IsMouseButtonTriggered(wnd::Mouse::Left))
      sys_machines->AddMachine(currentBuilding, currentRotation, wx, wy, wz);
  }
}
