
#pragma once

class Point3DRenderer;
class CubeRenderer;
struct CubeRenderInfo;
struct BillboardRenderInfo;
class PerlinNoise;
class SmoothTimer;

#include "..\utility\Vec4.h"

enum class Orientation : uint8 { right, left, top, bottom, front, back, none };

Orientation RotateOrientation(Orientation orientation, int32 rotation);
int32 OrientationToRotation(Orientation orientation);
void OffsetFromOrientation(Orientation orientation, int32& dx, int32& dy, int32& dz);

class Terrain
{
public:
	Terrain();
	~Terrain();
	void Update(Vec4 pos);
	void Render(Vec4 pos);
  int32 GetCube(uint32 x, uint32 y, uint32 z);
	float FindGroundLevel(float x, float y, float z);
	bool CanPlayerFit(float x, float y, float z);

  void MakeBillboardRenderData(uint32 wx, uint32 wy, uint32 wz, Orientation orientation, int8 rotation, int32 billboardIndex, BillboardRenderInfo& info);

	struct IntersectionData
	{
		float x, y, z;
		uint32 wx, wy, wz;
    Orientation orientation;
    static int32 Distance(IntersectionData& a, IntersectionData& b);
    bool IsValid();
	};

	IntersectionData FindIntersect(const Vec4 p, const Vec4 v, Point3DRenderer* pointRenderer);

private:
	PerlinNoise* perlin;
	SmoothTimer* renderTimer;
	SmoothTimer* updateTimer;

	void InitializeCubes(uint32 chunkX, uint32 chunkY, uint32 chunkZ);
	void InitializeRenderData(uint32 chunkX, uint32 chunkY, uint32 chunkZ);
	void GetCubeAndChunkIndex(float x, uint32& index, uint32& chunk);

	static const uint32 chunkDimX = 16;
	static const uint32 chunkDimY = 16;
	static const uint32 chunkDimZ = 16;
	int32 visibleRadiusX;
	int32 visibleRadiusY;
	int32 visibleRadiusZ;

	struct Position
	{
		uint32 x, y, z;
		bool operator<(const Position& rhs) const
		{
			return x < rhs.x || (x == rhs.x && y < rhs.y) || (x == rhs.x && y == rhs.y && z < rhs.z);
		}
	};

	struct ChunkInfo
	{
		ChunkInfo() : renderData(nullptr), renderCount(0) {}
		int32 cubes[chunkDimX][chunkDimY][chunkDimZ];
		CubeRenderInfo* renderData;
		int32 renderCount;
	};
	std::map<Position, ChunkInfo> chunks;
};