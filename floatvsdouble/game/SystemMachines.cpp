#include "stdafx.h"

#include <set>

#include "SystemMachines.h"
#include "SystemGraphics.h"
#include "SystemGame.h"
#include "..\Vulkan\Obj.h"
#include "..\Vulkan\Point3D.h"
#include "..\utility\Stats.h"

SystemMachines* sys_machines;

SystemMachines::SystemMachines()
{
  head = new OctreeNode();
  head->size = 32;

  renderTimer = new SmoothTimer("machines.render");
  availableMachines.push_back({"Nothing", "assert", 1, 1, 1});
  //availableMachines.push_back({"Converter", "machine7", 3, 3, 2});
  availableMachines.push_back({"Belt", "belt1", 1, 1, 1});
  availableMachines.push_back({"BeltUp", "beltUp", 1, 1, 2});
  availableMachines.push_back({"BeltDown", "beltDown", 1, 1, 2});
  availableMachines.push_back({"Power", "wires1", 1, 1, 1});
  availableMachines.push_back({"box1x2", "machine_1x2", 1, 2, 1});
    availableMachines.back().AddConnection(0, 0, 0, ConnectionDirection::in, Orientation::back, ConnectionType::belt);
    availableMachines.back().AddConnection(0, 0, 1, ConnectionDirection::out, Orientation::front, ConnectionType::belt);
  //availableMachines.push_back({"box2x2", "machine_2x2", 2, 2, 1});
}

SystemMachines::~SystemMachines()
{
  delete renderTimer;
  DeleteRecursive(head);
}

void SystemMachines::DeleteRecursive(OctreeNode* node)
{
  if(node)
  {
    DeleteRecursive(node->rtf);
    DeleteRecursive(node->rtk);
    DeleteRecursive(node->rbf);
    DeleteRecursive(node->rbk);
    DeleteRecursive(node->ltf);
    DeleteRecursive(node->ltk);
    DeleteRecursive(node->lbf);
    DeleteRecursive(node->lbk);
    delete node;
  }
}

void SystemMachines::Update()
{
}

void SystemMachines::Render()
{
	renderTimer->Start();

  RenderRecursive(head);

	renderTimer->Stop();
}

void SystemMachines::RenderRecursive(OctreeNode* node)
{
  if(node)
  {
    if(node->machine.index > 0)
    {
      ObjRenderInfo info;
      MakeMachineRenderInfo(node->machine, info);
      sys_graphics->objRenderer->RenderOne(info);
    }/*
    else if(node->machineIndex < 0)
    {
      Point3DRenderInfo data;
      data.x = node->minx;
      data.y = node->miny;
      data.z = node->minz;
      data.r = data.g = data.b = 0.5f;
      sys_graphics->pointRenderer->RenderOne(data);
    }*/
    else
    {
      RenderRecursive(node->rtf);
      RenderRecursive(node->rtk);
      RenderRecursive(node->rbf);
      RenderRecursive(node->rbk);
      RenderRecursive(node->ltf);
      RenderRecursive(node->ltk);
      RenderRecursive(node->lbf);
      RenderRecursive(node->lbk);
    }
  }
}


bool SystemMachines::GetNeededPositions(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz, std::vector<Pos>& spaces)
{
  uint32 sx = wx;
  uint32 sy = wy;
  uint32 sz = wz;
  uint32 ex, ez;
  const SystemMachines::Data& machineData = availableMachines[machineIndex];
  uint32 ey = sy + machineData.height - 1;

  if(rotation == 0) { ex = sx + machineData.width - 1; ez = sz + machineData.depth - 1; }
  if(rotation == 1) { ex = sx - machineData.depth + 1; ez = sz - machineData.width + 1; }
  if(rotation == 2) { ex = sx - machineData.width + 1; ez = sz - machineData.depth + 1; }
  if(rotation == 3) { ex = sx + machineData.depth - 1; ez = sz + machineData.width - 1; }

  if(sx > ex) Swap(sx, ex);
  if(sz > ez) Swap(sz, ez);

  if(ex - sx > 1000) return false;
  if(ez - sz > 1000) return false;

  for(uint32 x = sx; x <= ex; x++)
  for(uint32 y = sy; y <= ey; y++)
  for(uint32 z = sz; z <= ez; z++)
    spaces.push_back({x, y, z});

  return true;
}

bool SystemMachines::HasSpace(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz)
{
  std::vector<Pos> positions;
  if(!GetNeededPositions(machineIndex, rotation, wx, wy, wz, positions))
    return false;

  MachineInfo info;
  for(Pos pos : positions)
    if(GetMachineInfo(pos.x, pos.y, pos.z, info) || sys_game->terrain->GetCube(pos.x, pos.y, pos.z))
      return false;

  return true;
}

void SystemMachines::AddMachine(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz)
{
  std::vector<Pos> positions;
  GetNeededPositions(machineIndex, rotation, wx, wy, wz, positions);

  MachineInfo proxy(wx, wy, wz, machineIndex, (int8)rotation);
  for(Pos pos : positions)
    FindRecursive(&head, pos.x, pos.y, pos.z, true)->machine = proxy;

  MachineInfo root(wx, wy, wz, machineIndex, (int8)rotation);
  FindRecursive(&head, wx, wy, wz, true)->machine = root;
}

void SystemMachines::AddBelt(uint32 x1, uint32 y1, uint32 z1, ConnectionInfo c1, uint32 x2, uint32 y2, uint32 z2, ConnectionInfo c2)
{
  const float COST_FLAT_BELT = 1.0f;
  const float COST_RAMP_BELT = 2.0001f;
  const float COST_TURN = 0.0001f;
  const float COST_FLOATING = 0.001f;

  if(c1.direction == c2.direction)
    __debugbreak();
  if(c1.direction == ConnectionDirection::in)
  {
    Swap(x1, x2);
    Swap(y1, y2);
    Swap(z1, z2);
    Swap(c1, c2);
  }

  struct NodeInfo
  {
    uint32 wx, wy, wz;
    float g, h;
    int32 machineIndex;
    Orientation orientation;
    std::list<NodeInfo>::iterator parent;
  };
  struct OffsetInfo
  {
    int32 dx, dy, dz;
    int32 machineIndex;
    Orientation orientation;
    float cost;
  };

  auto CalculateHeuristic = [COST_FLOATING](uint32 x1, uint32 y1, uint32 z1, uint32 x2, uint32 y2, uint32 z2) -> float
  {
    float floatingModifier = (!sys_game->terrain->GetCube(x1, y1-1, z1)) * COST_FLOATING;
    return (float)(AbsoluteValue((int64)x1 - (int64)x2) + AbsoluteValue((int64)y1 - (int64)y2) + AbsoluteValue((int64)z1 - (int64)z2)) + floatingModifier;
  };

  std::list<NodeInfo> closedList;
  std::multimap<float, NodeInfo> openList;
  auto nodeInfoIteratorComparer = [](const NodeInfo& a, const NodeInfo& b)
  {
    return a.wx < b.wx || (a.wx == b.wx && a.wy < b.wy) || (a.wx == b.wx && a.wy == b.wy && a.wz < b.wz) ||
      (a.wx == b.wx && a.wy == b.wy && a.wz == b.wz && a.machineIndex < b.machineIndex) ||
      (a.wx == b.wx && a.wy == b.wy && a.wz == b.wz && a.machineIndex == b.machineIndex && a.orientation < b.orientation);
  };
  std::map<NodeInfo, std::multimap<float, NodeInfo>::iterator, decltype(nodeInfoIteratorComparer)> cubeMap(nodeInfoIteratorComparer);
  float g = 1.0f;
  float h = CalculateHeuristic(x1, y1, z1, x2, y2, z2);
  auto newIt = openList.insert(std::pair<float, NodeInfo>(g + h, {x1, y1, z1, g, h, 1, c1.orientation, closedList.end()}));
  cubeMap[newIt->second] = newIt;

  int whatchdog = 10000;
  while(whatchdog-- && !openList.empty())
  {
    std::multimap<float, NodeInfo>::iterator it = openList.begin();
    NodeInfo currentNode = it->second;
    cubeMap.erase(currentNode);
    openList.erase(it);
    closedList.push_back(currentNode);

    std::vector<OffsetInfo> offsets;
    int32 dx, dy, dz;
    OffsetFromOrientation(currentNode.orientation, dx, dy, dz);
    offsets.push_back({dx, dy, dz, 1, RotateOrientation(currentNode.orientation, 0),  COST_FLAT_BELT});
    offsets.push_back({dx, dy, dz, 1, RotateOrientation(currentNode.orientation, -1), COST_FLAT_BELT + COST_TURN});
    offsets.push_back({dx, dy, dz, 1, RotateOrientation(currentNode.orientation, +1), COST_FLAT_BELT + COST_TURN});
    offsets.push_back({dx,  1, dz, 2, currentNode.orientation, COST_RAMP_BELT});
    offsets.push_back({dx, -1, dz, 3, currentNode.orientation, COST_RAMP_BELT});
    
    for(int i = 0; i < offsets.size(); i++)
    {
      NodeInfo newNode;
      newNode.wx = currentNode.wx + offsets[i].dx;
      newNode.wy = currentNode.wy + offsets[i].dy;
      newNode.wz = currentNode.wz + offsets[i].dz;
      newNode.parent = --closedList.end();

      // if we've reached destination, stop
      if(newNode.wx == x2 && newNode.wy == y2 && newNode.wz == z2 && newNode.machineIndex == 1)
      {
        int32 dx, dy, dz;
        OffsetFromOrientation(c2.orientation, dx, dy, dz);
        if(newNode.parent->wx == x2 + dx && newNode.parent->wy == y2 + dy && newNode.parent->wz == z2 + dz)
          goto end;
      }

      newNode.machineIndex = offsets[i].machineIndex;
      newNode.orientation = offsets[i].orientation;
      newNode.g = currentNode.g + offsets[i].cost;
      newNode.h = CalculateHeuristic(newNode.wx, newNode.wy, newNode.wz, x2, y2, z2);
      float f = newNode.g + newNode.h;

      if(AbsoluteValue((int64)newNode.wx - (int64)currentNode.wx) > 1000) continue;
      if(AbsoluteValue((int64)newNode.wy - (int64)currentNode.wy) > 1000) continue;
      if(AbsoluteValue((int64)newNode.wz - (int64)currentNode.wz) > 1000) continue;
      if(!HasSpace(offsets[i].machineIndex, OrientationToRotation(offsets[i].orientation), newNode.wx, newNode.wy - (offsets[i].machineIndex == 2 ? 1 : 0), newNode.wz))
        continue;

      auto existingNode = cubeMap.find(newNode);
      if(existingNode == cubeMap.end() || existingNode->second->second.g + existingNode->second->second.h > f)
      {
        if(existingNode != cubeMap.end())
        {
          openList.erase(existingNode->second);
          cubeMap.erase(existingNode);
        }
        auto newIt = openList.insert(std::pair<float, NodeInfo>(f, newNode));
        cubeMap[newNode] = newIt;
      }
    }
  }

end:
  std::list<NodeInfo>::iterator currentNode = --closedList.end();
  while(currentNode->parent != closedList.end())
  {
    AddMachine(currentNode->machineIndex, OrientationToRotation(currentNode->orientation), currentNode->wx, currentNode->wy - (currentNode->machineIndex == 2 ? 1 : 0), currentNode->wz);
    currentNode = currentNode->parent;
  }

  return;
}

SystemMachines::OctreeNode** SystemMachines::CalculateChild(OctreeNode* node, uint32 wx, uint32 wy, uint32 wz)
{
  uint32 halfsize = 1 << (node->size - 1);
  bool l = wx < node->minx + halfsize;
  bool b = wy < node->miny + halfsize;
  bool k = wz < node->minz + halfsize;
  int32 index = l << 2 | b << 1 | k << 0;
  return ((OctreeNode**)((uint8*)&node->rtf + childNodeOffsets[index]));
}

bool SystemMachines::GetMachineInfo(uint32 wx, uint32 wy, uint32 wz, MachineInfo& info)
{
  OctreeNode* node = FindRecursive(&head, wx, wy, wz);
  if(node)
  {
    info = node->machine;
    return true;
  }
  else
    return false;
}

bool SystemMachines::GetConnectionInfo(uint32 wx, uint32 wy, uint32 wz, ConnectionInfo& info)
{
  MachineInfo machineInfo;
  if(!GetMachineInfo(wx, wy, wz, machineInfo))
    return false;

  for(Data::Connection connection : availableMachines[machineInfo.index].connections)
  {
    int dx;
    int dz;
    switch(machineInfo.rotation)
    {
    case 0: dx = +connection.ox; dz = +connection.oz; break;
    case 1: dx = -connection.oz; dz = -connection.ox; break;
    case 2: dx = -connection.ox; dz = -connection.oz; break;
    case 3: dx = +connection.oz; dz = +connection.ox; break;
    }

    if(machineInfo.wx + dx == wx && machineInfo.wy + connection.oy == wy && machineInfo.wz + dz == wz)
    {
      info = connection.info;
      info.orientation = RotateOrientation(info.orientation, machineInfo.rotation);
      return true;
    }
  }
  return false;
}

void SystemMachines::MakeMachineRenderInfo(MachineInfo machine, ObjRenderInfo& info)
{
  const SystemMachines::Data& machineData = availableMachines[machine.index];
  info.obj = machineData.model;
  info.x = (float)machine.wx;
  info.y = (float)machine.wy - 0.5f;
  info.z = (float)machine.wz;
  float angle = 0.0f;
  if(machine.rotation == 1) angle = - 90 * PI / 180.0f;
  if(machine.rotation == 2) angle = -180 * PI / 180.0f;
  if(machine.rotation == 3) angle = -270 * PI / 180.0f;
  info.q = Quat(Vec4(0.0f, 1.0f, 0.0f, 0.0f), angle);
}

SystemMachines::OctreeNode* SystemMachines::FindRecursive(OctreeNode** node, uint32 wx, uint32 wy, uint32 wz, bool createNew)
{
  OctreeNode* snode = *node;

  if(snode->minx == wx && snode->miny == wy && snode->minz == wz && snode->size == 0)
    return snode;

  OctreeNode** child = CalculateChild(snode, wx, wy, wz);

  if(*child)
    return FindRecursive(child, wx, wy, wz, createNew);
  else if(createNew)
  {
    OctreeNode* nnode = new OctreeNode();

    uint32 halfsize = 1 << (snode->size - 1);
    bool l = wx < snode->minx + halfsize;
    bool b = wy < snode->miny + halfsize;
    bool k = wz < snode->minz + halfsize;

    nnode->minx = snode->minx + (l ? 0 : halfsize);
    nnode->miny = snode->miny + (b ? 0 : halfsize);
    nnode->minz = snode->minz + (k ? 0 : halfsize);
    nnode->size = snode->size - 1;
    *child = nnode;
    return FindRecursive(child, wx, wy, wz, createNew);
  }
  else
    return nullptr;
}

// defined in Terrain.cpp
void tToXYZ(Vec4 p, Vec4 v, float t, float& outX, float& outY, float& outZ);

Terrain::IntersectionData SystemMachines::FindIntersect(const Vec4 p, const Vec4 v)
{
  uint32 wx = uint32(p.x + 0.5f);
  uint32 wy = uint32(p.y + 0.5f);
  uint32 wz = uint32(p.z + 0.5f);

  float nx = wx + (v.x < 0 ? -0.5f : 0.5f);
  float ny = wy + (v.y < 0 ? -0.5f : 0.5f);
  float nz = wz + (v.z < 0 ? -0.5f : 0.5f);
  float x, y, z;

  MachineInfo info;

  while(true)
  {
    float tx = v.x == 0.0f ? 1e30f : (nx - p.x) / v.x;
    float ty = v.y == 0.0f ? 1e30f : (ny - p.y) / v.y;
    float tz = v.z == 0.0f ? 1e30f : (nz - p.z) / v.z;

    if(tx <= ty && tx <= tz)
    {
      wx += v.x > 0 ? 1 : -1;
      nx += v.x > 0 ? 1.0f : -1.0f;
      if(GetMachineInfo(wx, wy, wz, info)) { tToXYZ(p, v, tx, x, y, z); return {x, y, z, wx, wy, wz, v.x < 0 ? Orientation::right : Orientation::left}; }
    }
    else if(ty <= tx && ty <= tz)
    {
      wy += v.y > 0 ? 1 : -1;
      ny += v.y > 0 ? 1.0f : -1.0f;
      if(GetMachineInfo(wx, wy, wz, info)) { tToXYZ(p, v, ty, x, y, z); return {x, y, z, wx, wy, wz, v.y < 0 ? Orientation::top : Orientation::bottom}; }
    }
    else if(tz <= tx && tz <= ty)
    {
      wz += v.z > 0 ? 1 : -1;
      nz += v.z > 0 ? 1.0f : -1.0f;
      if(GetMachineInfo(wx, wy, wz, info)) { tToXYZ(p, v, tz, x, y, z); return {x, y, z, wx, wy, wz, v.z < 0 ? Orientation::front : Orientation::back}; }
    }

    // Check if we went out bounds
    if(fabs(nx - wx) > 2.0f || fabs(ny - wy) > 2.0f || fabs(nz - wz) > 2.0f)
      return {0, 0, 0, 0, 0, 0, Orientation::none};

    // Went too far
    Vec4 a = {nx, ny, nz, 0.0f};
    if(Distance(a, p) > 50.0f)
      return {0, 0, 0, 0, 0, 0, Orientation::none};
  }
}
