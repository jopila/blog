
#include "stdafx.h"

#include "SystemGraphics.h"

#include "..\utility\Mat4x4.h"
#include "..\Vulkan\Gfx.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"
#include "..\Vulkan\Cube.h"
#include "..\Vulkan\Billboard.h"
#include "..\Vulkan\SkyBox.h"
#include "..\Vulkan\Point3D.h"
#include "..\Vulkan\Obj.h"
#include "..\Vulkan\Window.h"
#include "..\gui\CoreControls.h"
#include "SystemPlayer.h"

SystemGraphics* sys_graphics;

SystemGraphics::SystemGraphics()
{
  // Create our window
  screenWidth = 1024;
  screenHeight = 768;

  window = wnd::WindowHandle::Create(screenWidth, screenHeight, "Game");
  gfx = new gfx::Gfx(window);
  gui::Clip::InitScreenSize((float)screenWidth, (float)screenHeight);

  // Make sure our UI adjusts when the window changes
  gfx->OnScreenResize = [=](int32 width, int32 height)
  {
	  screenWidth = width;
	  screenHeight = height;
	  gui::Clip::InitScreenSize((float)screenWidth, (float)screenHeight);
  };

  window->SetCursorDisabled(true);

  skyBoxRenderer = new SkyBoxRenderer(gfx->GetDevice(), "assets\\CubeMaps");
  pointRenderer = new Point3DRenderer(gfx->GetDevice());
  objRenderer = new ObjRenderer(gfx->GetDevice(), "cooked\\meshes");
  cubeRenderer = new CubeRenderer(gfx->GetDevice(), "cooked\\Textures");
  billboardRenderer = new BillboardRenderer(gfx->GetDevice(), "..\\vulkan\\Sprites\\Billboards");
  guiSpriteRenderer = new SpriteRenderer(gfx->GetDevice(), "..\\vulkan\\Sprites\\GUI");
  guiTextRenderer = new TextRenderer(gfx->GetDevice(), "Inconsolata-Regular");
  gfx->RegisterRenderer(skyBoxRenderer);
  gfx->RegisterRenderer(pointRenderer);
  gfx->RegisterRenderer(objRenderer);
  gfx->RegisterRenderer(cubeRenderer);
  gfx->RegisterRenderer(billboardRenderer);
  gfx->RegisterRenderer(guiSpriteRenderer);
  gfx->RegisterRenderer(guiTextRenderer);
}

SystemGraphics::~SystemGraphics()
{
  delete guiTextRenderer;
  delete guiSpriteRenderer;
  delete cubeRenderer;
  delete objRenderer;
  delete billboardRenderer;
  delete pointRenderer;
  delete skyBoxRenderer;
  delete gfx;
  wnd::WindowHandle::Destroy(window);
}

void SystemGraphics::BeginFrame()
{
  gfx->BeginFrame();
  window->PollEvents();

}

void SystemGraphics::Update()
{
  Mat4x4 view = Mat4x4::MakeLookAt(sys_player->pos, sys_player->pos + sys_player->forward);
  Mat4x4 proj = Mat4x4::MakePerspective(90.0f, (float)screenWidth/screenHeight, 0.01f, 1000.0f);
  Mat4x4 viewProj = proj * view;
  skyBoxRenderer->UpdateViewProj(view, proj);
  pointRenderer->UpdateViewProj(viewProj);
  objRenderer->UpdateViewProj(viewProj);
  cubeRenderer->UpdateViewProj(viewProj);
  cubeRenderer->UpdateCameraPosition(sys_player->pos);
  cubeRenderer->UpdatePointLight(sys_player->pos + Vec4(0.0, 5.0f, 0.0f, 0.0f), Vec4(23.47f, 21.31f, 20.79f, 0.0f) * 4.0f);//Vec4(23.47f, 21.31f, 20.79f, 0.0f) * 0.2f);
  cubeRenderer->UpdateDirectionalLight(Vec4(1.0, -1.0, 1.0, 0.0), Vec4(1.0f, 1.0f, 1.0f, 0.0f) * 0.0);
  billboardRenderer->UpdateViewProj(viewProj);
}

void SystemGraphics::EndFrame()
{
  gfx->EndFrame();
  requestingExit = window->ShouldCloseWindow() || window->GetInput().IsKeyTriggered(wnd::Keyboard::Escape);
}
