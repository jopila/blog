#pragma once

#include "Terrain.h"

class SmoothTimer;
struct ObjRenderInfo;

struct MachineInfo
{
  MachineInfo(uint32 wx, uint32 wy, uint32 wz, int32 index, int8 rotation) : wx(wx), wy(wy), wz(wz), index(index), rotation(rotation) {}
  MachineInfo() {}

  uint32 wx, wy, wz;
  int32 index;
  int8 rotation;
};

enum class ConnectionType : uint8 { belt, power };
enum class ConnectionDirection : uint8 { in, out };

struct ConnectionInfo
{
  ConnectionType type;
  ConnectionDirection direction;
  Orientation orientation;
};

class SystemMachines
{
public:
  SystemMachines();
  ~SystemMachines();
	void Update();
  void Render();

  struct Pos {uint32 x, y, z;};
  bool GetNeededPositions(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz, std::vector<Pos>& spaces);
  bool HasSpace(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz);
  void AddMachine(int32 machineIndex, int32 rotation, uint32 wx, uint32 wy, uint32 wz);
  void AddBelt(uint32 x1, uint32 y1, uint32 z1, ConnectionInfo o1, uint32 x2, uint32 y2, uint32 z2, ConnectionInfo o2);

  bool GetMachineInfo(uint32 wx, uint32 wy, uint32 wz, MachineInfo& info);
  bool GetConnectionInfo(uint32 wx, uint32 wy, uint32 wz, ConnectionInfo& info);
  void MakeMachineRenderInfo(MachineInfo machine, ObjRenderInfo& info);

  Terrain::IntersectionData FindIntersect(const Vec4 p, const Vec4 v);

  struct Data
  {
    Data(std::string name, std::string model, uint8 width, uint8 depth, uint8 height) : name(name), model(model), width(width), depth(depth), height(height) {}
    void AddConnection(int8 offsetX, int8 offsetY, int8 offsetZ, ConnectionDirection direction, Orientation orientation, ConnectionType type)
      { connections.push_back({offsetX, offsetY, offsetZ, {type, direction, orientation}}); }
    std::string name;
    std::string model;
    uint8 width;
    uint8 depth;
    uint8 height;
    struct Connection
    {
      int8 ox;
      int8 oy;
      int8 oz;
      ConnectionInfo info;
    };
    std::vector<Connection> connections;
  };
  std::vector<Data> availableMachines;

private:

  SmoothTimer* renderTimer;

  struct OctreeNode
  {
    OctreeNode() {memset(this, 0, sizeof(*this));}
    OctreeNode* rtf; //+x, +y, +z
    OctreeNode* rtk;
    OctreeNode* rbf;
    OctreeNode* rbk;
    OctreeNode* ltf;
    OctreeNode* ltk;
    OctreeNode* lbf;
    OctreeNode* lbk; //-x, -y, -z
    uint32 minx, miny, minz, size;
    MachineInfo machine;
  };

  const size_t childNodeOffsets[8] = 
  {
    offsetof(OctreeNode, rtf),
    offsetof(OctreeNode, rtk),
    offsetof(OctreeNode, rbf),
    offsetof(OctreeNode, rbk),
    offsetof(OctreeNode, ltf),
    offsetof(OctreeNode, ltk),
    offsetof(OctreeNode, lbf),
    offsetof(OctreeNode, lbk),
  };

  OctreeNode* head;
  OctreeNode** CalculateChild(OctreeNode* node, uint32 wx, uint32 wy, uint32 wz);
  void RenderRecursive(OctreeNode* node);
  void DeleteRecursive(OctreeNode* node);
  OctreeNode* FindRecursive(OctreeNode** node, uint32 wx, uint32 wy, uint32 wz, bool createNew = false);
};

extern SystemMachines* sys_machines;
