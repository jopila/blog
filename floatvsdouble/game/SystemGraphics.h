
#pragma once

namespace wnd { class WindowHandle; }
namespace gfx { class Gfx; }
class SkyBoxRenderer;
class Point3DRenderer;
class ObjRenderer;
class CubeRenderer;
class BillboardRenderer;
class SpriteRenderer;
class TextRenderer;


class SystemGraphics
{
public:
  SystemGraphics();
  ~SystemGraphics();

  void BeginFrame();
  void Update();
  void EndFrame();

  // core
  int32 screenWidth;
  int32 screenHeight;
  wnd::WindowHandle* window;
  gfx::Gfx* gfx;
  bool requestingExit;

  // renderers
  SkyBoxRenderer* skyBoxRenderer;
  Point3DRenderer* pointRenderer;
  ObjRenderer* objRenderer;
  CubeRenderer* cubeRenderer;
  BillboardRenderer* billboardRenderer;
  SpriteRenderer* guiSpriteRenderer;
  TextRenderer* guiTextRenderer;
};

extern SystemGraphics* sys_graphics;
