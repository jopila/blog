#pragma once

#include "..\utility\Vec4.h"

class SystemPlayer
{
public:
  SystemPlayer();
  void Update();

  Vec4 pos;
  Vec4 forward;
  Vec4 up;
  float yaw;
  float pitch;
  float height;
  bool fly;

  enum class BuildingType { nothing, belt, power, smallFactory, COUNT };
  int32 currentBuilding;
  int8 currentRotation;
};

extern SystemPlayer* sys_player;
