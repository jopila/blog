
#include "stdafx.h"

#include "Terrain.h"
#include "..\Vulkan\Cube.h"
#include "..\Vulkan\CubeAO.h"
#include "..\Vulkan\Point3D.h"
#include "..\Vulkan\Billboard.h"
#include "..\utility\Perlin.h"
#include "..\utility\Stats.h"
#include "SystemGraphics.h"

//cx = chunk x
//ix = internal x (index within a chunk)
//wx = world x    (ix + ChunkDimX * cx)
//ox = offset x

int16 ti_sideDirt;
int16 ti_sideDirtNormal;
int16 ti_sideDirtORM;

int16 ti_rock;
int16 ti_rockNormal;
int16 ti_rockORM;

Terrain::Terrain()
{
	perlin = new PerlinNoise(0);
	renderTimer = new SmoothTimer("terrain.render");
	updateTimer = new SmoothTimer("terrain.update");

	visibleRadiusX = 3;
	visibleRadiusY = 2;
	visibleRadiusZ = 3;

	ti_sideDirt = sys_graphics->cubeRenderer->GetTextureIndex("leaves.diffuse");
	ti_sideDirtNormal = sys_graphics->cubeRenderer->GetTextureIndex("leaves.normal");
	ti_sideDirtORM = sys_graphics->cubeRenderer->GetTextureIndex("leaves.orm");

	ti_rock = sys_graphics->cubeRenderer->GetTextureIndex("rocks.diffuse");
	ti_rockNormal = sys_graphics->cubeRenderer->GetTextureIndex("rocks.normal");
	ti_rockORM = sys_graphics->cubeRenderer->GetTextureIndex("rocks.orm");
}

Terrain::~Terrain()
{
	for(auto it = chunks.begin(); it != chunks.end(); it++)
		delete[] it->second.renderData;
	delete updateTimer;
	delete renderTimer;
	delete perlin;
}

double SmoothRegion(double var, double center, double radius)
{
	return 1.0 - AbsoluteValue(Clamp((var - center) / radius, -1.0, 1.0));
}

void Terrain::Update(Vec4 pos)
{
	updateTimer->Start();
	uint32 ix = (uint32)(pos.x/chunkDimX);
	uint32 iy = (uint32)(pos.y/chunkDimY);
	uint32 iz = (uint32)(pos.z/chunkDimZ);
	std::map<Position, bool> reRenderChunks;

	for(uint32 cx = ix - visibleRadiusX; cx != ix + visibleRadiusX + 1; cx++)
	for(uint32 cy = iy - visibleRadiusY; cy != iy + visibleRadiusY + 1; cy++)
	for(uint32 cz = iz - visibleRadiusZ; cz != iz + visibleRadiusZ + 1; cz++)
	{
		auto chunk = chunks.find({cx, cy, cz});
		if(chunk == chunks.end())
		{
			InitializeCubes(cx, cy, cz);
      for(int ox = -1; ox <= 1; ox++)
      for(int oy = -1; oy <= 1; oy++)
      for(int oz = -1; oz <= 1; oz++)
        reRenderChunks[{cx+ox, cy+oy, cz+oz}] = true;
		}
	}

	for(auto it = reRenderChunks.begin(); it != reRenderChunks.end(); it++)
	{
		uint32 cx = it->first.x;
		uint32 cy = it->first.y;
		uint32 cz = it->first.z;
    if(chunks.find({cx, cy, cz}) != chunks.end())
  		InitializeRenderData(cx, cy, cz);
	}
	updateTimer->Stop();
}

void Terrain::InitializeCubes(uint32 chunkX, uint32 chunkY, uint32 chunkZ)
{
	int32(&cubes)[chunkDimX][chunkDimY][chunkDimZ] = chunks[{chunkX, chunkY, chunkZ}].cubes;
	for(uint32 u = 0; u < chunkDimX; u++)
	for(uint32 v = 0; v < chunkDimY; v++)
	for(uint32 w = 0; w < chunkDimZ; w++)
	{
		double nx = (chunkX*chunkDimX + u);
		double ny = (chunkY*chunkDimY + v);
		double nz = (chunkZ*chunkDimZ + w);

		double noise = 1.0;
		double ground = SmoothRegion(ny, 0, 300);
		//noise *= ground * perlin->Noise(nx * 0.1, ny * 0.01, nz * 0.1);
		//noise += ground;
		if(ny > -10.0)
		{
			double mountSubNoise = 0.1 * perlin->Noise(nx * 0.4, ny * 0.2, nz * 0.4);
			double mountainNoise = perlin->Noise(nx * 0.04 + mountSubNoise, ny * 0.02 + mountSubNoise, nz * 0.04 + mountSubNoise);
			double flatVsMountainNoise = Max(0.0, perlin->Noise(nx * 0.003, ny * 0.003, nz * 0.003) - 0.2) * (1.0/0.8);
			flatVsMountainNoise *= flatVsMountainNoise * flatVsMountainNoise; // ~0.3 is mountain, 0 is flat
			//if(ny > 0.0)
				noise *= 3*mountainNoise * SmoothRegion(ny, 0.0, (flatVsMountainNoise * 300));
			//else
			//{
				//noise *= mountainNoise + SmoothRegion(ny, -10.0 - 10 * flatVsMountainNoise, 10);
				//noise += SmoothRegion(noise, 0.5, 0.4) * flatVsMountainNoise * perlin->Noise(nx * .1, ny * .1, nz * .1);
			//}
			//noise += Clamp(SmoothRegion(noise, 0.5, 0.3) * perlin->Noise(nx*4.0, ny*4.0, nz*4.0), 0.0, 1.0);
		}
		if(ny > -10.0 && ny < 0.0)
		{
			double mountainGroundDepth = 10 * perlin->Noise(nx * 0.2, 0, nz * 0.2);
			double flatGroundNoiseDepth = 2 * perlin->Noise(nx * 0.01, 0, nz * 0.01);
			double flatVsMountainNoise = perlin->Noise(nx * 0.005, ny * 0.005, nz * 0.005);
			flatVsMountainNoise *= flatVsMountainNoise * flatVsMountainNoise;
			double depth = Lerp(flatGroundNoiseDepth, mountainGroundDepth, flatVsMountainNoise);
			//noise += SmoothRegion(ny, -5, 5);
			//if(ny + depth > 0)
			//  noise = 0.0;
		}
		if(ny > -10.0 && ny < 10.0)
		{
			//double groundNoise = perlin->Noise(nx * 0.1, ny * 0.1, nz * 0.1);
			//noise *= SmoothRegion(ny, 0, 100) * groundNoise;
		}

		//if(ny <= 0.0)
		//	noise = 0.0;
		//else if(ny > -30.0)
		//	noise *= 10 * (1.0 - AbsoluteValue(Clamp((ny + 15.0) / 15.0, -1.0, 1.0)));

		cubes[u][v][w] = noise > 0.5 ? 1 : 0;
	}
}

CubeRenderInfo CreateCubeRenderData(uint32 x, uint32 y, uint32 z, uint16 neighborFlags, uint32 aoFlags1, uint32 aoFlags2)
{
	CubeRenderInfo cubeRenderInfo;
	cubeRenderInfo.x = (float)x;
	cubeRenderInfo.y = (float)y;
	cubeRenderInfo.z = (float)z;
	cubeRenderInfo.textureSet = 1;
	cubeRenderInfo.neighborFlags = neighborFlags;
  cubeRenderInfo.aoFlags1 = aoFlags1;
  cubeRenderInfo.aoFlags2 = aoFlags2;
	return cubeRenderInfo;
}

void Terrain::InitializeRenderData(uint32 chunkX, uint32 chunkY, uint32 chunkZ)
{
	ChunkInfo& chunk = chunks[{chunkX, chunkY, chunkZ}];
	if(chunk.renderData)
		delete[] chunk.renderData;

	int32(&cubes)[chunkDimX][chunkDimY][chunkDimZ] = chunk.cubes;
	std::map<Position, ChunkInfo>::iterator it;
	int32(*rightNeighborCubes) [chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX+1, chunkY, chunkZ}); if(it != chunks.end()) rightNeighborCubes  = &it->second.cubes;
	int32(*leftNeighborCubes)  [chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX-1, chunkY, chunkZ}); if(it != chunks.end()) leftNeighborCubes   = &it->second.cubes;
	int32(*topNeighborCubes)   [chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX, chunkY+1, chunkZ}); if(it != chunks.end()) topNeighborCubes    = &it->second.cubes;
	int32(*bottomNeighborCubes)[chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX, chunkY-1, chunkZ}); if(it != chunks.end()) bottomNeighborCubes = &it->second.cubes;
	int32(*frontNeighborCubes) [chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX, chunkY, chunkZ+1}); if(it != chunks.end()) frontNeighborCubes  = &it->second.cubes;
	int32(*backNeighborCubes)  [chunkDimX][chunkDimY][chunkDimZ] = nullptr; it = chunks.find({chunkX, chunkY, chunkZ-1}); if(it != chunks.end()) backNeighborCubes   = &it->second.cubes;


	std::vector<CubeRenderInfo> newCubes;
	uint32 offsetX = chunkX * chunkDimX;
	uint32 offsetY = chunkY * chunkDimY;
	uint32 offsetZ = chunkZ * chunkDimZ;

	for(uint32 x = 0; x < chunkDimX; x++)
	for(uint32 y = 0; y < chunkDimY; y++)
	for(uint32 z = 0; z < chunkDimZ; z++)
	{
		if(!cubes[x][y][z])
			continue;

		bool r = x+1 < chunkDimX ? cubes[x+1][y][z] : (rightNeighborCubes  ? (*rightNeighborCubes) [0][y][z]           : 0);
		bool l = x   != 0        ? cubes[x-1][y][z] : (leftNeighborCubes   ? (*leftNeighborCubes)  [chunkDimX-1][y][z] : 0);
		bool t = y+1 < chunkDimY ? cubes[x][y+1][z] : (topNeighborCubes    ? (*topNeighborCubes)   [x][0][z]           : 0);
		bool b = y   != 0        ? cubes[x][y-1][z] : (bottomNeighborCubes ? (*bottomNeighborCubes)[x][chunkDimY-1][z] : 0);
		bool f = z+1 < chunkDimZ ? cubes[x][y][z+1] : (frontNeighborCubes  ? (*frontNeighborCubes) [x][y][0]           : 0);
		bool k = z   != 0        ? cubes[x][y][z-1] : (backNeighborCubes   ? (*backNeighborCubes)  [x][y][chunkDimZ-1] : 0);

		uint16 neighborFlags = !r | !l << 1 | !t << 2 | !b << 3 | !f << 4 | !k << 5;

    if(!r || !l || !t || !b || !f || !k)
    {
      int gx = x + chunkX * chunkDimX;
      int gy = y + chunkY * chunkDimY;
      int gz = z + chunkZ * chunkDimZ;

      uint8 topAO = 0;
      if(!t && y+1 < chunkDimY && x+1 < chunkDimX && x != 0 && z+1 < chunkDimZ && z != 0) topAO =
        ((!!cubes[x+1][y+1][z+1]) << 7) |
        ((!!cubes[x+0][y+1][z+1]) << 6) |
        ((!!cubes[x-1][y+1][z+1]) << 5) |
        ((!!cubes[x-1][y+1][z+0]) << 4) |
        ((!!cubes[x-1][y+1][z-1]) << 3) |
        ((!!cubes[x+0][y+1][z-1]) << 2) |
        ((!!cubes[x+1][y+1][z-1]) << 1) |
        ((!!cubes[x+1][y+1][z+0]) << 0);
      else if(!t) topAO =
        ((!!GetCube(gx + 1, gy + 1, gz + 1)) << 7) |
        ((!!GetCube(gx + 0, gy + 1, gz + 1)) << 6) |
        ((!!GetCube(gx - 1, gy + 1, gz + 1)) << 5) |
        ((!!GetCube(gx - 1, gy + 1, gz + 0)) << 4) |
        ((!!GetCube(gx - 1, gy + 1, gz - 1)) << 3) |
        ((!!GetCube(gx + 0, gy + 1, gz - 1)) << 2) |
        ((!!GetCube(gx + 1, gy + 1, gz - 1)) << 1) |
        ((!!GetCube(gx + 1, gy + 1, gz + 0)) << 0);
      uint8 rightAO = 0;
      if(!r && x+1 < chunkDimX && y+1 < chunkDimY && y != 0 && z+1 < chunkDimZ && z != 0) rightAO =
        ((!!cubes[x+1][y-1][z-1]) << 7) |
        ((!!cubes[x+1][y-1][z+0]) << 6) |
        ((!!cubes[x+1][y-1][z+1]) << 5) |
        ((!!cubes[x+1][y+0][z+1]) << 4) |
        ((!!cubes[x+1][y+1][z+1]) << 3) |
        ((!!cubes[x+1][y+1][z+0]) << 2) |
        ((!!cubes[x+1][y+1][z-1]) << 1) |
        ((!!cubes[x+1][y+0][z-1]) << 0);
      else if(!r) rightAO =
        ((!!GetCube(gx + 1, gy - 1, gz - 1)) << 7) |
        ((!!GetCube(gx + 1, gy - 1, gz + 0)) << 6) |
        ((!!GetCube(gx + 1, gy - 1, gz + 1)) << 5) |
        ((!!GetCube(gx + 1, gy + 0, gz + 1)) << 4) |
        ((!!GetCube(gx + 1, gy + 1, gz + 1)) << 3) |
        ((!!GetCube(gx + 1, gy + 1, gz + 0)) << 2) |
        ((!!GetCube(gx + 1, gy + 1, gz - 1)) << 1) |
        ((!!GetCube(gx + 1, gy + 0, gz - 1)) << 0);
      uint8 leftAO = 0;
      if(!l && x != 0 && y+1 < chunkDimY && y != 0 && z+1 < chunkDimZ && z != 0) leftAO =
        ((!!cubes[x-1][y-1][z+1]) << 7) |
        ((!!cubes[x-1][y-1][z+0]) << 6) |
        ((!!cubes[x-1][y-1][z-1]) << 5) |
        ((!!cubes[x-1][y+0][z-1]) << 4) |
        ((!!cubes[x-1][y+1][z-1]) << 3) |
        ((!!cubes[x-1][y+1][z+0]) << 2) |
        ((!!cubes[x-1][y+1][z+1]) << 1) |
        ((!!cubes[x-1][y+0][z+1]) << 0);
      else if(!l) leftAO =
        ((!!GetCube(gx - 1, gy - 1, gz + 1)) << 7) |
        ((!!GetCube(gx - 1, gy - 1, gz + 0)) << 6) |
        ((!!GetCube(gx - 1, gy - 1, gz - 1)) << 5) |
        ((!!GetCube(gx - 1, gy + 0, gz - 1)) << 4) |
        ((!!GetCube(gx - 1, gy + 1, gz - 1)) << 3) |
        ((!!GetCube(gx - 1, gy + 1, gz + 0)) << 2) |
        ((!!GetCube(gx - 1, gy + 1, gz + 1)) << 1) |
        ((!!GetCube(gx - 1, gy + 0, gz + 1)) << 0);
      int aoFlags1 = (AdjacencyToTextureIndex(topAO) << 12) + (AdjacencyToTextureIndex(leftAO) << 6) + AdjacencyToTextureIndex(rightAO);

      uint8 bottomAO = 0;
      if(!b && y != 0 && x+1 < chunkDimX && x != 0 && z+1 < chunkDimZ && z != 0) bottomAO =
        ((!!cubes[x+1][y-1][z-1]) << 7) |
        ((!!cubes[x+0][y-1][z-1]) << 6) |
        ((!!cubes[x-1][y-1][z-1]) << 5) |
        ((!!cubes[x-1][y-1][z+0]) << 4) |
        ((!!cubes[x-1][y-1][z+1]) << 3) |
        ((!!cubes[x+0][y-1][z+1]) << 2) |
        ((!!cubes[x+1][y-1][z+1]) << 1) |
        ((!!cubes[x+1][y-1][z+0]) << 0);
      else if(!b) bottomAO =
        ((!!GetCube(gx + 1, gy - 1, gz - 1)) << 7) |
        ((!!GetCube(gx + 0, gy - 1, gz - 1)) << 6) |
        ((!!GetCube(gx - 1, gy - 1, gz - 1)) << 5) |
        ((!!GetCube(gx - 1, gy - 1, gz + 0)) << 4) |
        ((!!GetCube(gx - 1, gy - 1, gz + 1)) << 3) |
        ((!!GetCube(gx + 0, gy - 1, gz + 1)) << 2) |
        ((!!GetCube(gx + 1, gy - 1, gz + 1)) << 1) |
        ((!!GetCube(gx + 1, gy - 1, gz + 0)) << 0);
      uint8 frontAO = 0;
      if(!f && z+1 < chunkDimZ && x+1 < chunkDimX && x != 0 && y+1 < chunkDimY && y != 0) frontAO =
        ((!!cubes[x+1][y-1][z+1]) << 7) |
        ((!!cubes[x+0][y-1][z+1]) << 6) |
        ((!!cubes[x-1][y-1][z+1]) << 5) |
        ((!!cubes[x-1][y+0][z+1]) << 4) |
        ((!!cubes[x-1][y+1][z+1]) << 3) |
        ((!!cubes[x+0][y+1][z+1]) << 2) |
        ((!!cubes[x+1][y+1][z+1]) << 1) |
        ((!!cubes[x+1][y+0][z+1]) << 0);
      else if(!f) frontAO =
        ((!!GetCube(gx + 1, gy - 1, gz + 1)) << 7) |
        ((!!GetCube(gx + 0, gy - 1, gz + 1)) << 6) |
        ((!!GetCube(gx - 1, gy - 1, gz + 1)) << 5) |
        ((!!GetCube(gx - 1, gy + 0, gz + 1)) << 4) |
        ((!!GetCube(gx - 1, gy + 1, gz + 1)) << 3) |
        ((!!GetCube(gx + 0, gy + 1, gz + 1)) << 2) |
        ((!!GetCube(gx + 1, gy + 1, gz + 1)) << 1) |
        ((!!GetCube(gx + 1, gy + 0, gz + 1)) << 0);
      uint8 backAO = 0;
      if(!k && z != 0 && x+1 < chunkDimX && x != 0 && y+1 < chunkDimY && y != 0) backAO =
        ((!!cubes[x-1][y-1][z-1]) << 7) |
        ((!!cubes[x+0][y-1][z-1]) << 6) |
        ((!!cubes[x+1][y-1][z-1]) << 5) |
        ((!!cubes[x+1][y+0][z-1]) << 4) |
        ((!!cubes[x+1][y+1][z-1]) << 3) |
        ((!!cubes[x+0][y+1][z-1]) << 2) |
        ((!!cubes[x-1][y+1][z-1]) << 1) |
        ((!!cubes[x-1][y+0][z-1]) << 0);
      else if(!k) backAO =
        ((!!GetCube(gx - 1, gy - 1, gz - 1)) << 7) |
        ((!!GetCube(gx + 0, gy - 1, gz - 1)) << 6) |
        ((!!GetCube(gx + 1, gy - 1, gz - 1)) << 5) |
        ((!!GetCube(gx + 1, gy + 0, gz - 1)) << 4) |
        ((!!GetCube(gx + 1, gy + 1, gz - 1)) << 3) |
        ((!!GetCube(gx + 0, gy + 1, gz - 1)) << 2) |
        ((!!GetCube(gx - 1, gy + 1, gz - 1)) << 1) |
        ((!!GetCube(gx - 1, gy + 0, gz - 1)) << 0);
      int aoFlags2 = (AdjacencyToTextureIndex(backAO) << 12) + (AdjacencyToTextureIndex(frontAO) << 6) + (AdjacencyToTextureIndex(bottomAO));

      newCubes.push_back(CreateCubeRenderData(chunkX * chunkDimX + x, chunkY * chunkDimY + y, chunkZ * chunkDimZ + z, neighborFlags, aoFlags1, aoFlags2));
    }
	}
	CubeRenderInfo*& data = chunk.renderData;
	chunk.renderCount = (int32)newCubes.size();
	data = new CubeRenderInfo[newCubes.size()];
	memcpy(data, newCubes.data(), newCubes.size() * sizeof(CubeRenderInfo));
}

void Terrain::GetCubeAndChunkIndex(float x, uint32& index, uint32& chunk)
{
	x += 0.5f;
	index = (uint32)x;
	chunk = (uint32)x / 16;
	index -= 16 * chunk;
}

int32 Terrain::GetCube(uint32 x, uint32 y, uint32 z)
{
	uint32 cx = x / chunkDimX;
	uint32 cy = y / chunkDimY;
	uint32 cz = z / chunkDimZ;

  auto chunk = chunks.find({cx, cy, cz});
	if(chunk != chunks.end())
	{
		return chunk->second.cubes[x - cx*chunkDimX][y - cy*chunkDimY][z - cz*chunkDimZ];
	}
	return -1;
}

void Terrain::Render(Vec4 pos)
{
	renderTimer->Start();
	uint32 ix = (uint32)(pos.x/chunkDimX);
	uint32 iy = (uint32)(pos.y/chunkDimY);
	uint32 iz = (uint32)(pos.z/chunkDimZ);
	for(uint32 chunkX = ix - visibleRadiusX; chunkX != ix + visibleRadiusX + 1; chunkX++)
	for(uint32 chunkY = iy - visibleRadiusY; chunkY != iy + visibleRadiusY + 1; chunkY++)
	for(uint32 chunkZ = iz - visibleRadiusZ; chunkZ != iz + visibleRadiusZ + 1; chunkZ++)
	{
		ChunkInfo& chunk = chunks[{chunkX, chunkY, chunkZ}];
		CubeRenderInfo* data = chunk.renderData;
		int32 count = chunk.renderCount;
		for(int32 i = 0; i < count; i++)
      sys_graphics->cubeRenderer->RenderOne(data[i]);
	}
	renderTimer->Stop();
}

float Terrain::FindGroundLevel(float x, float y, float z)
{
	uint32 ix, cx; GetCubeAndChunkIndex(x, ix, cx);
	uint32 iy, cy; GetCubeAndChunkIndex(y, iy, cy);
	uint32 iz, cz; GetCubeAndChunkIndex(z, iz, cz);

	for(int32 i = 0; i <= 30; i++)
	{
		int32(&cubes)[chunkDimX][chunkDimY][chunkDimZ] = chunks[{cx, cy, cz}].cubes;
		if(cubes[ix][iy][iz])
			return (float)iy + chunkDimY * cy;
		iy--;
		if(iy == ~0)
		{
			iy = 15;
			cy--;
		}
	}
	return y;
}

bool Terrain::CanPlayerFit(float x, float y, float z)
{
	return false;
}

void Terrain::MakeBillboardRenderData(uint32 wx, uint32 wy, uint32 wz, Orientation orientation, int8 rotation, int32 billboardIndex, BillboardRenderInfo& info)
{
  info.x = (float)wx + (orientation == Orientation::left  ? -0.51f : 0.0f) + (orientation == Orientation::right  ? +0.51f : 0.0f);
  info.y = (float)wy + (orientation == Orientation::top   ? +0.51f : 0.0f) + (orientation == Orientation::bottom ? -0.51f : 0.0f);
  info.z = (float)wz + (orientation == Orientation::front ? +0.51f : 0.0f) + (orientation == Orientation::back   ? -0.51f : 0.0f);
  info.w = 1.0f;
  info.h = 1.0f;
  if(orientation == Orientation::left)  rotation = (rotation - 1) & 3;
  if(orientation == Orientation::right) rotation = (rotation + 1) & 3;
  Quat rotationQuat = Quat(Vec4(0.0f, 1.0f, 0.0f, 0.0f), rotation * 90.0f * PI / 180.0f);
  Quat orientationQuat;

  switch(orientation)
  {
  case Orientation::top:    orientationQuat = Quat(Vec4(1.0f, 0.0f, 0.0f, 0.0f), 0.0f);                 break;
  case Orientation::bottom: orientationQuat = Quat(Vec4(1.0f, 0.0f, 0.0f, 0.0f), 180.0f * PI / 180.0f); break;
  case Orientation::left:   orientationQuat = Quat(Vec4(0.0f, 0.0f, 1.0f, 0.0f),  90.0f * PI / 180.0f); break;
  case Orientation::right:  orientationQuat = Quat(Vec4(0.0f, 0.0f, 1.0f, 0.0f), -90.0f * PI / 180.0f); break;
  case Orientation::front:  orientationQuat = Quat(Vec4(1.0f, 0.0f, 0.0f, 0.0f),  90.0f * PI / 180.0f); break;
  case Orientation::back:   orientationQuat = Quat(Vec4(1.0f, 0.0f, 0.0f, 0.0f),-270.0f * PI / 180.0f); break;
  }


  info.q = orientationQuat * rotationQuat;
  info.colorOffset = ColorOffset::Neutral;
  info.BillboardIndex = billboardIndex;
}

void tToXYZ(Vec4 p, Vec4 v, float t, float& outX, float& outY, float& outZ)
{
	outX = p.x + v.x * t;
	outY = p.y + v.y * t;
	outZ = p.z + v.z * t;
}

Terrain::IntersectionData Terrain::FindIntersect(const Vec4 p, const Vec4 v, Point3DRenderer* pointRenderer)
{
	uint32 ix, cx; GetCubeAndChunkIndex(p.x, ix, cx); uint32 wx = ix + chunkDimX * cx;
	uint32 iy, cy; GetCubeAndChunkIndex(p.y, iy, cy); uint32 wy = iy + chunkDimX * cy;
	uint32 iz, cz; GetCubeAndChunkIndex(p.z, iz, cz); uint32 wz = iz + chunkDimX * cz;

	float nx = wx + (v.x < 0 ? -0.5f : 0.5f);
	float ny = wy + (v.y < 0 ? -0.5f : 0.5f);
	float nz = wz + (v.z < 0 ? -0.5f : 0.5f);
	float x, y, z;

	while(true)
	{
		float tx = v.x == 0.0f ? 1e30f : (nx - p.x) / v.x;
		float ty = v.y == 0.0f ? 1e30f : (ny - p.y) / v.y;
		float tz = v.z == 0.0f ? 1e30f : (nz - p.z) / v.z;

		if(pointRenderer)
			pointRenderer->RenderOne({(float)wx, (float)wy, (float)wz, 0.0, 1.0, 1.0});

		if(tx <= ty && tx <= tz)
		{
			wx += v.x > 0 ? 1 : -1;
			nx += v.x > 0 ? 1.0f : -1.0f;
			int32 cube = GetCube(wx, wy, wz);
			if(cube < 0) return {0, 0, 0, 0, 0, 0, Orientation::none};
			if(cube > 0) {tToXYZ(p, v, tx, x, y, z); return {x, y, z, wx, wy, wz, v.x < 0 ? Orientation::right : Orientation::left};}
		}
		else if(ty <= tx && ty <= tz)
		{
			wy += v.y > 0 ? 1 : -1;
			ny += v.y > 0 ? 1.0f : -1.0f;
			int32 cube = GetCube(wx, wy, wz);
			if(cube < 0) return {0, 0, 0, 0, 0, 0, Orientation::none};
			if(cube > 0) {tToXYZ(p, v, ty, x, y, z); return {x, y, z, wx, wy, wz, v.y < 0 ? Orientation::top : Orientation::bottom};}
		}
		else if(tz <= tx && tz <= ty)
		{
			wz += v.z > 0 ? 1 : -1;
			nz += v.z > 0 ? 1.0f : -1.0f;
			int32 cube = GetCube(wx, wy, wz);
			if(cube < 0) return {0, 0, 0, 0, 0, 0, Orientation::none};
			if(cube > 0) {tToXYZ(p, v, tz, x, y, z); return {x, y, z, wx, wy, wz, v.z < 0 ? Orientation::front : Orientation::back};}
		}
	}
}

int32 Terrain::IntersectionData::Distance(IntersectionData& a, IntersectionData& b)
{
  return abs((signed)(a.wx - b.wx)) + abs((signed)(a.wy - b.wy)) + abs((signed)(a.wz - b.wz));
}

bool Terrain::IntersectionData::IsValid()
{
    return orientation != Orientation::none;
}

Orientation RotateOrientation(Orientation orientation, int32 rotation)
{
  int32 orientationRotation = (OrientationToRotation(orientation) + rotation) & 3;
  switch(orientationRotation)
  {
  case 0: return Orientation::front;
  case 1: return Orientation::left;
  case 2: return Orientation::back;
  case 3: return Orientation::right;
  default: __debugbreak(); return Orientation::top;
  }
}

int32 OrientationToRotation(Orientation orientation)
{
  switch(orientation)
  {
  case Orientation::front: return 0;
  case Orientation::left:  return 1;
  case Orientation::back:  return 2;
  case Orientation::right: return 3;
  default: __debugbreak(); return 0;
  }
}

void OffsetFromOrientation(Orientation orientation, int32& dx, int32& dy, int32& dz)
{
  switch(orientation)
  {
  case Orientation::top:   dx =  0; dy =  1; dz =  0; break;
  case Orientation::bottom:dx =  0; dy = -1; dz =  0; break;
  case Orientation::front: dx =  0; dy =  0; dz =  1; break;
  case Orientation::back:  dx =  0; dy =  0; dz = -1; break;
  case Orientation::right: dx =  1; dy =  0; dz =  0; break;
  case Orientation::left:  dx = -1; dy =  0; dz =  0; break;
  }
}
