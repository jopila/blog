#pragma once

#include <functional>
#include <sstream>

class SmoothTimer;
class SpriteRenderer;
class TextRenderer;

class SystemConsole
{
public:
  SystemConsole();
  ~SystemConsole();

  template<typename T>
  void AddVariable(std::string name, T& var);

  void Update();
  void Render();

  bool isActive;

private:
  SmoothTimer* renderTimer;
  SpriteRenderer* guiSpriteRenderer;
  TextRenderer* guiTextRenderer;

  void RenderButton(float x, float y, float w, float h);

  struct Command
  {
    virtual ~Command() { for(std::pair<std::string, Command*> it : subCommands) delete it.second; }
    std::map<std::string, Command*> subCommands;
  };
  struct Variable : public Command
  {
    std::string type;
    std::function<std::string()> read;
    std::function<void(std::string)> write;
  };
  Command root;

  Command* FindCommand(std::string name);
  Variable* CreateVariableCommand(std::string name);

  std::string text;
  std::vector<std::string> history;
  int historyIndex;
};

extern SystemConsole* sys_console;

template<typename T>
inline void SystemConsole::AddVariable(std::string name, T& var)
{
  Variable* newCommand = CreateVariableCommand(name);
  newCommand->type = typeid(T).name();
  newCommand->read = [&var]()->std::string { std::stringstream stream; stream << var; return stream.str(); };
  newCommand->write = [&var](std::string str) { std::stringstream stream(str); stream >> var; };
}
