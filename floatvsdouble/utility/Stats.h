#pragma once

#include <list>
#include <map>
#include <string>

class SmoothTimer
{
public:
	SmoothTimer(std::string name, float smoothDuration = 0.5f);
	~SmoothTimer();
	void Start();
	void Stop();
	float GetTime();

private:
	std::string name;
	float smoothDuration;
	struct Time
	{
		int64 start;
		int64 end;
	};
	std::list<Time> timings;
	int64 accumulatedTime;
};

class SystemStats
{
public:
	std::map<std::string, SmoothTimer*> timers;
};

extern SystemStats* sys_stats;
