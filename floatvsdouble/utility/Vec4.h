#pragma once

struct Vec4
{
	float x, y, z, w;

	Vec4() {}
	Vec4(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

	void Normalize();
	friend float Dot(const Vec4& a, const Vec4& b);
	friend Vec4 Cross(const Vec4& a, const Vec4& b);
  friend float Distance(const Vec4& a, const Vec4& b);
	Vec4 operator*(const Vec4& rhs) const;
	Vec4 operator+(const Vec4& rhs) const;
	Vec4 operator-(const Vec4& rhs) const;
	Vec4 operator*(float rhs) const;

	Vec4& operator+=(const Vec4& rhs);
	Vec4& operator-=(const Vec4& rhs);
};

struct Quat
{
	float q0, q1, q2, q3;
  Quat() {}
	Quat(Vec4 v, float theta);
  Quat(float q0, float q1, float q2, float q3) : q0(q0), q1(q1), q2(q2), q3(q3) {}
  Quat operator*(const Quat& rhs) const;
};
