#pragma once

#include <functional>
#include <map>


namespace uti
{
	template <typename ...T>
	class Delegate
	{
	public:
		int32 Add(std::function<void(T...)> function) { functionList[nextId] = function; return nextId++; }
		void Remove(int32 id) { functionList.erase(id); }
		void Clear() {functionList.clear(); }
		void Broadcast(T... args)
		{
			for(auto func : functionList)
				func.second(args...);
		}
	private:
		int32 nextId = 0;
		std::map<int32, std::function<void(T... args)>> functionList;
	};
}
