
#include "stdafx.h"

#include "Stats.h"

#include <chrono>

SystemStats* sys_stats;

SmoothTimer::SmoothTimer(std::string name, float smoothDuration)
	: name(name), smoothDuration(smoothDuration), accumulatedTime(0)
{
	sys_stats->timers[name] = this;
}

SmoothTimer::~SmoothTimer()
{
	sys_stats->timers.erase(name);
}

void SmoothTimer::Start()
{
	using namespace std::chrono;
	timings.push_back({0, 0});
	Time& time = timings.back();
	time.start = high_resolution_clock::now().time_since_epoch().count();
}

void SmoothTimer::Stop()
{
	using namespace std::chrono;
	Time& time = timings.back();
	time.end = high_resolution_clock::now().time_since_epoch().count();
	accumulatedTime += time.end - time.start;
	for(auto it = timings.begin(); it != timings.end(); )
	{
		duration<float, std::nano> dur(time.end - it->start);
		float seconds = dur.count() / 1.0e9f;
		if(seconds > smoothDuration)
		{
			accumulatedTime -= it->end - it->start;
			it++;
			timings.pop_front();
		}
		else
			break;
	}
}

float SmoothTimer::GetTime()
{
	using namespace std::chrono;
	duration<float, std::nano> dur(accumulatedTime);
	float seconds = dur.count() / 1.0e9f;
	return timings.size() ? seconds / timings.size() : 0.0f;
}

