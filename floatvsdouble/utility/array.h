#pragma once

#include "stdafx.h"
#include <cassert>
#include <initializer_list>

namespace uti
{
	template <typename T>
	class Array
	{
	public:
		Array(int32 initialSize = 0, int32 initialAllocated = 10)
		{
			if(initialAllocated < initialSize)
				initialAllocated = initialSize;
			num = initialSize;
			allocated = initialAllocated;
			arr = new T[allocated];
		}

		Array(std::initializer_list<T> list)
		{
			num = 0;
			allocated = (int32)list.size();
			arr = new T[allocated];
			for(T const& item : list)
				arr[num++] = item;
		}

		~Array()
		{
			delete[] arr;
		}

		void push_back(const T& val)
		{
			if(num >= allocated)
				reserve(allocated * 2);
			arr[num++] = val;
		}

		T& pop_back()
		{
			return arr[--num];
		}

		void clear()
		{
			num = 0;
		}

		void erase(T item)
		{
			for(int32 i = 0; i < num; i++)
			{
				if(arr[i] == item)
				{
					arr[i] = arr[--num];
					return;
				}
			}

		}

		void reserve(int32 newAllocated)
		{
			if(num != 0)
			{
				allocated = newAllocated;
				T* newArr = new T[allocated];
				memcpy(newArr, arr, num * sizeof(T));
				delete[] arr;
				arr = newArr;
			}
			else
			{
				delete[] arr;
				allocated = newAllocated;
				arr = new T[allocated];
			}
		}

		void resize(int32 newSize)
		{
			if(newSize >= allocated)
				reserve(newSize);
			num = newSize;
		}

		T& operator[] (int32 index) const { return arr[index]; }
		T* data() const { return arr; }
		T& back() const { return arr[num-1]; }
		int32 size() const { return num; }
		int32 room() const { return allocated; }
		T* begin() const { return num>0 ? &arr[0] : nullptr; }
		T* end()   const { return num>0 ? &arr[num] : nullptr; }

	private:
		T* arr;
		int32 num;
		int32 allocated;
	};
}

