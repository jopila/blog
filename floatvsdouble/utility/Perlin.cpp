
#include "stdafx.h"

#define _ITERATOR_DEBUG_LEVEL 0
#include <vector>
#include "Perlin.h"

#include <numeric>
#include <random>

// Generate a new permutation vector based on the value of seed
PerlinNoise::PerlinNoise(unsigned int seed)
{
	std::iota(p, p + PERMUTATION_SIZE, 0);
	std::default_random_engine engine(seed);
	std::shuffle(p, p + PERMUTATION_SIZE, engine);
	memcpy(p + PERMUTATION_SIZE, p, sizeof(int32) * PERMUTATION_SIZE); // Duplicate the permutation vector
}

double PerlinNoise::Noise(double x, double y, double z)
{
	int32 ix = (int32)floor(x) & 255;
	int32 iy = (int32)floor(y) & 255;
	int32 iz = (int32)floor(z) & 255;
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);
	double u = Fade(x);
	double v = Fade(y);
	double w = Fade(z);

	int32 A  = p[ix] + iy;
	int32 AA = p[A] + iz;
	int32 AB = p[A+1] + iz;
	int32 B  = p[ix+1] + iy;
	int32 BA = p[B] + iz;
	int32 BB = p[B+1] + iz;

  return 0.5 + 0.5 * Lerp(Lerp(Lerp(Grad(p[AA  ], x  , y  , z  ),
                                    Grad(p[BA  ], x-1, y  , z  ), u),
                               Lerp(Grad(p[AB  ], x  , y-1, z  ),
                                    Grad(p[BB  ], x-1, y-1, z  ), u), v),
                          Lerp(Lerp(Grad(p[AA+1], x  , y  , z-1),
                                    Grad(p[BA+1], x-1, y  , z-1), u),
                               Lerp(Grad(p[AB+1], x  , y-1, z-1),
                                    Grad(p[BB+1], x-1, y-1, z-1), u), v), w);
}

double PerlinNoise::Fade(double t)
{
	return t*t*t*(t*(t*6-15)+10);
}

double PerlinNoise::Lerp(double a, double b, double t)
{
	return a + t*(b-a);
}

double PerlinNoise::Grad(int hash, double x, double y, double z)
{
	int h = hash & 15;
	double u = h < 8 ? x : y;
	double v = h < 4 ? y : h==12||h==14 ? x : z;
	return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}
