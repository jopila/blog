// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _ITERATOR_DEBUG_LEVEL 0

#include "targetver.h"

typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned int uint32;
typedef int int32;
typedef unsigned short uint16;
typedef short int16;
typedef unsigned char uint8;
typedef char int8;

template<typename T>
void Swap(T& a, T& b) { T t = a; a = b; b = t; }

const float PI = 3.141592654f;

template<typename T>
T Clamp(T value, T min, T max) { return value < min ? min : (value > max ? max : value); }
template<typename T>
T Min(T a, T b) { return a < b ? a : b; }
template<typename T>
T Max(T a, T b) { return a < b ? b : a; }
template<typename T>
T Within(T x, T y, T w, T h, T px, T py) { return px >= x && px < x+w && py >= y && py < y+h; }
template<typename T>
T Lerp(T a, T b, T t) { return (1 - t) * a + t * b; }
template<typename T>
T AbsoluteValue(T a) { return a < 0 ? -a : a; }


void Assert(bool condition, const char* string);

// TODO: reference additional headers your program requires here
