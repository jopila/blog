#pragma once

namespace uti
{
	template <typename T, int MAX_SIZE>
	class SmallArray
	{
	public:
		static_assert(MAX_SIZE < 127);
		SmallArray() { num = 0; }

		SmallArray(std::initializer_list<T> list)
		{
			num = 0;
			for(T const& item : list)
				arr[num++] = item;
		}

		void push_back(const T& val) { arr[num++] = val; }
		T& pop_back()                { return arr[--num]; }
		void clear()                 { num = 0; }
		void resize(int32 newSize)   { num = newSize; }

		T& operator[] (int64 index) { return arr[index]; }
		T* data() const { return arr; }
		T& back() const { return arr[num-1]; }

		int32 size() const { return num; }
		int32 room() const { return allocated; }

		T* begin() const { return num>0 ? &arr[0] : nullptr; }
		T* end()   const { return num>0 ? &arr[num] : nullptr; }

	private:
		T arr[MAX_SIZE];
		int8 num;
	};
}
