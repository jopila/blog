// stdafx.cpp : source file that includes just the standard includes
// utl.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <cstdio>

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

void Assert(bool condition, const char* string)
{
  if(!condition)
  {
    __debugbreak();
    printf("FAILURE: %s", string);
  }
}
