#pragma once

#include "Vec4.h"

struct Mat4x4
{
	union
	{
		float data[16];
		Vec4 vecData[4];
	};
	Mat4x4() {}
	static Mat4x4 MakeIdentity();
	static Mat4x4 MakeTranslation(Vec4& offset);
  static Mat4x4 MakeScale(Vec4& scale);
	static Mat4x4 MakeLookAt(const Vec4& eye, const Vec4& target);
	static Mat4x4 MakePerspective(float fov, float aspect, float near, float far);
	static Mat4x4 MakeYaw(float radians);
	static Mat4x4 MakePitch(float radians);
	static Mat4x4 MakeRoll(float radians);
	static Mat4x4 MakeRotation(const Quat& quat);


	void Transpose();
	void print() const;

	Mat4x4 operator*(const Mat4x4& rhs) const;
	Vec4 operator*(const Vec4& rhs) const;
};