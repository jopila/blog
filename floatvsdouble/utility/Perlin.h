#pragma once

class PerlinNoise
{
	// The permutation vector
	static const int PERMUTATION_SIZE = 256;
	int p[PERMUTATION_SIZE * 2];
public:
	// Generate a new permutation vector based on the value of seed
	PerlinNoise(unsigned int seed);
	// Get a noise value, for 2D images z can have any value
	double Noise(double x, double y, double z);
private:
	double Fade(double t);
	double Lerp(double a, double b, double t);
	double Grad(int hash, double x, double y, double z);
};