#pragma once

#include <cstring>

namespace uti
{
	class String
	{
	public:
		String()
		{
			len = 0;
			str = nullptr;
		}

		String(const char* newStr)
		{
			len = (int32)strlen(newStr);
			str = new char[len+1];
			strcpy_s(str, len+1, newStr);
		}

		String(const String& newStr)
		{
			len = newStr.len;
			if(newStr.str == nullptr)
				str = nullptr;
			else
			{
				str = new char[len+1];
				strcpy_s(str, len+1, newStr.str);
			}
		}

		~String()
		{
			delete[] str;
		}

		void Replace(const char* find, const char* replace)
		{
			if(Find(find) == len)
				return;

			int32 offset = 0;
			int32 replacementCount = 0;
			while((offset = Find(find, offset)) != len)
				replacementCount++, offset++;

			char* oldStr = str;
			int32 oldLen = len;
			int32 findLen = (int32)strlen(find);
			len += replacementCount * ((int32)strlen(replace) - findLen);
			str = new char[len+1];

			int32 dest = 0;
			for(int i = 0; i < oldLen; i++)
			{
				int32 j = 0;
				while(find[j] && find[j] == oldStr[i+j])
					j++;

				if(find[j] == 0)
				{
					j = 0;
					while(replace[j])
						str[dest++] = replace[j++];
					i += findLen - 1;
				}
				else
					str[dest++] = oldStr[i];
			}
			str[len] = 0;
			delete[] oldStr;
		}

		int32 Find(const char* find, int32 offset = 0)
		{
			for(int32 i = offset; i < len; i++)
			{
				int32 j = 0;
				while(find[j])
				{
					if(find[j] != str[i + j++])
						break;
					if(find[j] == 0)
						return i;
				}
			}
			return len;
		}

		int32 FindIgnoreCase(const char* find, int32 offset = 0)
		{
			for(int32 i = offset; i < len; i++)
			{
				int32 j = 0;
				while(find[j])
				{
					if(std::toupper(find[j]) != std::toupper(str[i + j]))
						break;
					if(find[++j] == 0)
						return i;
				}
			}
			return len;
		}

		bool BeginsWith(const char* find)
		{
			int32 i = 0;
			while(find[i])
				if(find[i] != str[i++])
					return false;
			return true;
		}

		String& operator=(const char* newStr)
		{
			delete[] str;
			return *new(this) String(newStr);
		}

		String& operator=(const String& newStr)
		{
			delete[] str;
			return *new(this) String(newStr.cstr());
		}

		bool operator==(const String& rhs) const
		{
			if(len != rhs.len) return false;
			if(!str || !rhs.str) return false;
			return strcmp(str, rhs.str) == 0;
		}

		bool operator!=(const String& rhs) const
		{
			return !(*this == rhs);
		}

		bool operator==(const char* rhs) const
		{
			if(str == nullptr) return rhs[0] == '\0';
			return strcmp(str, rhs) == 0;
		}

		bool operator!=(const char* rhs) const
		{
			return !(*this == rhs);
		}

		bool operator<(const String& rhs) const
		{
			if(!str || !rhs.str) return false;
			return strcmp(str, rhs.str) < 0;
		}

		bool operator>(const String& rhs) const
		{
			if(!str || !rhs.str) return false;
			return strcmp(str, rhs.str) > 0;
		}

		String& operator+=(const char* rhs)
		{
			len += (int32)strlen(rhs);
			char* newStr = new char[len + 1];
			char* newStrCopy = newStr;
			char* strCopy = str;
			while(*newStrCopy++ = *strCopy++);
			newStrCopy--;
			while(*newStrCopy++ = *rhs++);
			delete[] str;
			str = newStr;
			return *this;
		}

		String& operator+=(const String& rhs)
		{
			len += rhs.len;
			char* newStr = new char[len + 1];
			char* newStrCopy = newStr;
			char* strCopy = str;
			char* rhsStrCopy = rhs.str;
			while(*newStrCopy++ = *strCopy++);
			newStrCopy--;
			while(*newStrCopy++ = *rhsStrCopy++);
			delete[] str;
			str = newStr;
			return *this;
		}

		const char* cstr() const { return str; }
		uint32 length() const { return len; }

	private:
		int32 len;
		char* str;
	};
}
