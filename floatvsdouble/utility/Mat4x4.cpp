#include "stdafx.h"

#include "Mat4x4.h"
#include <math.h>
#include <cstdio>

#include "Vec4.h"

Mat4x4 Mat4x4::MakeIdentity()
{
	Mat4x4 result;
	result.data[ 0] = 1.0f;
	result.data[ 1] = 0.0f;
	result.data[ 2] = 0.0f;
	result.data[ 3] = 0.0f;

	result.data[ 4] = 0.0f;
	result.data[ 5] = 1.0f;
	result.data[ 6] = 0.0f;
	result.data[ 7] = 0.0f;

	result.data[ 8] = 0.0f;
	result.data[ 9] = 0.0f;
	result.data[10] = 1.0f;
	result.data[11] = 0.0f;

	result.data[12] = 0.0f;
	result.data[13] = 0.0f;
	result.data[14] = 0.0f;
	result.data[15] = 1.0f;
	return result;
}

Mat4x4 Mat4x4::MakeTranslation(Vec4& offset)
{
	Mat4x4 result;
	result.data[ 0] = 1.0f;
	result.data[ 1] = 0.0f;
	result.data[ 2] = 0.0f;
	result.data[ 3] = offset.x;

	result.data[ 4] = 0.0f;
	result.data[ 5] = 1.0f;
	result.data[ 6] = 0.0f;
	result.data[ 7] = offset.y;

	result.data[ 8] = 0.0f;
	result.data[ 9] = 0.0f;
	result.data[10] = 1.0f;
	result.data[11] = offset.z;

	result.data[12] = 0.0f;
	result.data[13] = 0.0f;
	result.data[14] = 0.0f;
	result.data[15] = 1.0f;
	return result;
}

Mat4x4 Mat4x4::MakeScale(Vec4& scale)
{
  Mat4x4 result;
  result.data[ 0] = scale.x;
  result.data[ 1] = 0.0f;
  result.data[ 2] = 0.0f;
  result.data[ 3] = 0.0f;

  result.data[ 4] = 0.0f;
  result.data[ 5] = scale.y;
  result.data[ 6] = 0.0f;
  result.data[ 7] = 0.0f;

  result.data[ 8] = 0.0f;
  result.data[ 9] = 0.0f;
  result.data[10] = scale.z;
  result.data[11] = 0.0f;

  result.data[12] = 0.0f;
  result.data[13] = 0.0f;
  result.data[14] = 0.0f;
  result.data[15] = 1.0f;
  return result;
}

Mat4x4 Mat4x4::MakeLookAt(const Vec4& eye, const Vec4& target)
{
	Vec4 zaxis = target - eye;
	zaxis.Normalize();
	Vec4 xaxis = Cross(zaxis, Vec4(0.0f, 1.0f, 0.0f, 0.0f));
	xaxis.Normalize();
	Vec4 yaxis = Cross(xaxis, zaxis);
	Mat4x4 result;
	result.data[ 0] = xaxis.x;
	result.data[ 1] = xaxis.y;
	result.data[ 2] = xaxis.z;
	result.data[ 3] = -Dot(xaxis, eye);

	result.data[ 4] = yaxis.x;
	result.data[ 5] = yaxis.y;
	result.data[ 6] = yaxis.z;
	result.data[ 7] = -Dot(yaxis, eye);

	result.data[ 8] = -zaxis.x;
	result.data[ 9] = -zaxis.y;
	result.data[10] = -zaxis.z;
	result.data[11] = +Dot(zaxis, eye);

	result.data[12] = 0.0f;
	result.data[13] = 0.0f;
	result.data[14] = 0.0f;
	result.data[15] = 1.0f;
	return result;
}

Mat4x4 Mat4x4::MakePerspective(float fov, float aspect, float near, float far)
{
	float scale = 1 / tanf(fov/2 * PI/180.0f);
	Mat4x4 result;
	result.data[ 0] = scale/aspect;
	result.data[ 1] = 0.0f;
	result.data[ 2] = 0.0f;
	result.data[ 3] = 0.0f;

	result.data[ 4] = 0.0f;
	result.data[ 5] = -scale;
	result.data[ 6] = 0.0f;
	result.data[ 7] = 0.0f;

	result.data[ 8] = 0.0f;
	result.data[ 9] = 0.0f;
	result.data[10] = -far/(far - near);
	result.data[11] = -2*far*near/(far - near);

	result.data[12] = 0.0f;
	result.data[13] = 0.0f;
	result.data[14] = -1.0f;
	result.data[15] = 0.0f;
	return result;
}

Mat4x4 Mat4x4::MakeYaw(float radians)
{
	Mat4x4 result = MakeIdentity();

	result.data[ 0] = cosf(radians);
	result.data[ 2] = sinf(radians);

	result.data[ 8] = -sinf(radians);
	result.data[10] = cosf(radians);

	return result;
}

Mat4x4 Mat4x4::MakePitch(float radians)
{
	Mat4x4 result = MakeIdentity();

	result.data[ 5] = cosf(radians);
	result.data[ 6] = -sinf(radians);

	result.data[ 9] = sinf(radians);
	result.data[10] = cosf(radians);

	return result;
}

Mat4x4 Mat4x4::MakeRoll(float radians)
{
	Mat4x4 result = MakeIdentity();

	result.data[ 0] = cosf(radians);
	result.data[ 1] = -sinf(radians);

	result.data[ 4] = sinf(radians);
	result.data[ 5] = cosf(radians);

	return result;
}

Mat4x4 Mat4x4::MakeRotation(const Quat& quat)
{
	Mat4x4 result;

	float q0 = quat.q0;
	float q1 = quat.q1;
	float q2 = quat.q2;
	float q3 = quat.q3;
	result.data[ 0] = 1.0f - 2.0f * q2 * q2 - 2.0f * q3 * q3;
	result.data[ 1] = 2.0f * q1 * q2 - 2.0f * q0 * q3;
	result.data[ 2] = 2.0f * q1 * q3 + 2.0f * q0 * q2;
	result.data[ 3] = 0.0f;

	result.data[ 4] = 2.0f * q1 * q2 + 2.0f * q0 * q3;
	result.data[ 5] = 1 - 2.0f * q1 * q1 - 2.0f * q3 * q3;
	result.data[ 6] = 2.0f * q2 * q3 - 2.0f * q0 * q1;
	result.data[ 7] = 0.0f;

	result.data[ 8] = 2.0f * q1 * q3 - 2.0f * q0 * q2;
	result.data[ 9] = 2.0f * q2 * q3 + 2.0f * q0 * q1;
	result.data[10] = 1 - 2.0f * q1 * q1 - 2.0f * q2 * q2;
	result.data[11] = 0.0f;

	result.data[12] = 0.0f;
	result.data[13] = 0.0f;
	result.data[14] = 0.0f;
	result.data[15] = 1.0f;

	return result;
}

void Mat4x4::Transpose()
{
	Swap(data[ 1], data[ 4]);
	Swap(data[ 2], data[ 8]);
	Swap(data[ 3], data[12]);
	Swap(data[ 6], data[ 9]);
	Swap(data[ 7], data[13]);
	Swap(data[11], data[14]);
}

void Mat4x4::print() const
{
	printf("  %.2f, %.2f, %.2f, %.2f\n",  data[0],  data[1],  data[2],  data[3]);
	printf("  %.2f, %.2f, %.2f, %.2f\n",  data[4],  data[5],  data[6],  data[7]);
	printf("  %.2f, %.2f, %.2f, %.2f\n",  data[8],  data[9], data[10], data[11]);
	printf("  %.2f, %.2f, %.2f, %.2f\n", data[12], data[13], data[14], data[15]);
}

Mat4x4 Mat4x4::operator*(const Mat4x4& rhs) const
{
	Mat4x4 transposedRhs = rhs;
	transposedRhs.Transpose();
	Mat4x4 result;
	result.data[ 0] = Dot(vecData[0], transposedRhs.vecData[0]);
	result.data[ 1] = Dot(vecData[0], transposedRhs.vecData[1]);
	result.data[ 2] = Dot(vecData[0], transposedRhs.vecData[2]);
	result.data[ 3] = Dot(vecData[0], transposedRhs.vecData[3]);
	result.data[ 4] = Dot(vecData[1], transposedRhs.vecData[0]);
	result.data[ 5] = Dot(vecData[1], transposedRhs.vecData[1]);
	result.data[ 6] = Dot(vecData[1], transposedRhs.vecData[2]);
	result.data[ 7] = Dot(vecData[1], transposedRhs.vecData[3]);
	result.data[ 8] = Dot(vecData[2], transposedRhs.vecData[0]);
	result.data[ 9] = Dot(vecData[2], transposedRhs.vecData[1]);
	result.data[10] = Dot(vecData[2], transposedRhs.vecData[2]);
	result.data[11] = Dot(vecData[2], transposedRhs.vecData[3]);
	result.data[12] = Dot(vecData[3], transposedRhs.vecData[0]);
	result.data[13] = Dot(vecData[3], transposedRhs.vecData[1]);
	result.data[14] = Dot(vecData[3], transposedRhs.vecData[2]);
	result.data[15] = Dot(vecData[3], transposedRhs.vecData[3]);
	return result;
}

Vec4 Mat4x4::operator*(const Vec4& rhs) const
{
	Vec4 result;
	result.x = Dot(vecData[0], rhs);
	result.y = Dot(vecData[1], rhs);
	result.z = Dot(vecData[2], rhs);
	result.w = Dot(vecData[3], rhs);
	return result;
}
