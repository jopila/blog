#include "stdafx.h"

#include <math.h>

#include "Vec4.h"

void Vec4::Normalize()
{
	float div = sqrtf(x*x + y*y + z*z + w*w);
	x /= div;
	y /= div;
	z /= div;
	w /= div;
}

Vec4 Vec4::operator*(const Vec4& rhs) const { return Vec4(x*rhs.x, y*rhs.y, z*rhs.z, w*rhs.w); }
Vec4 Vec4::operator+(const Vec4& rhs) const { return Vec4(x+rhs.x, y+rhs.y, z+rhs.z, w+rhs.w); }
Vec4 Vec4::operator-(const Vec4& rhs) const { return Vec4(x-rhs.x, y-rhs.y, z-rhs.z, w-rhs.w); }

Vec4 Vec4::operator*(float rhs) const { return Vec4(x*rhs, y*rhs, z*rhs, w*rhs); }

Vec4& Vec4::operator+=(const Vec4& rhs) { x += rhs.x; y += rhs.y; z += rhs.z; w += rhs.w; return *this;}
Vec4& Vec4::operator-=(const Vec4& rhs) { x -= rhs.x; y -= rhs.y; z -= rhs.z; w -= rhs.w; return *this; }

float Dot(const Vec4& a, const Vec4& b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}

Vec4 Cross(const Vec4& a, const Vec4& b)
{
	return Vec4(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x, 0.0f);
}

float Distance(const Vec4& a, const Vec4& b)
{
  float dx = a.x - b.x; dx = dx * dx;
  float dy = a.y - b.y; dy = dy * dy;
  float dz = a.z - b.z; dz = dz * dz;
  return sqrtf(dx + dy + dz);
}

Quat::Quat(Vec4 v, float theta)
{
	v.Normalize();
	theta /= 2.0f;
	q0 = cosf(theta);
	q1 = v.x * sinf(theta);
	q2 = v.y * sinf(theta);
	q3 = v.z * sinf(theta);
}

Quat Quat::operator*(const Quat& rhs) const
{
  return Quat
  (
    q0 * rhs.q0 - q1 * rhs.q1 - q2 * rhs.q2 - q3 * rhs.q3,
    q1 * rhs.q0 + q0 * rhs.q1 + q2 * rhs.q3 - q3 * rhs.q2,
    q0 * rhs.q2 - q1 * rhs.q3 + q2 * rhs.q0 + q3 * rhs.q1,
    q0 * rhs.q3 + q1 * rhs.q2 - q2 * rhs.q1 + q3 * rhs.q0
  );
}
