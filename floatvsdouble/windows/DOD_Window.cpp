
#include "stdafx.h"

#include "DOD_Window.h"

namespace dod
{
	std::vector<std::vector<MenuData>> SysWindow::menus;
	std::vector<nonstd::vector<SysWindow::RenderData>> SysWindow::menuRenderDatas;



	float FinishDimension(float inputDim, float parentDim)
	{
		/**/ if(inputDim <= 1.0f)
			return parentDim * inputDim;
		else if(inputDim <= parentDim)
			return inputDim;
		else
			return parentDim;
	}

	void SysWindow::Layout(MenuHandle menuIndex, float x, float y, float width, float height)
	{
		std::vector<MenuData>& datas = menus[menuIndex]; // hahaha datas with an s!
		nonstd::vector<RenderData>& renderDatas = menuRenderDatas[menuIndex];

		//renderDatas.resize(datas.size());
		//MenuDataIterator currentData = datas.begin();
		//RenderDataIterator currentRenderData = renderDatas.begin();
		//
		//LayoutRecursive({x, y, width, height}, WT_Overlay, 0, currentData, datas.end(), currentRenderData, renderDatas.end(), 0);
		//
		//return;

		struct StackInfo
		{
			RenderData renderData;
			WindowType type;
			MenuData::CustomData customData;
		};
		StackInfo parentStack[128]; // Might want to do something more dynamic at some point

		renderDatas.resizeUninitialized(datas.size());
		parentStack[0].renderData = {x, y, width, height};
		parentStack[0].type = WT_Overlay;
		memset(&parentStack[0].customData, 0, sizeof(parentStack[0].customData));

		MenuDataIterator dataIt = datas.begin();
		RenderDataIterator renderDataIt = renderDatas.begin();
		for(; dataIt != datas.end(); dataIt++, renderDataIt++)
		{
			// Aliases
			MenuData& data = *dataIt;
			RenderData& renderData = *renderDataIt;
			RenderData& parent = parentStack[data.depth-1].renderData;
			WindowType parentType = parentStack[data.depth-1].type;

			// Handle current node
			renderData.width  = FinishDimension(data.width,  parent.width);
			renderData.height = FinishDimension(data.height, parent.height);
			renderData.x = parent.x;
			renderData.y = parent.y;

			// Update parent info
			switch(parentType)
			{
				case WT_StackPanel:
				{
					Orientation orientation = parentStack[data.depth-1].customData.stackPanelOrientation;
					if(orientation == Horizontal)
						parent.x += renderData.width;
					if(orientation == Vertical)
						parent.y += renderData.height;
					break;
				}
				case WT_Overlay:
				case WT_Spacer:
				case WT_Image:
				case WT_Text:
				default:
					;
			}

			// If this window can handle children, add to the stack
			if(data.type == WT_StackPanel || data.type == WT_Overlay)
			{
				parentStack[data.depth].renderData = renderData;
				parentStack[data.depth].type = data.type;
				parentStack[data.depth].customData = data.customData;
			}
		}
	}

	void SysWindow::LayoutRecursive
	(
		RenderData parentRenderData,
		WindowType parentType,
		MenuData::CustomData parentCustomData,
		MenuDataIterator& currentData,
		MenuDataIterator endData,
		RenderDataIterator& currentRenderData,
		RenderDataIterator endRenderData,
		int parentDepth
	)
	{
		do
		{
			// Aliases
			const MenuData& data = *currentData;
			RenderData& renderData = *currentRenderData;

			// Handle current node
			renderData.width  = FinishDimension(data.width,  parentRenderData.width);
			renderData.height = FinishDimension(data.height, parentRenderData.height);
			renderData.x = parentRenderData.x;
			renderData.y = parentRenderData.y;

			// Update parent info
			switch(parentType)
			{
				case WT_StackPanel:
				{
					Orientation orientation = parentCustomData.stackPanelOrientation;
					if(orientation == Horizontal)
						parentRenderData.x += renderData.width;
					if(orientation == Vertical)
						parentRenderData.y += renderData.height;
					break;
				}
				case WT_Overlay:
				case WT_Spacer:
				case WT_Image:
				case WT_Text:
				default:
					;
			}

			// Next!
			currentData++;
			currentRenderData++;
			if(data.type == WT_StackPanel || data.type == WT_Overlay)
			{
				LayoutRecursive(renderData, data.type, data.customData, currentData, endData, currentRenderData, endRenderData, data.depth);
			}
		}while(currentData != endData && currentData->depth == parentDepth + 1);
	}



	void SysWindow::Print(MenuHandle menuIndex)
	{
		std::vector<MenuData>& datas = menus[menuIndex];
		std::vector<RenderData>& renderDatas = menuRenderDatas[menuIndex];

		for(size_t i = 0; i < datas.size(); i++)
		{
			for(int j = 0; j < datas[i].depth - 1; j++)
				printf("  ");

			RenderData& renderData = renderDatas[i];
			printf("x,y = %0.2f,%0.2f w,h = %0.2f,%0.2f\n", renderData.x, renderData.y, renderData.width, renderData.height);
		}
	}

} //namespace dod
