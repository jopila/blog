#pragma once

#include "nonstd.h"

namespace dod
{
	enum WindowType : char
	{
		WT_Overlay,
		WT_StackPanel,
		WT_Spacer,
		WT_Image,
		WT_Text,
	};

	enum Orientation
	{
		Vertical,
		Horizontal,
	};

	struct MenuData
	{
		// Whelp, this is hella janky. Two const char* variables in the union means we can't distinguish between them in parameter overrides.
		// So we've got one constructor for two window types?! How does fix...
		// Also maybe add some assertions?...
		MenuData(WindowType type, int depth, float width, float height)
			: type(type), depth(depth), width(width), height(height) {}
		MenuData(WindowType type, int depth, float width, float height, Orientation stackPanelOrientation)
			: type(type), depth(depth), width(width), height(height), customData(stackPanelOrientation) {}
		MenuData(WindowType type, int depth, float width, float height, const char* data)
			: type(type), depth(depth), width(width), height(height), customData(data) {}

		float width;
		float height;
		
		struct CustomData
		{
			CustomData() {}
			CustomData(Orientation stackPanelOrientation) {this->stackPanelOrientation = stackPanelOrientation;}
			CustomData(const char* data) {this->imageUrl = data;}
			union
			{
				Orientation stackPanelOrientation;
				const char* imageUrl;
				const char* textText;
			};
		};
		CustomData customData;
		WindowType type;
		char depth;

	private:
		friend class SysWindow;
		MenuData() {}
	};

	class SysWindow
	{
	public:
		typedef size_t MenuHandle;
		//static void Reset(int expectedMenuCount) {menus.reserve(expectedMenuCount); menuRenderDatas.reserve(expectedMenuCount);}
		static MenuHandle CreateMenu(std::vector<MenuData>&& menuData) { menus.push_back(std::move(menuData)); menuRenderDatas.resize(menus.size()); return menus.size() - 1;}
		static void Layout(MenuHandle menuIndex, float x, float y, float width, float height);
		static void Print(MenuHandle menuIndex);
		static void DestroyMenu(MenuHandle menuIndex) { menus[menuIndex].clear(); menuRenderDatas[menuIndex].clear(); } // Need to recycle these somehow...but MenuHandle won't let us...

	private:

		struct RenderData
		{
			float x, y;
			float width, height;
		};

		typedef std::vector<MenuData>::iterator MenuDataIterator;
		typedef std::vector<RenderData>::iterator RenderDataIterator;
		static void LayoutRecursive
		(
			RenderData parentRenderData,
			WindowType parentType,
			MenuData::CustomData parentCustomData,
			MenuDataIterator& currentData,
			MenuDataIterator endData,
			RenderDataIterator& currentRenderData,
			RenderDataIterator endRenderData,
			int parentDepth
		);

		static std::vector<std::vector<MenuData>> menus;
		static std::vector<nonstd::vector<RenderData>> menuRenderDatas;
	};

} //namespace dod
