#pragma once

class SpriteRenderer;
class TextRenderer;

namespace oop
{
	struct WindowRenderer
	{
		SpriteRenderer* spriteRenderer;
		TextRenderer* textRenderer;
	};


	class Window
	{
	public:
		Window(Window* parent, float width, float height);
		virtual ~Window();

		//virtual void MouseMove(int x, int y) = 0;
		//virtual void Click(int x, int y) = 0;
		//virtual void RightClick(int x, int y) = 0;
		virtual void Layout(float x, float y, float parentWidth, float parentHeight);
		virtual void Print(int depth);
		virtual void Render(const WindowRenderer& windowRenderer);

	protected:
		friend class StackPanel;

		Window* parent;
		std::vector<Window*> children;

		float x, y;
		float width, height;
		float finishedWidth, finishedHeight;

		float FinishDimension(float inputDim, float parentDim);
	};

	class Overlay : public Window
	{
	public:
		Overlay(Window* parent, float width, float height);

		void Layout(float x, float y, float parentWidth, float parentHeight);
	};

	class StackPanel : public Window
	{
	public:
		enum Orientation
		{
			Vertical,
			Horizontal,
		};

		StackPanel(Window* parent, float width, float height, Orientation orientation);

		void Layout(float x, float y, float parentWidth, float parentHeight);

	private:
		Orientation orientation;
	};

	class Spacer : public Window
	{
	public:
		Spacer(Window* parent, float width, float height);
	};

	class Image : public Window
	{
	public:
		Image(Window* parent, float width, float height, std::string sprite);

		void Render(const WindowRenderer & windowRenderer);
	};

	class Text : public Window
	{
	public:
		Text(Window* parent, float width, float height, std::string text);

		void Render(const WindowRenderer & windowRenderer);
	};
}



// StackPanel
// Grid
// StaticText
// Image
// ScrollBox
// Spacer
// Splitter?
// ListView?
// TreeView?
// Canvas?