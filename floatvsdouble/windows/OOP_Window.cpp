
#include "stdafx.h"

#include "OOP_Window.h"
#include "..\Vulkan\Sprite.h"
#include "..\Vulkan\TextRenderer.h"

namespace oop
{
	Window::Window(Window * parent, float width, float height)
		: parent(parent)
		, width(width)
		, height(height)
	{
		if(parent)
			parent->children.push_back(this);
	}

	Window::~Window()
	{
		// delete ourselves from our parent
		if(parent)
			for(size_t i = 0; i < parent->children.size(); i++)
				if(parent->children[i] == this)
					parent->children.erase(parent->children.begin() + i);

		//delete all our children
		for(size_t i = 0; i < children.size(); i++)
			delete children[i];
	}

	float Window::FinishDimension(float inputDim, float parentDim)
	{
		/**/ if(inputDim <= 1.0f)
			return parentDim * inputDim;
		else if(inputDim <= parentDim)
			return inputDim;
		else
			return parentDim;
	}

	void Window::Layout(float x, float y, float parentWidth, float parentHeight)
	{
		finishedWidth = FinishDimension(width, parentWidth);
		finishedHeight = FinishDimension(height, parentHeight);
		this->x = x;
		this->y = y;
	}

	void Window::Print(int depth)
	{
		for(int i = 0; i < depth; i++)
			printf("  ");

		printf("x,y = %0.2f,%0.2f w,h = %0.2f,%0.2f\n", x, y, finishedWidth, finishedHeight);
		for(size_t i = 0; i < children.size(); i++)
			children[i]->Print(depth + 1);
	}

	void Window::Render(const WindowRenderer& windowRenderer)
	{
		for(size_t i = 0; i < children.size(); i++)
			children[i]->Render(windowRenderer);
	}

	Overlay::Overlay(Window* parent, float width, float height)
		: Window(parent, width, height)
	{
	}

	void Overlay::Layout(float x, float y, float parentWidth, float parentHeight)
	{
		Window::Layout(x, y, parentWidth, parentHeight);

		for(size_t i = 0; i < children.size(); i++)
		{
			children[i]->Layout(x, y, finishedWidth, finishedHeight);
		}
	}

	StackPanel::StackPanel(Window* parent, float width, float height, Orientation orientation)
		: Window(parent, width, height)
		, orientation(orientation)
	{
	}

	void StackPanel::Layout(float x, float y, float parentWidth, float parentHeight)
	{
		Window::Layout(x, y, parentWidth, parentHeight);

		for(size_t i = 0; i < children.size(); i++)
		{
			children[i]->Layout(x, y, finishedWidth, finishedHeight);
			if(orientation == Horizontal)
				x += children[i]->finishedWidth;
			if(orientation == Vertical)
				y += children[i]->finishedHeight;
		}
	}

	Spacer::Spacer(Window* parent, float width, float height)
		: Window(parent, width, height)
	{
	}

	Image::Image(Window* parent, float width, float height, std::string sprite)
		: Window(parent, width, height)
	{
	}

	Text::Text(Window* parent, float width, float height, std::string text)
		: Window(parent, width, height)
	{
	}


	void Image::Render(const WindowRenderer& windowRenderer)
	{
		SpriteRenderInfo renderInfo;
		renderInfo.x = x;
		renderInfo.y = y;
		renderInfo.w = finishedWidth;
		renderInfo.h = finishedHeight;
		renderInfo.r = 0xff;
		renderInfo.g = 0xff;
		renderInfo.b = 0xff;
		renderInfo.a = 0xff;
		renderInfo.spriteIndex = 0;
		RenderSprite(windowRenderer.spriteRenderer, renderInfo);
	}

	void Text::Render(const WindowRenderer& windowRenderer)
	{

	}
}
