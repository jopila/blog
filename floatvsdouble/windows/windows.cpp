// windows.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "../common/Timing.h"
#include "../common/Test.h"
#include <iostream>

#include "DOD_Window.h"
#include "OOP_Window.h"

#include "..\Vulkan\Gfx.h"


std::vector<dod::MenuData> CreateTestMenu(int buttonWidth, int buttonHeight)
{
	std::vector<dod::MenuData> result;

	result.push_back(dod::MenuData(dod::WT_StackPanel, 1, 1.0f, 1.0f, dod::Vertical));

	for(int y = 0; y < buttonHeight; y++)
	{
		result.push_back(dod::MenuData(dod::WT_StackPanel, 2, 1.0f, 1.0f / buttonHeight, dod::Horizontal));
		for(int x = 0; x < buttonWidth; x++)
		{
			result.push_back(dod::MenuData(dod::WT_Overlay, 3, 1.0f / buttonWidth, 1.0f));
			result.push_back(dod::MenuData(dod::WT_Image, 4, 1.0f, 1.0f, "image.png"));
			result.push_back(dod::MenuData(dod::WT_Text, 4, 1.0f, 1.0f, "text..."));
		}
	}

	return result;
}

std::vector<double> DODTest(/*std::vector<dod::MenuData>&& testMenu*/)
{
	Timer timer;
	double createTime, layoutTime, destroyTime;

	std::vector<dod::MenuData> testMenu = CreateTestMenu(50,50);

	timer.Start();
	dod::SysWindow::MenuHandle menuHandle = dod::SysWindow::CreateMenu(std::move(testMenu));
	createTime = timer.End();

	timer.Start();
	dod::SysWindow::Layout(menuHandle, 0.0f, 0.0f, 100.0f, 100.0f);
	layoutTime = timer.End();

	//dod::SysWindow::Print(menuHandle);

	timer.Start();
	dod::SysWindow::DestroyMenu(menuHandle);
	destroyTime = timer.End();

	//printf("DOD: %.3fus, %.3fus, %.3fus\n", createTime*1'000'000, layoutTime*1'000'000, destroyTime*1'000'000);
	return {createTime, layoutTime, destroyTime};
}

std::vector<double> OOPTest(std::vector<dod::MenuData>& testMenu)
{
	Timer timer;
	double createTime, layoutTime, destroyTime;

	timer.Start();
	oop::Window* parents[128]; // I gotta stop doing this...
	parents[0] = nullptr;

	for(size_t i = 0; i < testMenu.size(); i++)
	{
		dod::MenuData& data = testMenu[i];

		switch(data.type)
		{
			case dod::WT_Overlay:    parents[data.depth] = new oop::Overlay(parents[data.depth-1], data.width, data.height); break;
			case dod::WT_StackPanel: parents[data.depth] = new oop::StackPanel(parents[data.depth-1], data.width, data.height, (oop::StackPanel::Orientation)data.customData.stackPanelOrientation); break;
			case dod::WT_Spacer:     parents[data.depth] = new oop::Spacer(parents[data.depth-1], data.width, data.height); break;
			case dod::WT_Image:      parents[data.depth] = new oop::Image(parents[data.depth-1], data.width, data.height, data.customData.imageUrl); break;
			case dod::WT_Text:       parents[data.depth] = new oop::Text(parents[data.depth-1], data.width, data.height, data.customData.textText); break;
		}
	}
	createTime = timer.End();

	timer.Start();
	parents[1]->Layout(0.0f, 0.0f, 100.0f, 100.0f);
	layoutTime = timer.End();

	//parents[1]->Print(0);

	timer.Start();
	delete parents[1];
	destroyTime = timer.End();

	//printf("OOP: %.3fus, %.3fus, %.3fus\n", createTime*1'000'000, layoutTime*1'000'000, destroyTime*1'000'000);
	return {createTime, layoutTime, destroyTime};
}

int main()
{

	oop::Window* top = new oop::StackPanel(nullptr, 1.0f, 1.0f, oop::StackPanel::Vertical);
	oop::Window* hPanelTop = new oop::StackPanel(top, 1.0f, 0.6f, oop::StackPanel::Horizontal);
	oop::Window* imageTopLeft = new oop::Image(hPanelTop, 0.2f, 1.0f, "left");
	oop::Window* overlayTop = new oop::Overlay(hPanelTop, 0.6f, 1.0f);
	oop::Window* imageTopCenter = new oop::Image(overlayTop, 1.0f, 1.0f, "textBackground");
	oop::Window* textTopCenter = new oop::Text(overlayTop, 1.0f, 1.0f, "rawrgrr");
	oop::Window* imageTopRight = new oop::Image(hPanelTop, 0.2f, 1.0f, "right");
	oop::Window* divider = new oop::Image(top, 1.0f, 4.0f, "divider");
	oop::Window* hPanelBoottom = new oop::StackPanel(top, 1.0f, 0.4f, oop::StackPanel::Horizontal);
	oop::Window* spacer = new oop::Spacer(hPanelBoottom, 0.4f, 1.0f);
	oop::Window* bottomImage = new oop::Image(hPanelBoottom, 0.2f, 1.0f, "bottom");


	top->Layout(0.0f, 0.0f, 100.0f, 100.0f);

	top->Print(0);
	printf("\n\n");

	delete top;

	std::vector<dod::MenuData> menu = 
	{
		dod::MenuData(dod::WT_StackPanel, 1, 1.0f, 1.0f, dod::Vertical),
		dod::MenuData(dod::WT_StackPanel, 2, 1.0f, 0.6f, dod::Horizontal),
		dod::MenuData(dod::WT_Image,      3, 0.2f, 1.0f, "left"),
		dod::MenuData(dod::WT_Overlay,    3, 0.6f, 1.0f),
		dod::MenuData(dod::WT_Image,      4, 1.0f, 1.0f, "textBackground"),
		dod::MenuData(dod::WT_Text,       4, 1.0f, 1.0f, "rawrgrr"),
		dod::MenuData(dod::WT_Image,      3, 0.2f, 1.0f, "right"),
		dod::MenuData(dod::WT_Image,      2, 1.0f, 4.0f, "divider"),
		dod::MenuData(dod::WT_StackPanel, 2, 1.0f, 0.4f, dod::Horizontal),
		dod::MenuData(dod::WT_Spacer,     3, 0.4f, 1.0f),
		dod::MenuData(dod::WT_Image,      3, 0.2f, 1.0f, "bottom"),
	};

	dod::SysWindow::MenuHandle menuHandle = dod::SysWindow::CreateMenu(std::move(menu));
	dod::SysWindow::Layout(menuHandle, 0.0f, 0.0f, 100.0f, 100.0f);

	dod::SysWindow::Print(menuHandle);
	printf("\n\n");

	dod::SysWindow::DestroyMenu(menuHandle);
	//menu.clear();

	std::vector<dod::MenuData> testMenu = CreateTestMenu(50, 50);

	//dod::SysWindow::Reset(100);
	std::cout << "DOD \n";
	RunTest(1, std::cout, DODTest);
	printf("\n\n");

	std::cout << "OOP \n";
	RunTest(1, std::cout, [&testMenu]()->std::vector<double>{return OOPTest(testMenu);}
	);
	printf("\n\n");

	int derp;
	std::cin >> derp;

  return 0;
}

