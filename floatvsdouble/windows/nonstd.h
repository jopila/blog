#pragma once


namespace nonstd
{
	template<typename T>
	class vector : public std::vector<T>
	{
	public:
		void resizeUninitialized(size_t _Newsize)
		{	// determine new length, padding as needed
			if (_Newsize < size())
				_Pop_back_n(size() - _Newsize);
			else if (size() < _Newsize)
			{	// pad as needed
				_Reserve(_Newsize - size());
				_TRY_BEGIN
					//_Uninitialized_default_fill_n(this->_Mylast(), _Newsize - size(),
					//	this->_Getal());
				_CATCH_ALL
					_Tidy();
				_RERAISE;
				_CATCH_END
					this->_Mylast() += _Newsize - size();
			}
		}
	};
};

