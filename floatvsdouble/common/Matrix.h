#pragma once

template <typename T>
class Matrix
{
public:
    Matrix() {}
    Matrix(int)
    {
        memset(data, 0, sizeof(data));
    }

    void Identity()
    {
        T temp[] = { 1,0,0,0,
                     0,1,0,0,
                     0,0,1,0,
                     0,0,0,1 };

        static_assert(sizeof(temp) == sizeof(data), "Sizes mismatch");
        memcpy(data, temp, sizeof(data));
    }

    void Translate(T x, T y, T z)
    {
        T temp[] = { 1,0,0,x,
                     0,1,0,y,
                     0,0,1,z,
                     0,0,0,1 };

        static_assert(sizeof(temp) == sizeof(data), "Sizes mismatch");
        memcpy(data, temp, sizeof(data));
    }

    const Matrix& operator*=(const Matrix& m)
    {
        Matrix result(0);

        for(int y = 0; y < 4; y++)
        for(int x = 0; x < 4; x++)
        {
            for(int i = 0; i < 4; i++)
                result.data[y][x] += data[y][i] * m.data[i][x];
        }

        memcpy(data, result.data, sizeof(data));
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Matrix& m)
    {
        for(int y = 0; y < 4; y++)
        {
            for(int x = 0; x < 4; x++)
            {
                os << m.data[y][x] << " ";
            }
            os << std::endl;
        }

        return os;
    }

private:
    T data[4][4];
};

