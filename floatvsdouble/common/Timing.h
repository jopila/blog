#pragma once

#include <chrono>

using namespace std::chrono;

typedef high_resolution_clock::time_point TimePoint;
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

class Timer
{
public:
    Timer()
		{
			LARGE_INTEGER Frequency;
			// Cycles per second. I don't know why, but QueryPerformanceCounter function wipes out 10 bits of precision. We're using rdtsc directly, so we need the un bitsmashed value
			QueryPerformanceFrequency(&Frequency);
			frequency = Frequency.QuadPart * 1024.0;
		}

    void Start()
    {
			//__cpuid(dummy, dummy2);
      start = __rdtsc();
			//__cpuid(dummy, dummy2);
    }

    double End()
    {
			//__cpuid(dummy, dummy2);
			end = __rdtsc();
			//__cpuid(dummy, dummy2);
			return double(end - start) / frequency;
    }

    static TimePoint CurrentTime()
    {
      return high_resolution_clock::now();
    }

    static double DurationBetweenPoints(TimePoint p1, TimePoint p2)
    {
      return duration_cast<duration<double>>(p2 - p1).count();
    }

private:
  long long start;
	long long end;
	double frequency;
	//int dummy[4];
	//int dummy2;
};
