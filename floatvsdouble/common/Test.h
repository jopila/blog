#pragma once

#include <iostream>
#include <functional>
#include <vector>

inline void RunTest(int runCount, std::ostream& output, std::function<std::vector<double>(void)> func)
{
	for(int i = 0; i < runCount; i++)
	{
		std::vector<double> timings = func();
		for(size_t j = 0; j < timings.size(); j++)
		{
			output << timings[j] * 1'000'000.0 << ",";
		}
		output << "\n";
	}
}

