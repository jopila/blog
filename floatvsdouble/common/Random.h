#pragma once

#include <random>

class Random
{
public:
    static int GetInt();
    static int GetInt(int max);
    static int GetInt(int min, int max);
		static float GetFloat();
		static float GetFloat(float max);
		static float GetFloat(float min, float max);

private:
    static std::random_device rd;
    static std::mt19937 mt;
};

extern Random gRandom;
