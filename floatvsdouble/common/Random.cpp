
#include "Random.h"

Random gRandom;
std::random_device Random::rd;
std::mt19937 Random::mt(rd());

int Random::GetInt()
{
    static std::uniform_int_distribution<int> dist(INT_MIN, INT_MAX);
    return dist(mt);
}

int Random::GetInt(int max)
{
    std::uniform_int_distribution<int> dist(0, max);
    return dist(mt);
}

int Random::GetInt(int min, int max)
{
  std::uniform_int_distribution<int> dist(min, max);
  return dist(mt);
}

float Random::GetFloat()
{
	static std::uniform_real_distribution<float> dist(0.0f, 1.0f);
	return dist(mt);
}

float Random::GetFloat(float max)
{
	std::uniform_real_distribution<float> dist(0.0f, max);
	return dist(mt);
}

float Random::GetFloat(float min, float max)
{
	std::uniform_real_distribution<float> dist(min, max);
	return dist(mt);
}
