
#define _ITERATOR_DEBUG_LEVEL 0
#include "..\utility\stdafx.h"

#include <cassert>
#include <cstdio>
#include <string>
#include <vector>
#include <cmath>
#include <functional>
#include <map>

#include <compressonator.h>

#include "..\Vulkan\File.h"

int main()
{
	std::string textureAssetDirectory = "..\\game\\assets\\textures";

	std::vector<std::string> textureFilenames;// = ListFilesInDirectory(textureAssetDirectory, true);

	CMP_InitFramework();

	for(size_t fileIndex = 0; fileIndex < textureFilenames.size(); fileIndex++)
	{
		printf("Cooking Texture: %s\n", textureFilenames[fileIndex].c_str());
		
		CMP_ERROR cmpStatus;
		CMP_MipSet MipSetIn;
		memset(&MipSetIn, 0, sizeof(CMP_MipSet));
		cmpStatus = CMP_LoadTexture(textureFilenames[fileIndex].c_str(), &MipSetIn);
		if (cmpStatus != CMP_OK)
			printf("Error %d: Loading source file!\n", cmpStatus);

		CMP_GenerateMIPLevels(&MipSetIn, 1);

		KernelOptions options;
		memset(&options, 0, sizeof(KernelOptions));
		options.format = CMP_FORMAT_BC1;
		options.fquality = 1.0f;

		CMP_MipSet MipSetCmp;
		memset(&MipSetCmp, 0, sizeof(CMP_MipSet));

		cmpStatus = CMP_ProcessTexture(&MipSetIn, &MipSetCmp, options, nullptr);
		if (cmpStatus != CMP_OK)
			printf("Error %d: Processing destination file!\n", cmpStatus);

		std::string outputFilename = GetFileNameFromPath(textureFilenames[fileIndex], false);
		outputFilename += ".dds";
		outputFilename = "..\\game\\cooked\\textures\\" + outputFilename;
		cmpStatus = CMP_SaveTexture(outputFilename.c_str(), &MipSetCmp);
		if (cmpStatus != CMP_OK)
			printf("Error %d: Saving file!\n", cmpStatus);

		CMP_FreeMipSet(&MipSetIn);
		CMP_FreeMipSet(&MipSetCmp);
	}

	std::string objAssetDirectory = "..\\game\\assets\\objs";

	std::vector<std::string> objFilenames = ListFilesInDirectory(objAssetDirectory, true);
	for(std::string objFilename : objFilenames)
	{
		std::vector<std::string> lines = ReadFileLines(objFilename);

		struct Pos {float x, y, z;};
		struct Normal {float x, y, z;};
		struct UV {float u, v;};
		struct Vertex
		{
			float x, y, z;
			float nx, ny, nz;
			float u, v;
		};
		std::vector<Pos> positions;  positions.push_back({0.0f, 0.0f, 0.0f});
		std::vector<Normal> normals; normals.push_back({0.0f, 0.0f, 0.0f});
		std::vector<UV> uvs;         uvs.push_back({0.0f, 0.0f});
		std::vector<Vertex> vertices;
		struct VertexPointers
		{
			int32 p, uv, n;
			bool operator<(const VertexPointers& rhs) const
			{
				return p < rhs.p || (p == rhs.p && n < rhs.n) || (p == rhs.p && n == rhs.n && uv < rhs.uv);
			}
		};
		std::map<VertexPointers, int16> uniqueVertices;
		std::vector<VertexPointers> vertexPointers;

		for(size_t i = 0; i < lines.size(); i++)
		{
			/**/ if(lines[i]._Starts_with("# "))      continue;
			else if(lines[i]._Starts_with("mtllib ")) continue;
			else if(lines[i]._Starts_with("usemtl ")) continue;
			else if(lines[i]._Starts_with("o "))      continue;
			else if(lines[i]._Starts_with("s "))      continue;
			else if(lines[i]._Starts_with("v "))
			{
				std::vector<std::string> data = Split(lines[i], ' ');
				Pos pos;
				pos.x = std::stof(data[1]);
				pos.y = std::stof(data[2]);
				pos.z = std::stof(data[3]);
				positions.push_back(pos);
			}
			else if(lines[i]._Starts_with("vt "))
			{
				std::vector<std::string> data = Split(lines[i], ' ');
				UV uv;
				uv.u = std::stof(data[1]);
				uv.v = std::stof(data[2]);
				uvs.push_back(uv);
			}
			else if(lines[i]._Starts_with("vn "))
			{
				std::vector<std::string> data = Split(lines[i], ' ');
				Normal normal;
				normal.x = std::stof(data[1]);
				normal.y = std::stof(data[2]);
				normal.z = std::stof(data[3]);
				normals.push_back(normal);
			}
			else if(lines[i]._Starts_with("f "))
			{
				std::vector<std::string> data = Split(lines[i], ' ');
				std::vector<std::string> v1 = Split(data[1], '/');
				std::vector<std::string> v2 = Split(data[2], '/');
				std::vector<std::string> v3 = Split(data[3], '/');

				auto addData = [&](std::vector<std::string>& raw)
				{
					VertexPointers vp = {atoi(raw[0].c_str()), atoi(raw[1].c_str()), atoi(raw[2].c_str())};
					uniqueVertices[vp] = 0;
					vertexPointers.push_back(vp);
				};

				addData(v1);
				addData(v2);
				addData(v3);
			}
		}

		std::string outputFilename = GetFileNameFromPath(objFilename, false);
		outputFilename += ".mesh";
		outputFilename = "..\\game\\cooked\\meshes\\" + outputFilename;
		FileWriter writer(outputFilename);

		int32 numVerts = (int32)uniqueVertices.size();
		int32 currentVert = 0;
		mWriteRaw(writer, numVerts);
		for(auto& vertex : uniqueVertices)
		{
			Vertex outVertex;
			Pos    pos    = positions[vertex.first.p];  outVertex.x = pos.x; outVertex.y = pos.y; outVertex.z = pos.z;
			UV     uv     = uvs      [vertex.first.uv]; outVertex.u = uv.u; outVertex.v = uv.v;
			Normal normal = normals  [vertex.first.n];  outVertex.nx = normal.x; outVertex.ny = normal.y; outVertex.nz = normal.z;
			mWriteRaw(writer, outVertex);
			vertex.second = currentVert++;
		}
		int32 numIndices = vertexPointers.size();
		mWriteRaw(writer, numIndices);
		for(VertexPointers vp : vertexPointers)
			mWriteRaw(writer, uniqueVertices[vp]);
	}
}
