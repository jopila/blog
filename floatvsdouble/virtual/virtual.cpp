// virtual.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "Object.h"
#include "..\common\Random.h"
#include "..\common\Timing.h"



// Define what sizes to use for the various objects
class TestMediumFew
{
public:
    static constexpr const int sizes[]
    {
        1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Define what sizes to use for the various objects
class TestMediumAverage
{
public:
    static constexpr const int sizes[]
    {
        1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009,
        1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019,
        1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029,
        1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Define what sizes to use for the various objects
class TestMediumMany
{
public:
    static constexpr const int sizes[]
    {
        1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009,
        1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019,
        1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029,
        1030, 1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039,
        1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049,
        1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 1059,
        1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069,
        1070, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1079,
        1080, 1081, 1082, 1083, 1084, 1085, 1086, 1087, 1088, 1089,
        1090, 1091, 1092, 1093, 1094, 1095, 1096, 1097, 1098, 1099,
        1100, 1101, 1102, 1103, 1104, 1105, 1106, 1107, 1108, 1109,
        1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119,
        1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1129,
        1130, 1131, 1132, 1133, 1134, 1135, 1136, 1137, 1138, 1139,
        1140, 1141, 1142, 1143, 1144, 1145, 1146, 1147, 1148, 1149,
        1150, 1151, 1152, 1153, 1154, 1155, 1156, 1157, 1158, 1159,
        1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169,
        1170, 1171, 1172, 1173, 1174, 1175, 1176, 1177, 1178, 1179,
        1180, 1181, 1182, 1183, 1184, 1185, 1186, 1187, 1188, 1189,
        1190, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199,
        1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209,
        1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219,
        1220, 1221, 1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229,
        1230, 1231, 1232, 1233, 1234, 1235, 1236, 1237, 1238, 1239,
        1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247, 1248, 1249,
        1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259,
        1260, 1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1269,
        1270, 1271, 1272, 1273, 1274, 1275, 1276, 1277, 1278, 1279,
        1280, 1281, 1282, 1283, 1284, 1285, 1286, 1287, 1288, 1289,
        1290, 1291, 1292, 1293, 1294, 1295, 1296, 1297, 1298, 1299,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Define what sizes to use for the various objects
class TestSmallFew
{
public:
    static constexpr const int sizes[]
    {
        100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Define what sizes to use for the various objects
class TestSmallAverage
{
public:
    static constexpr const int sizes[]
    {
        100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
        110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
        120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
        130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Define what sizes to use for the various objects
class TestSmallMany
{
public:
    static constexpr const int sizes[]
    {
        100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
        110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
        120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
        130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
        140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
        150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
        160, 161, 162, 163, 164, 165, 166, 167, 168, 169,
        170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
        180, 181, 182, 183, 184, 185, 186, 187, 188, 189,
        190, 191, 192, 193, 194, 195, 196, 197, 198, 199,
        200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
        210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
        220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
        230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
        240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
        250, 251, 252, 253, 254, 255, 256, 257, 258, 259,
        260, 261, 262, 263, 264, 265, 266, 267, 268, 269,
        270, 271, 272, 273, 274, 275, 276, 277, 278, 279,
        280, 281, 282, 283, 284, 285, 286, 287, 288, 289,
        290, 291, 292, 293, 294, 295, 296, 297, 298, 299,
        300, 301, 302, 303, 304, 305, 306, 307, 308, 309,
        310, 311, 312, 313, 314, 315, 316, 317, 318, 319,
        320, 321, 322, 323, 324, 325, 326, 327, 328, 329,
        330, 331, 332, 333, 334, 335, 336, 337, 338, 339,
        340, 341, 342, 343, 344, 345, 346, 347, 348, 349,
        350, 351, 352, 353, 354, 355, 356, 357, 358, 359,
        360, 361, 362, 363, 364, 365, 366, 367, 368, 369,
        370, 371, 372, 373, 374, 375, 376, 377, 378, 379,
        380, 381, 382, 383, 384, 385, 386, 387, 388, 389,
        390, 391, 392, 393, 394, 395, 396, 397, 398, 399,
    };
    static const int n = sizeof(sizes) / sizeof(sizes[0]);
};

// Recursive bit of the builder template. Each 'instance' of this will represent one object size in the Test class T
template<typename T, int N>
struct Builder
{
    // Get the nop size for this object
    static constexpr const int ObjSize = T::sizes[N-1];
    static ObjectBase* BuildCorrectObject(int selection)
    {
        // If the selection matches our size, make it! Otherwise ask the other 'instances' of this template
        if(selection == N)
            return new Object<ObjSize>();
        else
            return Builder<T, N - 1>::BuildCorrectObject(selection);
    }
};

// Base definition for the builder template
template<typename T>
struct Builder<T, 1>
{
    static ObjectBase* BuildCorrectObject(int selection)
    {
        return new Object<T::sizes[0]>();
    }
};

// Kick off function for building our virtual object list
template<typename T>
std::vector<ObjectBase*> BuildVirtualObjectList(int count)
{
    std::vector<ObjectBase*> result;
    for(int i = 0; i < count; i++)
    {
        int newObjIndex = Random::GetInt(T::n);
        result.push_back(Builder<T, T::n>::BuildCorrectObject(newObjIndex));
    }
    return result;
}

// Lists of specific object types. ie, one list for objects with 1000 nops, one list for objects with 1001 nops, etc.
template<int N>
class DODList
{
public:
    static std::vector<Object<N>*> objs;
};

template<int N>
std::vector<Object<N>*> DODList<N>::objs;

// Sort a generic object into a list of specific objects types (a form of bucketing)
template<typename T, int N>
struct Sorter
{
    static constexpr const int ObjSize = T::sizes[N - 1];
    static void AddObjectToCorrectList(ObjectBase* obj, int nopCount)
    {
        if(nopCount == ObjSize)
            DODList<ObjSize>::objs.push_back(dynamic_cast<Object<ObjSize>*>(obj));
        else
            Sorter<T, N - 1>::AddObjectToCorrectList(obj, nopCount);
    }
};

// Base case for above
template<typename T>
struct Sorter<T, 1>
{
    static void AddObjectToCorrectList(ObjectBase* obj, int nopCount)
    {
        DODList<T::sizes[0]>::objs.push_back(dynamic_cast<Object<T::sizes[0]>*>(obj));
    }
};

// Sort each virtual object into lists for each specific object type
template<typename T>
void BuildDODObjectLists(std::vector<ObjectBase*> virtualObjs)
{
    for(size_t i = 0; i < virtualObjs.size(); i++)
    {
        Sorter<T, T::n>::AddObjectToCorrectList(virtualObjs[i], virtualObjs[i]->nopCount());
    }
}

// Update each object in each DOD list
template<typename T, int N>
struct Updater
{
    static constexpr const int ObjSize = T::sizes[N - 1];
    static void UpdateLists()
    {
        for(size_t i = 0; i < DODList<ObjSize>::objs.size(); i++)
        {
            DODList<ObjSize>::objs[i]->update(0.0f);
        }
        Updater<T, N - 1>::UpdateLists();
    }
};

// Base case for above
template<typename T>
struct Updater<T, 1>
{
    static void UpdateLists()
    {
        for(size_t i = 0; i < DODList<T::sizes[0]>::objs.size(); i++)
        {
            DODList<T::sizes[0]>::objs[i]->update(0.0f);
        }
    }
};

// Kickoff function for above. Might be unnecessary but I'm new to TMP and this works ;)
template<typename T>
void UpdateDODObjectLists()
{
    Updater<T, T::n>::UpdateLists();
}

// Clear each DODList
template<typename T, int N>
struct Clearer
{
    static constexpr const int ObjSize = T::sizes[N - 1];
    static void ClearLists()
    {
        DODList<ObjSize>::objs.clear();
        Clearer<T, N - 1>::ClearLists();
    }
};

// Base case for above
template<typename T>
struct Clearer<T, 1>
{
    static void ClearLists()
    {
        DODList<T::sizes[0]>::objs.clear();
    }
};

// Kickoff function for above. Might be unnecessary but I'm new to TMP and this works ;)
template<typename T>
void ClearDODObjectLists()
{
    Clearer<T, T::n>::ClearLists();
}

template<typename T>
void RunTest()
{
    std::cout << "Running test: " << typeid(T).name() << std::endl;

    // Virtual test
    const int NUM_OBJECTS = 10000;
    std::vector<ObjectBase*> objs = BuildVirtualObjectList<T>(NUM_OBJECTS);

    Timer timer;
    timer.Start();
    for(int i = 0; i < NUM_OBJECTS; i++)
    {
        objs[i]->update(0.0f);
    }
    double virutalTime = timer.End();
    std::cout << "Virtual Time: " << virutalTime * 1000000 << std::endl;

    // DOD test
    BuildDODObjectLists<T>(objs);
    timer.Start();
    UpdateDODObjectLists<T>();
    double DODTime = timer.End();
    std::cout << "DOD Time: " << DODTime * 1000000 << std::endl;

    // Cleanup
    for(ObjectBase* obj : objs)
    {
       delete obj;
    }
    ClearDODObjectLists<T>();
}

int main()
{
    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    RunTest<TestSmallFew>();
    RunTest<TestSmallAverage>();
    RunTest<TestSmallMany>();
    std::cout << std::endl;
    RunTest<TestMediumFew>();
    RunTest<TestMediumAverage>();
    RunTest<TestMediumMany>();

    // Wait for input...
    int dummy;
    std::cin >> dummy;

    return 0;
}

