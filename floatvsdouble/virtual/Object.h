#pragma once


#include <intrin.h>

class ObjectBase
{
public:
    virtual void update(float t) = 0;
    virtual int nopCount() = 0;
};

template<int NOP_COUNT>
__forceinline void DoNop()
{
    __nop();
    DoNop<NOP_COUNT - 1>();
}

template<>
__forceinline void DoNop<1>()
{
    __nop();
}

template<int NOP_COUNT>
class Object : public ObjectBase
{
public:
    void update(float t)
    {
        DoNop<NOP_COUNT>();
    }

    int nopCount()
    {
        return NOP_COUNT;
    }
};
