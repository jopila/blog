﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerfTools2
{
  public partial class ProcessInfo
  {
    public string name { get; set; }
    public int id { get; set; }
    public int symbolCount { get; set; }
    private Process process = null;
    IntPtr processHandle;

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    struct Timestamp
    {
      public UInt64 address;
      public UInt64 time;
      public UInt32 coreId;
      public UInt32 threadId;
    };
    private const UInt32 sizeofTimestamp = sizeof(UInt64) + sizeof(UInt64) + sizeof(UInt32) + sizeof(UInt32);

    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    struct SharedMemoryInfo
    {
      public UInt32 injectedProcessId;
      public UInt32 command;
      public UInt64 commandParameter;
      public UInt64 currentBufferWritePosition;
      public UInt64 currentBufferReadPosition;
    };
    private const int commandOffset = 4;
    private const int commandParameterOffset = 8;
    private const int currentBufferWritePositionOffset = 16;
    private const int currentBufferReadPositionOffset = 24;
    private const int timestampBufferOffset = 32;
    private const UInt32 sizeofSharedMemoryInfo = sizeof(UInt32) + sizeof(UInt32) + sizeof(UInt64) + sizeof(UInt64);
    private const UInt32 fileMapBufferSize = 1024 * 1024;
    private const UInt32 fileMapSize = sizeofSharedMemoryInfo + fileMapBufferSize * sizeofTimestamp;

    class ModuleInfo
    {
      public string name;
      public UInt64 begin;
      public UInt64 end;
      public UInt64 jumpTable;
    };

    private List<ModuleInfo> modules = new List<ModuleInfo>();

    class ThreadInfo
    {
      public Int32 id;
      public IntPtr handle;
      public UInt64 stackSize;
      public UInt64[] stackMemory;
      public UInt64 rip;
      public UInt64 rsp;
    };
    private List<ThreadInfo> threads = new List<ThreadInfo>();

    public class SymbolInfo
    {
      public string name { get; set; }
      public UInt64 address { get; set; }
      public UInt64 size { get; set; }
      private bool _isCaptured;
      public bool isCaptured { get { return _isCaptured; } set { _isCaptured = value; OnIsCapturedChanged.Invoke(this); } }
      public void forceIsCaptured(bool value) { _isCaptured = value; }
      public delegate void IsCapturedChanged(SymbolInfo info);
      public static IsCapturedChanged OnIsCapturedChanged;
    }
    class FunctionInfo : SymbolInfo
    {
      public byte[] code;
      public List<byte> detouredCode;
      public List<Modification> modifications;
    };
    SortedList<UInt64, FunctionInfo> addressToFunction = new SortedList<UInt64, FunctionInfo>();
    Dictionary<UInt64, FunctionInfo> detourAddressToFunction = new Dictionary<UInt64, FunctionInfo>();
    public List<SymbolInfo> symbolInfos = new List<SymbolInfo>();

    class Modification
    {
      public UInt64 address;
      public Int64 size;
      public bool isDetour;
      public bool isRipPromotion;
    };
    class SafetyInfo
    {
      // Written by ensure function
      public List<UInt64> addressesOnStackThatNeedsToChange = new List<UInt64>();
      public List<IntPtr> threadHandlesWithRipsThatNeedsToChange = new List<IntPtr>();

      // Written by caller
      public List<Modification> modifications;
      public enum Mode { Capture, Release }
      public Mode ensureMode;
    };

    public class SourceInfo
    {
      public UInt64 address;
      public string filename;
      public int lineNumber;
    };


    private MemoryMappedFile memoryFile;
    private MemoryMappedViewAccessor memoryAccessor;

    UInt64 functionTime;

    ~ProcessInfo() { if(processHandle != null) Natives.CloseHandle(processHandle); }

    //=========================================================================
    // Simple info about a process so the user can determine what to capture
    int numSymbols = 0;
    bool EnumSymProc(ref Natives.SYMBOL_INFO symInfo, UInt32 SymbolSize, IntPtr UserContext)
    {
      if(symInfo.Tag == Natives.SymTagEnum.Function)
        numSymbols++;
      return true;
    }
    public bool InitializeProcessInfo(Process inProcess)
    {
      process = inProcess;
      IntPtr processHandle = Natives.OpenProcess(Natives.ProcessAccessFlags.All, false, (uint)process.Id);
      if(processHandle == IntPtr.Zero)
        return false;

      Natives.SymSetOptions(Natives.SymOpt.DEFERRED_LOADS);
      if(!Natives.SymInitialize(processHandle, null, true))
      {
        Natives.SymCleanup(processHandle);
        Natives.CloseHandle(processHandle);
        return false;
      }
      numSymbols = 0;
      Natives.SymEnumSymbols(processHandle, 0, process.ProcessName + "!*", new Natives.SymEnumSymbolsProc(EnumSymProc), IntPtr.Zero);

      Natives.SymCleanup(processHandle);
      Natives.CloseHandle(processHandle);

      if(numSymbols > 0)
      {
        name = process.ProcessName;
        id = process.Id;
        symbolCount = numSymbols;
      }

      return numSymbols > 0;
    }

    //=========================================================================
    // Full symbol info
    Dictionary<UInt64, FunctionInfo> tempAddressToFunction = new Dictionary<ulong, FunctionInfo>();
    bool EnumSymProcFull(ref Natives.SYMBOL_INFO symInfo, UInt32 SymbolSize, IntPtr UserContext)
    {
      if(symInfo.Tag == Natives.SymTagEnum.Function)
      {
        if(tempAddressToFunction.ContainsKey(symInfo.Address))
          tempAddressToFunction[symInfo.Address].name += ";" + symInfo.Name;
        else
          tempAddressToFunction.Add(symInfo.Address, new FunctionInfo { name = symInfo.Name, address = symInfo.Address, size = SymbolSize });
      }
      return true;
    }
    private bool GetProcessSymobls(string filter)
    {
      Natives.SymSetOptions(Natives.SymOpt.DEFERRED_LOADS);
      if(!Natives.SymInitialize(processHandle, null, true))
      {
        Natives.SymCleanup(processHandle);
        return false;
      }
      symbolInfos.Clear();
      Natives.SymEnumSymbols(processHandle, 0, filter, new Natives.SymEnumSymbolsProc(EnumSymProcFull), IntPtr.Zero);

      Natives.SymCleanup(processHandle);

      addressToFunction = new SortedList<ulong, FunctionInfo>(tempAddressToFunction);
      foreach(FunctionInfo info in addressToFunction.Values)
        symbolInfos.Add(info);

      return symbolInfos.Count > 0;
    }

    //=========================================================================
    // get the process ready for capturing and get all needed information from it
    public bool AttachToProcess()
    {
      processHandle = Natives.OpenProcess(Natives.ProcessAccessFlags.All, false, (uint)process.Id);
      if(processHandle == IntPtr.Zero)
        return false;

      InjectDLL();
      CaptureModules();
      GetProcessSymobls("*!*");
      return true;
    }

    
    private bool InjectDLL()
    {
      // Create shared memory
      memoryFile = MemoryMappedFile.CreateOrOpen("perfToolSharedMemory", fileMapSize, MemoryMappedFileAccess.ReadWrite);
      memoryAccessor = memoryFile.CreateViewAccessor(0, fileMapSize);

      memoryAccessor.WriteArray(0, new byte[fileMapSize], 0, (int)fileMapSize);

      byte[] dllPath = Encoding.ASCII.GetBytes(Path.GetFullPath(@"..\x64\Release\PerfInjector.dll"));
      IntPtr pszLibFileString = Natives.VirtualAllocEx(processHandle, IntPtr.Zero, (uint)dllPath.Length + 1, Natives.AllocationType.MEM_COMMIT, Natives.PageAccess.PAGE_READWRITE);
      IntPtr dummy;
      Natives.WriteProcessMemory(processHandle, pszLibFileString, dllPath, (uint)dllPath.Length + 1, out dummy);
      IntPtr pfnLoadLibrary = Natives.GetProcAddress(Natives.GetModuleHandle("Kernel32"), "LoadLibraryA");
      Natives.CreateRemoteThread(processHandle, IntPtr.Zero, 0, pfnLoadLibrary, pszLibFileString, 0, IntPtr.Zero);

      SharedMemoryInfo sharedInfo = new SharedMemoryInfo();
      while(sharedInfo.injectedProcessId != process.Id)
        memoryAccessor.Read(0, out sharedInfo);

      return true;
    }

    private bool EnumSymbolTime(ref Natives.SYMBOL_INFO symInfo, UInt32 SymbolSize, IntPtr UserContext)
    {
      functionTime = symInfo.Address;
      return true;
    }
    private bool CaptureModules()
    {
      if(!Natives.SymInitialize(processHandle, null, true))
      {
        Console.WriteLine("Error with SymInitialize...everything might not work!");
        Natives.SymCleanup(processHandle);
        return false;
      }

      functionTime = 0;
      Natives.SymEnumSymbols(processHandle, 0, "PerfInjector!PerfInjectorTime", EnumSymbolTime, IntPtr.Zero);
      if(functionTime == 0)
      {
        Console.WriteLine("Error getting timing function...everything might not work!");
        Natives.SymCleanup(processHandle);
        return false;
      }
      
      // Get modules and form our jump table
      ProcessModuleCollection processModules = process.Modules;
      foreach(ProcessModule module in processModules)
      {
        
        ModuleInfo newModule = new ModuleInfo();
        newModule.name = module.FileName;
        newModule.begin = (UInt64)module.BaseAddress;
        newModule.end = newModule.begin + (UInt64)module.ModuleMemorySize;


        Natives.MEMORY_BASIC_INFORMATION memInfo;
        Natives.VirtualQueryEx(processHandle, (IntPtr)newModule.end, out memInfo, (uint)Marshal.SizeOf(typeof(Natives.MEMORY_BASIC_INFORMATION)));
        
        // Attempt to make a jump table (first attempt is to use the end of the module memory, assuming null values at the end are unused)
        bool success = false;
        byte[] buf = { 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff, 0x99, 0x88 };
        byte[] functionTimeBytes = BitConverter.GetBytes(functionTime);
        IntPtr numBytes;
        Natives.ReadProcessMemory(processHandle, (IntPtr)(newModule.end - 8), buf, 8, out numBytes);
        if(buf[0] == 0 && buf[1] == 0 && buf[2] == 0 && buf[3] == 0 && buf[4] == 0 && buf[5] == 0 && buf[6] == 0 && buf[7] == 0)
        {
          newModule.jumpTable = newModule.end - 8;
          bool ok;
          Natives.PageAccess prevAccess;
          ok = Natives.VirtualProtectEx(processHandle, (IntPtr)newModule.jumpTable, IntPtr.Size, Natives.PageAccess.PAGE_READWRITE, out prevAccess);
          ok = ok && Natives.WriteProcessMemory(processHandle, (IntPtr)newModule.jumpTable, functionTimeBytes, (uint)IntPtr.Size, out numBytes);
          if(ok)
          {
            modules.Add(newModule);
            success = true;
          }
        }
        // Second attempt, allocating memory after the end of the module
        if(!success)
        {
          success = false;
          UInt64 jumpTable = ((newModule.end - 1) & ~(UInt64)0xffff) + 0x10000;
          jumpTable = (UInt64)Natives.VirtualAllocEx(processHandle, (IntPtr)jumpTable, (uint)IntPtr.Size,
            Natives.AllocationType.MEM_RESERVE | Natives.AllocationType.MEM_COMMIT, Natives.PageAccess.PAGE_READWRITE);
          if(jumpTable != 0)
          {
            if(Natives.WriteProcessMemory(processHandle, (IntPtr)jumpTable, functionTimeBytes, (uint)IntPtr.Size, out numBytes))
            {
              success = true;
              newModule.jumpTable = jumpTable;
              modules.Add(newModule);
            }
          }
          if(!success)
            Console.WriteLine(string.Format("Failed to allocate memory for a jumptable '{0}'", newModule.name));
        }
      }

      Natives.SymCleanup(processHandle);
      return true;
    }


    //=========================================================================
    // Inject our assembly to capture functions
    private void StartDetouring()
    {
      // Suspend the app whole-sale real quick
      Natives.NtSuspendProcess(processHandle);

      // List out all our threads
      threads.Clear();
      foreach(ProcessThread thread in process.Threads)
      {
        ThreadInfo newThread = new ThreadInfo();
        newThread.handle = Natives.OpenThread(Natives.ThreadAccess.ALL, true, (uint)thread.Id);
        newThread.id = thread.Id;

        Natives.CONTEXT64 context = new Natives.CONTEXT64();
        context.ContextFlags = Natives.CONTEXT_FLAGS.CONTEXT_ALL;
        if(!Natives.GetThreadContext(newThread.handle, ref context))
          continue;

        newThread.rip = context.Rip;
        newThread.rsp = context.Rsp;

        threads.Add(newThread);
      }

      for(int i = 0; i < threads.Count; i++)
      {
        // Get the thread's stack info so we can read the whole stack
        Natives.THREAD_BASIC_INFORMATION basicInfo = new Natives.THREAD_BASIC_INFORMATION();
        Natives.NtQueryInformationThread(threads[i].handle, 0, out basicInfo, Marshal.SizeOf<Natives.THREAD_BASIC_INFORMATION>(), IntPtr.Zero);
        byte[] buf = new byte[Marshal.SizeOf<Natives.NT_TIB>()];
        IntPtr bytesRead;
        if(!Natives.ReadProcessMemory(processHandle, basicInfo.TebBaseAddress, buf, Marshal.SizeOf<Natives.NT_TIB>(), out bytesRead))
        {
          threads.RemoveAt(i--);
          continue;
        }
        Natives.NT_TIB tib = Natives.Deserialize<Natives.NT_TIB>(buf);
        threads[i].stackSize = (UInt64)tib.StackBase - threads[i].rsp;
        byte[] stackMemoryBuf = new byte[threads[i].stackSize];
        Natives.ReadProcessMemory(processHandle, (IntPtr)threads[i].rsp, stackMemoryBuf, stackMemoryBuf.Count(), out bytesRead);
        threads[i].stackMemory = new UInt64[stackMemoryBuf.Count() / sizeof(UInt64)];
        Buffer.BlockCopy(stackMemoryBuf, 0, threads[i].stackMemory, 0, stackMemoryBuf.Count());
      }
    }

    private SafetyInfo StartFunctionDetour(FunctionInfo functionInfo)
    {
	    SafetyInfo safety = new SafetyInfo();
      
	    for(int i = 0; i < threads.Count(); i++)
      {
        ThreadInfo info = threads[i];
		    // And then go by each value on the stack and if it's within our function's address, it's probably a return address and we need to be careful
		    UInt64 functionSize = functionInfo.detouredCode != null && functionInfo.detouredCode.Count() > 0 ? (UInt64)functionInfo.detouredCode.Count() : functionInfo.size;
		    UInt64 stackElements = info.stackSize / sizeof(UInt64);
		    UInt64 functionBegin = functionInfo.address;
		    UInt64 functionEnd = functionInfo.address + functionSize;
        int length = info.stackMemory.Count();
		    for(int j = 0; j < length; j++)
		    {
          UInt64 stackValue = info.stackMemory[j];
			    if(stackValue >= functionBegin && stackValue < functionEnd)
				    safety.addressesOnStackThatNeedsToChange.Add(info.rsp + (UInt64)(sizeof(UInt64) * j));
		    }
		    // Also if the instruction pointer is already in our function...
		    if(info.rip >= functionInfo.address && info.rip < functionInfo.address + functionSize)
			    safety.threadHandlesWithRipsThatNeedsToChange.Add(info.handle);
      }
      
	    return safety;
    }

    private void FinishFunctionDetour(bool success, SafetyInfo safetyInfo)
    {
      if(success)
	    {
        Func<Int64, Int64> CalculateOffset = (Int64 addr) =>
        {
          Int64 offset = 0;
          for(int j = 0; j < safetyInfo.modifications.Count(); j++)
          {
            Int64 detourAddr = (Int64)safetyInfo.modifications[j].address;
            if(safetyInfo.modifications[j].isDetour)
            {
              if(addr < detourAddr)
                break;
              //else if(addr == detourAddr)
              //  Console.WriteLine("Error: Releasing a function while it's recording an event...this isn't handled yet");
              else
                offset += safetyInfo.ensureMode == SafetyInfo.Mode.Capture ? safetyInfo.modifications[j].size : -safetyInfo.modifications[j].size;
            }
            else if(safetyInfo.modifications[j].isRipPromotion)
            {
              if(addr >= detourAddr + safetyInfo.modifications[j].size)
                offset += safetyInfo.ensureMode == SafetyInfo.Mode.Capture ? safetyInfo.modifications[j].size : -safetyInfo.modifications[j].size;
              else
                break;
            }
          }
          return offset;
        };

		    for(int i = 0; i < safetyInfo.addressesOnStackThatNeedsToChange.Count(); i++)
		    {
          IntPtr addr = (IntPtr)safetyInfo.addressesOnStackThatNeedsToChange[i];
          byte[] stackValueBytes = new byte[sizeof(Int64)];
          IntPtr bytesRead;
          Natives.ReadProcessMemory(processHandle, addr, stackValueBytes, sizeof(byte) * stackValueBytes.Count(), out bytesRead);
          Int64 stackValue = Natives.Deserialize<Int64>(stackValueBytes);
          Int64 offset = CalculateOffset(stackValue);
			    stackValue += offset;
          stackValueBytes = Natives.Serialize<Int64>(stackValue);
			    Natives.WriteProcessMemory(processHandle, addr, stackValueBytes, (UInt32)(sizeof(byte) * stackValueBytes.Count()), out bytesRead);
		    }
		    for(int i = 0; i < safetyInfo.threadHandlesWithRipsThatNeedsToChange.Count(); i++)
		    {
			    Natives.CONTEXT64 context = new Natives.CONTEXT64();
			    context.ContextFlags = Natives.CONTEXT_FLAGS.CONTEXT_ALL;
			    Natives.GetThreadContext(safetyInfo.threadHandlesWithRipsThatNeedsToChange[i], ref context);
          Int64 offset = CalculateOffset((Int64)context.Rip);
			    context.Rip += (UInt64)offset;
			    Natives.SetThreadContext(safetyInfo.threadHandlesWithRipsThatNeedsToChange[i], ref context);
		    }
      }
    }
    
    public void FinishDetouring()
    {
      for(int i = 0; i < threads.Count(); i++)
        Natives.CloseHandle(threads[i].handle);

      Natives.NtResumeProcess(processHandle);
    }

    //=========================================================================
    // Actual function injection logic
    public static Int32 detourCallInstructionSize = 6;
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct CallRipInstruction
    {
      public CallRipInstruction(UInt64 _offset) { opcode = 0xff; opcode2 = 0x15; offset = (UInt32)_offset; padding = 0x9090; }
      public byte opcode; //0xff call instruction
      public byte opcode2; //0x15 modrm byte indicating rip relative
      public UInt32 offset; // rip offset
      public UInt16 padding; // nops to keep 8 byte instruction alignment
    };

    class InstructionMetaData
    {
      public bool needsDetour;
      public bool needsRipPromotion;
      public Int64 additionalOffset;

      //rip relative stuff
      public Int64 oldInstructionOffset;
      public Int64 ripRelativeOffsetOffset;
      public Assembler.OperandSize ripRelativeSize;
      public Int64 newRipRelativeOffset;
    };

    private bool InjectFunction(FunctionInfo functionInfo, SafetyInfo safety, UInt64 nextFunctionOffset)
    {
      //Console.WriteLine(string.Format("Injecting function '{0}' ({1})", functionInfo.name, functionInfo.address.ToString("X16")));

      // Lazy failsafe to allow redundant captures
      if(functionInfo.detouredCode != null && functionInfo.detouredCode.Count() > 0)
		    return false;

	    // Get our detour function's addresses
	    UInt64 jumpTable = 0;
	    for(int i = 0; i < modules.Count(); i++)
	    {
		    if(functionInfo.address >= modules[i].begin && functionInfo.address < modules[i].end)
		    {
			    jumpTable = modules[i].jumpTable;
			    break;
		    }
	    }
	    if(jumpTable == 0)
	    {
		    Console.WriteLine(string.Format("Module info not found for '{0}' ({1})", functionInfo.name, functionInfo.address.ToString("X16")));
		    return false;
	    }

      // Inject timing code
	    functionInfo.code = new byte[functionInfo.size];
      IntPtr numBytes;
	    Natives.ReadProcessMemory(processHandle, (IntPtr)functionInfo.address, functionInfo.code, (int)functionInfo.size, out numBytes);

	    // Start moving over the function
	    List<Assembler.Instruction> instructions = new List<Assembler.Instruction>();
      List<InstructionMetaData> metaData = new List<InstructionMetaData>();
	    Dictionary<Int64, Int64> offsetToInstructionIndex = new Dictionary<Int64, Int64>();
	    Int64 additionalOffset = Marshal.SizeOf<CallRipInstruction>();
      try
      {
        Assembler.Disassemble(functionInfo.code, out instructions);
      }
      catch(Exception e)
      {
        Console.WriteLine("{0} in '{1}' (0x{2}): {3}", "Exception trying to disassemble function", functionInfo.name, functionInfo.address.ToString("X16"), e);
        return false;
      }

      Int64 functionSizeSoFar = 0;
      for(int i = 0; i < instructions.Count(); i++)
	    {
        Assembler.Instruction instr = instructions[i];
        functionSizeSoFar += instr.bytes.Count();
        string failureReason = "";
        if(i == instructions.Count() - 1 && functionSizeSoFar != (Int64)functionInfo.size)
          failureReason = "Unrecognized instruction";
        else
        {
          InstructionMetaData newMetaData = new InstructionMetaData();

          newMetaData.needsDetour = instr.detourWorthy;
          if(instr.jumpToRegisterAddress)
          {
            if(i == instructions.Count() - 1)
              newMetaData.needsDetour = true; // make a special case for what we're assuming to be tail end virtual calls
            else
              failureReason = "Jump via register not supported (probably cuased by a jump-table/switch-statement)";
          }
          if(instr.detourWorthy && instr.conditionalJump)
          {
            failureReason = "Conditional jumps out of function not supported.";
          }

          if(failureReason == "")
          {
            if(newMetaData.needsDetour)
              additionalOffset += Marshal.SizeOf<CallRipInstruction>();
            newMetaData.needsRipPromotion = false;
            newMetaData.additionalOffset = additionalOffset;
            newMetaData.oldInstructionOffset = functionSizeSoFar;
            offsetToInstructionIndex.Add(functionSizeSoFar - instr.bytes.Count(), i);
            newMetaData.ripRelativeOffsetOffset = instr.ripRelativeOffsetOffset;
            newMetaData.ripRelativeSize = instr.ripRelativeSize;
            newMetaData.newRipRelativeOffset = 0;
            metaData.Add(newMetaData);
          }
        }

        if(failureReason != "")
        {
          Int64 failedOffset = functionSizeSoFar;
          Console.WriteLine("{0} in '{1}' (0x{2}+0x{3}):\n{{", failureReason, functionInfo.name, functionInfo.address.ToString("X16"), failedOffset.ToString("X16"));
          string bytesToPrint = "";
          for(int j = 0; j < instr.bytes.Count(); j++)
            bytesToPrint += instr.bytes[j].ToString("X2") + " ";
          Console.WriteLine("{0}\n}}", bytesToPrint);
          functionInfo.code = null;
          functionInfo.detouredCode = null;
          return false;
        }
      }

      bool needsAnotherPass = true;
      while(needsAnotherPass)
	    {
		    needsAnotherPass = false;
		    Int64 newAdditionalOffset = Marshal.SizeOf<CallRipInstruction>();
        for(int i = 0; i < instructions.Count(); i++)
		    {
          if(metaData[i].needsDetour)
				    newAdditionalOffset += Marshal.SizeOf<CallRipInstruction>();

          if(metaData[i].ripRelativeSize != Assembler.OperandSize.opNone)
			    {
				    Int32 rel;
            rel = Assembler.GetOperandFromBytes(instructions[i].bytes.ToArray(), (int)metaData[i].ripRelativeOffsetOffset, metaData[i].ripRelativeSize);

				    Int64 adjustment = 0;
				    Int64 ripDestination = rel + metaData[i].oldInstructionOffset;
				    /**/ if(ripDestination == 0 && instructions[i].bytes[0] == 0xe8)
					    adjustment = 0 - metaData[i].additionalOffset;
				    else if(ripDestination < 0)
					    adjustment = -metaData[i].additionalOffset;
				    else if(ripDestination >= (Int64)functionInfo.size)
					    adjustment = -metaData[i].additionalOffset;
				    else
            {
              int destInstructionIndex = (int)offsetToInstructionIndex[ripDestination];
              // If we're jumping to a rip promoted instruction it has too much of an additional offset
              // since we're jumping to the beginning of the instruction instead of the end. Use the previous
              // instruction's additional offset to compensate.
              if(rel > 0 && metaData[destInstructionIndex].needsRipPromotion)
                adjustment = metaData[destInstructionIndex - 1].additionalOffset - metaData[i].additionalOffset;
              else
                adjustment = metaData[destInstructionIndex].additionalOffset - metaData[i].additionalOffset;

              // Somehow I managed to not set this for the first instruction which is good for us as some
              // functions will loop back to the beginning. With that said, we need to account for the rest of 
              // the detoured instructions which still need to hit the detour as well as the moved instruction.
              if(metaData[destInstructionIndex].needsDetour)
                adjustment -= Marshal.SizeOf<CallRipInstruction>();
            }

				    rel += (Int32)adjustment;

				    bool rel8NeedsPromotion = metaData[i].ripRelativeSize == Assembler.OperandSize.op8 && (rel >= 0x80 || rel <= -0x80);
				    bool rel16NeedsPromotion = metaData[i].ripRelativeSize == Assembler.OperandSize.op16 && (rel >= 0x8000 || rel <= -0x8000);
				    if((rel8NeedsPromotion || rel16NeedsPromotion))
				    {
					    needsAnotherPass = needsAnotherPass || !metaData[i].needsRipPromotion;
					    metaData[i].needsRipPromotion = true;
					    /**/ if(rel8NeedsPromotion  && (instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)) newAdditionalOffset += 4;
					    else if(rel8NeedsPromotion  && (instructions[i].bytes[0] == 0xeb))                                     newAdditionalOffset += 3;
					    else if(rel16NeedsPromotion && (instructions[i].bytes[0] >= 0x70 && instructions[i].bytes[0] <= 0x7f)) newAdditionalOffset += 1;
				    }

				    metaData[i].newRipRelativeOffset = rel;
			    }
			    metaData[i].additionalOffset = newAdditionalOffset;
		    }
	    }

      functionSizeSoFar = 0;
      Int64 newFunctionSizeSoFar = 0;
      List<Assembler.Instruction> newInstructions = new List<Assembler.Instruction>();
      functionInfo.modifications = new List<Modification>();
      safety.modifications = new List<Modification>();
      Dictionary<UInt64, FunctionInfo> tempDetourAddressToFunction = new Dictionary<ulong, FunctionInfo>();
      Action<bool> AddDetour = (bool begin) =>
	    {
		    UInt64 rip = functionInfo.address + (UInt64)newFunctionSizeSoFar + (UInt64)detourCallInstructionSize;
        CallRipInstruction callInstr = new CallRipInstruction(jumpTable - rip);
        Assembler.Instruction instr = new Assembler.Instruction();
        instr.bytes = Natives.Serialize(callInstr).ToList();
        newInstructions.Add(instr);
        tempDetourAddressToFunction.Add(rip, functionInfo);


        functionInfo.modifications.Add(new Modification{address = functionInfo.address + (UInt64)newFunctionSizeSoFar, size = Marshal.SizeOf<CallRipInstruction>(), isDetour = true, isRipPromotion = false});
		    safety.modifications.Add(new Modification{address = functionInfo.address + (UInt64)functionSizeSoFar, size = Marshal.SizeOf<CallRipInstruction>(), isDetour = true, isRipPromotion = false });
        newFunctionSizeSoFar += instr.bytes.Count();
      };
	    AddDetour(true);

	    for(int instrIndex = 0; instrIndex< instructions.Count(); instrIndex++)
	    {
		    if(metaData[instrIndex].needsDetour)
		    {
			    AddDetour(false);
		    }

		    if(metaData[instrIndex].needsRipPromotion)
		    {
			    // convert smaller conditional jump instructions into 32 bit conditional jumps
			    bool successfulPromote = true;
			    Int32 instructionSizeOffset = 0;
			    if(metaData[instrIndex].ripRelativeSize == Assembler.OperandSize.op8)
			    {
				    if(instructions[instrIndex].bytes[0] >= 0x70 && instructions[instrIndex].bytes[0] <= 0x7f)
				    {
					    instructions[instrIndex].bytes = new List<byte> { 0x0f, (byte)(instructions[instrIndex].bytes[0] + 0x10), 0, 0, 0, 0 };
					    metaData[instrIndex].ripRelativeOffsetOffset = 2;
					    instructionSizeOffset = 4;
				    }
				    else if(instructions[instrIndex].bytes[0] == 0xeb)
				    {
					    instructions[instrIndex].bytes = new List<byte> { 0xe9, 0, 0, 0, 0 };
					    instructionSizeOffset = 3;
				    }
				    else
					    successfulPromote = false;
			    }
			    if(metaData[instrIndex].ripRelativeSize == Assembler.OperandSize.op16)
			    {
				    if(instructions[instrIndex].bytes[0] >= 0x70 && instructions[instrIndex].bytes[0] <= 0x7f)
				    {
					    instructions[instrIndex].bytes = new List<byte> { 0x0f, instructions[instrIndex].bytes[2], 0, 0, 0, 0 };
					    metaData[instrIndex].ripRelativeOffsetOffset = 2;
					    instructionSizeOffset = 1;
				    }
				    else
					    successfulPromote = false;
			    }

			    if(!successfulPromote)
			    {
            string addressString = functionInfo.address.ToString("X16");
            string offsetString = (functionSizeSoFar - instructions[instrIndex].bytes.Count()).ToString("X8");
            Console.WriteLine("Unhandled rip promotion in '{0}' (0x{1}+0x{2}):\n{{", functionInfo.name, addressString, offsetString);
				    for(int i = 0; i < instructions[instrIndex].bytes.Count(); i++)
              Console.WriteLine("{0} ", instructions[instrIndex].bytes[i].ToString("X2"));
            Console.WriteLine("}}\n");
				    functionInfo.code = null;
				    functionInfo.detouredCode = null;
				    return false;
			    }

			    //This instruction is now at it's final form!
			    metaData[instrIndex].ripRelativeSize = Assembler.OperandSize.op32;

			    functionInfo.modifications.Add(new Modification{address = functionInfo.address + (UInt64)newFunctionSizeSoFar + (UInt64)instructions[instrIndex].bytes.Count(), size = instructionSizeOffset, isDetour = false, isRipPromotion = true});
			    safety.modifications.Add(new Modification{ address = functionInfo.address + (UInt64)functionSizeSoFar, size = instructionSizeOffset, isDetour =  false, isRipPromotion = true });
		    }

		    if(metaData[instrIndex].ripRelativeSize != Assembler.OperandSize.opNone)
		    {
			    if(metaData[instrIndex].ripRelativeSize == Assembler.OperandSize.op8)
				    instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset] = (byte)metaData[instrIndex].newRipRelativeOffset;
          if(metaData[instrIndex].ripRelativeSize == Assembler.OperandSize.op16)
          {
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset    ] = (byte)( metaData[instrIndex].newRipRelativeOffset       & 0xff);
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset + 1] = (byte)((metaData[instrIndex].newRipRelativeOffset >> 8) & 0xff);
          }
          if(metaData[instrIndex].ripRelativeSize == Assembler.OperandSize.op32)
          {
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset    ] = (byte)( metaData[instrIndex].newRipRelativeOffset        & 0xff);
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset + 1] = (byte)((metaData[instrIndex].newRipRelativeOffset >>  8) & 0xff);
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset + 2] = (byte)((metaData[instrIndex].newRipRelativeOffset >> 16) & 0xff);
            instructions[instrIndex].bytes[(Int32)metaData[instrIndex].ripRelativeOffsetOffset + 3] = (byte)((metaData[instrIndex].newRipRelativeOffset >> 24) & 0xff);
          }
		    }

        newInstructions.Add(instructions[instrIndex]);
        functionSizeSoFar += instructions[instrIndex].bytes.Count();
        newFunctionSizeSoFar += instructions[instrIndex].bytes.Count();
      }

	    // Safe guard...
	    if(functionInfo.address + (UInt64)newFunctionSizeSoFar >= nextFunctionOffset)
	    {
		    Console.WriteLine("Not enough buffer to capture '{0}' ({1}), use a bigger padding (linker option /FUNCTIONPADMIN:{2} on msvc)",
          functionInfo.name, functionInfo.address, (UInt64)newFunctionSizeSoFar - functionInfo.size);
        functionInfo.code = null;
        functionInfo.detouredCode = null;
        return false;
	    }

      // Finally commit our changes to the process
      lock(detourAddressToFunction)
      {
        foreach(KeyValuePair<UInt64, FunctionInfo> detourInfo in tempDetourAddressToFunction)
          detourAddressToFunction.Add(detourInfo.Key, detourInfo.Value);
      }
      functionInfo.detouredCode = new List<byte>();
      foreach(Assembler.Instruction instr in newInstructions)
        functionInfo.detouredCode.AddRange(instr.bytes);
	    Natives.WriteProcessMemory(processHandle, (IntPtr)functionInfo.address, functionInfo.detouredCode.ToArray(), (UInt32)functionInfo.detouredCode.Count(), out numBytes);
      //Console.WriteLine("Successfully captured! '{0}' (0x{1})", functionInfo.name, functionInfo.address.ToString("X16"));
      return true;
    }

    void UninjectFunction(SymbolInfo symbolInfo)
    {
      FunctionInfo functionInfo = addressToFunction[symbolInfo.address];
      IntPtr numBytes;
      Natives.WriteProcessMemory(processHandle, (IntPtr)functionInfo.address, functionInfo.code, (UInt32)functionInfo.size, out numBytes);
      functionInfo.code = null;
      functionInfo.detouredCode = null;
      functionInfo.modifications = null;
    }

    public delegate void ReportProgress(int progress);
    public void InjectFunctions(List<SymbolInfo> symbolInfos, ReportProgress f)
    {
      int jobs = Environment.ProcessorCount;
      int symbolsPerJob = symbolInfos.Count() / jobs;
      int[] lastIndexExclusivePerJob = new int[jobs];
      for(int i = 0; i < jobs - 1; i++)
        lastIndexExclusivePerJob[i] = symbolsPerJob * i;
      lastIndexExclusivePerJob[jobs - 1] = symbolInfos.Count();

      StartDetouring();

      for(int t = 0; t < jobs; t++)
      {
        BackgroundWorker worker = new BackgroundWorker();
        AutoResetEvent resetEvent = new AutoResetEvent(false);

        int jobId = t;
        worker.DoWork += (object sender, DoWorkEventArgs e) =>
        {
          int progress = 0;
          const int progressIncrement = 99;
          int firstIndex = jobId == 0 ? 0 : lastIndexExclusivePerJob[jobId - 1];
          //foreach(SymbolInfo info in symbolInfos)
          for(int i = firstIndex; i < lastIndexExclusivePerJob[jobId]; i++)
          {
            SymbolInfo info = symbolInfos[i];
            int functionIndex = addressToFunction.IndexOfKey(info.address);
            FunctionInfo functionInfo = addressToFunction.Values[functionIndex];
            progress++;
            if(progress % progressIncrement == 0)
              worker.ReportProgress(progressIncrement);

            SafetyInfo safety = StartFunctionDetour(functionInfo);
            safety.ensureMode = SafetyInfo.Mode.Capture;

            bool success = false;
            if(functionIndex + 1 != addressToFunction.Count())
              success = InjectFunction(functionInfo, safety, addressToFunction.Values[functionIndex + 1].address);
            else
              Console.WriteLine("Can't capture the last function in our map due to laziness. (Haha. you unlucky bastard, how did you hit this?)");

            info.forceIsCaptured(success);
            FinishFunctionDetour(success, safety);
          }
          worker.ReportProgress(progress % progressIncrement);
        };
        worker.WorkerReportsProgress = true;
        worker.ProgressChanged += (object sender, ProgressChangedEventArgs e) =>
        {
          f(e.ProgressPercentage);
        };
        worker.RunWorkerAsync();
      }

      //FinishDetouring();
    }

    public void UninjectFunctions(List<SymbolInfo> symbolInfos)
    {
      StartDetouring();
      foreach(SymbolInfo info in symbolInfos)
      {
        int functionIndex = addressToFunction.IndexOfKey(info.address);
        FunctionInfo functionInfo = addressToFunction.Values[functionIndex];

        SafetyInfo safety = StartFunctionDetour(functionInfo);
        safety.ensureMode = SafetyInfo.Mode.Release;
        safety.modifications = functionInfo.modifications;

        UninjectFunction(info);

        FinishFunctionDetour(true, safety);
      }

      FinishDetouring();
    }

    public byte[] GetBytecode(SymbolInfo symbol)
    {
      return (symbol as FunctionInfo).code;
    }

    public void GetClosestFunction(UInt64 address, out SymbolInfo info, out UInt64 offset)
    {
      KeyValuePair<UInt64, FunctionInfo> kvp = addressToFunction.LastOrDefault(x => address >= x.Key);
      info = kvp.Value;
      offset = address - kvp.Key;
    }

    public List<SourceInfo> GetSource(List<UInt64> addresses)
    {
      if(!Natives.SymInitialize(processHandle, null, true))
      {
        Console.WriteLine("Error with SymInitialize...failed to get source code!");
        Natives.SymCleanup(processHandle);
      }
      List<SourceInfo> result = new List<SourceInfo>();

      foreach(UInt64 address in addresses)
      {
        SourceInfo newInfo = new SourceInfo();
        newInfo.address = address;
        Natives.IMAGEHLP_LINE64 lineInfo = new Natives.IMAGEHLP_LINE64();
        lineInfo.SizeOfStruct = (uint)Marshal.SizeOf(lineInfo);
        uint dummy = 0;
        Natives.SymGetLineFromAddr64(processHandle, address, ref dummy, ref lineInfo);
        newInfo.filename = Marshal.PtrToStringAnsi(lineInfo.FileName);
        newInfo.lineNumber = (int)lineInfo.LineNumber;
        result.Add(newInfo);
      }

      Natives.SymCleanup(processHandle);

      return result;
    }
  }
}
