﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for Capture.xaml
  /// </summary>
  public partial class Capture : UserControl
  {
    ProcessInfo processInfo;
    int captureCount = 0;
    ProcessInfo.ProfileSettings profileSettings = new ProcessInfo.ProfileSettings();
    List<ProcessInfo.SymbolInfo> displaySymbols;
    List<ProcessInfo.SymbolInfo> filteredDisplaySymbols;
    bool initialized = false;

    public Capture(ProcessInfo info)
    {
      profileSettings.time = 200;
      profileSettings.functionCount = 5;
      profileSettings.functionTime = 100;

      processInfo = info;
      InitializeComponent();
      displaySymbols = new List<ProcessInfo.SymbolInfo>(processInfo.symbolInfos);
      displaySymbols.Sort((a, b) => a.name.CompareTo(b.name));
      filteredDisplaySymbols = new List<ProcessInfo.SymbolInfo>(displaySymbols);
      uiFunctions.ItemsSource = filteredDisplaySymbols;
      ProcessInfo.SymbolInfo.OnIsCapturedChanged = new ProcessInfo.SymbolInfo.IsCapturedChanged(OnFunctionIsCapturedChanged);
      initialized = true;
    }

    public void OnFunctionIsCapturedChanged(ProcessInfo.SymbolInfo info)
    {
      if(info.isCaptured)
        processInfo.InjectFunctions(new List<ProcessInfo.SymbolInfo> { info }, (int progress) => { if(progress == 1) processInfo.FinishDetouring(); });
      else
        processInfo.UninjectFunctions(new List<ProcessInfo.SymbolInfo> { info });
      Console.WriteLine(info.name);
    }

    private void uiButtonProfile_Click(object sender, RoutedEventArgs e)
    {
      if(profileSettings.type == ProcessInfo.ProfileType.FunctionCount || profileSettings.type == ProcessInfo.ProfileType.FunctionTime)
      {
        profileSettings.selectedFunction = uiFunctions.SelectedItem as ProcessInfo.SymbolInfo;
        if(profileSettings.selectedFunction == null)
        {
          Console.WriteLine("Can't profile a function without selecting a function");
          return;
        }
      }

      captureCount++;
      string filename = processInfo.name + "_" + captureCount.ToString() + ".ipc";
      processInfo.Profile(profileSettings, filename);
      MainWindow.GetMainWindow().AddTab(filename, new Report(processInfo, filename));
    }

    private void uiButtonCaptureAll_Click(object sender, RoutedEventArgs e)
    {
      List<ProcessInfo.SymbolInfo> captureSymbols = new List<ProcessInfo.SymbolInfo>();
      UInt64 minSize = 0;
      if(uiCheckboxCaptureUnderSize.IsChecked.GetValueOrDefault())
        UInt64.TryParse(uiTextBoxCaptureUnderSize.Text, out minSize);

      foreach(ProcessInfo.SymbolInfo info in filteredDisplaySymbols)
        if(info.size >= minSize)
          captureSymbols.Add(info);

      InjectProgressDialog dialog = new InjectProgressDialog();
      dialog.Start(processInfo, captureSymbols);
      dialog.Owner = Window.GetWindow(this);
      dialog.ShowDialog();
      uiFunctions.Items.Refresh();
    }

    private void uiRadioProfileTypeTimed_Checked(object sender, RoutedEventArgs e)
    {
      profileSettings.type = ProcessInfo.ProfileType.Time;
      uiTextCaptureParameter.Text = "Time (ms):";
      uiTextInputCaptureParameter.Text = profileSettings.time.ToString();
    }

    private void uiRadioProfileTypeFCount_Checked(object sender, RoutedEventArgs e)
    {
      profileSettings.type = ProcessInfo.ProfileType.FunctionCount;
      uiTextCaptureParameter.Text = "Call Count:";
      uiTextInputCaptureParameter.Text = profileSettings.functionCount.ToString();
    }

    private void uiRadioProfileTypeFHitch_Checked(object sender, RoutedEventArgs e)
    {
      profileSettings.type = ProcessInfo.ProfileType.FunctionTime;
      uiTextCaptureParameter.Text = "Min Time (ms):";
      uiTextInputCaptureParameter.Text = profileSettings.functionTime.ToString();
    }

    private void uiTextInputCaptureParameter_PreviewTextInput(object sender, TextCompositionEventArgs e)
    {
      int i;
      e.Handled = !int.TryParse(e.Text, out i);
    }

    private void uiTextInputCaptureParameter_LostFocus(object sender, RoutedEventArgs e)
    {
      int i;
      if(int.TryParse((sender as TextBox).Text, out i))
      {
        switch(profileSettings.type)
        {
          case ProcessInfo.ProfileType.Time: profileSettings.time = i; break;
          case ProcessInfo.ProfileType.FunctionCount: profileSettings.functionCount = i; break;
          case ProcessInfo.ProfileType.FunctionTime: profileSettings.functionTime = i; break;
        }
      }
    }

    private void uiShowCheckboxChanged(object sender, RoutedEventArgs e)
    {
      if(initialized)
        UpdateFilteredDisplaySymbols();
    }

    private void uiTextBlockFunctionSearch_TextChanged(object sender, TextChangedEventArgs e)
    {
      UpdateFilteredDisplaySymbols();
    }

    private void UpdateFilteredDisplaySymbols()
    {
      string text = uiTextBlockFunctionSearch.Text;
      filteredDisplaySymbols = new List<ProcessInfo.SymbolInfo>();
      foreach(ProcessInfo.SymbolInfo info in displaySymbols)
      {
        if(!uiCheckboxShowGeneratedFunctions.IsChecked.GetValueOrDefault() && info.name[0] == '`')
          continue;
        if(!uiCheckboxShowStdFunctions.IsChecked.GetValueOrDefault() && info.name.IndexOf("std::") >= 0)
          continue;
        if(text.Length < 3 || info.name.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0)
          filteredDisplaySymbols.Add(info);
      }

      uiFunctions.ItemsSource = filteredDisplaySymbols;
    }
  }
}
