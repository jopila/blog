﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for InjectProgressDialog.xaml
  /// </summary>
  public partial class InjectProgressDialog : Window
  {
    private int cachedSymbolInfoCount;
    private ProcessInfo cachedProcessInfo;
    private int totalProcessedFunctions = 0;

    public InjectProgressDialog()
    {
      InitializeComponent();
    }

    public void Start(ProcessInfo processInfo, List<ProcessInfo.SymbolInfo> symbolInfos)
    {
      cachedSymbolInfoCount = symbolInfos.Count();
      cachedProcessInfo = processInfo;
      processInfo.InjectFunctions(symbolInfos, ReceiveProgress);
      uiInfoText.Text = "Capturing " + cachedSymbolInfoCount.ToString("N0") + " functions.";
      uiProgressBar.Maximum = cachedSymbolInfoCount;
    }

    public void ReceiveProgress(int progress)
    {
      totalProcessedFunctions += progress;
      uiProgressText.Text = "Completed: " + totalProcessedFunctions.ToString("N0");
      uiProgressBar.Value = totalProcessedFunctions;
      uiPercentText.Text = (100.0 * totalProcessedFunctions / cachedSymbolInfoCount).ToString("F2") + "%";
      if(totalProcessedFunctions == cachedSymbolInfoCount)
      {
        Close();
        cachedProcessInfo.FinishDetouring();
      }
    }
  }
}
