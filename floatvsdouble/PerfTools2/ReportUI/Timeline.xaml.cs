﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
	/// <summary>
	/// Interaction logic for Timeline.xaml
	/// </summary>
	public partial class Timeline : UserControl
	{
		public Report report;

		public const float barHeight = 20.0f;

		public double startTime;
		public double currentViewableTime;
		private double maxViewableTime;
		public double pixelsPerSecond;
		public double secondsPerPixel;
		public FormattedText hoverText = null;
		List<TimelineThread> threadControls = new List<TimelineThread>();
		public double timelineOffsetY;

		public Timeline()
		{
			InitializeComponent();
		}

		public void Initialize(Report _report, Dictionary<Int32, EventData> data)
		{
			report = _report;

			// Create function colors!
			Random rand = new Random();
			Dictionary<string, Color> functionColors = new Dictionary<string, Color>();
			foreach(KeyValuePair<Int32, EventData> thread in data)//auto it = eventData.begin(); it != eventData.end(); it++)
				foreach(KeyValuePair<string, EventData.ExclusiveData> excData in thread.Value.excData)
					if(!functionColors.ContainsKey(excData.Key))
						functionColors.Add(excData.Key, Color.FromRgb((byte)rand.Next(192, 255), (byte)rand.Next(192, 255), (byte)rand.Next(192, 255)));

			// Determine the 'time-size' of our timeline
			startTime = double.MaxValue;
			double endTime = 0.0;
			foreach(KeyValuePair<Int32, EventData> it in data)
			{
				startTime = Math.Min(startTime, it.Value.start);
				endTime = Math.Max(endTime, it.Value.end);
				TimelineThread thread = new TimelineThread();
				thread.Initialize(this, it.Key, it.Value, functionColors);
				uiThreads.Children.Add(thread);
				threadControls.Add(thread);
			}
			maxViewableTime = endTime - startTime;
			currentViewableTime = maxViewableTime;
			timelineOffsetY = 0;

			// 
			Render();
		}

		private void Render()
		{
			if(maxViewableTime <= 0.0) // If this is negative, we had no events and the math got wonky
				return;

			pixelsPerSecond = ActualWidth / currentViewableTime;
			secondsPerPixel = 1.0f / pixelsPerSecond;

			// Render threads?!?
			foreach(TimelineThread threadControl in threadControls)
				threadControl.Render();
		}

		public void SelectedEventsChanged()
		{
			Render();
		}

		public void SelectedEventChanged()
		{
			EventData eventData = report.selectedEvent;
			currentViewableTime = eventData.duration * 1.2;
			startTime = eventData.start - eventData.duration * 0.1;
			FixBounds();
			Render();
		}

		public bool MoveThreadUp(TimelineThread control)
		{
			int index = threadControls.FindIndex(x => Object.ReferenceEquals(x, control));

			bool canMove = index > 0;
			if(canMove)
				threadControls.Reverse(index - 1, 2);

			uiThreads.Children.Clear();
			foreach(TimelineThread thread in threadControls)
				uiThreads.Children.Add(thread);

			return canMove;
		}

		public bool MoveThreadDown(TimelineThread control)
		{
			int index = threadControls.FindIndex(x => Object.ReferenceEquals(x, control));

			bool canMove = index < threadControls.Count() - 2;
			if(canMove)
				threadControls.Reverse(index, 2);

			uiThreads.Children.Clear();
			foreach(TimelineThread thread in threadControls)
				uiThreads.Children.Add(thread);

			return canMove;
		}

		public void MoveThreadTop(TimelineThread control)
		{
			while(MoveThreadUp(control)) ;
		}

		public void MoveThreadBottom(TimelineThread control)
		{
			while(MoveThreadDown(control)) ;
		}

		void FixBounds()
		{
			// Keep us in bounds
			// x
			startTime = Math.Max(0.0, startTime);
			currentViewableTime = Math.Min(currentViewableTime, maxViewableTime);
			if(startTime + currentViewableTime > maxViewableTime)
				startTime = maxViewableTime - currentViewableTime;

			// y
			double threadHeight = uiThreads.ActualHeight;
			//foreach(TimelineThread thread in threadControls)
			//	threadHeight += thread.ActualHeight;
			if(ActualHeight - timelineOffsetY > threadHeight)
				timelineOffsetY = ActualHeight - threadHeight;
			if(timelineOffsetY > 0)
				timelineOffsetY = 0;
		}

    private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
    {
			// Zoom
			int scrollBias = e.Delta > 0 ? 1 : -1;

			pixelsPerSecond = ActualWidth / currentViewableTime;
			double oldCursorTime = Mouse.GetPosition(uiTimelineGrid).X / pixelsPerSecond;
			
			while(scrollBias-- > 0)
				currentViewableTime *= 0.75f;
			scrollBias++;
			while(scrollBias++ < 0)
				currentViewableTime *= 1.0f / 0.75f;
			
			pixelsPerSecond = ActualWidth / currentViewableTime;
			double newCursorTime = Mouse.GetPosition(uiTimelineGrid).X / pixelsPerSecond;
			startTime += oldCursorTime - newCursorTime;
			
			FixBounds();

			Render();
		}

		private bool mouseIsDown = false;
		private Point mouseStart;
    private void uiTimelineGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			(sender as UIElement).CaptureMouse();
			mouseIsDown = true;
			mouseStart = Mouse.GetPosition(uiTimelineGrid);
		}

    private void uiTimelineGrid_MouseUp(object sender, MouseButtonEventArgs e)
		{
			(sender as UIElement).ReleaseMouseCapture();
			mouseIsDown = false;
    }

    private void uiTimelineGrid_MouseMove(object sender, MouseEventArgs e)
		{
			if(mouseIsDown)
			{
				// click and drag scroll
				// x
				double offsetX = Mouse.GetPosition(uiTimelineGrid).X - mouseStart.X;
				pixelsPerSecond = ActualWidth / currentViewableTime;
				startTime -= offsetX / pixelsPerSecond;

				// y
				double offsetY = Mouse.GetPosition(uiTimelineGrid).Y - mouseStart.Y;
				timelineOffsetY += offsetY;

				FixBounds();

				uiThreads.Margin = new Thickness(0, timelineOffsetY, 0, 0);

				mouseStart = Mouse.GetPosition(uiTimelineGrid);
				Render();
			}

			foreach(TimelineThread threadControl in threadControls)
				threadControl.MouseMoved();

			// Hover text
			uiCanvas.Children.Clear();
			if(hoverText != null)
			{
				hoverText.MaxTextWidth = ActualWidth;
				Size hoverTextSize = new Size(Math.Ceiling(hoverText.Width + 4), Math.Ceiling(hoverText.Height + 2));
				Point mousePos = Mouse.GetPosition(uiCanvas);
				DrawingVisual drawingVisual = new DrawingVisual();
				using(var drawingContext = drawingVisual.RenderOpen())
				{
					drawingContext.DrawRectangle(new SolidColorBrush(Color.FromRgb(222, 222, 222)), new Pen(Brushes.Black, 1.0), new Rect(new Point(0, 0), hoverTextSize));
					drawingContext.DrawText(hoverText, new Point(2, 1));
				}
				if(mousePos.X + hoverTextSize.Width > ActualWidth)
					mousePos.X = ActualWidth - hoverTextSize.Width;
				RenderTargetBitmap rtb = new RenderTargetBitmap((int)hoverTextSize.Width, (int)hoverTextSize.Height, 96, 96, PixelFormats.Default);
				rtb.Render(drawingVisual);
				Image image = new Image();
				Canvas.SetLeft(image, mousePos.X);
				Canvas.SetTop(image, mousePos.Y + 12.0);
				image.Source = rtb;
				image.IsHitTestVisible = false;
				uiCanvas.Children.Add(image);
			}
			hoverText = null;
		}

		private void uiTimelineGrid_MouseLeave(object sender, MouseEventArgs e)
    {

    }
  }
}
