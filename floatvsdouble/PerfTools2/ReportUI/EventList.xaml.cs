﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
	public partial class EventList : UserControl
	{
		Report report;

		internal class RowData
		{
			public string name { get; set; }
			public double totalExclusive { get; set; }
			public double totalInclusive { get; set; }
			public List<EventData> events { get; set; } = new List<EventData>();
		};
		ObservableCollection<RowData> rowData = new ObservableCollection<RowData>();

		public EventList()
    {
      InitializeComponent();
			uiEventList.ItemsSource = rowData;
		}

		public void Initialize(Report _report, Dictionary<Int32, EventData> data)
		{
			report = _report;

			Dictionary<string, RowData> eventNameToRowData = new Dictionary<string, RowData>();

			Action<EventData> parse = null;
			parse = (EventData eventData) =>
			{
				if(eventData.name == "self")
					return;
				if(!eventNameToRowData.ContainsKey(eventData.name))
					eventNameToRowData.Add(eventData.name, new RowData() { name = eventData.name });
				RowData rowData = eventNameToRowData[eventData.name];

				rowData.totalInclusive += eventData.duration;
				if(eventData.children.Count() > 0 && eventData.children.Last().name == "self")
					rowData.totalExclusive += eventData.children.Last().duration;
				else
					rowData.totalExclusive += eventData.duration;
				rowData.events.Add(eventData);

				for(int i = 0; i < eventData.children.Count(); i++)
					parse(eventData.children[i]);
			};

			foreach(KeyValuePair<Int32, EventData> thread in data)
				parse(thread.Value);

			foreach(KeyValuePair<string, RowData> thread in eventNameToRowData)
				rowData.Add(thread.Value);

			rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.totalExclusive).ToList());
			uiEventList.ItemsSource = rowData;
		}

    private void uiEventList_Sorting(object sender, DataGridSortingEventArgs e)
    {
			if((string)e.Column.Header == "Total Exc")   rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.totalExclusive).ToList());
			if((string)e.Column.Header == "Total Inc")   rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.totalInclusive).ToList());
			if((string)e.Column.Header == "Average Exc") rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.totalExclusive / x.events.Count()).ToList());
			if((string)e.Column.Header == "Average Inc") rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.totalInclusive / x.events.Count()).ToList());
			if((string)e.Column.Header == "Count")       rowData = new ObservableCollection<RowData>(rowData.OrderByDescending(x => x.events.Count()).ToList());
			uiEventList.ItemsSource = rowData;
			e.Handled = true;
		}

    private void uiEventList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
			report.selectedEvents = ((sender as DataGrid).SelectedItem as RowData).events;
    }
  }

  public class GetAverageInclusiveConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			EventList.RowData data = value as EventList.RowData;
			return EventData.FormatTime(data.totalInclusive / data.events.Count());
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) { return value; }
	}
}
