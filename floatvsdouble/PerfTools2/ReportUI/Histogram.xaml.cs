﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for Histogram.xaml
  /// </summary>
  public partial class Histogram : UserControl
  {
    private Report report;
    MyVisualHost timelineVisuals = new MyVisualHost();

    public Histogram()
    {
      InitializeComponent();
      uiGrid.Children.Add(timelineVisuals);
    }

    public void Initialize(Report _report)
    {
      report = _report;
			Render();
		}

    private void Render()
		{
			if(timelineVisuals.IsLoaded)
				timelineVisuals.Render(this);
			else
			{
				RoutedEventHandler OnLoaded = null;
				OnLoaded = (object sender, RoutedEventArgs e) =>
				{
					timelineVisuals.Render(this);
					Loaded -= OnLoaded;
				};
				Loaded += OnLoaded;
			}
    }

		public class AliasedDrawingVisual : DrawingVisual
		{
			public AliasedDrawingVisual()
			{
				this.VisualEdgeMode = EdgeMode.Aliased;
			}
		}

		public class MyVisualHost : FrameworkElement
		{
			private VisualCollection children;
			private DrawingVisual drawingVisual;
			private SolidColorBrush barBrush = new SolidColorBrush(Color.FromRgb(204, 230, 255));
			private Pen barPen = new Pen(new SolidColorBrush(Color.FromRgb(169, 195, 245)), 1);
			private Pen axisPen = new Pen(Brushes.Gray, 1);

			public MyVisualHost()
			{
				children = new VisualCollection(this);
				barBrush.Freeze();
				barPen.Freeze();
				axisPen.Freeze();
				drawingVisual = new AliasedDrawingVisual();
				children.Add(drawingVisual);
			}

			public void Render(Histogram histogram)
			{
				if(histogram.report.selectedEvents == null || histogram.report.selectedEvents.Count() <= 1)
					return;

				DrawingContext drawingContext = drawingVisual.RenderOpen();
				drawingContext.PushClip(new RectangleGeometry(new Rect(0, 0, ActualWidth, ActualHeight)));

				int numBuckets;
				numBuckets = Math.Max(1, (int)(ActualHeight / 24.0f));

				// find our min/max
				double minDur = double.MaxValue;
				double maxDur = 0.0;
				foreach(EventData data in histogram.report.selectedEvents)
				{
					minDur = Math.Min(minDur, data.duration);
					maxDur = Math.Max(maxDur, data.duration);
				}
				double span = maxDur - minDur;

				// bucket the durations
				int biggestBucket = 0;
				List<int> buckets = new List<int>();
				buckets.AddRange(Enumerable.Repeat(0, numBuckets));

				if(span == 0.0)
					biggestBucket = 0;
				else
				{
					foreach(EventData data in histogram.report.selectedEvents)
					{
						int index = (int)((data.duration - minDur) / span * numBuckets);
						index = Math.Min(index, numBuckets - 1);
						buckets[index]++;
						biggestBucket = Math.Max(biggestBucket, buckets[index]);
					}
				}

				// Calculate the locations of the axis
				const double bufferSize = 12.0;
				FormattedText measureText = StringToFormattedText("1.23ms");
				double histoXOffset = bufferSize + measureText.Width;
				double histoYOffset = bufferSize;
				double histoWidth = ActualWidth - bufferSize * 2.0f - measureText.Width;
				double histoHeight = ActualHeight - 2.0f * bufferSize;
				string axisLabel;

				// Render backdrop
				drawingContext.DrawLine(axisPen, new Point(histoXOffset - 1.0f, histoYOffset), new Point(histoXOffset - 1.0f, histoYOffset + histoHeight));

				// Render each bucket as a bar
				for(int i = 0; i < numBuckets; i++)
				{
					double left = 0.0;
					double right = histoWidth * buckets[i] / biggestBucket;
					double top = histoHeight * i / numBuckets;
					double bottom = histoHeight * (i + 1) / numBuckets;
					top = Math.Round(top);
					bottom = Math.Round(bottom);

					left += histoXOffset;
					right += histoXOffset;
					top += histoYOffset;
					bottom += histoYOffset;

					if(buckets[i] != 0)
					{
						drawingContext.DrawRectangle(barBrush, barPen, new Rect(left, top, right - left, bottom - top));
						drawingContext.DrawText(StringToFormattedText(buckets[i].ToString()), new Point(left + 2.0, top + 2.0));
					}

					axisLabel = EventData.FormatTime(minDur + span / numBuckets * i);
					drawingContext.DrawText(StringToFormattedText(axisLabel), new Point(bufferSize - 0.5 * bufferSize, top - 0.5 * bufferSize));
				}

				axisLabel = EventData.FormatTime(maxDur);
				drawingContext.DrawText(StringToFormattedText(axisLabel), new Point(bufferSize - 0.5 * bufferSize, histoHeight + histoYOffset - 0.5 * bufferSize));

				drawingContext.Pop();
				drawingContext.Close();
			}

			// TODO: this is also in FunctionAnalysis.xaml.cs
			private FormattedText StringToFormattedText(string str)
			{
				return new FormattedText(str, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Segoe UI"), 12, Brushes.Black, 1.0);
			}

			protected override Visual GetVisualChild(int index)
			{
				return children[index];
			}

			protected override int VisualChildrenCount
			{
				get
				{
					return children.Count;
				}
			}
		}
	}
}
