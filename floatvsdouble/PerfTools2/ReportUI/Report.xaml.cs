﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for Report.xaml
  /// </summary>
  public partial class Report : UserControl
  {
    private ProcessInfo info;
    private List<EventData> _selectedEvents;
    public List<EventData> selectedEvents
    {
      get { return _selectedEvents; }
      set
      {
        _selectedEvents = value;
        uiTimeline.SelectedEventsChanged();
        uiSelectedEventsList.Initialize(this);
        uiHistogram.Initialize(this);
      }
    }
    private EventData _selectedEvent;
    public EventData selectedEvent
    {
      get { return _selectedEvent; }
      set
      {
        _selectedEvent = value;
        uiTimeline.SelectedEventChanged();
      }
    }

    public Report(ProcessInfo _info, string filename)
    {
      info = _info;

      InitializeComponent();

      if(info == null)
        uiAnalyzeFunction.Visibility = Visibility.Hidden;

      Dictionary<Int32, EventData> data = EventData.ParseFile(filename);

      RoutedEventHandler OnLoaded = null;
      OnLoaded = (object sender, RoutedEventArgs e) =>
      {
        uiEventTree.Initialize(this, data);
        uiEventList.Initialize(this, data);
        uiTimeline.Initialize(this, data);
        Loaded -= OnLoaded;
      };
      Loaded += OnLoaded;
    }

    private void uiAnalyzeFunction_Click(object sender, RoutedEventArgs e)
    {
      if(selectedEvents != null && selectedEvents.Count() > 0)
      {
        FunctionAnalysis functionAnalysis = new FunctionAnalysis();
        functionAnalysis.Initialize(info, selectedEvents[0].name);
        functionAnalysis.Show();
      }
    }
  }
}