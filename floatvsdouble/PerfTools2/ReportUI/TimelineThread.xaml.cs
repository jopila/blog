﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
	/// <summary>
	/// Interaction logic for TimelineThread.xaml
	/// </summary>
	public partial class TimelineThread : UserControl
	{
		Timeline parent;

		class TimelineDataEvent
		{
			public string name;
			public Brush brush;
			public double start;
			public double end;
		};

		class TimelineDataRow
		{
			public List<TimelineDataEvent> events = new List<TimelineDataEvent>();
		};

		class TimelineDataThread
		{
			public string name;
			public List<TimelineDataRow> rows = new List<TimelineDataRow>();
			public bool expanded;
			public float GetHeight() { return ((expanded ? rows.Count() : 0) + 1) * Timeline.barHeight; }
		};

		TimelineDataThread thread = new TimelineDataThread();
		MyVisualHost timelineVisuals = new MyVisualHost();

		public TimelineThread()
		{
			InitializeComponent();
			uiGrid.Children.Add(timelineVisuals);
			timelineVisuals.SetValue(Grid.RowProperty, 1);
		}

		Dictionary<string, FormattedText> functionTexts = new Dictionary<string, FormattedText>();

		public void Initialize(Timeline _parent, Int32 threadId, EventData data, Dictionary<string, Color> functionColors)
		{
			parent = _parent;
			uiThreadName.Text = threadId.ToString();

			//buttonTitle->onClicked = [=]()
			//{
			//	thread.expanded = !thread.expanded;
			//	h = thread.GetHeight();
			//	parent->RelayoutThreads();
			//};

			foreach(string functionName in functionColors.Keys)
				functionTexts.Add(functionName, new FormattedText(functionName, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Segoe UI"), 12, Brushes.Black, 1.0));

			// Refactor our raw event data into easily renderable data
			Action<EventData, int> BuildTimelineData = null;
			BuildTimelineData = (EventData eventData, int row) =>
			{
				if(thread.rows.Count() < row)
					thread.rows.Add(new TimelineDataRow());

				for(int i = 0; i < eventData.children.Count(); i++)
				{
					EventData child = eventData.children[i];
					if(child.name == "self")
						continue;

					TimelineDataEvent dataEvent = new TimelineDataEvent();
					dataEvent.name = child.name;
					try
					{
						dataEvent.brush = new SolidColorBrush(functionColors[dataEvent.name]);
					}
					catch(Exception)
					{
						dataEvent.brush = new SolidColorBrush(Colors.Black);
					}
					dataEvent.brush.Freeze();
					dataEvent.start = child.start;
					dataEvent.end = child.end;
					thread.rows[row - 1].events.Add(dataEvent);
					BuildTimelineData(child, row + 1);
				}

				if(thread.rows.Last().events.Count() == 0)
					thread.rows.RemoveAt(thread.rows.Count() - 1);
			};
			thread.name = threadId.ToString();
			thread.expanded = true;
			BuildTimelineData(data, 1);

			uiBackgroundRect.Height = thread.rows.Count() * Timeline.barHeight;

			//
			RoutedEventHandler OnLoaded = null;
			OnLoaded = (object sender, RoutedEventArgs e) =>
			{
				Render();
				Loaded -= OnLoaded;
			};
			Loaded += OnLoaded;
		}

		public void Render()
		{
			timelineVisuals.Render(this);
		}

		private void ButtonUp_Click(object sender, RoutedEventArgs e)
		{
			parent.MoveThreadUp(this);
		}

		private void ButtonDown_Click(object sender, RoutedEventArgs e)
		{
			parent.MoveThreadDown(this);
		}

		private void ButtonFirst_Click(object sender, RoutedEventArgs e)
		{
			parent.MoveThreadTop(this);
		}

		private void ButtonLast_Click(object sender, RoutedEventArgs e)
		{
			parent.MoveThreadBottom(this);
		}

		public void MouseMoved()
		{
			timelineVisuals.MouseMoved(this);
		}

		public class AliasedDrawingVisual : DrawingVisual
		{
			public AliasedDrawingVisual()
			{
				this.VisualEdgeMode = EdgeMode.Aliased;
			}
		}

		public class MyVisualHost : FrameworkElement
		{
			private VisualCollection children;
			private SolidColorBrush grayBrush = new SolidColorBrush(Colors.Gray);
			private SolidColorBrush highlightBrush = new SolidColorBrush(Color.FromArgb(128, 70, 130, 180));
			private Pen outlinePen = new Pen(Brushes.Black, 1);
			private Pen highlightPen = new Pen(Brushes.SteelBlue, 2);
			private DrawingVisual drawingVisual;
			struct CachedRenderData { public Rect rect; public List<TimelineDataEvent> events; };
			List<CachedRenderData> renderedEvents = new List<CachedRenderData>();

			public MyVisualHost()
			{
				children = new VisualCollection(this);
				grayBrush.Freeze();
				outlinePen.Freeze();
				drawingVisual = new AliasedDrawingVisual();
				children.Add(drawingVisual);
			}

			public void Render(TimelineThread timeline)
			{
				renderedEvents.Clear();

				DrawingContext drawingContext = drawingVisual.RenderOpen();

				string selectedEventName = "";
				if(timeline.parent.report.selectedEvents != null && timeline.parent.report.selectedEvents.Count() > 0)
					selectedEventName = timeline.parent.report.selectedEvents[0].name;

				if(!timeline.thread.expanded)
					return;

				Rect rect = new Rect(new Point(), new Point());
				drawingContext.PushClip(new RectangleGeometry(new Rect(0, 0, ActualWidth, ActualHeight)));

				// Events
				for(int rowIndex = 0; rowIndex < timeline.thread.rows.Count(); rowIndex++)
				{
					TimelineDataRow row = timeline.thread.rows[rowIndex];
					for(int eventIndex = 0; eventIndex < row.events.Count(); eventIndex++)
					{
						TimelineDataEvent dataEvent = row.events[eventIndex];

						if(dataEvent.end < timeline.parent.startTime)
							continue; // event is to the left the viewable area

						if(dataEvent.start > timeline.parent.startTime + timeline.parent.currentViewableTime)
							break; // event is past the viewable area

						double duration = dataEvent.end - dataEvent.start;

						// If an event is too small, try to find similar events and group them into one render
						if(duration * timeline.parent.pixelsPerSecond < 3.0)
						{
							double childStartTime = dataEvent.start;
							double endTime = dataEvent.end;
							bool combinedEvents = false;
							List<TimelineDataEvent> events = new List<TimelineDataEvent>() { dataEvent };
							while(++eventIndex < row.events.Count() - 1 && endTime + timeline.parent.secondsPerPixel > row.events[eventIndex].end)
							{
								events.Add(row.events[eventIndex]);
								endTime = row.events[eventIndex].end;
								combinedEvents = true;
							}
							eventIndex--;

							double w = Math.Round(Math.Max(1.001f, (float)((endTime - childStartTime) * timeline.parent.pixelsPerSecond)));
							double h = Timeline.barHeight;
							double x = Math.Round((float)((childStartTime - timeline.parent.startTime) * timeline.parent.pixelsPerSecond));
							double y = Math.Round(Timeline.barHeight * rowIndex);
							Brush brush = combinedEvents ? grayBrush : dataEvent.brush;

							rect.X = x;
							rect.Y = y;
							rect.Width = w;
							rect.Height = h;
							drawingContext.DrawRectangle(brush, outlinePen, rect);
							renderedEvents.Add(new CachedRenderData { rect = rect, events = events });
						}
						else
						{
							double w = Math.Round(duration * timeline.parent.pixelsPerSecond);
							double h = Timeline.barHeight;
							double x = Math.Round((dataEvent.start - timeline.parent.startTime) * timeline.parent.pixelsPerSecond);
							double y = Math.Round(Timeline.barHeight * rowIndex);

							// Rectangle backdrop
							rect.X = x;
							rect.Y = y;
							rect.Width = w;
							rect.Height = h;
							drawingContext.DrawRectangle(dataEvent.brush, outlinePen, rect);
							renderedEvents.Add(new CachedRenderData { rect = rect, events = new List<TimelineDataEvent>() { dataEvent } });

							// highlight border for selected events
							if(selectedEventName == dataEvent.name)
								drawingContext.DrawRectangle(highlightBrush, highlightPen, new Rect(rect.X + 1, rect.Y + 1, rect.Width - 2, rect.Height - 2));

							// Text if it fits
							double textWidth = rect.Width + Math.Min(x, 0.0) - 4.0;
							if(textWidth > 20.0f)
							{
								drawingContext.PushClip(new RectangleGeometry(new Rect(x+2, y+1, textWidth, rect.Height)));
								drawingContext.DrawText(timeline.functionTexts[dataEvent.name], new Point(x + 2, y + 1));
								drawingContext.Pop();
							}
						}
					}
				}
				drawingContext.Pop();

				drawingContext.Close();
			}

			public void MouseMoved(TimelineThread timeline)
			{
				Point mousePos = Mouse.GetPosition(timeline.uiBackgroundRect);
				foreach(CachedRenderData eventData in renderedEvents)
				{
					double x = eventData.rect.X;
					double y = eventData.rect.Y;
					double w = eventData.rect.Width;
					double h = eventData.rect.Height;
					if(mousePos.X >= x && mousePos.X < x + w && mousePos.Y >= y && mousePos.Y < y + h)
					{
						double duration = eventData.events[0].end - eventData.events[0].start;
						string text = eventData.events[0].name;
						text += "\n";
						text += string.Format("{0} ({1}-{2})", EventData.FormatTime(duration), EventData.FormatTime(eventData.events[0].start), EventData.FormatTime(eventData.events[0].end));
						timeline.parent.hoverText = new FormattedText(text, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Segoe UI"), 12, Brushes.Black, 1.0);
					}
				}
			}

			protected override Visual GetVisualChild(int index)
			{
				return children[index];
			}

      protected override int VisualChildrenCount
			{
				get
				{
					return children.Count;
				}
			}
		}
  }


}
