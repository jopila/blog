﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for SelectedEventsList.xaml
  /// </summary>
  public partial class SelectedEventsList : UserControl
  {
    private Report report;

    public SelectedEventsList()
    {
      InitializeComponent();
    }

    public void Initialize(Report _report)
    {
      report = _report;
      uiSelectedEventsList.ItemsSource = report.selectedEvents;
    }

    private void uiSelectedEventsList_Sorting(object sender, DataGridSortingEventArgs e)
    {

    }

    private void uiSelectedEventsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        EventData data = (sender as DataGrid).SelectedItem as EventData;
        if(data != null)
          report.selectedEvent = data;
    }

    public class TimeConverter : IValueConverter
    {
      public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
        return EventData.FormatTime((double)value);
      }

      public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
        return value;
      }
    }
  }
}
