﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PerfTools2
{
	/// <summary>
	/// Interaction logic for EventTree.xaml
	/// </summary>
	public partial class EventTree : UserControl
	{
		Report report;

		class TreeData
		{
			public string name;
			public double total;
			public double min;
			public double max;
			public bool expanded;
			public RowData rowData;
			public List<TreeData> children = new List<TreeData>();
			public List<EventData> events = new List<EventData>();
		};
		TreeData treeData;

		internal class RowData
		{
			public string name { get; set; }
			public double total { get; set; }
			public double min { get; set; }
			public double max { get; set; }
			public Int32 depth { get; set; }
			public bool expanded { get; set; }
			public bool expandable { get; set; }
			public List<EventData> events { get; set; } = new List<EventData>();
		};
		ObservableCollection<RowData> rowData = new ObservableCollection<RowData>();

		public EventTree()
		{
			InitializeComponent();
			uiEventTree.ItemsSource = rowData;
		}

		public void Initialize(Report _report, Dictionary<Int32, EventData> data)
		{
			report = _report;
			treeData = new TreeData();
			treeData.name = "Application";
			treeData.total = 0.0;
			treeData.expanded = true;
			foreach(KeyValuePair<Int32, EventData> it in data)
			{
				EventData eventData = it.Value;
				TreeData threadTreeData = new TreeData();
				threadTreeData.name = it.Key.ToString();
				treeData.children.Add(threadTreeData);
				BuildTreeData(eventData, treeData.children.Last());
				SortTreeData(treeData, (a, b) => b.total.CompareTo(a.total));
			}
			BuildRowData(treeData, 0);
		}

		private void BuildTreeData(EventData eventData, TreeData treeData)
		{
			foreach(EventData child in eventData.children)
			{
				// find an already existing tree data for this child's event
				TreeData currentTreeData = null;
				for(int i = 0; i < treeData.children.Count(); i++)
				{
					if(treeData.children[i].name == child.name)
					{
						currentTreeData = treeData.children[i];
						break;
					}
				}

				// make a new one if we didn't find one
				if(currentTreeData == null)
				{
					currentTreeData = new TreeData();
					currentTreeData.name = child.name;
					currentTreeData.total = 0.0f;
					currentTreeData.min = double.MaxValue;
					treeData.children.Add(currentTreeData);
				}

				// update the tree data
				currentTreeData.total += child.duration;
				currentTreeData.min = Math.Min(currentTreeData.min, child.duration);
				currentTreeData.max = Math.Max(currentTreeData.max, child.duration);
				currentTreeData.events.Add(child);

				// so many kids
				BuildTreeData(child, currentTreeData);
			}
		}

		private void SortTreeData(TreeData treeData, Comparison<TreeData> comparison)
		{
			treeData.children.Sort(comparison);

			foreach(TreeData child in treeData.children)
				SortTreeData(child, comparison);
		}

		private void BuildRowData(TreeData treeData, int depth)
		{
			if(treeData.expanded)
			{
				foreach(TreeData child in treeData.children)
				{
					RowData newRowData = new RowData();
					newRowData.name       = child.name;
					newRowData.total      = child.total;
					newRowData.min        = child.min;
					newRowData.max        = child.max;
					newRowData.depth      = depth;
					newRowData.expanded   = child.expanded;
					newRowData.expandable = child.children.Count() > 0;
					newRowData.events     = child.events;
					child.rowData = newRowData;
					rowData.Add(newRowData);
					BuildRowData(child, depth + 1);
				}
			}
		}

		private TreeData FindTreeDataForRowData(TreeData treeData, RowData rowData)
		{
			if(Object.ReferenceEquals(treeData.rowData, rowData))
				return treeData;
			foreach(TreeData child in treeData.children)
			{
				TreeData foundTree = FindTreeDataForRowData(child, rowData);
				if(foundTree != null)
					return foundTree;
			}
			return null;
		}

    private void uiEventTree_Sorting(object sender, DataGridSortingEventArgs e)
    {
			if((string)e.Column.Header == "Total") SortTreeData(treeData, (a, b) => b.total.CompareTo(a.total));
			if((string)e.Column.Header == "Average") SortTreeData(treeData, (a, b) => (b.total / b.events.Count()).CompareTo(a.total / a.events.Count()));
			if((string)e.Column.Header == "Min") SortTreeData(treeData, (a, b) => b.min.CompareTo(a.min));
			if((string)e.Column.Header == "Max") SortTreeData(treeData, (a, b) => b.max.CompareTo(a.max));
			if((string)e.Column.Header == "Count") SortTreeData(treeData, (a, b) => b.events.Count().CompareTo(a.events.Count()));
			rowData.Clear();
			BuildRowData(treeData, 0);
			e.Handled = true;
    }

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			RowData data = (sender as ToggleButton).DataContext as RowData;
			data.expanded = (sender as ToggleButton).IsChecked ?? false;
			rowData.Clear();
			FindTreeDataForRowData(treeData, data).expanded = data.expanded;
			BuildRowData(treeData, 0);

		}

    private void uiEventTree_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
			RowData data = (sender as DataGrid).SelectedItem as RowData;
			if(data != null)
				report.selectedEvents = data.events;
		}
  }

  public class DepthToWidthConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(value is Int32)
				return (Int32)value * 8;
			return 0;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if(value is Int32)
				return (Int32)value / 8;
			return 0;
		}
	}

	public class GetAverageConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			EventTree.RowData data = value as EventTree.RowData;
			return EventData.FormatTime(data.total / data.events.Count());
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value;
		}
	}

	public class TimeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return EventData.FormatTime((double)value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return value;
		}
	}
}
