﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PerfTools2
{
  class LLVMWrangler
  {
    static string binDirectory = @"D:\Projects\Tools\llvm-project-main\build\Release\bin";

    public static List<string> GetAssemblyPrintout(byte[] bytes)
    {
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.FileName = Path.Combine(binDirectory, "llvm-mc.exe");
      startInfo.Arguments = "-disassemble --arch=x86-64 --output-asm-variant=1";
      startInfo.UseShellExecute = false;
      startInfo.RedirectStandardOutput = true;
      startInfo.RedirectStandardInput = true;

      Process llvmProcess = Process.Start(startInfo);

      foreach(byte b in bytes)
        llvmProcess.StandardInput.Write(string.Format("0x{0:X2} ", b));
      llvmProcess.StandardInput.Close();

      //Console.WriteLine(llvmProcess.StandardOutput.ReadToEnd());
      string raw = llvmProcess.StandardOutput.ReadToEnd();
      if(raw == "")
      {
        Console.WriteLine("llvm mysteriously failed");
        return new List<string>();
      }

      List<string> result = raw.Split('\n').ToList();
      result.RemoveAt(0);
      if(result.Last() == "")
        result.RemoveAt(result.Count() - 1);

      return result;
    }

    public static string GetMCAPrintout(byte[] bytes)
    {
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.FileName = Path.Combine(binDirectory, "llvm-mc.exe");
      startInfo.Arguments = "-disassemble --arch=x86-64";
      startInfo.UseShellExecute = false;
      startInfo.CreateNoWindow = true;
      startInfo.RedirectStandardOutput = true;
      startInfo.RedirectStandardInput = true;

      Process llvmProcess = Process.Start(startInfo);

      foreach(byte b in bytes)
        llvmProcess.StandardInput.Write(string.Format("0x{0:X2} ", b));
      llvmProcess.StandardInput.Close();

      //Console.WriteLine(llvmProcess.StandardOutput.ReadToEnd());
      string raw = llvmProcess.StandardOutput.ReadToEnd();
      if(raw == "")
      {
        Console.WriteLine("llvm mysteriously failed");
        return "";
      }

      startInfo = new ProcessStartInfo();
      startInfo.FileName = Path.Combine(binDirectory, "llvm-mca.exe");
      startInfo.Arguments = "-timeline -output-asm-variant=1";
      startInfo.UseShellExecute = false;
      startInfo.CreateNoWindow = true;
      startInfo.RedirectStandardOutput = true;
      startInfo.RedirectStandardInput = true;

      Process llvmProcess2 = Process.Start(startInfo);

      llvmProcess2.StandardInput.Write(raw);
      llvmProcess2.StandardInput.Close();
      raw = llvmProcess2.StandardOutput.ReadToEnd();
      if(raw == "")
      {
        Console.WriteLine("llvm mysteriously failed");
        return "";
      }

      return raw;
    }

    public class LLVMMCAData
    {
      public int iterations;
      public int instructions;
      public int cycles;
      public int uOps;
      public int dispatchWidth;
      public float uOpsPerCycle;
      public float ipc;
      public float blockRThroughput;

      public List<string> resourceNames = new List<string>();
      public List<double> resourceTotalPressure = new List<double>();
      public List<List<double>> resourcePressuresPerInstructions = new List<List<double>>();
      public enum ScheduleStatus { Dispatched, Executing, Executed, Retired, Waiting, None };
      public struct InstructinSchedule
      {
        public int iterationIndex;
        public int instructionIndex;
        public List<ScheduleStatus> schedule;
      };
      public List<InstructinSchedule> timelineSchedules = new List<InstructinSchedule>();
    };


    public static LLVMMCAData GetLLVMMCAData(byte[] bytes)
    {
      string raw = GetMCAPrintout(bytes);
      LLVMMCAData result = new LLVMMCAData();

      int overviewIndex = 0;
      int instructionInfoIndex = raw.IndexOf("Instruction Info:");
      int resourcesIndex = raw.IndexOf("Resources:");
      int resourcePressureIterationIndex = raw.IndexOf("Resource pressure per iteration:");
      int resourcePressureInstructionIndex = raw.IndexOf("Resource pressure by instruction:");
      int timelineIndex = raw.IndexOf("Timeline view:");
      int timelineWaitTimesIndex = raw.IndexOf("Average Wait times (based on the timeline view):");

      string overviewString = raw.Substring(overviewIndex, instructionInfoIndex - overviewIndex);
      string instructionInfoString = raw.Substring(instructionInfoIndex, resourcesIndex - instructionInfoIndex);
      string resourcesString = raw.Substring(resourcesIndex, resourcePressureIterationIndex - resourcesIndex);
      string resourcePressureIterationString = raw.Substring(resourcePressureIterationIndex, resourcePressureInstructionIndex - resourcePressureIterationIndex);
      string resourcePressureInstructionString = raw.Substring(resourcePressureInstructionIndex, timelineIndex - resourcePressureInstructionIndex);
      string timelineString = raw.Substring(timelineIndex, timelineWaitTimesIndex - timelineIndex);
      string timelineWaitTimesString = raw.Substring(timelineWaitTimesIndex, raw.Length - timelineWaitTimesIndex);

      // overview
      {
        result.iterations = int.Parse(Regex.Match(overviewString, @"Iterations:\s*(\d+)").Groups[1].Value);
        result.instructions = int.Parse(Regex.Match(overviewString, @"Instructions:\s*(\d+)").Groups[1].Value);
        result.cycles = int.Parse(Regex.Match(overviewString, @"Total Cycles:\s*(\d+)").Groups[1].Value);
        result.uOps = int.Parse(Regex.Match(overviewString, @"Total uOps:\s*(\d+)").Groups[1].Value);
        result.dispatchWidth = int.Parse(Regex.Match(overviewString, @"Dispatch Width:\s*(\d+)").Groups[1].Value);
        result.uOpsPerCycle = float.Parse(Regex.Match(overviewString, @"uOps Per Cycle:\s*([\d.]+)").Groups[1].Value);
        result.ipc = float.Parse(Regex.Match(overviewString, @"IPC:\s*([\d.]+)").Groups[1].Value);
        result.blockRThroughput = float.Parse(Regex.Match(overviewString, @"Block RThroughput:\s*([\d.]+)").Groups[1].Value);
      }

      // instruction info
      // nope

      // Resources
      {
        MatchCollection matches = Regex.Matches(resourcesString, @"\[\d+\]\s*-\s*(\w*)");
        foreach(Match match in matches)
          result.resourceNames.Add(match.Groups[1].Value);
      }

      // Resource pressure per iteration
      {
        string pressureString = resourcePressureIterationString.Substring(resourcePressureIterationString.LastIndexOf(']') + 1);
        MatchCollection matches = Regex.Matches(pressureString, @"[\d\.-]+");
        foreach(Match match in matches)
        {
          double pressure = 0;
          bool success = double.TryParse(match.Value, out pressure);
          result.resourceTotalPressure.Add(pressure);
        }
      }

      // Resource pressure per instruction
      {
        string[] pressuresPerInstructions = resourcePressureInstructionString.Trim().Split(new char[] { '\n' });
        foreach(string pressuresPerInstruction in pressuresPerInstructions)
        {
          if(pressuresPerInstruction.StartsWith("Resource") || pressuresPerInstruction.StartsWith("[0]"))
            continue; //skip headers

          result.resourcePressuresPerInstructions.Add(new List<double>());
          MatchCollection matches = Regex.Matches(pressuresPerInstruction, @"[\d\.-]+");
          for(int i = 0; i < result.resourceNames.Count(); i++)
          {
            double pressure = 0;
            bool success = double.TryParse(matches[i].Value, out pressure);
            result.resourcePressuresPerInstructions.Last().Add(pressure);
          }
        }
      }

      // Timeline
      {
        string[] timelineInstructions = timelineString.Trim().Split(new char[] { '\n' });
        foreach(string timelineInstruction in timelineInstructions)
        {
          if(!timelineInstruction.StartsWith("["))
            continue; // only parse the actual timeline bit

          Match match = Regex.Match(timelineInstruction, @"\[(\d+),(\d+)\]\s([DeER=\- .]*)\w");
          LLVMMCAData.InstructinSchedule schedule = new LLVMMCAData.InstructinSchedule();
          schedule.iterationIndex = int.Parse(match.Groups[1].Value);
          schedule.instructionIndex = int.Parse(match.Groups[2].Value);
          schedule.schedule = new List<LLVMMCAData.ScheduleStatus>();
          string rawSchedule = match.Groups[3].Value.Trim();
          foreach(char c in rawSchedule)
          {
            switch(c)
            {
              case 'D': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Dispatched); break;
              case 'e': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Executing); break;
              case 'E': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Executed); break;
              case 'R': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Retired); break;
              case '=': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Waiting); break;
              case '-': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.Waiting); break;
              case ' ': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.None); break;
              case '.': schedule.schedule.Add(LLVMMCAData.ScheduleStatus.None); break;
            }
          }
          result.timelineSchedules.Add(schedule);
        }
      }

      // timelineWaitTimesString
      // nah

      return result;
    }
  }
}
