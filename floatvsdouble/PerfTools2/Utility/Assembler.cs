﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfTools2
{
	class Assembler
	{
		public enum OperandSize { opNone = 0, op8 = 1, op16 = 2, op32 = 4 };
		public class Instruction
		{
			public List<byte> bytes = new List<byte>();
			public OperandSize ripRelativeSize;
			public Int64 ripRelativeOffsetOffset; // offset within the instruction for the rip relative offset // I have no idea what this means anymore // oh yeah...now I remember
			public Int32 stackOffset;
			public bool prologInstruction;
			public bool detourWorthy;
			public bool jumpToRegisterAddress;
			public bool callSite;
			public bool conditionalJump;
		};

		private static bool IsRexPrefix(byte instr) { return instr >= 0x40 && instr <= 0x4f; }
		private static bool IsSegmentPrefix(byte instr) { return instr == 0x64 || instr == 0x65; }
		private static bool IsWaitPrefix(byte instr) { return instr == 0x9b; }
		private static bool IsLockPrefix(byte instr) { return instr == 0xf0; }
		private static bool IsRepPrefix(byte instr) { return instr == 0xf2 || instr == 0xf3; }
		private static bool IsOperandSizePrefix(byte instr) { return instr == 0x66; }
		private static bool IsPrefix(byte instr)
		{
			return
				IsRexPrefix(instr) ||
				IsSegmentPrefix(instr) ||
				IsWaitPrefix(instr) ||
				IsLockPrefix(instr) ||
				IsRepPrefix(instr) ||
				IsOperandSizePrefix(instr);
		}

		public static Int32 GetOperandFromBytes(byte[] bytes, int offset, OperandSize opSize)
		{
			/**/
			if(opSize == OperandSize.opNone)
				return 0;
			else if(opSize == OperandSize.op8)
				return (sbyte)bytes[offset];
			else if(opSize == OperandSize.op16)
				return (short)(bytes[offset] + (bytes[offset + 1] << 8));
			else if(opSize == OperandSize.op32)
				return bytes[offset] + (bytes[offset + 1] << 8) + (bytes[offset + 2] << 16) + (bytes[offset + 3] << 24);
			return 0;
		}

		private static bool IsUnsupportedOpcode(byte instr)
		{
			return
				(instr >= 0x06 && instr <= 0x07) || // invalid
				(instr == 0x0e) || // invalid
				(instr >= 0x16 && instr <= 0x17) || // invalid
				(instr >= 0x1e && instr <= 0x1f) || // invalid
				(instr >= 0x26 && instr <= 0x27) || // null prefix/invalid
				(instr >= 0x2e && instr <= 0x2f) || // null prefix/invalid
				(instr >= 0x36 && instr <= 0x37) || // null prefix/invalid
				(instr >= 0x3e && instr <= 0x3f) || // null prefix/invalid
				(instr >= 0x60 && instr <= 0x62) || // invalid
				(instr == 0x82) || // invalid
				(instr == 0x9a) || // invalid
				(instr >= 0xc4 && instr <= 0xc5) || // invalid
				(instr >= 0xd4 && instr <= 0xd6) || // invalid
				(instr == 0xd7) || // xlat/xlatb
				(instr >= 0xd8 && instr <= 0xdf) || // x87 math instructions
				(instr == 0xea) || // invalid
				(instr == 0xf1);                    // undefined
		}

		private static bool IsUnsupported2ByteOpcode(byte instr, byte instr2)
		{
			return
				(instr >= 0x00 && instr <= 0x04) || // uhhhhh...
				(instr >= 0x06 && instr <= 0x0c) || // ^
				(instr >= 0x0e && instr <= 0x0e) || // unknown
				(instr >= 0x20 && instr <= 0x27) || // mov control registers, unknown
				(instr == 0x38) && (
					(instr2 != 0x80 && instr2 != 0x81 && instr2 != 0xf0 && instr2 != 0xf1)
					) || // inv(ept/vpid)/movbe/crc32
				(instr == 0x3a) && (
					(instr2 <= 0x07) ||
					(instr2 >= 0x10 && instr2 <= 0x13) ||
					(instr2 >= 0x18 && instr2 <= 0x1f) ||
					(instr2 >= 0x23 && instr2 <= 0x3f) ||
					(instr2 >= 0x43 && instr2 <= 0x5f) ||
					(instr2 >= 0x64)
					) || // round/blend/align/pextr/extract/insr/dpp/mpsad/pcmp
				(instr >= 0x3b && instr <= 0x3f) || // unknown
				(instr >= 0x7a && instr <= 0x7b) || // unknown
				(instr >= 0xa6 && instr <= 0xa7) || // unknown
				(instr == 0xff);                    // unknown
		}

		private static bool IsMemoryOpcode(byte instr)
		{
			return
				(instr >= 0x00 && instr <= 0x03) || // add
				(instr >= 0x08 && instr <= 0x0b) || // or
				(instr >= 0x10 && instr <= 0x13) || // adc
				(instr >= 0x18 && instr <= 0x1b) || // sbb
				(instr >= 0x20 && instr <= 0x23) || // and
				(instr >= 0x28 && instr <= 0x2b) || // sub
				(instr >= 0x30 && instr <= 0x33) || // xor
				(instr >= 0x38 && instr <= 0x3b) || // cmp
				(instr == 0x63) || // movsxd
				(instr == 0x69 || instr == 0x6b) || // imul
				(instr >= 0x6c && instr <= 0x6f) || // in(s/sb/sw/sd)/out(s/sb/sw/sd)
				(instr == 0x80) || // add/or/adc/sbb/and/sub/xor/cmp r/m8, imm8
				(instr == 0x81) || // add/or/adc/sbb/and/sub/xor/cmp r/m16/32/64, imm16/32
				(instr == 0x83) || // add/or/adc/sbb/and/sub/xor/cmp r/m16/32/64, imm8
				(instr >= 0x84 && instr <= 0x85) || // test
				(instr >= 0x86 && instr <= 0x87) || // xchg
				(instr >= 0x88 && instr <= 0x8e) || // mov
				(instr >= 0xc0 && instr <= 0xc1) || // rol/ror/rcl/rcr/shl/sal/shr/sal/shl/sar, imm8
				(instr >= 0xc6 && instr <= 0xc7) || // mov imm8/16/32
				(instr >= 0xd0 && instr <= 0xd3) || // rol/ror/rcl/rcr/shl/sal/shr/sal/shl/sar, 1/cl
				(instr >= 0xf6 && instr <= 0xf7) || // test/not/neg/mul/imul/div/idiv
				(instr >= 0xfe && instr <= 0xff);   // inc/dec/call/callf/jmp/jmpf/push
		}

		private static bool IsMemory2ByteOpcode(byte instr)
		{
			return
				(instr == 0x0d) || // nop
				(instr >= 0x10 && instr <= 0x1f) || // movxxx/unpckxxx/prefetchxxx/hint_nop
				(instr >= 0x28 && instr <= 0x2f) || // mova/cvtpi2/mvnt/cvttps2/cvtps2/ucomi/comi ps/pd/pi/si
				(instr == 0x38) || // invept/invvpid/movbe/crc32
				(instr == 0x3a) || // round(ps/pd/ss/sd)/blend(ps/pd)/pblendw/palignr/pextr(b/w/d/q)/extract(ps)/pinsr(b/d/q)/insert(ps)/dpp(s/d)/pasadbw/pcmp(e/i)str(m/i)
				(instr >= 0x40 && instr <= 0x76) || // uhhhhhh...
				(instr >= 0x78 && instr <= 0x7f) || // vmread/vmwrite/hadd(ps/pd)/hsub(ps/pd)/mov(d/q)/movdq(a/u)
				(instr >= 0x90 && instr <= 0x9f) || // setxx
				(instr >= 0xa3 && instr <= 0xa5) || // bt/shld
				(instr >= 0xab && instr <= 0xad) || // bts/shrd
				(instr == 0xae) || // fxsave/fxrstor/ldmxcsr/stmxcsr/xsave/xrstor/lfence/mfence/sfence/clflush
				(instr >= 0xaf && instr <= 0xb9) || // imul/cmxchg/lss/btr/lfs/lgs/movzx/popcnt/ud
				(instr >= 0xba && instr <= 0xc7) || // bt/bts//btr/btc/bsf/bsr/movsx/xadd/cmp(ps/ss/pd/sd)/movnti/pinsrw/pextrw/shufp(s/d)
				(instr >= 0xd0 && instr <= 0xfe);   // lesigh...
		}

		private static Int32 GetImmSizeForOpcode(byte instr, byte modrm, bool sizeOverride, bool rexSizeOverride)
		{
			if(instr == 0xf6)
			{
				if((modrm & 0b_0011_1000) == 0b_0000_0000 || (modrm & 0b_0011_1000) == 0b_0000_1000) // test imm8
					return 1;
			}
			if(instr == 0xf7)
			{
				if((modrm & 0b_0011_1000) == 0b_0000_0000 || (modrm & 0b_0011_1000) == 0b_0000_1000) // test imm16,32
					return sizeOverride ? 2 : 4;
			}

			switch(instr)
			{
				case 0x04: return 1;                    // add imm8
				case 0x05: return sizeOverride ? 2 : 4; // add imm16/32
				case 0x0c: return 1;                    // or imm8
				case 0x0d: return sizeOverride ? 2 : 4; // or imm16/32
				case 0x14: return 1;                    // adc imm8
				case 0x15: return sizeOverride ? 2 : 4; // adc imm16/32
				case 0x1c: return 1;                    // sbb imm8
				case 0x1d: return sizeOverride ? 2 : 4; // sbb imm16/32
				case 0x24: return 1;                    // and imm8
				case 0x25: return sizeOverride ? 2 : 4; // and imm16/32
				case 0x2c: return 1;                    // sub imm8
				case 0x2d: return sizeOverride ? 2 : 4; // sub imm16/32
				case 0x34: return 1;                    // xor imm8
				case 0x35: return sizeOverride ? 2 : 4; // xor imm16/32
				case 0x3c: return 1;                    // cmp imm8
				case 0x3d: return sizeOverride ? 2 : 4; // cmp imm16/32
				case 0x68: return sizeOverride ? 2 : 4; // push imm16/32
				case 0x69: return sizeOverride ? 2 : 4; // imul imm16/32
				case 0x6a: return 1;                    // push imm8
				case 0x6b: return 1;                    // imul imm8
				case 0x80: return 1;                    // add/or/adc/sbb/and/sub/xor/cmp imm8
				case 0x81: return sizeOverride ? 2 : 4; // add/or/adc/sbb/and/sub/xor/cmp imm16/32
				case 0x83: return 1;                    // add/or/adc/sbb/and/sub/xor/cmp imm8
				case 0xa8: return 1;                    // test imm8
				case 0xa9: return sizeOverride ? 2 : 4; // test imm16/32
				case 0xb0:
				case 0xb1:
				case 0xb2:
				case 0xb3:
				case 0xb4:
				case 0xb5:
				case 0xb6:
				case 0xb7: return 1;                    // mov imm8
				case 0xb8:
				case 0xb9:
				case 0xba:
				case 0xbb:
				case 0xbc:
				case 0xbd:
				case 0xbe:
				case 0xbf: return sizeOverride ? 2 : (rexSizeOverride ? 8 : 4); // mov imm16/32/64
				case 0xc0:
				case 0xc1: return 1;                    // rol/ror/rcl/rcr/shl/sal/shr/sal/shl/sar imm8
				case 0xc2: return 2;                    // retn imm16
				case 0xc6: return 1;                    // mov imm8
				case 0xc7: return sizeOverride ? 2 : 4; // mov imm16/32
				case 0xc8: return 3;                    // enter imm16, imm8
				case 0xca: return 2;                    // retf imm16
				case 0xcd: return 1;                    // int imm8
				case 0xe4: return 1;                    // in imm8
				case 0xe5: return 1;                    // in imm8
				case 0xe6: return 1;                    // out imm8
				case 0xe7: return 1;                    // out imm8
				default: return 0;
			}
		}

		private static Int32 GetImmSizeFor2ByteOpcode(byte instr, byte instr2, bool sizeOverride)
		{
			/**/
			if(instr == 0x3a && instr2 >= 0x08 && instr2 <= 0x0e) return 1; // round(ps/pd/ss/sd)/blend(ps/pd/dw)
			else if(instr == 0x3a && instr2 >= 0x14 && instr2 <= 0x17) return 1; // pextr(b/w/d/q)/extractps
			else if(instr == 0x3a && instr2 >= 0x20 && instr2 <= 0x22) return 1; // pinsr(b/d/q)/insertps
			else if(instr == 0x3a && instr2 == 0x42) return 1; // mpsadbw
			else if(instr == 0x3a && instr2 >= 0x62 && instr2 <= 0x63) return 1; // pcmpistr(m/i)
			else if(instr == 0x70) return 1; // pshuf(w/lw/hw/d)
			else if(instr >= 0x71 && instr <= 0x73) return 1; // ps(r/l)(l/a)(w/d/q/dq)
			else if(instr == 0xba) return 1; // bt(/s/r/c)
			else if(instr == 0xc2) return 1; // cmp(p/s)(s/d)
			else if(instr >= 0xc4 && instr <= 0xc6) return 1; // pinsrw/pextrw/shufp(s/d)
			return 0;
		}

		public static bool Disassemble(byte[] code, out List<Instruction> instructions)
		{
			byte rexByte = 0;
			byte modrmByte = 0;
			bool isOperandSizePrefix = false;
			bool is2ByteOpcode = false;

			instructions = new List<Instruction>();
			Instruction currentInstruction = new Instruction();

			int readIndex = 0;
			while(readIndex < code.Count())
			{
				// Prefixes
				/**/
				if(IsRexPrefix(code[readIndex]))
					currentInstruction.bytes.Add(rexByte = code[readIndex++]);
				else if(IsSegmentPrefix(code[readIndex]))
					currentInstruction.bytes.Add(code[readIndex++]);
				else if(IsWaitPrefix(code[readIndex]))
					currentInstruction.bytes.Add(code[readIndex++]);
				else if(IsLockPrefix(code[readIndex]))
					currentInstruction.bytes.Add(code[readIndex++]);
				else if(IsRepPrefix(code[readIndex]))
					currentInstruction.bytes.Add(code[readIndex++]);
				else if(IsOperandSizePrefix(code[readIndex]))
				{
					isOperandSizePrefix = true;
					currentInstruction.bytes.Add(code[readIndex++]);
				}
				// Handle the actual opcode and finish the instruction
				else
				{
					byte opcode2 = 0;
					if(code[readIndex] == 0x0f)
					{
						is2ByteOpcode = true;
						opcode2 = code[readIndex++];
						currentInstruction.bytes.Add(opcode2);
					}

					byte opcode = code[readIndex++];
					currentInstruction.bytes.Add(opcode);

					// Check for unsupported instructions real quick
					bool unsupported;
					if(is2ByteOpcode)
						unsupported = IsUnsupported2ByteOpcode(opcode, readIndex < code.Count() ? code[readIndex] : (byte)0);
					else
						unsupported = IsUnsupportedOpcode(opcode);
					if(unsupported)
						return false;

					// Handle more than 2 byte instructions
					if(is2ByteOpcode)
					{
						if(opcode == 0x38 || opcode == 0x3a)
							currentInstruction.bytes.Add(code[readIndex++]);
					}

					// Handle possible r/m byte
					if((is2ByteOpcode && IsMemory2ByteOpcode(opcode)) || (!is2ByteOpcode && IsMemoryOpcode(opcode)))
					{
						modrmByte = code[readIndex++];
						currentInstruction.bytes.Add(modrmByte);
						if((modrmByte & 0b11000000) == 0b11000000) // mov r??, r??
						{
						}
						else if((modrmByte & 0b11000111) == 0b00000101) // mov r64, rip+imm32
						{
							//ripRelativeOffsets.push_back({~0ull, newCode.size() + outInstruction.bytes.size(), numDetours, rel32});
							currentInstruction.ripRelativeOffsetOffset = currentInstruction.bytes.Count();
							currentInstruction.ripRelativeSize = OperandSize.op32;
							currentInstruction.bytes.Add(code[readIndex++]);
							currentInstruction.bytes.Add(code[readIndex++]);
							currentInstruction.bytes.Add(code[readIndex++]);
							currentInstruction.bytes.Add(code[readIndex++]);
						}
						else if((modrmByte & 0b11000111) == 0b00000100) // mov r64, sib
						{
							byte sibByte = code[readIndex++];
							currentInstruction.bytes.Add(sibByte); // sib
							if((sibByte & 0b00000111) == 0b00000101) // sib has an additional displacement
							{
								int mod = (modrmByte & 0b11000000) >> 6;
								if(mod == 0b00 || mod == 0b10)
								{
									currentInstruction.bytes.Add(code[readIndex++]); // imm32a
									currentInstruction.bytes.Add(code[readIndex++]); // imm32b
									currentInstruction.bytes.Add(code[readIndex++]); // imm32c
									currentInstruction.bytes.Add(code[readIndex++]); // imm32d
								}
								else if(mod == 0b01)
									currentInstruction.bytes.Add(code[readIndex++]); // imm8
							}
						}
						else if((modrmByte & 0b11000111) == 0b01000100) // mov r64, sib+imm8
						{
							currentInstruction.bytes.Add(code[readIndex++]); // sib
							currentInstruction.bytes.Add(code[readIndex++]); // imm8
						}
						else if((modrmByte & 0b11000111) == 0b10000100) // mov r64, sib+imm32
						{
							currentInstruction.bytes.Add(code[readIndex++]); // sib
							currentInstruction.bytes.Add(code[readIndex++]); // imm32a
							currentInstruction.bytes.Add(code[readIndex++]); // imm32b
							currentInstruction.bytes.Add(code[readIndex++]); // imm32c
							currentInstruction.bytes.Add(code[readIndex++]); // imm32d
						}
						else if((modrmByte & 0b11000000) == 0b00000000) // mov r64, r64
						{
						}
						else if((modrmByte & 0b11000000) == 0b01000000) // mov r64, r64+imm8
						{
							currentInstruction.bytes.Add(code[readIndex++]); // imm8
						}
						else if((modrmByte & 0b11000000) == 0b10000000) // mov r64, r64+imm32
						{
							currentInstruction.bytes.Add(code[readIndex++]); // imm32a
							currentInstruction.bytes.Add(code[readIndex++]); // imm32b
							currentInstruction.bytes.Add(code[readIndex++]); // imm32c
							currentInstruction.bytes.Add(code[readIndex++]); // imm32d
						}
					}

					// Handle all the relative instructions
					bool isJump = false;
					if(is2ByteOpcode)
					{
						// conditional jumps
						if(opcode >= 0x80 && opcode <= 0x8f)
						{
							currentInstruction.ripRelativeOffsetOffset = currentInstruction.bytes.Count();
							currentInstruction.ripRelativeSize = isOperandSizePrefix ? OperandSize.op16 : OperandSize.op32;
							currentInstruction.bytes.Add(code[readIndex++]);
							currentInstruction.bytes.Add(code[readIndex++]);
							if(!isOperandSizePrefix)
							{
								currentInstruction.bytes.Add(code[readIndex++]);
								currentInstruction.bytes.Add(code[readIndex++]);
							}
							isJump = true;
							currentInstruction.conditionalJump = true;
						}
					}
					else
					{
						// conditional jumps
						if(opcode >= 0x70 && opcode <= 0x7f)
							currentInstruction.conditionalJump = true;

            // rel8/16/32 instructions
						if(opcode >= 0x70 && opcode <= 0x7f || opcode >= 0xe0 && opcode <= 0xe3 || opcode == 0xeb) // jmpxx/loopxx/jmp
						{
							currentInstruction.ripRelativeOffsetOffset = currentInstruction.bytes.Count();
							currentInstruction.ripRelativeSize = OperandSize.op8;
							currentInstruction.bytes.Add(code[readIndex++]);
							//isJump = true;
						}
						// call/jmp imm16/32
						if(opcode == 0xe8 || opcode == 0xe9)
						{
							if(opcode == 0xe8) // call
								currentInstruction.callSite = true;
							else // jmp
								isJump = true;

							// Push our rip info to be adjusted later
							currentInstruction.ripRelativeOffsetOffset = currentInstruction.bytes.Count();
							currentInstruction.ripRelativeSize = isOperandSizePrefix ? OperandSize.op16 : OperandSize.op32;
							currentInstruction.bytes.Add(code[readIndex++]);
							currentInstruction.bytes.Add(code[readIndex++]);
							if(!isOperandSizePrefix)
							{
								currentInstruction.bytes.Add(code[readIndex++]);
								currentInstruction.bytes.Add(code[readIndex++]);
							}
						}

						// call r/m64
						if(opcode == 0xff && ((modrmByte & 0b00111000) == 0b00010000))
							currentInstruction.callSite = true;

						// jmp r/m16/32/64 ( this is likely a jump table from a switch statement. We can't easily handle that)
						if(opcode == 0xff && (modrmByte & 0b00111000) == 0b00100000)
							currentInstruction.jumpToRegisterAddress = true;

						// ret
						if(opcode == 0xc2 || opcode == 0xc3 || opcode == 0xca || opcode == 0xcb)
							currentInstruction.detourWorthy = true;
					}

					// Handle jumps outside the function (we need an exit detour for these)
					if(isJump)
					{
						// get the offset from this instruction
						Int32 rel = GetOperandFromBytes(code, readIndex - (int)currentInstruction.ripRelativeSize, currentInstruction.ripRelativeSize);
						Int32 offset = rel + readIndex;

						// only add a detour if it's a jmp outside of the function
						if(offset < 0 || offset >= code.Count())
						{
							currentInstruction.detourWorthy = true;
							currentInstruction.callSite = true;
						}
					}

					// immediate arguments
					Int32 immSize;
					if(is2ByteOpcode)
						immSize = GetImmSizeFor2ByteOpcode(opcode, opcode2, isOperandSizePrefix);
					else
						immSize = GetImmSizeForOpcode(opcode, modrmByte, isOperandSizePrefix, (rexByte & 0x08) != 0);
					for(int i = 0; i < immSize; i++)
						currentInstruction.bytes.Add(code[readIndex++]);

					// Handle the stack modifying instructions
					if(!is2ByteOpcode)
					{
						if(opcode >= 0x50 && opcode <= 0x57) // push r64
							currentInstruction.stackOffset = -8;
						if(opcode >= 0x58 && opcode <= 0x5f) // pop r64
							currentInstruction.stackOffset = 8;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x83 && modrmByte == 0xec) //sub rsp, imm8
							currentInstruction.stackOffset = -GetOperandFromBytes(currentInstruction.bytes.ToArray(), 3, OperandSize.op8);
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x81 && modrmByte == 0xec) //sub rsp, imm32 
							currentInstruction.stackOffset = -GetOperandFromBytes(currentInstruction.bytes.ToArray(), 3, OperandSize.op32);
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x83 && modrmByte == 0xc4) //add rsp, imm8
							currentInstruction.stackOffset = GetOperandFromBytes(currentInstruction.bytes.ToArray(), 3, OperandSize.op8);
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x81 && modrmByte == 0xc4) //add rsp, imm32
							currentInstruction.stackOffset = GetOperandFromBytes(currentInstruction.bytes.ToArray(), 3, OperandSize.op32);
					}

					// Handle prolog likely instructions (I probably don't need all these as some are more an epilog type instruction...but meh)
					if(!is2ByteOpcode)
					{
						if(opcode >= 0x50 && opcode <= 0x57) // push r64
							currentInstruction.prologInstruction = true;
						if(opcode >= 0x58 && opcode <= 0x5f) // pop r64
							currentInstruction.prologInstruction = true;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x83 && modrmByte == 0xec) //sub rsp, imm8
							currentInstruction.prologInstruction = true;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x81 && modrmByte == 0xec) //sub rsp, imm32 
							currentInstruction.prologInstruction = true;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x83 && modrmByte == 0xc4) //add rsp, imm8
							currentInstruction.prologInstruction = true;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x81 && modrmByte == 0xc4) //add rsp, imm32
							currentInstruction.prologInstruction = true;
						if(currentInstruction.bytes[0] == 0x48 && opcode == 0x89)
						{
							// typically MS uses sib byte moves for the prolog...hopefully they keep doing that ~.~
							if((modrmByte & 0b11000111) == 0b01000100 && currentInstruction.bytes[3] == 0x24) // mov [rsp+imm8], r64
								currentInstruction.prologInstruction = true;
							if((modrmByte & 0b11000111) == 0b10000100 && currentInstruction.bytes[3] == 0x24) // mov [rsp+imm32], r64
								currentInstruction.prologInstruction = true;
						}
					}
					instructions.Add(currentInstruction);
					currentInstruction = new Instruction();
					rexByte = 0;
					modrmByte = 0;
					isOperandSizePrefix = false;
					is2ByteOpcode = false;
				}
			}
			return true;
		}
	}
}
