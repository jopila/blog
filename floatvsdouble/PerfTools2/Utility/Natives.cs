﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

// https://stackoverflow.com/questions/12656737/how-to-obtain-the-dll-list-of-a-specified-process-and-loop-through-it-to-check-i

namespace PerfTools2
{
  public static class Natives
  {
    public static byte[] Serialize<T>(T s) where T : struct
    {
      var size = Marshal.SizeOf(typeof(T));
      var array = new byte[size];
      var ptr = Marshal.AllocHGlobal(size);
      Marshal.StructureToPtr(s, ptr, true);
      Marshal.Copy(ptr, array, 0, size);
      Marshal.FreeHGlobal(ptr);
      return array;
    }

    public static T Deserialize<T>(byte[] array) where T : struct
    {
      var size = Marshal.SizeOf(typeof(T));
      var ptr = Marshal.AllocHGlobal(size);
      Marshal.Copy(array, 0, ptr, size);
      var s = (T)Marshal.PtrToStructure(ptr, typeof(T));
      Marshal.FreeHGlobal(ptr);
      return s;
    }

    [Flags]
    public enum SymOpt : uint
    {
      CASE_INSENSITIVE = 0x00000001,
      UNDNAME = 0x00000002,
      DEFERRED_LOADS = 0x00000004,
      NO_CPP = 0x00000008,
      LOAD_LINES = 0x00000010,
      OMAP_FIND_NEAREST = 0x00000020,
      LOAD_ANYTHING = 0x00000040,
      IGNORE_CVREC = 0x00000080,
      NO_UNQUALIFIED_LOADS = 0x00000100,
      FAIL_CRITICAL_ERRORS = 0x00000200,
      EXACT_SYMBOLS = 0x00000400,
      ALLOW_ABSOLUTE_SYMBOLS = 0x00000800,
      IGNORE_NT_SYMPATH = 0x00001000,
      INCLUDE_32BIT_MODULES = 0x00002000,
      PUBLICS_ONLY = 0x00004000,
      NO_PUBLICS = 0x00008000,
      AUTO_PUBLICS = 0x00010000,
      NO_IMAGE_SEARCH = 0x00020000,
      SECURE = 0x00040000,
      SYMOPT_DEBUG = 0x80000000
    };

    [Flags]
    public enum SymFlag : uint
    {
      VALUEPRESENT = 0x00000001,
      REGISTER = 0x00000008,
      REGREL = 0x00000010,
      FRAMEREL = 0x00000020,
      PARAMETER = 0x00000040,
      LOCAL = 0x00000080,
      CONSTANT = 0x00000100,
      EXPORT = 0x00000200,
      FORWARDER = 0x00000400,
      FUNCTION = 0x00000800,
      VIRTUAL = 0x00001000,
      THUNK = 0x00002000,
      TLSREL = 0x00004000,
    }

    [Flags]
    public enum SymTagEnum : uint
    {
      Null,
      Exe,
      Compiland,
      CompilandDetails,
      CompilandEnv,
      Function,
      Block,
      Data,
      Annotation,
      Label,
      PublicSymbol,
      UDT,
      Enum,
      FunctionType,
      PointerType,
      ArrayType,
      BaseType,
      Typedef,
      BaseClass,
      Friend,
      FunctionArgType,
      FuncDebugStart,
      FuncDebugEnd,
      UsingNamespace,
      VTableShape,
      VTable,
      Custom,
      Thunk,
      CustomType,
      ManagedType,
      Dimension
    };

    [Flags]
    public enum SymType : uint
    {
      SymNone,
      SymCoff,
      SymCv,
      SymPdb,
      SymExport,
      SymDeferred,
      SymSym,
      SymDia,
      SymVirtual,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SYMBOL_INFO
    {
      public uint SizeOfStruct;
      public uint TypeIndex;
      public ulong Reserved1;
      public ulong Reserved2;
      public uint Reserved3;
      public uint Size;
      public ulong ModBase;
      public SymFlag Flags;
      public ulong Value;
      public ulong Address;
      public uint Register;
      public uint Scope;
      public SymTagEnum Tag;
      public int NameLen;
      public int MaxNameLen;

      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
      public string Name;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct MODLOAD_DATA
    {
      UInt32 ssize;
      UInt32 ssig;
      object data;
      UInt32 size;
      UInt32 flags;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGEHLP_LINE64
    {
      public uint SizeOfStruct;
      public IntPtr Key;
      public uint LineNumber;
      public IntPtr FileName;
      public ulong Address;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct IMAGEHLP_MODULE64
    {
      public uint SizeOfStruct;
      public ulong BaseOfImage;
      public uint ImageSize;
      public uint TimeDateStamp;
      public uint CheckSum;
      public uint NumSyms;
      public SymType SymType;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
      public string ModuleName;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
      public string ImageName;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
      public string LoadedImageName;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
      public string LoadedPdbName;
      public uint CVSig;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 780)]
      public string CVData;
      public uint PdbSig;
      public Guid PdbSig70;
      public uint PdbAge;
      public bool PdbUnmatched;
      public bool DbgUnmatched;
      public bool LineNumbers;
      public bool GlobalSymbols;
      public bool TypeInfo;
      public bool SourceIndexed;
      public bool Publics;
    }

    public delegate bool SymEnumSymbolsProc(ref SYMBOL_INFO pSymInfo, uint SymbolSize, IntPtr UserContext);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymInitialize(IntPtr hProcess, string UserSearchPath, bool fInvadeProcess);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern uint SymSetOptions(SymOpt SymOptions);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern ulong SymLoadModule64(IntPtr hProcess, IntPtr hFile,
       string ImageName, string ModuleName,
       ulong BaseOfDll, uint SizeOfDll);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern ulong SymLoadModuleEx(IntPtr hProcess, IntPtr hFile,
        string ImageName, string ModuleName, IntPtr BaseOfDll,
        UInt32 DllSize, ref MODLOAD_DATA Data, UInt32 Flags);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern ulong SymLoadModuleEx(IntPtr hProcess, IntPtr hFile,
        string ImageName, string ModuleName, IntPtr BaseOfDll,
        UInt32 DllSize, IntPtr Data, UInt32 Flags);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymGetModuleInfo64(IntPtr hProcess, ulong dwAddress, ref IMAGEHLP_MODULE64 ModuleInfo);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymEnumSymbols(IntPtr hProcess, ulong BaseOfDll, string Mask, SymEnumSymbolsProc EnumSymbolsCallback, IntPtr UserContext);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymGetLineFromAddr64(IntPtr hProcess,
       ulong dwAddr, ref uint pdwDisplacement, ref IMAGEHLP_LINE64 Line);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymFromAddr(IntPtr hProcess,
       ulong dwAddr, ref ulong pdwDisplacement, ref SYMBOL_INFO symbolInfo);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymEnumSymbolsForAddr(IntPtr hProcess,
       ulong Address, SymEnumSymbolsProc EnumSymbolsCallback, IntPtr UserContext);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymUnloadModule64(IntPtr hProcess, ulong BaseOfDll);

    [DllImport("dbghelp.dll", SetLastError = true)]
    public static extern bool SymCleanup(IntPtr hProcess);

    // 
    [Flags]
    public enum ProcessAccessFlags : uint
    {
      All = 0x001F0FFF,
      Terminate = 0x00000001,
      CreateThread = 0x00000002,
      VirtualMemoryOperation = 0x00000008,
      VirtualMemoryRead = 0x00000010,
      VirtualMemoryWrite = 0x00000020,
      DuplicateHandle = 0x00000040,
      CreateProcess = 0x000000080,
      SetQuota = 0x00000100,
      SetInformation = 0x00000200,
      QueryInformation = 0x00000400,
      QueryLimitedInformation = 0x00001000,
      Synchronize = 0x00100000
    }

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, bool bInheritHandle, uint dwProcessId);

    [Flags]
    public enum ThreadAccess : int
    {
      ALL = 0x001F0FFF,
      TERMINATE = (0x0001),
      SUSPEND_RESUME = (0x0002),
      GET_CONTEXT = (0x0008),
      SET_CONTEXT = (0x0010),
      SET_INFORMATION = (0x0020),
      QUERY_INFORMATION = (0x0040),
      SET_THREAD_TOKEN = (0x0080),
      IMPERSONATE = (0x0100),
      DIRECT_IMPERSONATION = (0x0200)
    }

    [DllImport("kernel32.dll")]
    public static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool CloseHandle(IntPtr handle);
    
    [Flags]
    public enum PageAccess : uint
    {
      PAGE_NOACCESS                   =       0x01,
      PAGE_READONLY                   =       0x02,
      PAGE_READWRITE                  =       0x04,
      PAGE_WRITECOPY                  =       0x08,
      PAGE_EXECUTE                    =       0x10,
      PAGE_EXECUTE_READ               =       0x20,
      PAGE_EXECUTE_READWRITE          =       0x40,
      PAGE_EXECUTE_WRITECOPY          =       0x80,
      PAGE_GUARD                      =      0x100,
      PAGE_NOCACHE                    =      0x200,
      PAGE_WRITECOMBINE               =      0x400,
      PAGE_GRAPHICS_NOACCESS          =     0x0800,
      PAGE_GRAPHICS_READONLY          =     0x1000,
      PAGE_GRAPHICS_READWRITE         =     0x2000,
      PAGE_GRAPHICS_EXECUTE           =     0x4000,
      PAGE_GRAPHICS_EXECUTE_READ      =     0x8000,
      PAGE_GRAPHICS_EXECUTE_READWRITE =    0x10000,
      PAGE_GRAPHICS_COHERENT          =    0x20000,
      PAGE_ENCLAVE_THREAD_CONTROL     = 0x80000000,
      PAGE_REVERT_TO_FILE_MAP         = 0x80000000,
      PAGE_TARGETS_NO_UPDATE          = 0x40000000,
      PAGE_TARGETS_INVALID            = 0x40000000,
      PAGE_ENCLAVE_UNVALIDATED        = 0x20000000,
      PAGE_ENCLAVE_DECOMMIT           = 0x10000000,
    }

    [Flags]
    public enum AllocationType : uint
    {
      MEM_COMMIT                      = 0x00001000,
      MEM_RESERVE                     = 0x00002000,
      MEM_REPLACE_PLACEHOLDER         = 0x00004000,
      MEM_RESERVE_PLACEHOLDER         = 0x00040000,
      MEM_RESET                       = 0x00080000,
      MEM_TOP_DOWN                    = 0x00100000,
      MEM_WRITE_WATCH                 = 0x00200000,
      MEM_PHYSICAL                    = 0x00400000,
      MEM_ROTATE                      = 0x00800000,
      MEM_DIFFERENT_IMAGE_BASE_OK     = 0x00800000,
      MEM_RESET_UNDO                  = 0x01000000,
      MEM_LARGE_PAGES                 = 0x20000000,
      MEM_4MB_PAGES                   = 0x80000000,
      MEM_64K_PAGES                   = (MEM_LARGE_PAGES | MEM_PHYSICAL),
      MEM_UNMAP_WITH_TRANSIENT_BOOST  = 0x00000001,
      MEM_COALESCE_PLACEHOLDERS       = 0x00000001,
      MEM_PRESERVE_PLACEHOLDER        = 0x00000002,
      MEM_DECOMMIT                    = 0x00004000,
      MEM_RELEASE                     = 0x00008000,
      MEM_FREE                        = 0x00010000,
    }


    public struct MEMORY_BASIC_INFORMATION
    {
      public ulong BaseAddress;
      public ulong AllocationBase;
      public int AllocationProtect;
      public ulong RegionSize;
      public int State;
      public ulong Protect;
      public ulong Type;
    }

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, AllocationType flAllocationType, PageAccess flProtect);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint dwFreeType);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, int dwSize, PageAccess flNewProtect, out PageAccess lpflOldProtect);
    
    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out MEMORY_BASIC_INFORMATION lpBuffer, uint dwLength);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out IntPtr lpNumberOfBytesWritten);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GetModuleHandle(string lpModuleName);

    [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

    [DllImport("ntdll.dll", PreserveSig = false)]
    public static extern void NtSuspendProcess(IntPtr processHandle);

    [DllImport("ntdll.dll", PreserveSig = false)]
    public static extern void NtResumeProcess(IntPtr processHandle);

    public enum CONTEXT_FLAGS : uint
    {
      CONTEXT_i386 = 0x10000,
      CONTEXT_i486 = 0x10000,   //  same as i386
      CONTEXT_CONTROL = CONTEXT_i386 | 0x01, // SS:SP, CS:IP, FLAGS, BP
      CONTEXT_INTEGER = CONTEXT_i386 | 0x02, // AX, BX, CX, DX, SI, DI
      CONTEXT_SEGMENTS = CONTEXT_i386 | 0x04, // DS, ES, FS, GS
      CONTEXT_FLOATING_POINT = CONTEXT_i386 | 0x08, // 387 state
      CONTEXT_DEBUG_REGISTERS = CONTEXT_i386 | 0x10, // DB 0-3,6,7
      CONTEXT_EXTENDED_REGISTERS = CONTEXT_i386 | 0x20, // cpu specific extensions
      CONTEXT_FULL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS,
      CONTEXT_ALL = CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS | CONTEXT_EXTENDED_REGISTERS
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct M128A
    {
      public ulong High;
      public long Low;

      public override string ToString()
      {
        return string.Format("High:{0}, Low:{1}", this.High, this.Low);
      }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 16)]
    public struct XSAVE_FORMAT64
    {
      public ushort ControlWord;
      public ushort StatusWord;
      public byte TagWord;
      public byte Reserved1;
      public ushort ErrorOpcode;
      public uint ErrorOffset;
      public ushort ErrorSelector;
      public ushort Reserved2;
      public uint DataOffset;
      public ushort DataSelector;
      public ushort Reserved3;
      public uint MxCsr;
      public uint MxCsr_Mask;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
      public M128A[] FloatRegisters;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
      public M128A[] XmmRegisters;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 96)]
      public byte[] Reserved4;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 16)]
    public struct CONTEXT64
    {
      public ulong P1Home;
      public ulong P2Home;
      public ulong P3Home;
      public ulong P4Home;
      public ulong P5Home;
      public ulong P6Home;

      public CONTEXT_FLAGS ContextFlags;
      public uint MxCsr;

      public ushort SegCs;
      public ushort SegDs;
      public ushort SegEs;
      public ushort SegFs;
      public ushort SegGs;
      public ushort SegSs;
      public uint EFlags;

      public ulong Dr0;
      public ulong Dr1;
      public ulong Dr2;
      public ulong Dr3;
      public ulong Dr6;
      public ulong Dr7;

      public ulong Rax;
      public ulong Rcx;
      public ulong Rdx;
      public ulong Rbx;
      public ulong Rsp;
      public ulong Rbp;
      public ulong Rsi;
      public ulong Rdi;
      public ulong R8;
      public ulong R9;
      public ulong R10;
      public ulong R11;
      public ulong R12;
      public ulong R13;
      public ulong R14;
      public ulong R15;
      public ulong Rip;

      public XSAVE_FORMAT64 DUMMYUNIONNAME;

      [MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
      public M128A[] VectorRegister;
      public ulong VectorControl;

      public ulong DebugControl;
      public ulong LastBranchToRip;
      public ulong LastBranchFromRip;
      public ulong LastExceptionToRip;
      public ulong LastExceptionFromRip;
    }

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool GetThreadContext(IntPtr hThread, ref CONTEXT64 lpContext);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool SetThreadContext(IntPtr hThread, ref CONTEXT64 lpContext);

    [StructLayout(LayoutKind.Sequential)]
    public struct NT_TIB
    {
      public IntPtr ExceptionList;
      public IntPtr StackBase;
      public IntPtr StackLimit;
      public IntPtr SubSystemTib;
      public IntPtr FiberData;
      public IntPtr ArbitraryUserPointer;
      public IntPtr Self;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CLIENT_ID
    {
      IntPtr UniqueProcess;
      IntPtr UniqueThread;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct THREAD_BASIC_INFORMATION
    {
      public Int32 ExitStatus;
      public IntPtr TebBaseAddress;
      CLIENT_ID ClientId;
      UInt64 AffinityMask;
      Int32 Priority;
      Int32 BasePriority;
    }

    [DllImport("ntdll.dll", SetLastError = true)]
    public static extern int NtQueryInformationThread(IntPtr threadHandle, int threadInformationClass, out THREAD_BASIC_INFORMATION threadInformation, int threadInformationLength, IntPtr returnLengthPtr);

  }
}
