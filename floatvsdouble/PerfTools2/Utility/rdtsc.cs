﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PerfTools2
{
  public static class rdtsc
  {
    private static readonly byte[] Rdtscp = { 0x0F, 0x01, 0xF9, 0x48, 0xC1, 0xE2, 0x20, 0x48, 0x09, 0xD0, 0xC3 }; //64bit
    [MethodImpl(MethodImplOptions.NoInlining)]
    public static ulong read()
    {
      Stopwatch.GetTimestamp();
      Stopwatch.GetTimestamp();
      return 0;
    }

    public static unsafe void init()
    {
      Func<ulong> func = read;
      var reference = __makeref(func);

      var ptrReference = (IntPtr*)*(IntPtr*)&reference;
      var ptrFunc = (IntPtr*)*ptrReference;
      var ptrTimestampP = (byte*)*(ptrFunc + 0x4);

      foreach(var b in Rdtscp)
        *(ptrTimestampP++) = b;
    }
  }
}
