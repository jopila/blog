﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfTools2
{
  class MyBinaryWriter : BinaryWriter
  {
    public MyBinaryWriter(FileStream stream) : base(stream) { }

    public void Write(string value, bool terminate)
    {
      byte[] buffer = Encoding.UTF8.GetBytes(value);
      Write(buffer);
      if(terminate)
        Write((byte)0);
    }
  }

  class MyBinaryReader : BinaryReader
  {
    public MyBinaryReader(FileStream stream) : base(stream) { }

    public string ReadTerminatedString()
    {
      StringBuilder result = new StringBuilder();
      byte character;
      do
      {
        character = ReadByte();
        result.Append((char)character);
      } while(character != 0);
      result.Remove(result.Length - 1, 1);
      return result.ToString();
    }
  }
}
