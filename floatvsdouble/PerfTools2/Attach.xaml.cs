﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for Capture.xaml
  /// </summary>
  public partial class Attach : UserControl
  {
    public Attach()
    {
      InitializeComponent();

      string line;
      List<string> exceptions = new List<string>();
      System.IO.StreamReader file = new System.IO.StreamReader(@"CaptureExceptions.ini");
      while((line = file.ReadLine()) != null)
      {
        exceptions.Add(line);
      }
      file.Close();


      Process[] processes = Process.GetProcesses();
      foreach(Process process in processes)
      {
        if(Process.GetCurrentProcess().Id == process.Id)
          continue;
        bool except = false;
        foreach(string exception in exceptions)
          if(process.ProcessName == exception)
            except = true;
        if(except)
          continue;

        ProcessInfo processInfo = new ProcessInfo();
        if(processInfo.InitializeProcessInfo(process))
        {
          uiProcesses.Items.Add(processInfo);
        }


      }
      uiProcesses.Items.SortDescriptions.Add(new SortDescription("symbolCount", ListSortDirection.Descending));
    }

    private void uiAttachButton_Click(object sender, RoutedEventArgs e)
    {
      if(uiProcesses.SelectedItem == null)
        return;
      Console.WriteLine(uiProcesses.SelectedItem.ToString());
      ProcessInfo info = (ProcessInfo)uiProcesses.SelectedItem;
      info.AttachToProcess();
      MainWindow.GetMainWindow().AddTab(info.name, new Capture(info));
    }
  }
}
