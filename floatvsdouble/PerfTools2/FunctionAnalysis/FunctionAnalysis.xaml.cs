﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for FunctionAnalysis.xaml
  /// </summary>
  public partial class FunctionAnalysis : Window
  {
    private ProcessInfo processInfo;
    
    private FontFamily font;
    private byte[] bytecode;
    private List<Assembler.Instruction> instructions;
    private List<ProcessInfo.SourceInfo> sourceLines;
    List<RichTextLine> asmDisplayLines;
    List<RichTextLine> cppDisplayLines;

    private class RichTextLine : IComparable<RichTextLine>
    {
      public Brush lineColor { get; set; } = Brushes.White;
      public string lineOffset { get; set; }
      public FrameworkElement line { get; set; }
      public int index;

      public int CompareTo(RichTextLine rhs)
      {
        return lineOffset.CompareTo(rhs.lineOffset);
      }
    }

    private class SelectedAsmLines
    {
      public int richTextLineIndex;
    }
    private List<SelectedAsmLines> detailedAsmSelection = new List<SelectedAsmLines>();

    public FunctionAnalysis()
    {
      InitializeComponent();
    }

    public void Initialize(ProcessInfo info, string functionName)
    {
      font = FindInstalledFont();

      processInfo = info;
      ProcessInfo.SymbolInfo functionInfo = info.symbolInfos.Find(x => x.name == functionName);

      // Get the source info for our function
      bytecode = processInfo.GetBytecode(functionInfo);
      if(bytecode.Length == 0)
      {
        Console.WriteLine("Couldn't get bytecode for function {0}", functionInfo.name);
        return;
      }
      List<UInt64> addresses = new List<ulong>();
      if(Assembler.Disassemble(bytecode, out instructions))
      {
        UInt64 address = functionInfo.address;
        foreach(Assembler.Instruction instruction in instructions)
        {
          addresses.Add(address);
          address += (UInt64)instruction.bytes.Count();
        }
      }
      else
      {
        Console.WriteLine("Couldn't disassemble bytecode");
        return;
      }
      sourceLines = processInfo.GetSource(addresses);

      InitializeAsm(functionInfo);
      InitializeCpp(functionInfo);

      //RoutedEventHandler OnLoaded = null;
      //OnLoaded = (object sender, RoutedEventArgs e) =>
      //{
      //  InitializeDetails();
      //  Loaded -= OnLoaded;
      //};
      //Loaded += OnLoaded;
    }

    private void InitializeDetails(byte[] selectedBytecode)
    {
      LLVMWrangler.LLVMMCAData mca = LLVMWrangler.GetLLVMMCAData(selectedBytecode);

      //=========================================================================
      // Overview
      {
        uiOverviewTextValues.Text = string.Format("{0}\n{1}\n{2}\n{3}\n\n{4}\n{5}\n{6}\n{7}",
          mca.iterations,
          mca.instructions,
          mca.cycles,
          mca.uOps,
          mca.dispatchWidth,
          mca.uOpsPerCycle,
          mca.ipc,
          mca.blockRThroughput);

        double marginX = 12;
        double marginY = 12;
        double barHeight = 24;

        double resourceNamesWidth = 0.0;
        foreach(string resourceName in mca.resourceNames)
          resourceNamesWidth = Math.Max(resourceNamesWidth, StringToFormattedText(resourceName).Width);
        double headerLineX = marginX + resourceNamesWidth + 6.0;
        double barMaxWidth = uiOverviewCanvas.ActualWidth - headerLineX - marginX;

        SolidColorBrush barBrush = new SolidColorBrush(Color.FromRgb(204, 230, 255));
        SolidColorBrush barStroke = new SolidColorBrush(Color.FromRgb(169, 195, 245));


        double maxPressure = 0.0;
        foreach(double pressure in mca.resourceTotalPressure)
          maxPressure = Math.Max(maxPressure, pressure);

        uiOverviewCanvas.Children.Clear();
        for(int i = 0; i < mca.resourceNames.Count(); i++)
        {
          TextBlock text = new TextBlock();
          text.Text = mca.resourceNames[i];
          Canvas.SetLeft(text, marginX + resourceNamesWidth - StringToFormattedText(text.Text).Width);
          Canvas.SetTop(text, i * barHeight + marginY + 4);
          uiOverviewCanvas.Children.Add(text);

          Rectangle rect = new Rectangle();
          rect.SnapsToDevicePixels = true;
          rect.Fill = barBrush;
          rect.Stroke = barStroke;
          rect.Width = mca.resourceTotalPressure[i] / maxPressure * barMaxWidth;
          rect.Height = barHeight;
          Canvas.SetLeft(rect, headerLineX);
          Canvas.SetTop(rect, i * barHeight + marginY);
          uiOverviewCanvas.Children.Add(rect);
        }

        Line headerLine = new Line();
        headerLine.SnapsToDevicePixels = true;
        headerLine.Stroke = Brushes.Black;
        headerLine.StrokeThickness = 1.0;
        headerLine.X1 = headerLineX;
        headerLine.Y1 = marginY;
        headerLine.X2 = headerLineX;
        headerLine.Y2 = marginY + barHeight * mca.resourceNames.Count();
        uiOverviewCanvas.Children.Add(headerLine);
      }

      //=========================================================================
      // Timeline
      {
        double boxWidth = 8;
        double boxHeight = 8;
        //Dispatched, Executing, Executed, Retired, Waiting, None
        Brush brushDispatched = new SolidColorBrush(Color.FromRgb(96, 128, 196));
        Brush brushExecuting = new SolidColorBrush(Color.FromRgb(128, 192, 96));
        Brush brushExecuted = new SolidColorBrush(Color.FromRgb(128, 255, 96));
        Brush brushRetired = new SolidColorBrush(Color.FromRgb(96, 128, 255));
        Brush brushWaiting = new SolidColorBrush(Color.FromRgb(255, 128, 96));
        Brush brushNone = new SolidColorBrush(Color.FromRgb(222, 222, 222));
        Brush boxStroke = new SolidColorBrush(Color.FromRgb(196, 196, 196));
        Brush[] brushes = { brushDispatched, brushExecuting, brushExecuted, brushRetired, brushWaiting, brushNone };

        uiTimelineCanvas.Children.Clear();
        for(int instructionIndex = 0; instructionIndex < mca.timelineSchedules.Count(); instructionIndex++)
        {
          List<LLVMWrangler.LLVMMCAData.ScheduleStatus> schedule = mca.timelineSchedules[instructionIndex].schedule;

          // Add some hover UI feedback
          Rectangle rectInstruction = new Rectangle();
          rectInstruction.SnapsToDevicePixels = true;
          rectInstruction.Fill = Brushes.Transparent;
          rectInstruction.Width = boxWidth * schedule.Count();
          rectInstruction.Height = boxHeight;
          Canvas.SetLeft(rectInstruction, 0);
          Canvas.SetTop(rectInstruction, boxHeight * instructionIndex);
          int copyIndex = detailedAsmSelection[instructionIndex % detailedAsmSelection.Count()].richTextLineIndex;
          FrameworkElement copyElement = asmDisplayLines[copyIndex].line;
          rectInstruction.MouseEnter += (sender, e) =>
          {
            uiTimelineSourceLine.Content = DuplicateRichTextBox((RichTextBox)copyElement);
            Console.WriteLine("Hovering instruction index: {0}", copyIndex);
          };
          uiTimelineCanvas.Children.Add(rectInstruction);

          // Add all the pretty squares for each schedule event
          for(int i = 0; i < schedule.Count(); i++)
          {
            Rectangle rect = new Rectangle();
            rect.SnapsToDevicePixels = true;
            rect.Fill = brushes[(int)schedule[i]];
            rect.Stroke = boxStroke;
            rect.Width = boxWidth;
            rect.Height = boxHeight;
            rect.IsHitTestVisible = false;
            Canvas.SetLeft(rect, boxWidth * i);
            Canvas.SetTop(rect, boxHeight * instructionIndex);
            uiTimelineCanvas.Children.Add(rect);
          }
        }
      }
    }

    private void InitializeAsm(ProcessInfo.SymbolInfo functionInfo)
    {
      List<string> rawPrintedInstructions = LLVMWrangler.GetAssemblyPrintout(bytecode);
      if(rawPrintedInstructions.Count() != instructions.Count())
      {
        Console.WriteLine("Mismatch in disassembly");
        return;
      }
      for(int i = 0; i < rawPrintedInstructions.Count(); i++)
        rawPrintedInstructions[i] = rawPrintedInstructions[i].Split('#')[0].Trim();

      // find rip relative addresses that we can identify
      UInt64 address = functionInfo.address;
      for(int i = 0; i < instructions.Count(); i++)
      {
        address += (UInt64)instructions[i].bytes.Count();
        if(instructions[i].ripRelativeSize != Assembler.OperandSize.opNone)
        {
          int rel = Assembler.GetOperandFromBytes(instructions[i].bytes.ToArray(), (int)instructions[i].ripRelativeOffsetOffset, instructions[i].ripRelativeSize);
          ProcessInfo.SymbolInfo info;
          UInt64 symbolOffset;
          processInfo.GetClosestFunction(address + (UInt64)rel, out info, out symbolOffset);
          if(symbolOffset < info.size)
          {
            string comment = " # " + info.name;
            if(symbolOffset != 0)
              comment += " + " + symbolOffset.ToString("X");
            if(Object.ReferenceEquals(functionInfo, info))
              comment += " (" + (address + (UInt64)rel).ToString("X16") + ")";
            rawPrintedInstructions[i] = rawPrintedInstructions[i] + comment;
          }
        }
      }

      string asmSourceString = string.Join("\n", rawPrintedInstructions);

      // find words, comments, numbers, etc.
      MatchCollection words = Regex.Matches(asmSourceString, @"\b(\w+)\b");
      MatchCollection numbers = Regex.Matches(asmSourceString, @"\b(\d+)\b");
      MatchCollection singlelineComments = Regex.Matches(asmSourceString, @"#.*");
      Brush brushDarkPurple = new SolidColorBrush(Color.FromRgb(0x50, 0, 0x50));
      brushDarkPurple.Freeze();

      // Set the colors for our source based on the matches we found
      Brush[] asmSourceColor = new Brush[asmSourceString.Length];
      for(int i = 0; i < asmSourceColor.Length; i++)
        asmSourceColor[i] = Brushes.Black;

      Action<Match, Brush> setColors = (match, color) =>
      {
        for(int i = match.Index; i < match.Index + match.Length; i++)
          asmSourceColor[i] = color;
      };
      foreach(Match match in words)
      {
        if(IsOpcode(match.Value))
          setColors(match, Brushes.DarkGreen);
        else if(IsRegister(match.Value))
          setColors(match, Brushes.DarkBlue);
        else
          setColors(match, brushDarkPurple);
      }
      foreach(Match match in numbers) setColors(match, Brushes.DarkRed);
      foreach(Match match in singlelineComments) setColors(match, Brushes.Gray);

      asmDisplayLines =  CreateDataGridEntries(asmSourceColor, asmSourceString, uiAsm);
      UInt64 offset = functionInfo.address;
      for(int i = 0; i < asmDisplayLines.Count(); i++)
      {
        asmDisplayLines[i].lineOffset = offset.ToString("X16");
        asmDisplayLines[i].index = i;
        offset += (UInt64)instructions[i].bytes.Count();
      }
    }

    private class FileSourceInfo
    {
      public int firstLineNumber = int.MaxValue;
      public int lastLineNumber = 0;
    };
    private class AggregateSourceInfo
    {
      public Dictionary<string, FileSourceInfo> files = new Dictionary<string, FileSourceInfo>();
    };

    private void InitializeCpp(ProcessInfo.SymbolInfo functionInfo)
    {
      // Clean up the source info so we have one contiguous source
      AggregateSourceInfo cppSourceInfo = new AggregateSourceInfo();
      foreach(ProcessInfo.SourceInfo lineSourceInfo in sourceLines)
      {
        if(!cppSourceInfo.files.ContainsKey(lineSourceInfo.filename))
          cppSourceInfo.files.Add(lineSourceInfo.filename, new FileSourceInfo());

        ref int firstLineNumber = ref cppSourceInfo.files[lineSourceInfo.filename].firstLineNumber;
        firstLineNumber = Math.Min(firstLineNumber, lineSourceInfo.lineNumber);
        ref int lastLineNumber = ref cppSourceInfo.files[lineSourceInfo.filename].lastLineNumber;
        lastLineNumber = Math.Max(lastLineNumber, lineSourceInfo.lineNumber);
      }

      if(cppSourceInfo.files.Count() == 0)
      {
        Console.WriteLine("Failed to find any source!");
        return;
      }

      // a bit before and after
      foreach(FileSourceInfo fileInfo in cppSourceInfo.files.Values)
      {
        fileInfo.firstLineNumber = Math.Max(0, fileInfo.firstLineNumber - 3);
        fileInfo.lastLineNumber += 3;
      }

      // read in the source from files
      KeyValuePair<string, FileSourceInfo> cppInfo = cppSourceInfo.files.ToList()[0];
      List<string> fullFileLines = System.IO.File.ReadLines(cppInfo.Key).ToList();
      int firstLinesIndex = cppInfo.Value.firstLineNumber - 1;
      firstLinesIndex = Math.Max(0, firstLinesIndex);
      int linesCount = cppInfo.Value.lastLineNumber - cppInfo.Value.firstLineNumber + 1;
      if(firstLinesIndex + linesCount > fullFileLines.Count())
        linesCount = fullFileLines.Count() - firstLinesIndex;
      List<string> lines = fullFileLines.GetRange(firstLinesIndex, linesCount);
      string cppSourceString = string.Join("\n", lines);
      if(cppSourceString.Length == 0)
      {
        Console.WriteLine("Source file was empty?!?");
        return;
      }

      // find words, comments, functions, etc.
      MatchCollection words = Regex.Matches(cppSourceString, @"\b(\w+)\b");
      MatchCollection functions = Regex.Matches(cppSourceString, @"\b(\w+)\b\s*\(");
      MatchCollection strings = Regex.Matches(cppSourceString, @"""[\S\s]*?""");
      MatchCollection singlelineComments = Regex.Matches(cppSourceString, @"\/\/.*");
      MatchCollection multilineComments = Regex.Matches(cppSourceString, @"\/\*[\S\s]*?\*\/");

      // Set the colors for our source based on the matches we found
      Brush[] cppSourceColor = new Brush[cppSourceString.Length];
      for(int i = 0; i < cppSourceColor.Length; i++)
        cppSourceColor[i] = Brushes.Black;

      Action<Match, Brush> setColors = (match, color) =>
      {
        for(int i = match.Index; i < match.Index + match.Length; i++)
          cppSourceColor[i] = color;
      };
      foreach(Match match in words)
      {
        if(IsKeyword(match.Value))
          setColors(match, Brushes.DarkBlue);
        else
          setColors(match, Brushes.DarkCyan);
      }
      foreach(Match match in functions) setColors(match, Brushes.DarkRed);
      foreach(Match match in strings)   setColors(match, Brushes.DarkOrange);
      foreach(Match match in singlelineComments) setColors(match, Brushes.DarkGreen);
      foreach(Match match in multilineComments)  setColors(match, Brushes.DarkGreen);

      cppDisplayLines = CreateDataGridEntries(cppSourceColor, cppSourceString, uiCpp);
      for(int i = 0; i < cppDisplayLines.Count(); i++)
      {
        cppDisplayLines[i].lineOffset = (cppInfo.Value.firstLineNumber + i).ToString();
        cppDisplayLines[i].index = i;
      }
    }

    RichTextLine CreateNewLine()
    {
      RichTextBox textBox = new RichTextBox();
      textBox.BorderThickness = new Thickness(0);
      textBox.IsHitTestVisible = false;
      textBox.Background = new SolidColorBrush(Colors.Transparent);
      textBox.Document = new FlowDocument();
      textBox.Document.FontFamily = font;

      Paragraph theParagraph = new Paragraph();
      textBox.Document.Blocks.Add(theParagraph);

      RichTextLine newLine = new RichTextLine();
      newLine.line = textBox;

      return newLine;
    }

    RichTextBox DuplicateRichTextBox(RichTextBox source)
    {
      RichTextBox result = (RichTextBox)CreateNewLine().line;
      foreach(Run run in (source.Document.Blocks.Last() as Paragraph).Inlines.ToList())
      {
        Run theRun = new Run(run.Text);
        theRun.Foreground = run.Foreground;
        (result.Document.Blocks.Last() as Paragraph).Inlines.Add(theRun);
      }
      return result;
    }

    List<RichTextLine> CreateDataGridEntries(Brush[] colorPerCharacter, string fullString, DataGrid outputGrid)
    {
      // turn all this data into actual visible wpf lines
      List<RichTextLine> displayLines = new List<RichTextLine>();

      RichTextLine currentLine = CreateNewLine();
      Brush currentColor = colorPerCharacter[0];
      int currentRunStart = 0;
      double minWidth = 0;
      Action<string> AddRun = str =>
      {
        Run theRun = new Run(str);
        theRun.Foreground = currentColor;
        ((currentLine.line as RichTextBox).Document.Blocks.Last() as Paragraph).Inlines.Add(theRun);
      };
      Action AddLine = () =>
      {
        displayLines.Add(currentLine);
        RichTextBox theBox = currentLine.line as RichTextBox;
        theBox.Width = theBox.Document.GetFormattedText().WidthIncludingTrailingWhitespace + 20.0;
        minWidth = Math.Max(minWidth, theBox.Width);
      };
      for(int i = 0; i < fullString.Length; i++)
      {
        if(colorPerCharacter[i] != currentColor || fullString[i] == '\n')
        {
          AddRun(fullString.Substring(currentRunStart, i - currentRunStart).Trim(new char[] { '\n' }));
          currentColor = colorPerCharacter[i];
          currentRunStart = i;
        }
        if(fullString[i] == '\n')
        {
          AddLine();
          currentLine = CreateNewLine();
        }
      }
      AddRun(fullString.Substring(currentRunStart, fullString.Length - currentRunStart).Trim(new char[] { '\n' }));
      AddLine();

      outputGrid.Columns[1].MinWidth = minWidth;

      outputGrid.ItemsSource = displayLines;

      outputGrid.Loaded += (sender, eventArgs) =>
      {
        var scp = WPFHelpers.FindVisualChild<ScrollContentPresenter>(outputGrid);
        scp.RequestBringIntoView += (s, e) => e.Handled = true;
      };

      return displayLines;
    }

    private bool IsKeyword(string text)
    {
	    List<string> keywords = new List<string>(new string[]
	    {
		    "alignas", "default", "register",
		    "alignof", "delete", "reinterpret_cast",
		    "and", "do", "requires",
		    "and_eq", "double", "return",
		    "asm", "dynamic_cast", "short",
		    "atomic_cancel", "else", "signed",
		    "atomic_commit", "enum", "sizeof",
		    "atomic_noexcept", "explicit", "static",
		    "auto", "export", "static_assert",
		    "bitand", "extern", "static_cast",
		    "bitor", "false", "struct",
		    "bool", "float", "switch",
		    "break", "for", "synchronized",
		    "case", "friend", "template",
		    "catch", "goto", "this",
		    "char", "if", "thread_local",
		    "char8_t", "inline", "throw",
		    "char16_t", "int", "true",
		    "char32_t", "long", "try",
		    "class", "mutable", "typedef",
		    "compl", "namespace", "typeid",
		    "concept", "new", "typename",
		    "const", "noexcept", "union",
		    "consteval", "not", "unsigned",
		    "constexpr", "not_eq", "using",
		    "constinit", "nullptr", "virtual",
		    "const_cast", "operator", "void",
		    "continue", "or", "volatile",
		    "co_await", "or_eq", "wchar_t",
		    "co_return", "private", "while",
		    "co_yield", "protected", "xor",
		    "decltype", "public", "xor_eq",
		    "reflexpr",
	    });
	    for(int i = 0; i < keywords.Count(); i++)
		    if(text == keywords[i])
			    return true;
	    return false;
    }

    private bool IsOpcode(string text)
    {
      List<string> keywords = new List<string>(new string[]
      {
        "aaa", "aad", "aam", "aas", "adc", "adcx", "add", "addpd", "addps", "addsd", "addss", "addsubpd", "addsubps", "adox", "aesdec", "aesdeclast", "aesenc", "aesenclast", "aesimc", "aeskeygenassist", "and", "andn", "andnpd", "andnps", "andpd", "andps", "arpl", "bextr", "blendpd", "blendps", "blendvpd", "blendvps", "blsi", "blsmsk", "blsr", "bndcl", "bndcn", "bndcu", "bndldx", "bndmk", "bndmov", "bndstx", "bound", "bsf", "bsr", "bswap", "bt", "btc", "btr", "bts", "bzhi", "call", "cbw", "cdq", "cdqe", "clac", "clc", "cld", "cldemote", "clflush", "clflushopt", "cli", "clts", "clwb", "cmc", "cmovcc", "cmp", "cmppd", "cmpps", "cmps", "cmpsb", "cmpsd", "cmpsd", "cmpsq", "cmpss", "cmpsw", "cmpxchg", "cmpxchg16b", "cmpxchg8b", "comisd", "comiss", "cpuid", "cqo", "crc32", "cvtdq2pd", "cvtdq2ps", "cvtpd2dq", "cvtpd2pi", "cvtpd2ps", "cvtpi2pd", "cvtpi2ps", "cvtps2dq", "cvtps2pd", "cvtps2pi", "cvtsd2si", "cvtsd2ss", "cvtsi2sd", "cvtsi2ss", "cvtss2sd", "cvtss2si", "cvttpd2dq", "cvttpd2pi", "cvttps2dq", "cvttps2pi", "cvttsd2si", "cvttss2si", "cwd", "cwde", "daa", "das", "dec", "div", "divpd", "divps", "divsd", "divss", "dppd", "dpps", "emms", "enter", "extractps", "f2xm1", "fabs", "fadd", "faddp", "fbld", "fbstp", "fchs", "fclex", "fcmovcc", "fcom", "fcomi", "fcomip", "fcomp", "fcompp", "fcos", "fdecstp", "fdiv", "fdivp", "fdivr", "fdivrp", "ffree", "fiadd", "ficom", "ficomp", "fidiv", "fidivr", "fild", "fimul", "fincstp", "finit", "fist", "fistp", "fisttp", "fisub", "fisubr", "fld", "fld1", "fldcw", "fldenv", "fldl2e", "fldl2t", "fldlg2", "fldln2", "fldpi", "fldz", "fmul", "fmulp", "fnclex", "fninit", "fnop", "fnsave", "fnstcw", "fnstenv", "fnstsw", "fpatan", "fprem", "fprem1", "fptan", "frndint", "frstor", "fsave", "fscale", "fsin", "fsincos", "fsqrt", "fst", "fstcw", "fstenv", "fstp", "fstsw", "fsub", "fsubp", "fsubr", "fsubrp", "ftst", "fucom", "fucomi", "fucomip", "fucomp", "fucompp", "fwait", "fxam", "fxch", "fxrstor", "fxsave", "fxtract", "fyl2x", "fyl2xp1", "gf2p8affineinvqb", "gf2p8affineqb", "gf2p8mulb", "haddpd", "haddps", "hlt", "hsubpd", "hsubps", "idiv", "imul", "in", "inc", "ins", "insb", "insd", "insertps", "insw", "int", "int1", "int3", "into", "invd", "invlpg", "invpcid", "iret", "iretd", "jmp", "jcc", "kaddb", "kaddd", "kaddq", "kaddw", "kandb", "kandd", "kandnb", "kandnd", "kandnq", "kandnw", "kandq", "kandw", "kmovb", "kmovd", "kmovq", "kmovw", "knotb", "knotd", "knotq", "knotw", "korb", "kord", "korq", "kortestb", "kortestd", "kortestq", "kortestw", "korw", "kshiftlb", "kshiftld", "kshiftlq", "kshiftlw", "kshiftrb", "kshiftrd", "kshiftrq", "kshiftrw", "ktestb", "ktestd", "ktestq", "ktestw", "kunpckbw", "kunpckdq", "kunpckwd", "kxnorb", "kxnord", "kxnorq", "kxnorw", "kxorb", "kxord", "kxorq", "kxorw", "lahf", "lar", "lddqu", "ldmxcsr", "lds", "lea", "leave", "les", "lfence", "lfs", "lgdt", "lgs", "lidt", "lldt", "lmsw", "lock", "lods", "lodsb", "lodsd", "lodsq", "lodsw", "loop", "loopcc", "lsl", "lss", "ltr", "lzcnt", "maskmovdqu", "maskmovq", "maxpd", "maxps", "maxsd", "maxss", "mfence", "minpd", "minps", "minsd", "minss", "monitor", "mov", "mov", "mov", "movapd", "movaps", "movbe", "movd", "movddup", "movdir64b", "movdiri", "movdq2q", "movdqa", "movdqu", "movhlps", "movhpd", "movhps", "movlhps", "movlpd", "movlps", "movmskpd", "movmskps", "movntdq", "movntdqa", "movnti", "movntpd", "movntps", "movntq", "movq", "movq", "movq2dq", "movs", "movsb", "movsd", "movsd", "movshdup", "movsldup", "movsq", "movss", "movsw", "movsx", "movsxd", "movupd", "movups", "movzx", "mpsadbw", "mul", "mulpd", "mulps", "mulsd", "mulss", "mulx", "mwait", "neg", "nop", "not", "or", "orpd", "orps", "out", "outs", "outsb", "outsd", "outsw", "pabsb", "pabsd", "pabsq", "pabsw", "packssdw", "packsswb", "packusdw", "packuswb", "paddb", "paddd", "paddq", "paddsb", "paddsw", "paddusb", "paddusw", "paddw", "palignr", "pand", "pandn", "pause", "pavgb", "pavgw", "pblendvb", "pblendw", "pclmulqdq", "pcmpeqb", "pcmpeqd", "pcmpeqq", "pcmpeqw", "pcmpestri", "pcmpestrm", "pcmpgtb", "pcmpgtd", "pcmpgtq", "pcmpgtw", "pcmpistri", "pcmpistrm", "pdep", "pext", "pextrb", "pextrd", "pextrq", "pextrw", "phaddd", "phaddsw", "phaddw", "phminposuw", "phsubd", "phsubsw", "phsubw", "pinsrb", "pinsrd", "pinsrq", "pinsrw", "pmaddubsw", "pmaddwd", "pmaxsb", "pmaxsd", "pmaxsq", "pmaxsw", "pmaxub", "pmaxud", "pmaxuq", "pmaxuw", "pminsb", "pminsd", "pminsq", "pminsw", "pminub", "pminud", "pminuq", "pminuw", "pmovmskb", "pmovsx", "pmovzx", "pmuldq", "pmulhrsw", "pmulhuw", "pmulhw", "pmulld", "pmullq", "pmullw", "pmuludq", "pop", "popa", "popad", "popcnt", "popf", "popfd", "popfq", "por", "prefetchw", "prefetchh", "psadbw", "pshufb", "pshufd", "pshufhw", "pshuflw", "pshufw", "psignb", "psignd", "psignw", "pslld", "pslldq", "psllq", "psllw", "psrad", "psraq", "psraw", "psrld", "psrldq", "psrlq", "psrlw", "psubb", "psubd", "psubq", "psubsb", "psubsw", "psubusb", "psubusw", "psubw", "ptest", "ptwrite", "punpckhbw", "punpckhdq", "punpckhqdq", "punpckhwd", "punpcklbw", "punpckldq", "punpcklqdq", "punpcklwd", "push", "pusha", "pushad", "pushf", "pushfd", "pushfq", "pxor", "rcl", "rcpps", "rcpss", "rcr", "rdfsbase", "rdgsbase", "rdmsr", "rdpid", "rdpkru", "rdpmc", "rdrand", "rdseed", "rdtsc", "rdtscp", "rep", "repe", "repne", "repnz", "repz", "ret", "rol", "ror", "rorx", "roundpd", "roundps", "roundsd", "roundss", "rsm", "rsqrtps", "rsqrtss", "sahf", "sal", "sar", "sarx", "sbb", "scas", "scasb", "scasd", "scasw", "setcc", "sfence", "sgdt", "sha1msg1", "sha1msg2", "sha1nexte", "sha1rnds4", "sha256msg1", "sha256msg2", "sha256rnds2", "shl", "shld", "shlx", "shr", "shrd", "shrx", "shufpd", "shufps", "sidt", "sldt", "smsw", "sqrtpd", "sqrtps", "sqrtsd", "sqrtss", "stac", "stc", "std", "sti", "stmxcsr", "stos", "stosb", "stosd", "stosq", "stosw", "str", "sub", "subpd", "subps", "subsd", "subss", "swapgs", "syscall", "sysenter", "sysexit", "sysret", "test", "tpause", "tzcnt", "ucomisd", "ucomiss", "ud", "umonitor", "umwait", "unpckhpd", "unpckhps", "unpcklpd", "unpcklps", "valignd", "valignq", "vblendmpd", "vblendmps", "vbroadcast", "vcompresspd", "vcompressps", "vcvtpd2qq", "vcvtpd2udq", "vcvtpd2uqq", "vcvtph2ps", "vcvtps2ph", "vcvtps2qq", "vcvtps2udq", "vcvtps2uqq", "vcvtqq2pd", "vcvtqq2ps", "vcvtsd2usi", "vcvtss2usi", "vcvttpd2qq", "vcvttpd2udq", "vcvttpd2uqq", "vcvttps2qq", "vcvttps2udq", "vcvttps2uqq", "vcvttsd2usi", "vcvttss2usi", "vcvtudq2pd", "vcvtudq2ps", "vcvtuqq2pd", "vcvtuqq2ps", "vcvtusi2sd", "vcvtusi2ss", "vdbpsadbw", "verr", "verw", "vexpandpd", "vexpandps", "vextractf128", "vextractf32x4", "vextractf32x8", "vextractf64x2", "vextractf64x4", "vextracti128", "vextracti32x4", "vextracti32x8", "vextracti64x2", "vextracti64x4", "vfixupimmpd", "vfixupimmps", "vfixupimmsd", "vfixupimmss", "vfmadd132pd", "vfmadd132ps", "vfmadd132sd", "vfmadd132ss", "vfmadd213pd", "vfmadd213ps", "vfmadd213sd", "vfmadd213ss", "vfmadd231pd", "vfmadd231ps", "vfmadd231sd", "vfmadd231ss", "vfmaddsub132pd", "vfmaddsub132ps", "vfmaddsub213pd", "vfmaddsub213ps", "vfmaddsub231pd", "vfmaddsub231ps", "vfmsub132pd", "vfmsub132ps", "vfmsub132sd", "vfmsub132ss", "vfmsub213pd", "vfmsub213ps", "vfmsub213sd", "vfmsub213ss", "vfmsub231pd", "vfmsub231ps", "vfmsub231sd", "vfmsub231ss", "vfmsubadd132pd", "vfmsubadd132ps", "vfmsubadd213pd", "vfmsubadd213ps", "vfmsubadd231pd", "vfmsubadd231ps", "vfnmadd132pd", "vfnmadd132ps", "vfnmadd132sd", "vfnmadd132ss", "vfnmadd213pd", "vfnmadd213ps", "vfnmadd213sd", "vfnmadd213ss", "vfnmadd231pd", "vfnmadd231ps", "vfnmadd231sd", "vfnmadd231ss", "vfnmsub132pd", "vfnmsub132ps", "vfnmsub132sd", "vfnmsub132ss", "vfnmsub213pd", "vfnmsub213ps", "vfnmsub213sd", "vfnmsub213ss", "vfnmsub231pd", "vfnmsub231ps", "vfnmsub231sd", "vfnmsub231ss", "vfpclasspd", "vfpclassps", "vfpclasssd", "vfpclassss", "vgatherdpd", "vgatherdpd", "vgatherdps", "vgatherdps", "vgatherqpd", "vgatherqpd", "vgatherqps", "vgatherqps", "vgetexppd", "vgetexpps", "vgetexpsd", "vgetexpss", "vgetmantpd", "vgetmantps", "vgetmantsd", "vgetmantss", "vinsertf128", "vinsertf32x4", "vinsertf32x8", "vinsertf64x2", "vinsertf64x4", "vinserti128", "vinserti32x4", "vinserti32x8", "vinserti64x2", "vinserti64x4", "vmaskmov", "vmovdqa32", "vmovdqa64", "vmovdqu16", "vmovdqu32", "vmovdqu64", "vmovdqu8", "vpblendd", "vpblendmb", "vpblendmd", "vpblendmq", "vpblendmw", "vpbroadcast", "vpbroadcastb", "vpbroadcastd", "vpbroadcastm", "vpbroadcastq", "vpbroadcastw", "vpcmpb", "vpcmpd", "vpcmpq", "vpcmpub", "vpcmpud", "vpcmpuq", "vpcmpuw", "vpcmpw", "vpcompressd", "vpcompressq", "vpconflictd", "vpconflictq", "vperm2f128", "vperm2i128", "vpermb", "vpermd", "vpermi2b", "vpermi2d", "vpermi2pd", "vpermi2ps", "vpermi2q", "vpermi2w", "vpermilpd", "vpermilps", "vpermpd", "vpermps", "vpermq", "vpermt2b", "vpermt2d", "vpermt2pd", "vpermt2ps", "vpermt2q", "vpermt2w", "vpermw", "vpexpandd", "vpexpandq", "vpgatherdd", "vpgatherdd", "vpgatherdq", "vpgatherdq", "vpgatherqd", "vpgatherqd", "vpgatherqq", "vpgatherqq", "vplzcntd", "vplzcntq", "vpmadd52huq", "vpmadd52luq", "vpmaskmov", "vpmovb2m", "vpmovd2m", "vpmovdb", "vpmovdw", "vpmovm2b", "vpmovm2d", "vpmovm2q", "vpmovm2w", "vpmovq2m", "vpmovqb", "vpmovqd", "vpmovqw", "vpmovsdb", "vpmovsdw", "vpmovsqb", "vpmovsqd", "vpmovsqw", "vpmovswb", "vpmovusdb", "vpmovusdw", "vpmovusqb", "vpmovusqd", "vpmovusqw", "vpmovuswb", "vpmovw2m", "vpmovwb", "vpmultishiftqb", "vprold", "vprolq", "vprolvd", "vprolvq", "vprord", "vprorq", "vprorvd", "vprorvq", "vpscatterdd", "vpscatterdq", "vpscatterqd", "vpscatterqq", "vpsllvd", "vpsllvq", "vpsllvw", "vpsravd", "vpsravq", "vpsravw", "vpsrlvd", "vpsrlvq", "vpsrlvw", "vpternlogd", "vpternlogq", "vptestmb", "vptestmd", "vptestmq", "vptestmw", "vptestnmb", "vptestnmd", "vptestnmq", "vptestnmw", "vrangepd", "vrangeps", "vrangesd", "vrangess", "vrcp14pd", "vrcp14ps", "vrcp14sd", "vrcp14ss", "vreducepd", "vreduceps", "vreducesd", "vreducess", "vrndscalepd", "vrndscaleps", "vrndscalesd", "vrndscaless", "vrsqrt14pd", "vrsqrt14ps", "vrsqrt14sd", "vrsqrt14ss", "vscalefpd", "vscalefps", "vscalefsd", "vscalefss", "vscatterdpd", "vscatterdps", "vscatterqpd", "vscatterqps", "vshuff32x4", "vshuff64x2", "vshufi32x4", "vshufi64x2", "vtestpd", "vtestps", "vzeroall", "vzeroupper", "wait", "wbinvd", "wrfsbase", "wrgsbase", "wrmsr", "wrpkru", "xabort", "xacquire", "xadd", "xbegin", "xchg", "xend", "xgetbv", "xlat", "xlatb", "xor", "xorpd", "xorps", "xrelease", "xrstor", "xrstors", "xsave", "xsavec", "xsaveopt", "xsaves", "xsetbv", "xtest",
        "jo", "jno", "jb", "jnae", "jc", "jnb", "jae", "jnc", "jz", "je", "jnz", "jne", "jbe", "jna", "jnbe", "ja", "js", "jns", "jp", "jpe", "jnp", "jpo", "jl", "jnge", "jnl", "jge", "jle", "jng", "jnle", "jg",
        "cmovo", "cmovno", "cmovb", "cmovnae", "cmovc", "cmovnb", "cmovae", "cmovnc", "cmovz", "cmove", "cmovnz", "cmovne", "cmovbe", "cmovna", "cmovnbe", "cmova", "cmovs", "cmovns", "cmovp", "cmovpe", "cmovnp", "cmovpo", "cmovl", "cmovnge", "cmovnl", "cmovge", "cmovle", "cmovng", "cmovnle", "cmovg",
      });
      for(int i = 0; i < keywords.Count(); i++)
        if(text == keywords[i])
          return true;
      return false;
    }

    private bool IsRegister(string text)
    {
      List<string> keywords = new List<string>(new string[]
      {
        "rax", "eax", "ax", "al",
        "rcx", "ecx", "cx", "cl",
        "rdx", "edx", "dx", "dl",
        "rbx", "ebx", "bx", "bl",
        "rsi", "esi", "si", "sil",
        "rdi", "edi", "di", "dil",
        "rsp", "esp", "sp", "spl",
        "rbp", "ebp", "bp", "bpl",
        "r8",  "r8d",  "r8w",  "r8b",
        "r9",  "r9d",  "r9w",  "r9b",
        "r10", "r10d", "r10w", "r10b",
        "r11", "r11d", "r11w", "r11b",
        "r12", "r12d", "r12w", "r12b",
        "r13", "r13d", "r13w", "r13b",
        "r14", "r14d", "r14w", "r14b",
        "r15", "r15d", "r15w", "r15b",
      });
      for(int i = 0; i < 8; i++)
        keywords.Add("mm" + i);
      for(int i = 0; i < 32; i++)
        keywords.Add("xmm" + i);
      for(int i = 0; i < 32; i++)
        keywords.Add("ymm" + i);
      for(int i = 0; i < 32; i++)
        keywords.Add("zmm" + i);


      for(int i = 0; i < keywords.Count(); i++)
        if(text == keywords[i])
          return true;
      return false;
    }

    FontFamily FindInstalledFont()
    {
      if(new System.Drawing.Font("Consolas", 12).Name == "Consolas")
        return new FontFamily("Consolas");

      if(new System.Drawing.Font("Courier New", 12).Name == "Courier New")
        return new FontFamily("Courier New");

      return new FontFamily();
    }

    Brush brushHighlighted = new SolidColorBrush(Color.FromRgb(0xE6, 0xF2, 0xFF));

    private void uiAsm_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {

    }

    private void uiCpp_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      uiAsm.SelectedItems.Clear();

      for(int i = 0; i < sourceLines.Count(); i++)
        if(sourceLines[i].lineNumber == int.Parse((uiCpp.SelectedItems[0] as RichTextLine).lineOffset))
          uiAsm.SelectedItems.Add(asmDisplayLines[i]);
    }

    private void uiCpp_DataGridRow_MouseEnter(object sender, MouseEventArgs e)
    {
      brushHighlighted.Freeze();
      RichTextLine line = (sender as DataGridRow).Item as RichTextLine;

      for(int i = 0; i < sourceLines.Count(); i++)
        if(sourceLines[i].lineNumber == int.Parse(line.lineOffset))
          asmDisplayLines[i].lineColor = brushHighlighted;
      uiAsm.Items.Refresh();
    }

    private void uiCpp_DataGridRow_MouseLeave(object sender, MouseEventArgs e)
    {
      RichTextLine line = (sender as DataGridRow).Item as RichTextLine;

      for(int i = 0; i < sourceLines.Count(); i++)
        if(sourceLines[i].lineNumber == int.Parse(line.lineOffset))
          asmDisplayLines[i].lineColor = Brushes.White;
      uiAsm.Items.Refresh();
    }

    // TODO: this is also in Histogram.xaml.cs
    private FormattedText StringToFormattedText(string str)
    {
      return new FormattedText(str, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Segoe UI"), 12, Brushes.Black, 1.0);
    }

    private void uiRefreshDetails_Click(object sender, RoutedEventArgs e)
    {
      List<RichTextLine> asms = uiAsm.SelectedItems.Cast<RichTextLine>().ToList();
      asms.Sort();

      List<byte> selectedBytecode = new List<byte>();
      detailedAsmSelection.Clear();
      foreach(RichTextLine asm in asms)
      {
        detailedAsmSelection.Add(new SelectedAsmLines { richTextLineIndex = asm.index});
        selectedBytecode.AddRange(instructions[asm.index].bytes);
      }

      InitializeDetails(selectedBytecode.ToArray());
    }
  }
}
