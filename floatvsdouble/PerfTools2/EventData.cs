﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfTools2
{
  public class EventData
	{
		public class ExclusiveData
		{
			public string name;
			public double totalDuration;
			public Dictionary<string, double> durationsPerParent = new Dictionary<string, double>();
		};

		public string name;
		public UInt64 startInt;
		public UInt64 endInt;
		public double start { get; set; }
		public double end { get; set; }
		public double duration { get; set; }
		public List<EventData> children = new List<EventData>();
		public Dictionary<string, ExclusiveData> excData = new Dictionary<string, ExclusiveData>();
		public EventData parent;
		
		/*
		void DebugPrintEventData(FileWriter debugFile, EventData* data, int depth = 0)
		{
			char spaces[100];
			for(int i = 0; i < 100; i++) spaces[i] = ' ';
			spaces[depth < 100 ? depth : 99] = '\0';

			char debugWriteBuf[10000];
			sprintf_s(debugWriteBuf, 10000, "%s%s: %f(%llu-%llu)\n", spaces, data.name.cstr(), data.duration, data.startInt, data.endInt);
			debugFile.WriteRaw((uint8*)debugWriteBuf, strlen(debugWriteBuf));

			for(size_t i = 0; i < data.children.size(); i++)
				DebugPrintEventData(debugFile, data.children[i], depth+2);
		}
		*/

		public static Dictionary<Int32, EventData> ParseFile(string filename)
		{
			UInt64 globalStart = 0;
			List<UInt64> coreOffsets = new List<UInt64>();
			double secondsPerCycle;
			Dictionary<Int32, EventData> currentParentPerThread = new Dictionary<Int32, EventData>();
			Dictionary<Int32, EventData> result = new Dictionary<Int32, EventData>();

			Func<Int32, UInt64> GetCoreOffset = (Int32 coreId) =>
			{
				if(coreId < coreOffsets.Count())
					return coreOffsets[coreId];
				else
					Console.WriteLine("core doesn't exist {0}?!\n", coreId);
				return 0L;
			};


			using(MyBinaryReader file = new MyBinaryReader(File.Open(filename, FileMode.Open)))
			{
				secondsPerCycle = file.ReadDouble();

				Action<Int32> EnsureThreadExists = (Int32 threadId) =>
				{
					if(!result.ContainsKey(threadId))
					{
						EventData newEvent = new EventData();
						newEvent.name = threadId.ToString();
						result.Add(threadId, newEvent);
						currentParentPerThread.Add(threadId, newEvent);
					}
				};

				while(file.BaseStream.Position != file.BaseStream.Length)
				{
					string tagBuf = Encoding.ASCII.GetString(file.ReadBytes(4));
					if(tagBuf == "push")
					{
						EventData newEvent = new EventData();

						// start time
						Int32 threadId;
						Int32 coreId;
						newEvent.startInt = file.ReadUInt64();
						coreId = file.ReadInt32();
						threadId = file.ReadInt32();
						EnsureThreadExists(threadId);

						// name
						newEvent.name = file.ReadTerminatedString();

						// offset start times
						UInt64 rawStartTime = newEvent.startInt;
						if(globalStart == 0)
							globalStart = newEvent.startInt;
						newEvent.startInt -= globalStart - GetCoreOffset(coreId);
						//Console.WriteLine("New event '{0}', thread {1), start {2}\n", newEvent.name, threadId, newEvent.startInt);

						// link nodes
						newEvent.parent = currentParentPerThread[threadId];
						currentParentPerThread[threadId].children.Add(newEvent);
						currentParentPerThread[threadId] = newEvent;
					}
					else if(tagBuf == "pop ")
					{
						UInt64 time;
						Int32 coreId;
						Int32 threadId;
						time = file.ReadUInt64();
						coreId = file.ReadInt32();
						threadId = file.ReadInt32();
						EnsureThreadExists(threadId);

						// we're starting mid way into a frame, chop off some dangling events.
						if(currentParentPerThread[threadId].parent == null)
						{
							Console.WriteLine("No parent on thread {0}, clearing children\n", threadId);
							globalStart = 0;
							currentParentPerThread[threadId] = result[threadId];
							result[threadId].children.Clear();
						}
						else
						{
							currentParentPerThread[threadId].endInt = time - (globalStart - GetCoreOffset(coreId));
							//Console.WriteLine("Popped event '{0}', thread {1}, start {2}\n", currentParentPerThread[threadId].name, threadId, currentParentPerThread[threadId].endInt);
							EventData currentParent = currentParentPerThread[threadId];
							EndEvent(result[threadId], ref currentParent, secondsPerCycle);
							currentParentPerThread[threadId] = currentParent;
						}
					}
					else if(tagBuf == "ctxs")
					{
						UInt64 time;
						UInt32 prevThreadId;
						UInt32 newThreadId;
						UInt32 coreId;
						time = file.ReadUInt64();
						prevThreadId = file.ReadUInt32();
						newThreadId = file.ReadUInt32();
						coreId = file.ReadUInt32();
					}
					else if(tagBuf == "tsc ")
					{
						UInt32 coreId;
						UInt64 offset;
						coreId = file.ReadUInt32();
						offset = file.ReadUInt64();
						if(coreOffsets.Count() == coreId)
							coreOffsets.Add(offset);
						else
							Console.WriteLine("Unexpected core Id {0} (expecting {1} next)\n", coreId, coreOffsets.Count());
					}
				}

				// 
				foreach(int threadId in result.Keys)
				{
					while(currentParentPerThread[threadId].endInt == 0 && !Object.ReferenceEquals(currentParentPerThread[threadId], result[threadId]))
					{
						if(currentParentPerThread[threadId].children.Count() > 0)
							break;
						EventData temp = currentParentPerThread[threadId];
						currentParentPerThread[threadId].parent.children.RemoveAt(currentParentPerThread[threadId].parent.children.Count() - 1);
						currentParentPerThread[threadId] = currentParentPerThread[threadId].parent;
					}

					while(currentParentPerThread[threadId] != null && currentParentPerThread[threadId].children.Count() > 0)
					{
						currentParentPerThread[threadId].endInt = currentParentPerThread[threadId].children.Last().endInt;
						EventData currentParent = currentParentPerThread[threadId];
						EndEvent(result[threadId], ref currentParent, secondsPerCycle);
						currentParentPerThread[threadId] = currentParent;
					}
				}
			}

			return result;
		}

		private static void EndEvent(EventData result, ref EventData currentParent, double secondsPerCycle)
		{					
			currentParent.start = currentParent.startInt * secondsPerCycle;
			currentParent.end = currentParent.endInt * secondsPerCycle;
			currentParent.duration = currentParent.end - currentParent.start;

			// add to our exclusive data
			if(!result.excData.ContainsKey(currentParent.name))
				result.excData.Add(currentParent.name, new ExclusiveData());
			ExclusiveData excData = result.excData[currentParent.name];
			excData.name = currentParent.name;
			double selfTime = currentParent.duration;
			foreach(EventData child in currentParent.children)
				selfTime -= child.duration;
			excData.totalDuration += selfTime;
			if(currentParent.parent != null)
				if(excData.durationsPerParent.ContainsKey(currentParent.parent.name))
					excData.durationsPerParent[currentParent.parent.name] += selfTime;
				else
					excData.durationsPerParent.Add(currentParent.parent.name, selfTime);

			// add a self child
			if(currentParent.children.Count() > 0)
			{
				EventData newEvent = new EventData();
				newEvent.name = "self";
				newEvent.duration = selfTime;
				newEvent.parent = currentParent;
				currentParent.children.Add(newEvent);
			}

			// Next!
			currentParent = currentParent.parent;
		}

		public static string FormatTime(double time)
		{
			if (time > 1e10)           return string.Format(">1e10s");
			if (time > 100)            return string.Format("{0:0.}s",    time);
			if (time > 10)             return string.Format("{0:0.0}s",   time);
			if (time > 1)              return string.Format("{0:0.00}s",  time);
			if (time > 0.1)            return string.Format("{0:0.}ms",   time * 1000.0);
			if (time > 0.01)           return string.Format("{0:0.0}ms",  time * 1000.0);
			if (time > 0.001)          return string.Format("{0:0.00}ms", time * 1000.0);
			if (time > 0.0001)         return string.Format("{0:0.}us",   time * 1000000.0);
			if (time > 0.00001)        return string.Format("{0:0.0}us",  time * 1000000.0);
			if (time > 0.000001)       return string.Format("{0:0.00}us", time * 1000000.0);
			if (time > 0.0000001)      return string.Format("{0:0.}ns",   time * 1000000000.0);
			if (time > 0.00000001)     return string.Format("{0:0.0}ns",  time * 1000000000.0);
			if (time > 0.000000001)    return string.Format("{0:0.00}ns", time * 1000000000.0);
			if (time > 0.0000000001)   return string.Format("{0:0.}ps",   time * 1000000000000.0);
			if (time > 0.00000000001)  return string.Format("{0:0.0}ps",  time * 1000000000000.0);
			if (time > 0.000000000001) return string.Format("{0:0.00}ps", time * 1000000000000.0);
			return "0s";
		}

  }
}
