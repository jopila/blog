﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerfTools2
{
  partial class ProcessInfo
	{
		public enum ProfileType
		{
			Time,          // time - time to profile for (ms)
			FunctionCount, // count - time that function has to be called before capture ends
			FunctionTime,  // functionTime - min time function has to take for a capture (ms)
		};

		public struct ProfileSettings
		{
			public ProfileType type;
			public Int32 time;
			public SymbolInfo selectedFunction;
			public Int32 functionCount;
			public Int32 functionTime;
		};

		[StructLayout(LayoutKind.Sequential)]
		private struct NativeProfileSettings
		{
			public ProfileType type;
			public Int32 time;
			public Int32 functionCount;
			public Int32 functionTime;
			public UInt64[] functionModificationAddresses;
			public UInt64 detourSize;
		};

		[StructLayout(LayoutKind.Sequential)]
		struct BufferData
		{
			public double secondsPerCycle;
			public double failureSpan;
			public UInt64 failureEventsWritten;
			public Int32 waitingCount;
			public Int32 bufferSize;
			public IntPtr buffer;
		}
		[DllImport(@"..\x64\Release\PerfTools2Native.dll", SetLastError = true)]
		static extern BufferData NativeProfile(NativeProfileSettings settings);

		class ThreadCaptureInfo
		{
			public UInt32 threadId = unchecked((UInt32)(-1));
			public UInt64 prevTime = unchecked((UInt32)(-1));
			public UInt32 prevCoreId = unchecked((UInt32)(-1));
			public List<FunctionInfo> stack = new List<FunctionInfo>();
		};

		public void Profile(ProfileSettings settings, string filename)
		{
			/////////////////////////////////////////////////////////////////////////////
			// Marshal data and call our native helper function
			NativeProfileSettings nativeSettings = new NativeProfileSettings();

			FunctionInfo selectedFunctionInfo = null;
			if(settings.type == ProfileType.FunctionTime || settings.type == ProfileType.FunctionCount)
			{
				if(settings.selectedFunction == null)
				{
					Console.WriteLine("Function type profile selected but no function selected.");
					return;
				}
				selectedFunctionInfo = addressToFunction[settings.selectedFunction.address];
				nativeSettings.functionModificationAddresses = new UInt64[selectedFunctionInfo.modifications.Count()];
				for(int i = 0; i < selectedFunctionInfo.modifications.Count(); i++)
					nativeSettings.functionModificationAddresses[i] = selectedFunctionInfo.modifications[i].address;
			}

			nativeSettings.type = settings.type;
			nativeSettings.time = settings.time;
			nativeSettings.functionCount = settings.functionCount;
			nativeSettings.functionTime = settings.functionTime;
			nativeSettings.detourSize = (UInt64)ProcessInfo.detourCallInstructionSize;

			BufferData bufferData = NativeProfile(nativeSettings);

			List<Timestamp> buffer = new List<Timestamp>();
			buffer.Capacity = bufferData.bufferSize;
			IntPtr ptr = bufferData.buffer;
			Int32 sizeofTimestamp = Marshal.SizeOf(typeof(Timestamp));
			for(int i = 0; i < bufferData.bufferSize; i++)
			{
				buffer.Add((Timestamp)Marshal.PtrToStructure(ptr, typeof(Timestamp)));
				ptr += sizeofTimestamp;
			}
			Marshal.FreeCoTaskMem(bufferData.buffer);
			if(bufferData.failureEventsWritten != 0)
				Console.WriteLine("Warning: Failed to keep up with event stream. Abandoning. Captured  {0}/{1} events over {2}ms", bufferData.bufferSize, bufferData.failureEventsWritten, bufferData.failureSpan * 1000.0);
			Console.WriteLine("Profile Finished: Captured {0} events ({1} waits, {2}s/cycle)", bufferData.bufferSize, bufferData.waitingCount, bufferData.secondsPerCycle);
			
			/////////////////////////////////////////////////////////////////////////////
			// compile the data into our more useful capture file format
			Dictionary<UInt32, ThreadCaptureInfo> threadInfos = new Dictionary<uint, ThreadCaptureInfo>();
			int depth = 0;

			MyBinaryWriter file = new MyBinaryWriter(File.Open(filename, FileMode.Create));

			// Calculate the real time for a tsc tick 
			file.Write(bufferData.secondsPerCycle);

			for(int i = 0; i < Environment.ProcessorCount; i++)
			{
				file.Write("tsc ", false);
				file.Write(i);
				file.Write((Int64)0);
			}

			for(int bufferIndex = 0; bufferIndex < buffer.Count(); bufferIndex++)
			{
				Timestamp timestamp = buffer[bufferIndex];
				FunctionInfo info = detourAddressToFunction[timestamp.address];
				if(!threadInfos.ContainsKey(timestamp.threadId))
					threadInfos.Add(timestamp.threadId, new ThreadCaptureInfo() { threadId = timestamp.threadId });
				ThreadCaptureInfo threadInfo = threadInfos[timestamp.threadId];

				threadInfo.prevCoreId = timestamp.coreId;
				threadInfo.prevTime = timestamp.time;

				if(timestamp.address == info.address + (UInt64)detourCallInstructionSize)
				{
					file.Write("push", false);
					file.Write(timestamp.time);
					file.Write(timestamp.coreId);
					file.Write(timestamp.threadId);
					file.Write(info.name, true);
					//Console.WriteLine("push:\n  {0}\n  time: {1}\n  core: {2}\n  thread: {3}\n\n", info.name, timestamp.time, timestamp.coreId, timestamp.threadId);

					// Debug track the stack to see if some functions aren't capturing the end
					depth++;
					threadInfo.stack.Add(info);
				}
				else
				{
					// Debug track the stack to see if some functions aren't capturing the end
					depth--;
					if(depth < 0)
						depth = 0;
					List<FunctionInfo> stack = threadInfo.stack;
					if(stack.Count() > 0 && stack.Last().name != info.name)
					{
						Console.WriteLine("Warning popped '{0}' (0x{1}) when '{2}' (0x{3}) was on the stack\n", info.name, info.address.ToString("X16"), stack.Last().name, stack.Last().address.ToString("X16"));
						continue;
					}
					if(stack.Count() > 0)
						stack.RemoveAt(stack.Count() - 1);
					else
						continue;

					file.Write("pop ", false);
					file.Write(timestamp.time);
					file.Write(timestamp.coreId);
					file.Write(timestamp.threadId);
					//Console.WriteLine("pop:\n  {0}\n  time: {1}\n  core: {2}\n  thread: {3}\n\n", info.name, timestamp.time, timestamp.coreId, timestamp.threadId);
				}
			}
			file.Close();
		}
  }
}
