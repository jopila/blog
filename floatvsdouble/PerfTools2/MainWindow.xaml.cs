﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace PerfTools2
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private List<TabItem> unsavedReports = new List<TabItem>();

    public MainWindow()
    {
      InitializeComponent();
      rdtsc.init();
      Console.WriteLine(Directory.GetCurrentDirectory());
      Console.WriteLine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location));
      mainWindow = this;
    }

    static MainWindow mainWindow;
    static public MainWindow GetMainWindow() { return mainWindow; }

    private void uiOpen_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog dialog = new OpenFileDialog();
      dialog.Filter = "Injection Profiler Capture File (*.ipc)|*.ipc";
      if(dialog.ShowDialog() == true)
      {
        string filename = dialog.FileName;
        filename = filename.Substring(filename.LastIndexOf('\\') + 1);
        AddTab(filename, new Report(null, dialog.FileName));
      }
    }

    public void AddTab(string name, object content)
    {
      TabItem newTab = new TabItem();
      newTab.Header = name;
      newTab.Content = content;
      uiTabs.Items.Add(newTab);
      uiTabs.SelectedItem = newTab;

      if(content is Report)
        unsavedReports.Add(newTab);
    }

    private void uiSave_Click(object sender, RoutedEventArgs e)
    {
      if(unsavedReports.Contains(uiTabs.SelectedItem))
      {
        SaveFileDialog dialog = new SaveFileDialog();
        dialog.Filter = "Injection Profiler Capture File (*.ipc)|*.ipc";
        if(dialog.ShowDialog() == true)
        {
          string filename = dialog.FileName;
          if(!filename.EndsWith(".ipc"))
            filename += ".ipc";
          File.Delete(filename);
          File.Move((uiTabs.SelectedItem as TabItem).Header as string, filename);
          filename = filename.Substring(filename.LastIndexOf('\\') + 1);
          (uiTabs.SelectedItem as TabItem).Header = filename;
        }
      }
      else
        Console.WriteLine("Unsavable tab selected.");
    }
  }
}
