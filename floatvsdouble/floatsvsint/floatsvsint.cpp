// floatsvsint.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <windows.h>
#include <intrin.h>

#include "..\common\Timing.h"

void TestAdd(unsigned int& acc, int iterations)
{
    // Start with our 5x loop
    Timer timer;
    timer.Start();
    
    // static keeps the compiler from optimizing out the whole loop!
    for(int i = 0; i < iterations; i++)
    {
        // Some wrangling with the compiler needed...
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
    }

    double loopTime = timer.End();
    std::cout << "5x int add Time: " << loopTime * 1000000 << std::endl;

    // And now for the 10x loop
    timer.Start();
    for(int i = 0; i < iterations; i++)
    {
        // Some wrangling with the compiler needed...
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
        _addcarry_u32(0, acc, acc, &acc);
    }

    double intTime = timer.End();
    std::cout << "10x int add Time: " << intTime * 1000000 << std::endl;
}

void TestAdd(float& acc, int iterations)
{
    // Start with our 5x loop
    Timer timer;
    timer.Start();

    // static keeps the compiler from optimizing out the whole loop!
    for(int i = 0; i < iterations; i++)
    {
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
    }

    double loopTime = timer.End();
    std::cout << "5x float add Time: " << loopTime * 1000000 << std::endl;

    // And now for the 10x loop
    timer.Start();
    for(int i = 0; i < iterations; i++)
    {
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
        acc += acc;
    }

    double intTime = timer.End();
    std::cout << "10x float add Time: " << intTime * 1000000 << std::endl;
}


// Copy paste...argh!
template<typename T>
void TestMul(T& acc, int iterations)
{
    Timer timer;
    timer.Start();
  
    for(int i = 0; i < iterations; i++)
    {
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
    }

    double loopTime = timer.End();
    std::cout << "5x " << typeid(T).name() << " mul Time: " << loopTime * 1000000 << std::endl;


    timer.Start();
    for(int i = 0; i < iterations; i++)
    {
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
        acc *= acc;
    }

    double intTime = timer.End();
    std::cout << "10x " << typeid(T).name() << " mul Time: " << intTime * 1000000 << std::endl;
}

// Copy paste...argh!
template<typename T>
void TestDiv(T& acc, int iterations)
{
    Timer timer;
    timer.Start();

    for(int i = 0; i < iterations; i++)
    {
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
    }

    double loopTime = timer.End();
    std::cout << "5x " << typeid(T).name() << " div Time: " << loopTime * 1000000 << std::endl;

    timer.Start();
    for(int i = 0; i < iterations; i++)
    {
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
        acc /= acc;
    }

    double intTime = timer.End();
    std::cout << "10x " << typeid(T).name() << " div Time: " << intTime * 1000000 << std::endl;
}


// Copy paste...argh!
template<typename T>
void TestFixed(T& acc, int iterations)
{
    Timer timer;
    timer.Start();

    for(int i = 0; i < iterations; i++)
    {
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
    }

    double loopTime = timer.End();
    std::cout << "5x " << typeid(T).name() << " fixed point mul Time: " << loopTime * 1000000 << std::endl;

    timer.Start();
    for(int i = 0; i < iterations; i++)
    {
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
        acc *= acc;
        acc >>= 16;
    }

    double intTime = timer.End();
    std::cout << "10x " << typeid(T).name() << " fixed point mul Time: " << intTime * 1000000 << std::endl;
}


int main()
{
    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    volatile int iterations = 1000;
    unsigned int iacc = 1;
    TestAdd(iacc, iterations);
    float facc = 1.0f;
    TestAdd(facc, iterations);
    iacc = 1;
    std::cout << std::endl;

    TestMul<unsigned int>(iacc, iterations);
    int siacc = 1;
    TestMul<int>(siacc, iterations);
    facc = 1.0f;
    TestMul<float>(facc, iterations);
    std::cout << std::endl;

    iacc = 1;
    TestDiv<unsigned int>(iacc, iterations);
    siacc = 1;
    TestDiv<int>(siacc, iterations);
    facc = 1.0f;
    TestDiv<float>(facc, iterations);
    std::cout << std::endl;

    iacc = 1;
    TestFixed(iacc, iterations);

    // Wait for input...
    int dummy;
    std::cin >> dummy;

    return 0;
}

