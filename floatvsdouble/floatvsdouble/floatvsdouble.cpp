
#include "stdafx.h"

#include <vector>

#include "../common/Matrix.h"
#include "../common/Timing.h"

template<typename T, int iterations>
double ArithmeticTest()
{
    Timer timer;
    timer.Start();
    Matrix<T> mat;
    mat.Identity();
    for(int i = 0; i < iterations; i++)
        mat *= mat;
    return timer.End();
}

template<typename T, int iterations>
double ArithmeticMemoryTest()
{
    // Init
    Timer timer;
    Matrix<T>* mats = new Matrix<T>[iterations*2];
    for(int i = 0; i < iterations * 2; i++)
        mats[i].Identity();

    // Test time!
    timer.Start();
    for(int i = 0; i < iterations; i++)
        mats[i] *= mats[i + iterations];
    double time = timer.End();

    // All done
    delete[] mats;
    return time;
}

template<typename T, int iterations>
double ArithmeticInitializationTest()
{
    // Init
    Timer timer;
    Matrix<T>* mats = new Matrix<T>[iterations * 2];

    // Init test?!
    timer.Start();
    for(int i = 0; i < iterations * 2; i++)
        mats[i].Identity();
    double time = timer.End();

    // All done
    delete[] mats;
    return time;
}



int main()
{
    Matrix<float> fmat;
    Matrix<double> dmat;

    // We'll be printing microseconds with 3 decimal digits (to get nanoseconds too)
    std::cout.precision(3);
    std::cout << std::fixed;

    // Print out chrono stats so we know how precise our timing is
    std::cout << "high_resolution_clock" << std::endl;
    std::cout << std::chrono::high_resolution_clock::period::num << std::endl;
    std::cout << std::chrono::high_resolution_clock::period::den << std::endl;
    std::cout << "steady = " << std::chrono::high_resolution_clock::is_steady << std::endl << std::endl;
    
    // Store the results so we can print after the fact
    std::vector<double> times;
    times.reserve(8);

    // Simple test of multiplying matrix by itself
    times.push_back(ArithmeticTest<float, 100>());
    times.push_back(ArithmeticTest<double, 100>());
    times.push_back(ArithmeticTest<float, 1000>());
    times.push_back(ArithmeticTest<double, 1000>());
    times.push_back(ArithmeticTest<float, 10000>());
    times.push_back(ArithmeticTest<double, 10000>());
    times.push_back(ArithmeticTest<float, 1000000>());
    times.push_back(ArithmeticTest<double, 1000000>());

    std::cout << "ArithmeticTest" << std::endl;
    std::cout << "float,     100: " << times[0] * 1000000 << " us" << std::endl;
    std::cout << "float,     100: " << times[1] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[2] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[3] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[4] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[5] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[6] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[7] * 1000000 << " us" << std::endl;

    times.clear();

    // Second test that actually uses some amount of memory
    times.push_back(ArithmeticMemoryTest<float, 100>());
    times.push_back(ArithmeticMemoryTest<double, 100>());
    times.push_back(ArithmeticMemoryTest<float, 1000>());
    times.push_back(ArithmeticMemoryTest<double, 1000>());
    times.push_back(ArithmeticMemoryTest<float, 10000>());
    times.push_back(ArithmeticMemoryTest<double, 10000>());
    times.push_back(ArithmeticMemoryTest<float, 1000000>());
    times.push_back(ArithmeticMemoryTest<double, 1000000>());

    std::cout << "ArithmeticMemoryTest" << std::endl;
    std::cout << "float,     100: " << times[0] * 1000000 << " us" << std::endl;
    std::cout << "float,     100: " << times[1] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[2] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[3] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[4] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[5] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[6] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[7] * 1000000 << " us" << std::endl;

    times.clear();

    // Third test times just the initialization bit of the previous test (Assigning each matrix to an identity matrix)
    times.push_back(ArithmeticInitializationTest<float, 100>());
    times.push_back(ArithmeticInitializationTest<double, 100>());
    times.push_back(ArithmeticInitializationTest<float, 1000>());
    times.push_back(ArithmeticInitializationTest<double, 1000>());
    times.push_back(ArithmeticInitializationTest<float, 10000>());
    times.push_back(ArithmeticInitializationTest<double, 10000>());
    times.push_back(ArithmeticInitializationTest<float, 1000000>());
    times.push_back(ArithmeticInitializationTest<double, 1000000>());

    std::cout << "ArithmeticMemoryTest" << std::endl;
    std::cout << "float,     100: " << times[0] * 1000000 << " us" << std::endl;
    std::cout << "float,     100: " << times[1] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[2] * 1000000 << " us" << std::endl;
    std::cout << "float,    1000: " << times[3] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[4] * 1000000 << " us" << std::endl;
    std::cout << "float,   10000: " << times[5] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[6] * 1000000 << " us" << std::endl;
    std::cout << "float, 1000000: " << times[7] * 1000000 << " us" << std::endl;

    int derp;
    std::cin >> derp;

    return 0;
}

