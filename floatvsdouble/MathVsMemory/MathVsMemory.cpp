// MathVsMemory.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include "..\common\iacaMarks.h"

class ObjectBase
{
public:
	virtual void update(float t) = 0;
	virtual int nopCount() = 0;
};

template<int NOP_COUNT>
__forceinline void DoNop()
{
	__nop();
	DoNop<NOP_COUNT - 1>();
}

template<>
__forceinline void DoNop<1>()
{
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
	__nop();
}

template<int NOP_COUNT>
class Object : public ObjectBase
{
public:
	void update(float t)
	{
		DoNop<NOP_COUNT>();
	}

	int nopCount()
	{
		return NOP_COUNT;
	}
};



struct CharInfo
{
	float w, h;
	float u, v, uw, vh;
	float xoffset;
	float yoffset;
	float xadvance;
};

struct RenderInfo
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
	float r, g, b, a;
};

struct RenderInfoIntColor
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
	int r, g, b, a;
};

struct RenderInfoByteColor
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
	unsigned char r, g, b, a;
};

struct RenderInfoByteColorPadding
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
	unsigned char r, g, b, a;
	float foo, bar, baz;
};

struct RenderInfoGrayscale
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
	float brightness;
};

struct RenderInfoNoColor
{
	float x, y;
	float w, h;
	float u, v, uw, vh;
};


std::vector<CharInfo> charInfos;
std::vector<RenderInfo> renderInfosAsCharInfo;
RenderInfo* renderInfos;
RenderInfoByteColor* renderInfosByteColor;
RenderInfoByteColor* renderInfosByteColorFakeFloat;
RenderInfoByteColorPadding* renderInfosByteColorPadding;
RenderInfoGrayscale* renderInfosGrayscale;
RenderInfoNoColor* renderInfosNoColor;
RenderInfoIntColor* renderInfosIntColor;
float thatOtherFloat = 300.0f;

#define LOG(x) x

Timer timer;

template<typename T>
void InitializeData(int num, std::vector<T>& data)
{
	timer.Start();

	data.reserve(num);
	for(int i = 0; i < num; i++)
	{
		T info;
		for(int j = 0; j < sizeof(T) / sizeof(float); j++)
		{
			*((float*)&info + j) = 1.0f;//0.5f + Random::GetFloat();
		}
		data.push_back(info);
	}

	double time = timer.End();
	LOG(printf("InitializeCharInfo %f\n", time*1'000'000);)
}


void TestBlitting(int num, std::string str)
{
	timer.Start();
	RenderInfo* current = renderInfos;

	for(int i = 0; i < num; i++)
	{
		for(char c : str)
		{
			memcpy(current++, &renderInfosAsCharInfo[c], sizeof(RenderInfo));
		}
	}

	double time = timer.End();
	LOG(printf("TestBlitting %f\n", time*1'000'000);)
}

void TestMath(int num, std::string str)
{
	timer.Start();

	float result = 1.0f;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		for(char c : str)
		{
			result *= charInfos[0].w;
			result *= charInfos[0].h;
			result *= charInfos[0].xoffset;
			result *= charInfos[0].yoffset;
			result *= charInfos[0].xadvance;
			result += thatOtherFloat * ratio;
		}
	}

	double time1 = timer.End();
	LOG(printf("TestMath1 %f %f\n", time1*1'000'000, result);)

	timer.Start();
	result = 1.0f;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		for(char c : str)
		{
			result *= charInfos[c].w;
			result *= charInfos[c].h;
			result *= charInfos[c].xoffset;
			result *= charInfos[c].yoffset;
			result *= charInfos[c].xadvance;
			result += thatOtherFloat * ratio;
		}
	}


	double time2 = timer.End();
	LOG(printf("TestMath2 %f %f\n", time2*1'000'000, result);)

	timer.Start();
	float ratio = 1.0f / num;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		for(char c : str)
		{
			charInfos[c].w *= ratio;
			charInfos[c].h *= ratio;
			charInfos[c].xoffset *= ratio;
			charInfos[c].yoffset *= ratio;
			charInfos[c].xadvance *= thatOtherFloat + ratio;
		}
	}


	double time3 = timer.End();
	LOG(printf("TestMath3 %f\n", time3*1'000'000);)
}

void TestBoth(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfo* current = renderInfos;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current->r = 1.0f;
			current->g = 1.0f;
			current->b = 1.0f;
			current->a = 1.0f;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBoth %f\n", time*1'000'000);)
}

void TestBothByteColor(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoByteColor* current = renderInfosByteColor;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current->r = 255;
			current->g = 255;
			current->b = 255;
			current->a = 255;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothByteColor %f\n", time*1'000'000);)
}

void TestBothByteColorFakeFloat(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoByteColor* current = renderInfosByteColorFakeFloat;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			*(float*)&current->r = colorf;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothByteColorFakeFloat %f\n", time*1'000'000);)
}

void TestBothByteColorPadding(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoByteColorPadding* current = renderInfosByteColorPadding;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current->r = 255;
			current->g = 255;
			current->b = 255;
			current->a = 255;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothByteColorPadding %f\n", time*1'000'000);)
}

void TestBothGrayscale(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoGrayscale* current = renderInfosGrayscale;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current->brightness = 1.0f;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothGrayscale %f\n", time*1'000'000);)
}

void TestBothNoColor(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoNoColor* current = renderInfosNoColor;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothNoColor %f\n", time*1'000'000);)
}

void TestBothNoSettingColor(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfo* current = renderInfos;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothNoSettingColor %f\n", time*1'000'000);)
}

void TestBothIntColorNoSetting(int num, std::string str, float x, float y)
{
	timer.Start();

	RenderInfoIntColor* current = renderInfosIntColor;

	const int color = 0xffffffff;
	const float colorf = *(const float*)&color;

	for(int i = 0; i < num; i++)
	{
		float ratio = 1.0f / num;
		float advance = 0.0f;

		for(char c : str)
		{
			current->x = x + advance + charInfos[c].xoffset * ratio;
			advance += charInfos[c].xadvance * ratio;
			current->y = y + charInfos[c].yoffset * ratio + thatOtherFloat * ratio;
			current->w = charInfos[c].w * ratio;
			current->h = charInfos[c].h * ratio;
			current->u = charInfos[c].u;
			current->v = charInfos[c].v;
			current->uw = charInfos[c].uw;
			current->vh = charInfos[c].vh;
			current++;
		}
	}

	double time = timer.End();
	LOG(printf("TestBothIntColorNoSetting %f\n", time*1'000'000);)
}


struct SimpleData
{
	float a, b, c, d;
};

std::vector<SimpleData> src;
std::vector<SimpleData> dst;

void TestSimpleBlitting(int num)
{
	timer.Start();

	for(int i = 0; i < num; i++)
	{
		dst[i] = src[i];
	}

	double time = timer.End();
	LOG(printf("TestSimpleBlitting %f\n", time*1'000'000);)
}

void TestSimpleMath(int num)
{
	timer.Start();

	float result = 1.0f;

	for(int i = 0; i < num; i++)
	{
		result *= src[0].a;
		result *= src[0].b;
		result *= src[0].c;
		result *= src[0].d;
	}

	double time1 = timer.End();
	LOG(printf("TestSimpleMath1 %f %f\n", time1*1'000'000, result);)

	timer.Start();

	result = 1.0f;

	for(int i = 0; i < num; i++)
	{
		result *= src[i].a;
		result *= src[i].b;
		result *= src[i].c;
		result *= src[i].d;
	}

	double time2 = timer.End();
	LOG(printf("TestSimpleMath2 %f %f\n", time2*1'000'000, result);)

	timer.Start();

	float ratio = 1.0f / num;

	for(int i = 0; i < num; i++)
	{
		src[i].a *= ratio;
		src[i].b *= ratio;
		src[i].c *= ratio;
		src[i].d *= ratio;
	}

	double time3 = timer.End();
	LOG(printf("TestSimpleMath3 %f\n", time3*1'000'000);)
}

void TestSimpleBoth(int num)
{
	timer.Start();

	float ratio = 1.0f / num;

	for(int i = 0; i < num; i++)
	{
		dst[i].a = src[i].a * ratio;
		dst[i].b = src[i].b * ratio;
		dst[i].c = src[i].c * ratio;
		dst[i].d = src[i].d * ratio;
	}

	double time = timer.End();
	LOG(printf("TestSimpleBoth %f\n", time*1'000'000);)
}


int main()
{
	InitializeData(255, charInfos);
	InitializeData(255, renderInfosAsCharInfo);

	const int bufferSize = 40'000;
	renderInfos = new RenderInfo[bufferSize];
	renderInfosByteColor = new RenderInfoByteColor[bufferSize];
	renderInfosByteColorFakeFloat = new RenderInfoByteColor[bufferSize];
	renderInfosByteColorPadding = new RenderInfoByteColorPadding[bufferSize];
	renderInfosGrayscale = new RenderInfoGrayscale[bufferSize];
	renderInfosNoColor = new RenderInfoNoColor[bufferSize];
	renderInfosIntColor = new RenderInfoIntColor[bufferSize];

	const int cacheScratchSize = 10'000'000;
	int* cacheScratch = new int[cacheScratchSize];
	
	const char* theString = "asdf1234567!2#$%^&";
	const int iterations = 300;


	Object<512> codeCacheScratch1;
	Object<513> codeCacheScratch2;
	Object<514> codeCacheScratch3;
	Object<515> codeCacheScratch4;

	for(int i = 0; i < 10; i++)
	{
		//TestBlitting(iterations, theString);
		//TestMath(iterations, theString);
		TestBoth(iterations, theString, 1.0f, 2.0f);
		TestBothByteColor(iterations, theString, 1.0f, 2.0f);
		TestBothByteColorFakeFloat(iterations, theString, 1.0f, 2.0f);
		TestBothByteColorPadding(iterations, theString, 1.0f, 2.0f);
		TestBothGrayscale(iterations, theString, 1.0f, 2.0f);
		TestBothNoColor(iterations, theString, 1.0f, 2.0f);
		TestBothNoSettingColor(iterations, theString, 1.0f, 2.0f);
		TestBothIntColorNoSetting(iterations, theString, 1.0f, 2.0f);
		//TestBoth(300, theString, 1.0f, 2.0f);

		for(int j = 0; j < cacheScratchSize; j++)
			cacheScratch[j] = i;

		codeCacheScratch1.update(1.0f);
		codeCacheScratch2.update(1.0f);
		codeCacheScratch3.update(1.0f);
		codeCacheScratch4.update(1.0f);
	}

	printf("\n\nDone!\n\n");

	InitializeData(5000, src);
	dst.resize(5000);
	TestSimpleBlitting(5000);
	TestSimpleMath(5000);
	TestSimpleBoth(5000);


	int derp;
	std::cin >> derp;

	delete[] renderInfos;

  return 0;
}

