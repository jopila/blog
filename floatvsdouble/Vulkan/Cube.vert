
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in uint textureSet;
layout(location = 1) in uint neighborFlags;
layout(location = 2) in uint aoFlags1;
layout(location = 3) in uint aoFlags2;
layout(location = 4) in vec3 pos;

layout(location = 0) out uint out_textureSet;
layout(location = 1) out uint out_neighborFlags;
layout(location = 2) out uint out_aoFlags1;
layout(location = 3) out uint out_aoFlags2;


void main()
{
	gl_Position = vec4(pos, 1);
	out_textureSet = textureSet;
	out_neighborFlags = neighborFlags;
	out_aoFlags1 = aoFlags1;
	out_aoFlags2 = aoFlags2;
}