
#version 450

layout(points) in;
layout(location = 0) in mat4 model[];
layout(location = 4) in vec4 color[];
layout(location = 5) in vec4 uvwh[];


layout(triangle_strip, max_vertices=4) out;
layout(location = 0) out vec2 outUv;
layout(location = 1) out vec4 outColor;

void main()
{
	vec2 uv = uvwh[0].xy;
	gl_PointSize = 1.0f;

	outUv = uv;
	gl_Position = model[0] * vec4(-0.5f, 0.0f, -0.5f, 1.0f);
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x, uv.y + uvwh[0].w);
	gl_Position = model[0] * vec4(-0.5f, 0.0f, +0.5f, 1.0f);
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x + uvwh[0].z, uv.y);
	gl_Position = model[0] * vec4(+0.5f, 0.0f, -0.5f, 1.0f);
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x + uvwh[0].z, uv.y + uvwh[0].w);
	gl_Position = model[0] * vec4(+0.5f, 0.0f, +0.5f, 1.0f);
	outColor = color[0];
	EmitVertex();

  EndPrimitive();
} 

