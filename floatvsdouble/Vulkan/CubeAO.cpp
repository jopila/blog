
#include "stdafx.h"

#include "CubeAO.h"

#include <intrin.h>


const uint8 min = 255;
const uint8 mid = 128;
const uint8 max = 64;

uint8 aoTextureData[16][3*3] = 
{
  // nothing
  {min, min, min,
   min, min, min,
   min, min, min},
  // 1 edge
  {min, min, min,
   mid, mid, mid,
   max, max, max},
  // 2 edge adjacent
  {max, mid, min,
   max, mid, mid,
   max, max, max},
  // 2 edge opposite
  {max, max, max,
   mid, mid, mid,
   max, max, max},
  // 3 edge
  {max, max, max,
   max, mid, mid,
   max, max, max},
  // 4 edge
  {max, max, max,
   max, mid, max,
   max, max, max},
  // 1 edge 1 corner a
  {max, mid, min,
   mid, mid, mid,
   max, max, max},
  // 1 edge 1 corner b
  {min, mid, max,
   mid, mid, mid,
   max, max, max},
  // 1 edge 2 corner
  {max, mid, max,
   mid, mid, mid,
   max, max, max},
  // 2 edge 1 corner
  {max, mid, max,
   max, mid, mid,
   max, max, max},
  // 1 corner
  {min, min, min,
   min, mid, mid,
   min, mid, max},
  // 2 corner adjacent
  {min, min, min,
   mid, mid, mid,
   max, mid, max},
  // 2 corner opposite
  {max, mid, min,
   mid, mid, mid,
   min, mid, max},
  // 3 corner
  {max, mid, min,
   mid, mid, mid,
   max, mid, max},
  // 4 corner
  {max, mid, max,
   mid, mid, mid,
   max, mid, max},
  // unused
  {min, mid, min,
   mid, max, mid,
   min, mid, min},
};


int ExactMatch(uint8 adjacencyFlags)
{
  if(adjacencyFlags == 0) return 0;
  if((adjacencyFlags & 0b01011111) == 0b01000000) return 1; // 1 edge
  if((adjacencyFlags & 0b01010111) == 0b01010000) return 2; // 2 edge adjacent
  if((adjacencyFlags & 0b01010101) == 0b01000100) return 3; // 2 edge opposite
  if((adjacencyFlags & 0b01010101) == 0b01010100) return 4; // 3 edge
  if((adjacencyFlags & 0b01010101) == 0b01010101) return 5; // 4 edge
  if((adjacencyFlags & 0b01011111) == 0b01001000) return 6; // 1 edge 1 corner a
  if((adjacencyFlags & 0b01011111) == 0b01000010) return 7; // 1 edge 1 corner b
  if((adjacencyFlags & 0b01011111) == 0b01001010) return 8; // 1 edge 2 corner
  if((adjacencyFlags & 0b01010111) == 0b01010010) return 9; // 2 edge 1 corner
  if(adjacencyFlags == 0b10000000) return 10; // 1 corner
  if(adjacencyFlags == 0b10100000) return 11; // 2 corner adjacent
  if(adjacencyFlags == 0b10001000) return 12; // 2 corner opposite
  if(adjacencyFlags == 0b10101000) return 13; // 3 corner
  if(adjacencyFlags == 0b10101010) return 14; // 4 corner
  return -1;
}

int AdjacencyToTextureIndex(uint8 adjacencyFlags)
{
  for(int i = 0; i < 4; i++)
  {
    int match = ExactMatch(adjacencyFlags);
    if(match != -1)
      return match + (i << 4);

    adjacencyFlags = _rotl8(adjacencyFlags, 2);
  }
  return 15;
}




// Notes!
////////////////////////////////////////////////////////////
//0 corner           (1 rotations)
//1 corner           (4 rotations)
//2 corner adjacent  (4 rotations)
//2 corner opposites (2 rotations)
//3 corner           (4 rotations)
//4 corner           (1 rotations)
//
//0 edge             (1 rotations)
//1 edge             (4 rotations)
//2 edege adjacent   (4 rotations)
//2 edge opposites   (2 rotations)
//3 edge             (4 rotaitons)
//4 edge             (1 rotations)
//
//1 edge 1 corner(a) (4 rotations)
//1 edge 1 corner(b) (4 rotations)
//1 edge 2 corner    (4 rotations)
//2 edge 1 corner    (4 rotations)
//
// nothing
// 1 edge
// 2 edge adjacent
// 2 edge opposite
// 3 edge
// 4 edge
// 1 edge 1 corner a
// 1 edge 1 corner b
// 1 edge 2 corner
// 2 edge 1 corner
// 1 corner
// 2 corner adjacent
// 2 corner opposite
// 3 corner
// 4 corner
// unused
//
//
//tl (bit 7)
//t
//tr
//r
//br
//b
//bl
//l  (bit 0)
//
//top face:
//tl (+x, +z)
//tr (-x, +z)
//br (-x, -z)
//bl (+x, -z)
//
//right face:
//tl (+z, +y)
//tr (-z, +y)
//br (-z, -y)
//bl (+z, -y)
//
//front face:
//tl (-x, +y)
//tr (+x, +y)
//br (+x, -y)
//bl (-x, -y)