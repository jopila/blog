
#include "stdafx.h"

#include "Gfx.h"
#include "CommandBuffer.h"
#include "Device.h"
#include "RenderPass.h"
#include "Synchronization.h"
#include "Pipeline.h"
#include "DescriptorSet.h"
#include "Vertex.h"
#include "Buffer.h"

namespace gfx
{
	CommandBufferHandle* CommandBufferHandle::Create(DeviceHandle* device) { return new CommandBuffer((Device*)device); }
	void CommandBufferHandle::Destroy(CommandBufferHandle* handle) { delete (CommandBuffer*)handle; }

	CommandBuffer::CommandBuffer(Device* device) : device(device)
	{
		StackEvents ev("CommandBuffer::CommandBuffer");
		StackEvents::Push("vkAllocateCommandBuffers");
		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandPool = device->commandPool;
		allocInfo.commandBufferCount = 1;
		vkAllocateCommandBuffers(device->device, &allocInfo, &commandBuffer);
		StackEvents::Pop();
	}

	CommandBuffer::~CommandBuffer()
	{
		StackEvents ev("CommandBuffer::~CommandBuffer");
		vkFreeCommandBuffers(device->device, device->commandPool, 1, &commandBuffer);
	}

	void CommandBuffer::CopyBufferToBuffer(VkBuffer src, int srcOffset, VkBuffer dst, int dstOffset, VkDeviceSize size)
	{
		StackEvents ev("CommandBuffer::CopyBufferToBuffer");
		VkBufferCopy copyRegion = {};
		copyRegion.srcOffset = srcOffset; // Optional
		copyRegion.dstOffset = dstOffset; // Optional
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, src, dst, 1, &copyRegion);
	}

	void CommandBuffer::CopyBufferToImage(VkBuffer src, VkImage dst, VkBufferImageCopy region)
	{
		StackEvents ev("CommandBuffer::CopyBufferToImage");
		vkCmdCopyBufferToImage(commandBuffer, src, dst, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
	}

	void CommandBuffer::CopyBufferToImage(VkBuffer src, VkImage dst, std::vector<VkBufferImageCopy> regions)
	{
		StackEvents ev("CommandBuffer::CopyBufferToImage");
		vkCmdCopyBufferToImage(commandBuffer, src, dst, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, (unsigned)regions.size(), &regions[0]);
	}

	void CommandBuffer::PipelineBarrier(VkPipelineStageFlags src, VkPipelineStageFlags dst, VkImageMemoryBarrier barrier)
	{
		StackEvents ev("CommandBuffer::PipelineBarrier");
		vkCmdPipelineBarrier(commandBuffer, src, dst, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	}

	void CommandBuffer::BlitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, VkImageBlit blit, VkFilter filter)
	{
		StackEvents ev("CommandBuffer::BlitImage");
		vkCmdBlitImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, 1, &blit, filter);
	}

	void CommandBuffer::BeginRenderPass(const RenderPassHandle* renderPass)
	{
		StackEvents ev("CommandBuffer::BeginRenderPass");

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = ((RenderPass*)renderPass)->renderPass;
		renderPassInfo.framebuffer = device->swapChainFramebuffers[device->currentSwapChainImageIndex];
		renderPassInfo.renderArea.offset = {0, 0};
		renderPassInfo.renderArea.extent = device->swapChainExtent;

		vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	}

	void CommandBuffer::BeginDraw(const PipelineHandle* pipeline, const DescriptorSetHandle* descriptorSet)
	{
		StackEvents ev("CommandBuffer::BeginDraw");
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ((Pipeline*)pipeline)->pipeline);
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ((Pipeline*)pipeline)->pipelineLayout, 0, 1, &((DescriptorSet*)descriptorSet)->descriptorSet, 0, nullptr);

		SetScissorRect(0, 0, device->swapChainExtent.width, device->swapChainExtent.height);
		SetViewport(0, 0, device->swapChainExtent.width, device->swapChainExtent.height, 0.0f, 1.0f);
	}

	void CommandBuffer::Draw(const VertexInfoHandle* vertexInfo, int32 numVertices, int32 firstVertex)
	{
		StackEvents ev("CommandBuffer::Draw");

		VkBuffer vertexBuffers[] = {((VertexInfo*)vertexInfo)->vertexBuffer->buffer};
		VkDeviceSize offsets[] = {0};
		vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffers, offsets);
		if(((VertexInfo*)vertexInfo)->indexBuffer != VK_NULL_HANDLE)
		{
			vkCmdBindIndexBuffer(commandBuffer, ((VertexInfo*)vertexInfo)->indexBuffer->buffer, 0, VK_INDEX_TYPE_UINT16);
			vkCmdDrawIndexed(commandBuffer, numVertices, 1, firstVertex, 0, 0);
		}
		else
		{
			vkCmdDraw(commandBuffer, numVertices, 1, firstVertex, 0);
		}
	}

	void CommandBuffer::DrawInstanced(const VertexInfoHandle* vertexInfo, int32 numIndices, int32 numInstances, int32 firstIndex, int32 firstVertex, int32 instanceBufferIndex)
	{
		VkBuffer vertexBuffers[2];
		VkDeviceSize offsets[] = { 0, 0 };
		vertexBuffers[0] = ((VertexInfo*)vertexInfo)->vertexBuffer->buffer;
		vertexBuffers[1] = ((VertexInfo*)vertexInfo)->instanceBuffers[instanceBufferIndex]->buffer;
		vkCmdBindVertexBuffers(commandBuffer, 0, 2, vertexBuffers, offsets);
		vkCmdBindIndexBuffer(commandBuffer, ((VertexInfo*)vertexInfo)->indexBuffer->buffer, 0, VK_INDEX_TYPE_UINT16);
		vkCmdDrawIndexed(commandBuffer, numIndices, numInstances, firstIndex, firstVertex, 0);
	}

	void CommandBuffer::EndDraw()
	{
	}
	
	void CommandBuffer::EndRenderPass()
	{
		StackEvents ev("CommandBuffer::EndRenderPass");
		vkCmdEndRenderPass(commandBuffer);
	}

	void CommandBuffer::SetScissorRect(int32 x, int32 y, int32 w, int32 h)
	{
		VkRect2D rect;
		rect.offset.x = x;
		rect.offset.y = y;
		rect.extent.width = w;
		rect.extent.height = h;
		vkCmdSetScissor(commandBuffer, 0, 1, &rect);
	}

	void CommandBuffer::SetViewport(int32 x, int32 y, int32 w, int32 h, float minZ, float maxZ)
	{
		VkViewport viewport;
		viewport.x = (float)x;
		viewport.y = (float)y;
		viewport.width = (float)w;
		viewport.height = (float)h;
		viewport.minDepth = minZ;
		viewport.maxDepth = maxZ;
		vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
	}

	void CommandBuffer::Clear(FloatColor4 clearColor)
	{
		StackEvents ev("CommandBuffer::Clear");
		VkClearAttachment attColor = {VK_IMAGE_ASPECT_COLOR_BIT, 0, *(VkClearValue*)&clearColor};
		VkClearAttachment attDepth = {VK_IMAGE_ASPECT_DEPTH_BIT, 0, {1.0f, 0}};
		VkClearAttachment att[] = {attColor, attDepth};
		VkClearRect rect = {{{0, 0},{(uint32_t)device->screenWidth, (uint32_t)device->screenHeight}}, 0, 1};
		vkCmdClearAttachments(commandBuffer, sizeof(att) / sizeof(att[0]), att, 1, &rect);
	}

	void CommandBuffer::Begin()
	{
		VkCommandBufferResetFlags flags = {VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT};
		vkResetCommandBuffer(commandBuffer, flags);

		StackEvents::Push("vkBeginCommandBuffer");
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		vkBeginCommandBuffer(commandBuffer, &beginInfo);
		StackEvents::Pop();
	}

	void CommandBuffer::End()
	{
		StackEvents ev("CommandBuffer::End");
		VK_ASSERT(vkEndCommandBuffer(commandBuffer), "failed to record command buffer!");
	}
}