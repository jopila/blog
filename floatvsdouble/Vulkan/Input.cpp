
#include "stdafx.h"

#include "Input.h"


namespace wnd
{
	Input::Input()
	{
		memset(keyStates, 0, sizeof(keyStates));
		memset(mouseStates, 0, sizeof(mouseStates));
		scrollX = 0;
		scrollY = 0;
		mouseOffsetX = 0;
		mouseOffsetY = 0;
	}
	void Input::FrameUpdate()
	{
		memcpy_s(prevKeyStates, sizeof(prevKeyStates), keyStates, sizeof(keyStates));
		memcpy_s(prevMouseStates, sizeof(prevMouseStates), mouseStates, sizeof(mouseStates));
		scrollX = 0;
		scrollY = 0;
		mouseOffsetX = 0;
		mouseOffsetY = 0;
		charInput.clear();
	}
} //namespace wnd