
#include "stdafx.h"

#include <stdexcept>

#include "Gfx.h"
#include "Device.h"
#include "Buffer.h"
#include "CommandBuffer.h"

namespace gfx
{
	BufferHandle* BufferHandle::Create(DeviceHandle* device, int32 size, uint32 usage, uint32 properties) { return new Buffer((Device*)device, size, usage, properties); }
	void BufferHandle::Destroy(BufferHandle* handle) { delete (Buffer*)handle; }

	Buffer::Buffer(Device* device, int32 size, uint32 usage, uint32 properties) : device(device)
	{
		StackEvents ev("Buffer::Buffer");
		VkBufferCreateInfo bufferInfo = {};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = size;
		bufferInfo.usage = usage;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		StackEvents::Push("vkCreateBuffer");
		VK_ASSERT(vkCreateBuffer(device->device, &bufferInfo, nullptr, &buffer), "failed to create vertex buffer!");
		printf("Created vk object: %llu (VkBuffer)\n", (uint64)buffer);
		StackEvents::Pop();

		StackEvents::Push("vkGetBufferMemoryRequirements");
		VkMemoryRequirements memRequirements;
		vkGetBufferMemoryRequirements(device->device, buffer, &memRequirements);
		StackEvents::Pop();

		// Look for a 'memory type';
		StackEvents::Push("vkGetPhysicalDeviceMemoryProperties");
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(device->physicalDevice, &memProperties);
		StackEvents::Pop();

		// Allocate memory
		StackEvents::Push("Allocate Prep");
		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = FindMemoryType(device->physicalDevice, memRequirements.memoryTypeBits, properties);
		StackEvents::Pop();
		StackEvents::Push("vkAllocateMemory");
		VK_ASSERT(vkAllocateMemory(device->device, &allocInfo, nullptr, &memory), "failed to allocate vertex buffer memory!");
		StackEvents::Pop();
		StackEvents::Push("vkBindBufferMemory");
		vkBindBufferMemory(device->device, buffer, memory, 0);
		StackEvents::Pop();
	}

	void Buffer::Update(void* data, int32 offset, int32 size)
	{
		StackEvents ev("Buffer::Update");
		void* mapData;
		vkMapMemory(device->device, memory, offset, size, 0, &mapData);
		memcpy(mapData, data, size);
		vkUnmapMemory(device->device, memory);
	}

	void Buffer::Map(void *& mappedData, int32 offset, int32 size)
	{
		StackEvents ev("Buffer::Map");
		vkMapMemory(device->device, memory, offset, size, 0, &mappedData);
	}

	void Buffer::Unmap()
	{
		vkUnmapMemory(device->device, memory);
	}

	void Buffer::Copy(CommandBufferHandle* commandBuffer, BufferHandle* srcBuffer, int srcOffset, BufferHandle* dstBuffer, int dstOffset, VkDeviceSize size, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("CopyBuffer");
		((CommandBuffer*)commandBuffer)->CopyBufferToBuffer(((Buffer*)srcBuffer)->buffer, srcOffset, ((Buffer*)dstBuffer)->buffer, dstOffset, size);
	}

	Buffer::~Buffer()
	{
		StackEvents ev("Buffer::~Buffer");
		vkDestroyBuffer(device->device, buffer, nullptr);
		vkFreeMemory(device->device, memory, nullptr);
	}

	uint32_t FindMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
	{
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
				return i;

		throw std::runtime_error("failed to find suitable memory type!");
	}
}
