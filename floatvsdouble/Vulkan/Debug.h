#pragma once

const int STACK_SIZE = 20;

struct SkeletonStackInfo
{
	uint64 addr[STACK_SIZE];
};

void RecordStack(SkeletonStackInfo* info, int skipFrames);
void PrintRecordedStack(SkeletonStackInfo* info);

void StackTrace();
