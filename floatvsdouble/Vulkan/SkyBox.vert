

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 pos;

layout(location = 0) out vec3 outUVW;

layout(binding = 0) uniform UniformBufferObject
{
	mat4 view;
	mat4 proj;
} ubo;


void main()
{
	outUVW = pos;
	mat4 rotView = mat4(mat3(ubo.view)); // remove translation
	vec4 newPos = ubo.proj * rotView * vec4(pos.xyz, 1.0);
	newPos.z = newPos.w;
	gl_Position = newPos;
}

