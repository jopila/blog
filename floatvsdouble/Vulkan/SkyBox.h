#pragma once

#include "Gfx.h"

struct SkyBoxRenderData;
struct Mat4x4;
struct Vec4;

struct SkyBoxRenderInfo
{
	float x, y, z;
};

class SkyBoxRenderer : public gfx::RendererHandle
{
public:
	SkyBoxRenderer(gfx::DeviceHandle* device, std::string textureDirectory);
	~SkyBoxRenderer();

	void UpdateViewProj(const Mat4x4& view, const Mat4x4& proj);
	void RenderOne(SkyBoxRenderInfo renderInfo);

private:
	friend class gfx::Gfx;
  void BeginFrame() {}
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  SkyBoxRenderData* data;
};
