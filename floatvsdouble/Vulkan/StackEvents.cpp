
#include "stdafx.h"

#include <thread>
#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "File.h"
#include "StackEvents.h"


static const int StackEventBufferSize = 1024*1024;
static thread_local FileWriter* StackEventTimingFile;
static thread_local uint8 StackEventBuffer[StackEventBufferSize];
static thread_local uint8* StackEventBufferPos;

void CreateStackEventLogFile(std::string threadName)
{
	//std::string filename = "StackEvents_" + threadName + ".log";
	//StackEventTimingFile = new FileWriter(filename);
	//StackEventBufferPos = StackEventBuffer;
	//
	//// Cycles per second. I don't know why, but QueryPerformanceFrequency/Counter functions wipe out 10 bits of precision. We're using rdtsc directly, so we need the un-bitsmashed value
	//LARGE_INTEGER freq;
	//QueryPerformanceFrequency(&freq);
	//double secondsPerCycle = 1.0 / (freq.QuadPart * 1024.0);
	//StackEventTimingFile->WriteRaw((uint8*)&secondsPerCycle, sizeof(secondsPerCycle));
}

void CloseStackEventLogFile()
{
	//StackEvents::Flush();
	//delete StackEventTimingFile;
	//StackEventTimingFile = nullptr;
}

void StackEvents::Push(const char* name)
{
	if(!StackEventTimingFile)
		return;

	memcpy(StackEventBufferPos, "push", 4);
	//__cpuid((int*)&StackEventBufferPos[4], 0);
	*(long long*)(&StackEventBufferPos[4]) = __rdtsc();
	StackEventBufferPos += 4 + sizeof(long long);
	while(*StackEventBufferPos++ = *name++)
		;
}

void StackEvents::Pop()
{
	if(!StackEventTimingFile)
		return;

	memcpy(StackEventBufferPos, "pop ", 4);

	//__cpuid((int*)&StackEventBufferPos[4], 0);
	*(long long*)(&StackEventBufferPos[4]) = __rdtsc();

	StackEventBufferPos += 4 + sizeof(long long);

	if(StackEventBufferPos - StackEventBuffer > (StackEventBufferSize - 200))
		Flush();
}

void StackEvents::Flush()
{
	if(!StackEventTimingFile)
		return;

	StackEvents ev("StackEvents::Flush");
	//printf("Buffer used: %lld\n", StackEventBufferPos - StackEventBuffer);
	StackEventTimingFile->WriteRaw(StackEventBuffer, StackEventBufferPos - StackEventBuffer);
	StackEventBufferPos = StackEventBuffer;
}