
#include "stdafx.h"

#include "Debug.h"
#define _AMD64_
#include <wtypes.h>
#include <minwindef.h>
#include <winnt.h>
#include <DbgHelp.h>
#include <stdio.h>


#pragma comment(lib, "dbghelp.lib")

const int maxFrames = 100;
CRITICAL_SECTION DbgHelpLock;
bool initialized = false;

void RecordStack(SkeletonStackInfo* info, int skipFrames)
{
	ZeroMemory(info->addr, sizeof(uint64) * STACK_SIZE);
	ULONG BackTraceHash;
	uint32 framesCaptured = RtlCaptureStackBackTrace(0, STACK_SIZE, reinterpret_cast<PVOID*>(info->addr), &BackTraceHash);
}

void PrintRecordedStack(SkeletonStackInfo* info)
{
	if(!initialized)
	{
		initialized = true;
		InitializeCriticalSection(&DbgHelpLock);
		SymInitialize(GetCurrentProcess(), nullptr, true);
	}

	EnterCriticalSection( &DbgHelpLock );
	for(int i = 0; i < STACK_SIZE; i++)
	{
		if(!info->addr[i])
			break;

		DWORD64 offset;
		char buffer[1024];
		SYMBOL_INFO* symbolInfo = (SYMBOL_INFO*)buffer;
		symbolInfo->SizeOfStruct = sizeof(SYMBOL_INFO);
		symbolInfo->MaxNameLen = 1024 - sizeof(SYMBOL_INFO);
		SymFromAddr(GetCurrentProcess(), info->addr[i], &offset, symbolInfo);
		int error =  GetLastError();
		DWORD offset2;
		IMAGEHLP_LINE64 line;
		SymGetLineFromAddr64(GetCurrentProcess(), info->addr[i], &offset2, &line);
		std::string filename = line.FileName;
		filename = filename.erase(0, filename.find_last_of("\\/") + 1);
		printf("%s(%i): %s\n", filename.c_str(), line.LineNumber, symbolInfo->Name);
	}
	LeaveCriticalSection( &DbgHelpLock );
}

void StackTrace()
{
	//if(!initialized)
	//	InitSym();
	
	SkeletonStackInfo info;
	RecordStack(&info, 1);
	PrintRecordedStack(&info);
}

////////////////////////////////////////////////////////////////////////////////
// NEW/DELETE
////////////////////////////////////////////////////////////////////////////////

#if _DEBUG

static const int GUARD_VALUE = 0xFACE0666;
static const int GUARD_FREED = 0xFEEEFEEE;
static const int GUARD_SIZE = 5;

struct MemoryHeader
{
	SkeletonStackInfo stackInfo;
	MemoryHeader* prev;
	MemoryHeader* next;
	std::size_t size;
	int guard[GUARD_SIZE];
};

struct MemoryFooter
{
	int guard[GUARD_SIZE];
};

static bool initializedMemory = false;
static MemoryHeader* headHeader = nullptr;
static CRITICAL_SECTION criticalSection;


void CheckMemoryLeaks()
{
	// there seems to be an occasional issue with statically allocated objects not registerring atexit soon enough...
	// should only ever effect the first one. after that the atexit function should be assigned and good to go
	// ....grumble grumble...getting really disappointed in atexit's inability to handle statically allocated memory
	if(headHeader == nullptr || headHeader->next == nullptr || headHeader->next->next == nullptr)
		return;

	// Cycle through all the leaks
	while(headHeader)
	{
		printf("MemoryLeak!:\n");
		PrintRecordedStack(&headHeader->stackInfo);
		printf("\n");

		headHeader = headHeader->next;
	}
}

void* operator new(std::size_t count)
{
	//StackEvents event("operator new");
	if(!initializedMemory)
	{
		initializedMemory = true;
		atexit(CheckMemoryLeaks);
		InitializeCriticalSection(&criticalSection);
	}

	EnterCriticalSection(&criticalSection);
	uint8* result = (uint8*)malloc(count + sizeof(MemoryHeader) + sizeof(MemoryFooter));
	MemoryHeader* header = (MemoryHeader*)result;
	MemoryFooter* footer = (MemoryFooter*)(result + count + sizeof(MemoryHeader));

	// Guards
	for(int i = 0; i < GUARD_SIZE; i++)
	{
		header->guard[i] = GUARD_VALUE;
		footer->guard[i] = GUARD_VALUE;
	}

	// Linked List
	if(headHeader)
		headHeader->prev = header;
	header->next = headHeader;
	header->prev = nullptr;
	headHeader = header;
	RecordStack(&header->stackInfo, 1);

	header->size = count;
	LeaveCriticalSection(&criticalSection);

	return result + sizeof(MemoryHeader);
}

void operator delete(void* ptr)
{
	if(ptr == nullptr)
		return;

	EnterCriticalSection(&criticalSection);
	//StackEvents event("operator delete");
	ptr = (uint8*)ptr - sizeof(MemoryHeader);
	MemoryHeader* header = (MemoryHeader*)ptr;
	MemoryFooter* footer = (MemoryFooter*)((uint8*)ptr + header->size + sizeof(MemoryHeader));

	// Guards
	for(int i = 0; i < GUARD_SIZE; i++)
	{
		if(header->guard[i] != GUARD_VALUE)	__debugbreak();
		if(footer->guard[i] != GUARD_VALUE)	__debugbreak();
	}
	for(int i = 0; i < GUARD_SIZE; i++)
		header->guard[i] = footer->guard[i] = GUARD_FREED;

	// Linked List
	MemoryHeader* tempPrev = header->prev;
	MemoryHeader* tempNext = header->next;
	if(header->prev) header->prev->next = tempNext; else headHeader = tempNext;
	if(header->next) header->next->prev = tempPrev;

	free(header);
	LeaveCriticalSection(&criticalSection);
}

#endif
