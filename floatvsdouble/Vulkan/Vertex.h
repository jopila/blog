#pragma once


#include "Gfx.h"
#include "VulkanInclude.h"


namespace gfx
{

	class Device;
	class Buffer;
	class Semaphore;
	class CommandBuffer;

	class VertexInfo final : public VertexInfoHandle
	{
	public:
		VertexInfo(Device* device);
		~VertexInfo();

		void AddVar(const Format format);
		void AddInstancedVar(const Format format);
		void Finalize();
		void CreateVertexData(int maxVertices);
		void CreateInstanceData(int maxInstances, int numBuffers = 1);
		void CreateIndexData(int maxIndices);
		void UpdateVertexData(CommandBufferHandle* commandBuffer, void* data, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		void MapVertexData(void*& data, int vertexOffset, int numVertices);
		void UnmapVertexData(CommandBufferHandle* commandBuffer, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		void MapInstanceData(void*& data, int instanceOffset, int numInstances, int index = 0);
		void UnmapInstanceData(CommandBufferHandle* commandBuffer, int instanceOffset, int numInstances, int index = 0, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		void UpdateIndexData(void* data, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		void MapIndexData(void*& data, int indexOffset, int numIndices);
		void UnmapIndexData(CommandBufferHandle* commandBuffer, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		void CreateAndUpdateVertexData(CommandBufferHandle* commandBuffer, int numVertices, void* data);
		void CreateAndUpdateIndexData(CommandBufferHandle* commandBuffer, int numIndices, void* data);

		int GetMaxVertices() {return maxVertices;}
		int GetMaxInstances() {return maxInstances;}
		int GetMaxIndices() {return maxIndices;}

	private:
		friend class Device;
		friend class Pipeline;
		friend class CommandBuffer;
		Device* device;

		void CreateAndSetBuffer(CommandBufferHandle* commandBuffer, void* data, int32 dataSize, Buffer*& buffer, VkBufferUsageFlagBits usage);

		VkPipelineVertexInputStateCreateInfo vertexInputInfo;
		VkVertexInputBindingDescription bindingDescription[2];
		std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
		int currentLocation = 0;
		int currentSize = 0;
		int instanceCurrentSize = 0;

		//int numVertices;
		//int numIndices;
		int maxVertices;
		int maxInstances;
		int maxIndices;
		VkDeviceSize bufferSize;

		Buffer* vertexBuffer;
		Buffer** instanceBuffers;
		Buffer* indexBuffer;
		Buffer* vertexStagingBuffer;
		Buffer** instanceStagingBuffers;
		Buffer* indexStagingBuffer;
    int numInstanceBuffers;
	};
}