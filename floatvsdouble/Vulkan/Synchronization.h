#pragma once

namespace gfx
{
	class Device;


	class Semaphore final : public SemaphoreHandle
	{
	public:
		Semaphore(Device* device);
		~Semaphore();

	private:
		friend class Device;
		friend class CommandBuffer;

		Device* device;
		VkSemaphore semaphore;
	};

	class Fence final : public FenceHandle
	{
	public:
		Fence(Device* device, bool startSignaled);
		~Fence();

		bool IsSignaled();
		void WaitSignaled();
		void Reset();

	private:
		friend class Device;
		friend class CommandBuffer;

		Device* device;
		VkFence fence;
	};

	uint32_t FindMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);
}
