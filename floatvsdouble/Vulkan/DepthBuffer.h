#pragma once

#include "Gfx.h"
#include "Texture.h"
#include "VulkanInclude.h"

namespace gfx
{
	class Device;

	class DepthBuffer final : public TextureBase, public DepthBufferHandle
	{
	public:
		DepthBuffer(Device* device);

	private:
		friend class Device;
		friend class DescriptorSet;

		Device* device;
	};
}
