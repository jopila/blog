
#include "stdafx.h"

#include "Device.h"
#include "Shader.h"
#include "File.h"

namespace gfx
{
	ShaderHandle* ShaderHandle::Create(DeviceHandle* device, std::string name) { return new Shader((Device*)device, name); }
	void ShaderHandle::Destroy(ShaderHandle* handle) { delete (Shader*)handle; }

	Shader::Shader(Device* device, std::string name) : device(device)
	{
		std::vector<char> shaderCode = ReadFile("..\\vulkan\\" + name + ".spv");

		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = shaderCode.size();
		createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderCode.data());

		VK_ASSERT(vkCreateShaderModule(device->device, &createInfo, nullptr, &shaderModule), "failed to create shader module!");
		printf("Created vk object: %llu (VkShaderModule) (%s)\n", (uint64)shaderModule, name.c_str());

		/**/ if(EndsWith(name, "vert")) shaderType = gfx::ShaderType::Vertex;
		else if(EndsWith(name, "geom")) shaderType = gfx::ShaderType::Geometry;
		else if(EndsWith(name, "frag")) shaderType = gfx::ShaderType::Fragment;
		else __debugbreak();
	}

	Shader::~Shader()
	{
		vkDestroyShaderModule(device->device, shaderModule, nullptr);
	}
}