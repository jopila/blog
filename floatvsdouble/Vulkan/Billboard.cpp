
#include "stdafx.h"

#include "Billboard.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "File.h"
#include "Debug.h"
#include <map>
#include <sstream>
#include "..\utility\Mat4x4.h"

struct BillboardRenderData
{
  gfx::DeviceHandle* device;
  gfx::TextureHandle* texture;
  gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
  gfx::DescriptorSetHandle* descriptorSet;
  gfx::BufferHandle* uniformViewProjBuffer;
  gfx::BufferHandle* storageBufferBillboardAtlasInfo;
  gfx::SamplerHandle* sampler;
  gfx::VertexInfoHandle* vertexInfo;
  gfx::ShaderHandle* vert;
  gfx::ShaderHandle* geom;
  gfx::ShaderHandle* frag;
  gfx::PipelineHandle* pipeline;

  struct VertexData
  {
    Mat4x4 model;
    ColorOffset colorOffset;
    int32 BillboardIndex;
  };
  VertexData* stagingMemory;
  mutable VertexData* currentVertex;

  struct UBOViewProj
  {
    Mat4x4 viewProj;
  } uboViewProj;
  struct ScissorInfo
  {
    int32 x, y, w, h;
  };
  struct DrawInfo
  {
    ScissorInfo scissor;
    int32 lastVertex;
  };
  struct BillboardRendererBillboardAtlasInfo
  {
    float u, v;
    float uw, vh;
  };

  struct BillboardInfo
  {
    float w, h;
    float u, v, uw, vh;
  };
  std::vector<BillboardInfo> billboardInfos;
  std::map<std::string, uint32> billboardLookup;

  BillboardRendererBillboardAtlasInfo uboBillboardAtlasInfo[256];

  const int bufferSize = 4000;
};

BillboardRenderer::BillboardRenderer(gfx::DeviceHandle* device, std::string atlasName)
{
  data = new BillboardRenderData();
  data->device = device;

  // Texture
  data->texture = gfx::TextureHandle::Create(device, atlasName + ".png", false);
  int width = data->texture->GetWidth();
  int height = data->texture->GetHeight();

  // Billboard info
  std::vector<std::string> lines = ReadFileLines(atlasName + ".txt");
  for(std::string line : lines)
  {
    std::istringstream iss(line);
    std::string billboardName;
    std::string dummy;
    int x, y, w, h;
    if (!(iss >> billboardName >> dummy >> x >> y >> w >> h)) { break; } // error

    data->billboardLookup[billboardName] = (uint8)data->billboardInfos.size();
    BillboardRenderData::BillboardInfo newInfo;
    newInfo.w = (float)w;
    newInfo.h = (float)h;
    newInfo.u = (float)x / width;
    newInfo.v = (float)y / height;
    newInfo.uw = (float)w / width;
    newInfo.vh = (float)h / height;
    data->billboardInfos.push_back(newInfo);
  }

  data->storageBufferBillboardAtlasInfo = gfx::BufferHandle::Create(
    device, sizeof(data->uboBillboardAtlasInfo), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  for(int i = 0; i < data->billboardInfos.size(); i++)
  {
    BillboardRenderData::BillboardInfo billboardInfo = data->billboardInfos[i];
    if(i >= 256)
      __debugbreak();
    BillboardRenderData::BillboardRendererBillboardAtlasInfo& billboardAtlasInfo = data->uboBillboardAtlasInfo[i];
    billboardAtlasInfo.u = billboardInfo.u;
    billboardAtlasInfo.v = billboardInfo.v;
    billboardAtlasInfo.uw = billboardInfo.uw;
    billboardAtlasInfo.vh = billboardInfo.vh;

  }
  data->storageBufferBillboardAtlasInfo->Update(&data->uboBillboardAtlasInfo, 0, sizeof(data->uboBillboardAtlasInfo));

  // Pipeline
  data->uniformViewProjBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboViewProj), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  data->sampler = gfx::SamplerHandle::Create(device, gfx::Filter::Nearest, gfx::AddressMode::ClampToEdge);

  data->descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
  data->descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
  data->descriptorSetLayout->AddStorageBuffers(1, gfx::ShaderType::Vertex);
  data->descriptorSetLayout->AddSamplers(1, gfx::ShaderType::Fragment);
  data->descriptorSetLayout->AddTextures(1, gfx::ShaderType::Fragment);
  data->descriptorSetLayout->Finalize();

  data->descriptorSet = gfx::DescriptorSetHandle::Create(device, data->descriptorSetLayout);
  data->descriptorSet->UpdateUniformBuffer(0, data->uniformViewProjBuffer, sizeof(data->uboViewProj));
  data->descriptorSet->UpdateStorageBuffer(1, data->storageBufferBillboardAtlasInfo, sizeof(data->uboBillboardAtlasInfo));
  data->descriptorSet->UpdateSampler(2, data->sampler);
  data->descriptorSet->UpdateTexture(3, data->texture);

  data->vertexInfo = gfx::VertexInfoHandle::Create(device);
  data->vertexInfo->AddVar(gfx::Format::R32G32B32A32_SFLOAT); // model1
  data->vertexInfo->AddVar(gfx::Format::R32G32B32A32_SFLOAT); // model2
  data->vertexInfo->AddVar(gfx::Format::R32G32B32A32_SFLOAT); // model3
  data->vertexInfo->AddVar(gfx::Format::R32G32B32A32_SFLOAT); // model4
  data->vertexInfo->AddVar(gfx::Format::R8G8B8A8_UNORM);
  data->vertexInfo->AddVar(gfx::Format::R32_UINT);
  data->vertexInfo->Finalize();
  data->vertexInfo->CreateVertexData(data->bufferSize);

  data->vert = gfx::ShaderHandle::Create(device, "Billboard.vert");
  data->geom = gfx::ShaderHandle::Create(device, "Billboard.geom");
  data->frag = gfx::ShaderHandle::Create(device, "Billboard.frag");

  data->pipeline = gfx::PipelineHandle::Create(device);
  data->pipeline->AddShader(data->vert);
  data->pipeline->AddShader(data->geom);
  data->pipeline->AddShader(data->frag);

  data->pipeline->SetTopology(gfx::Topology::PointList);
  data->pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

  data->pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
  data->pipeline->SetDescriptorSetLayout(data->descriptorSetLayout);
  data->pipeline->Finalize(device->getFinalRenderPass(), data->vertexInfo);
}

BillboardRenderer::~BillboardRenderer()
{
  gfx::TextureHandle::Destroy(data->texture);
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformViewProjBuffer);
  gfx::BufferHandle::Destroy(data->storageBufferBillboardAtlasInfo);
  gfx::SamplerHandle::Destroy(data->sampler);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->geom);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

int32 BillboardRenderer::GetBillboardIndex(std::string name)
{
  return data->billboardLookup[name];
}

void BillboardRenderer::GetBillboardDims(int32 index, float& width, float& height)
{
  width = data->billboardInfos[index].w;
  height = data->billboardInfos[index].h;
}

void BillboardRenderer::UpdateViewProj(const Mat4x4& viewProj)
{
  data->uboViewProj.viewProj = viewProj;
  data->uboViewProj.viewProj.Transpose();
}

void BillboardRenderer::RenderOne(BillboardRenderInfo renderInfo)
{
  if(data->currentVertex - data->stagingMemory < data->bufferSize)
  {
    Mat4x4 transform;
    Mat4x4 translation = Mat4x4::MakeTranslation(Vec4(renderInfo.x, renderInfo.y, renderInfo.z, 1.0f));
    Mat4x4 scale = Mat4x4::MakeScale(Vec4(renderInfo.w, 1.0f, renderInfo.h, 0.0f));
    Mat4x4 rotation = Mat4x4::MakeRotation(renderInfo.q);
    transform = translation * scale * rotation;
    transform.Transpose();
    data->currentVertex->model = transform;
    data->currentVertex->colorOffset = renderInfo.colorOffset;
    data->currentVertex->BillboardIndex = renderInfo.BillboardIndex;

    data->currentVertex++;
  }
  else
    __debugbreak();
}

void BillboardRenderer::BeginFrame()
{
  StackEvents ev("RenderBillboardBeginFrame");
  void* temp = (void*)data->stagingMemory;
  data->vertexInfo->MapVertexData(temp, 0, data->vertexInfo->GetMaxVertices());
  data->stagingMemory = (BillboardRenderData::VertexData*)temp;
  data->currentVertex = data->stagingMemory;
}

void BillboardRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  StackEvents ev("RenderBillboardEndFrame");

  data->vertexInfo->UnmapVertexData(commandBuffer, 0, (int32)(data->currentVertex - data->stagingMemory));

  data->uniformViewProjBuffer->Update(&data->uboViewProj, 0, sizeof(data->uboViewProj));
}

void BillboardRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  if(data->currentVertex == data->stagingMemory)
    return;

  commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
  commandBuffer->Draw(data->vertexInfo, (int32)(data->currentVertex - data->stagingMemory), 0);
  commandBuffer->EndDraw();
}
