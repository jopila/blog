
#include "stdafx.h"

#include "SkyBox.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "File.h"
#include "Debug.h"
#include "..\utility\Mat4x4.h"
//#include "Types.h"

struct SkyBoxRenderData
{
	gfx::DeviceHandle* device;
	std::vector<gfx::TextureHandle*> textures;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformViewProjBuffer;
	gfx::SamplerHandle* sampler;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	SkyBoxRenderInfo* stagingMemory;
	SkyBoxRenderInfo* currentVertex;

	struct UBOViewProj
	{
		Mat4x4 view;
		Mat4x4 proj;
	} uboViewProj;

	struct Vertex
	{
		float x, y, z;
	};
	const std::vector<Vertex> vertices =
	{
		{-1.0f, -1.0f,  1.0f},
		{ 1.0f, -1.0f,  1.0f},
		{ 1.0f, -1.0f, -1.0f},
		{-1.0f, -1.0f, -1.0f},
		{-1.0f,  1.0f,  1.0f},
		{ 1.0f,  1.0f,  1.0f},
		{ 1.0f,  1.0f, -1.0f},
		{-1.0f,  1.0f, -1.0f},
	};

	const std::vector<uint16> indices =
	{
		// Right
		1, 2, 6,
		6, 5, 1,
		// Left
		0, 4, 7,
		7, 3, 0,
		// Top
		4, 5, 6,
		6, 7, 4,
		// Bottom
		0, 3, 2,
		2, 1, 0,
		// Back
		0, 1, 5,
		5, 4, 0,
		// Front
		3, 7, 6,
		6, 2, 3
	};

	const int bufferSize = 40;
};

SkyBoxRenderer::SkyBoxRenderer(gfx::DeviceHandle* device, std::string textureDirectory)
{
  data = new SkyBoxRenderData();
  data->device = device;

  // Load up our textures
  std::vector<std::string> textureFilenames = ListFilesInDirectory(textureDirectory, true);
  for(std::string textureFilename : textureFilenames)
  {
    data->textures.push_back(gfx::TextureHandle::CreateCubeMap(device, textureFilename));
  }

  // Pipeline
  data->uniformViewProjBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboViewProj), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  data->sampler = gfx::SamplerHandle::Create(device, gfx::Filter::Nearest, gfx::AddressMode::ClampToEdge);

  auto& descriptorSetLayout = data->descriptorSetLayout;
  descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
  descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
  descriptorSetLayout->AddCombinedTextureSampler(gfx::ShaderType::Fragment);
  descriptorSetLayout->Finalize();

  auto& descriptorSet = data->descriptorSet;
  descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
  descriptorSet->UpdateUniformBuffer(0, data->uniformViewProjBuffer, sizeof(data->uboViewProj));
  descriptorSet->UpdateCombinedTextureSampler(1, data->textures[1], data->sampler);

  gfx::CommandBufferHandle* commandBuffer = gfx::CommandBufferHandle::Create(device);

  auto& vertexInfo = data->vertexInfo;
  vertexInfo = gfx::VertexInfoHandle::Create(device);
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // pos
  vertexInfo->Finalize();
  vertexInfo->CreateAndUpdateVertexData(commandBuffer, (int)data->vertices.size(), (void*)data->vertices.data());
  vertexInfo->CreateAndUpdateIndexData(commandBuffer, (int)data->indices.size(), (void*)data->indices.data());

  gfx::CommandBufferHandle::Destroy(commandBuffer);

  data->vert = gfx::ShaderHandle::Create(device, "SkyBox.vert");
  data->frag = gfx::ShaderHandle::Create(device, "SkyBox.frag");

  auto& pipeline = data->pipeline;
  pipeline = gfx::PipelineHandle::Create(device);
  pipeline->AddShader(data->vert);
  pipeline->AddShader(data->frag);

  pipeline->SetTopology(gfx::Topology::TriangleList);
  pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

  pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
  pipeline->SetDescriptorSetLayout(descriptorSetLayout);
  pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);
}

SkyBoxRenderer::~SkyBoxRenderer()
{
  for(size_t i = 0; i < data->textures.size(); i++)
    gfx::TextureHandle::Destroy(data->textures[i]);
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformViewProjBuffer);
  gfx::SamplerHandle::Destroy(data->sampler);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

void SkyBoxRenderer::UpdateViewProj(const Mat4x4& view, const Mat4x4& proj)
{
  data->uboViewProj.view = view;
  data->uboViewProj.proj = proj;
  data->uboViewProj.view.Transpose();
  data->uboViewProj.proj.Transpose();
}

void SkyBoxRenderer::RenderOne(SkyBoxRenderInfo renderInfo)
{
  if(data->currentVertex - data->stagingMemory < data->bufferSize)
    *data->currentVertex++ = renderInfo;
  else
    __debugbreak();
}

void SkyBoxRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  data->uniformViewProjBuffer->Update(&data->uboViewProj, 0, sizeof(data->uboViewProj));
}

void SkyBoxRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
  commandBuffer->Draw(data->vertexInfo, (int)data->indices.size(), 0);
  commandBuffer->EndDraw();
}
