
#version 450

layout(points) in;
layout(location = 0) in uint textureSet[];
layout(location = 1) in uint neighborFlags[];
layout(location = 2) in uint aoFlags1[];
layout(location = 3) in uint aoFlags2[];

layout(triangle_strip, max_vertices=24) out;
layout(location = 0) out vec2 out_uv;
layout(location = 1) out vec4 out_pos;
layout(location = 2) out vec3 out_normal;
layout(location = 3) out vec3 out_tangent;
layout(location = 4) out uint out_textureSet;
layout(location = 5) out uint out_aoIndex;

layout(binding = 0) uniform UniformBufferObject
{
  mat4 viewProj;
	vec4 camera;
} ubo;

vec3 dup_normal;
vec3 dup_tangent;
uint dup_textureSet;
uint dup_aoIndex;

void SetDupValues()
{
	out_normal = dup_normal;
	out_tangent = dup_tangent;
	out_textureSet = dup_textureSet;
  out_aoIndex = dup_aoIndex;
}

void main()
{
	vec4 pos = gl_in[0].gl_Position;
	gl_PointSize = 1.0f;

	uint neighbors = neighborFlags[0];
	dup_textureSet = textureSet[0] << 2;
	
	vec3 ext = vec3(0.5, 0.5, 0.5);
	vec2 uvTL = vec2(0.0f, 0.0f);
	vec2 uvTR = vec2(1.0f, 0.0f);
	vec2 uvBL = vec2(0.0f, 1.0f);
	vec2 uvBR = vec2(1.0f, 1.0f);
	
	if((neighbors & (1 << 2)) != 0 && ubo.camera.y > pos.y + ext.y)
	{
		// Top
		dup_normal = vec3(0.0, 1.0, 0.0);
		dup_tangent = vec3(1.0, 0.0, 0.0);
    dup_aoIndex = aoFlags1[0] >> (2*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		gl_Position = out_pos = ubo.viewProj * vec4(pos.x - ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f) ;
		EmitVertex();

		out_uv = uvTR;
		SetDupValues();
		gl_Position = out_pos = ubo.viewProj * vec4(pos.x + ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f);
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		gl_Position = out_pos = ubo.viewProj * vec4(pos.x - ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		gl_Position = out_pos = ubo.viewProj * vec4(pos.x + ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		EmitVertex();

		EndPrimitive();
	}
	
	// Sides
	dup_textureSet++;

	if((neighbors & (1 << 4)) != 0 && ubo.camera.z > pos.z + ext.z)
	{
		// Side 1
		dup_normal = vec3(0.0, 0.0, 1.0);
		dup_tangent = vec3(1.0, 0.0, 0.0);
    dup_aoIndex = aoFlags2[0] >> (1*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvTR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();

		EndPrimitive();
	}
	
	if((neighbors & (1 >> 0)) != 0 && ubo.camera.x > pos.x + ext.x)
	{
		// Side 2
		dup_normal = vec3(1.0, 0.0, 0.0);
		dup_tangent = vec3(0.0, 0.0, -1.0);
    dup_aoIndex = aoFlags1[0] >> (0*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvTR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();

		EndPrimitive();
	}
	
	if((neighbors & (1 << 5)) != 0 && ubo.camera.z < pos.z - ext.z)
	{
		// Side 3
		dup_normal = vec3(0.0, 0.0, -1.0);
		dup_tangent = vec3(-1.0, 0.0, 0.0);
    dup_aoIndex = aoFlags2[0] >> (2*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvTR;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();

		EndPrimitive();
	}

	if((neighbors & (1 << 1)) != 0 && ubo.camera.x < pos.x - ext.x)
	{
		// Side 4
		dup_normal = vec3(-1.0, 0.0, 0.0);
		dup_tangent = vec3(0.0, 0.0, 1.0);
    dup_aoIndex = aoFlags1[0] >> (1*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y + ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvTR;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y + ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();

		EndPrimitive();
	}

	if((neighbors & (1 << 3)) != 0 && ubo.camera.y < pos.y - ext.y)
	{
		// Bottom
		dup_textureSet++;
		dup_normal = vec3(0.0, -1.0, 0.0);
		dup_tangent = vec3(1.0, 0.0, 0.0);
    dup_aoIndex = aoFlags2[0] >> (0*6) & 0x3f;

		out_uv = uvTL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvTR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z + ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBL;
		SetDupValues();
		out_pos = vec4(pos.x - ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();
	
		out_uv = uvBR;
		SetDupValues();
		out_pos = vec4(pos.x + ext.x, pos.y - ext.y, pos.z - ext.z, 1.0f);
		gl_Position = ubo.viewProj * out_pos;
		EmitVertex();

		EndPrimitive();
	}
} 
