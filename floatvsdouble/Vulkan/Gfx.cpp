
#include "stdafx.h"

#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>
#include <thread>

#include "Gfx.h"
#include "VulkanInclude.h"
#include "Sprite.h"
#include "TextRenderer.h"


namespace gfx
{


	struct Vertex
	{
		glm::vec2 pos;
		glm::vec3 color;
		glm::vec2 texCoord;

	};

	const std::vector<Vertex> vertices =
	{
		{{-0.5f, -0.5f}, {1.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
		{{ 0.5f, -0.5f}, {1.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},
		{{ 0.5f,  0.5f}, {0.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},
		{{-0.5f,  0.5f}, {0.5f, 0.0f, 0.0f}, {1.0f, 1.0f}},
	};

	const std::vector<uint16_t> indices =
	{
		0, 1, 2, 2, 3, 0,
	};

	struct UniformBufferObject
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
	};

	Gfx::Gfx(wnd::WindowHandle* window)
	{
		device = DeviceHandle::Create(window);

		texture = TextureHandle::Create(device, "..\\vulkan\\meow.jpg", false);
		sampler = SamplerHandle::Create(device, Filter::Nearest, AddressMode::ClampToEdge);
		uniformBuffer = BufferHandle::Create(device, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

		CreateDescriptorSetLayout();
		CreateDescriptorSet();
		CreateGraphicsPipeline();

		basePassCommandBuffer = CommandBufferHandle::Create(device);
		basePassFence = FenceHandle::Create(device, true);

		textRenderer = new TextRenderer(device, "Roboto-Regular");

		CreateStackEventLogFile("Main");
	}

	Gfx::~Gfx()
	{
		device->WaitIdle();

		delete textRenderer;

		gfx::VertexInfoHandle::Destroy(vertexInfo);
		gfx::BufferHandle::Destroy(uniformBuffer);

		gfx::TextureHandle::Destroy(texture);
		gfx::SamplerHandle::Destroy(sampler);

		gfx::CommandBufferHandle::Destroy(basePassCommandBuffer);
		gfx::FenceHandle::Destroy(basePassFence);

		gfx::ShaderHandle::Destroy(vertShader);
		gfx::ShaderHandle::Destroy(fragShader);

		gfx::DescriptorSetLayoutHandle::Destroy(descriptorSetLayout);
		gfx::DescriptorSetHandle::Destroy(descriptorSet);
		gfx::PipelineHandle::Destroy(pipeline);

		gfx::DeviceHandle::Destroy(device);

		CloseStackEventLogFile();
	}

	static int counter = 0;
	static float tallyTime = 0.0f;
	static auto startTime = std::chrono::high_resolution_clock::now();
	static auto currentTime = std::chrono::high_resolution_clock::now();
	static double frameTime = 0.0f;

	void Gfx::BeginFrame()
	{
		auto frameStart = std::chrono::high_resolution_clock::now();
		StackEvents mainLoop("mainLoop");


		// Rotate 90 degrees per second
		auto newTime = std::chrono::high_resolution_clock::now();
		frameTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
		currentTime = newTime;
		float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

		counter++;
		if(time - 1.0f >= tallyTime)
		{
			tallyTime = time;
			counter = 0;
		}

		// Update Fluffum's mvp matrix
		UniformBufferObject ubo = {};
		float degreesPerSecond = 30.0f;
		ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(degreesPerSecond), glm::vec3(0.0f, 0.0f, 1.0f));
		ubo.model = glm::scale(ubo.model, glm::vec3(20.0f, 20.0f, 20.0f));
		ubo.view = glm::lookAt(glm::vec3(20.0f, 20.0f, 20.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		ubo.proj = glm::perspective(glm::radians(45.0f), device->GetWidth() / (float)device->GetHeight(), 0.1f, 1000.0f);
		ubo.proj[1][1] *= -1;
		uniformBuffer->Update(&ubo, 0, sizeof(ubo));

		// Begin the frame and handle resizes
		if(device->BeginFrame())
		{
			gfx::CommandBufferHandle::Destroy(basePassCommandBuffer);
			basePassCommandBuffer = CommandBufferHandle::Create(device);
			device->WaitIdle();
			OnScreenResize(device->GetWidth(), device->GetHeight());
			device->OnScreenResize.Broadcast(device->GetWidth(), device->GetHeight());
		}
		textRenderer->BeginFrame();
		for(int32 i = 0; i < renderers.size(); i++)
			renderers[i]->BeginFrame();

		// Start recording our command buffer
		basePassFence->WaitSignaled();
		basePassFence->Reset();
		basePassCommandBuffer->Begin();

		// Calculate our framerate
		const int32 smoothCount = 300;
		static double times[smoothCount] = {0.0f};
		static double accumulatedTime = 0.0f;
		static int32 nextTime = 0;
		times[nextTime++] = frameTime;
		nextTime %= smoothCount;
		accumulatedTime += frameTime;
		accumulatedTime -= times[nextTime];
		double frameTimeSmoothed = accumulatedTime / smoothCount;
		// Render our framerate
		char buf[1000];
		sprintf_s(buf, "Frame Time!: %fms", frameTimeSmoothed * 1000.0);
		textRenderer->RenderString(buf, 12.0f, 550.0f, 0.0f, 0xffffffff);
	}

	void Gfx::EndFrame()
	{
		StackEvents mainLoop("EndFrame");

		// Let our renderers update the gpu with new memory
		textRenderer->UpdateBuffers(basePassCommandBuffer);
		for(int32 i = 0; i < renderers.size(); i++)
			renderers[i]->UpdateBuffers(basePassCommandBuffer);

		// Kitty!
		basePassCommandBuffer->BeginRenderPass(device->getFinalRenderPass());
		basePassCommandBuffer->BeginDraw(pipeline, descriptorSet);
		basePassCommandBuffer->Clear({0.95f, 0.95f, 0.95f, 1.0f});
		//basePassCommandBuffer->Draw(vertexInfo, 6);
		basePassCommandBuffer->EndDraw();

		// Actually tell the gpu to do something!
		textRenderer->SubmitDraws(basePassCommandBuffer);
		for(int32 i = 0; i < renderers.size(); i++)
			renderers[i]->SubmitDraws(basePassCommandBuffer);

		// All done
		basePassCommandBuffer->EndRenderPass();
		basePassCommandBuffer->End();

		device->Submit(basePassCommandBuffer, nullptr, nullptr, basePassFence);
		if(device->EndFrame())
		{
			gfx::CommandBufferHandle::Destroy(basePassCommandBuffer);
			basePassCommandBuffer = CommandBufferHandle::Create(device);
			device->WaitIdle();
			if(OnScreenResize)
				OnScreenResize(device->GetWidth(), device->GetHeight());
			device->OnScreenResize.Broadcast(device->GetWidth(), device->GetHeight());
		}

		StackEvents::Flush();
	}

	void Gfx::RegisterRenderer(RendererHandle* renderer)
	{
		renderers.push_back(renderer);
	}

	void Gfx::UnregisterRenderer(RendererHandle* renderer)
	{
		renderers.erase(std::find(renderers.begin(), renderers.end(), renderer));
	}

	void Gfx::CreateDescriptorSetLayout()
	{
		descriptorSetLayout = DescriptorSetLayoutHandle::Create(device);
		descriptorSetLayout->AddUniformBuffers(1, ShaderType::Vertex);
		descriptorSetLayout->AddSamplers(1, ShaderType::Fragment);
		descriptorSetLayout->AddTextures(1, ShaderType::Fragment);
		descriptorSetLayout->Finalize();
	}

	void Gfx::CreateDescriptorSet()
	{
		descriptorSet = DescriptorSetHandle::Create(device, descriptorSetLayout);
		descriptorSet->UpdateUniformBuffer(0, uniformBuffer, sizeof(UniformBufferObject));
		descriptorSet->UpdateSampler(1, sampler);
		descriptorSet->UpdateTexture(2, texture);
	}

	void Gfx::CreateGraphicsPipeline()
	{
		vertexInfo = VertexInfoHandle::Create(device);
		vertexInfo->AddVar(Format::R32G32_SFLOAT);
		vertexInfo->AddVar(Format::R32G32B32_SFLOAT);
		vertexInfo->AddVar(Format::R32G32_SFLOAT);
		vertexInfo->Finalize();
		vertexInfo->CreateVertexData(4);
		vertexInfo->CreateIndexData(6);
		void* stagingVerticesTemp;
		void* stagingIndicesTemp;
		vertexInfo->MapVertexData((void*)stagingVerticesTemp, 0, 4);
		vertexInfo->MapIndexData((void*)stagingIndicesTemp, 0, 6);
		memcpy(stagingVerticesTemp, vertices.data(), sizeof(Vertex)*4);
		memcpy(stagingIndicesTemp, indices.data(), sizeof(uint16)*6);
		CommandBufferHandle* commandBuffer = CommandBufferHandle::Create(device);
		commandBuffer->Begin();
		vertexInfo->UnmapVertexData(commandBuffer, 0, 4);
		vertexInfo->UnmapIndexData(commandBuffer, 0, 6);
		commandBuffer->End();
		device->SubmitOutOfFrame(commandBuffer);
		device->WaitIdle();
		CommandBufferHandle::Destroy(commandBuffer);

		vertShader = ShaderHandle::Create(device, "Triangle.vert");
		fragShader = ShaderHandle::Create(device, "Triangle.frag");

		pipeline = PipelineHandle::Create(device);
		pipeline->AddShader(vertShader);
		pipeline->AddShader(fragShader);

		pipeline->SetTopology(Topology::TriangleList);
		pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

		pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
		pipeline->SetDescriptorSetLayout(descriptorSetLayout);
		pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);
	}
}