
#include "stdafx.h"

#include "Cube.h"
#include "CubeAO.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "File.h"
#include "Debug.h"
#include "..\utility\Mat4x4.h"
//#include "Types.h"
#include <map>
#include <sstream>

struct CubeRenderData
{
	gfx::DeviceHandle* device;
	size_t realTextureCount;
	std::vector<gfx::TextureHandle*> textures;
  std::vector<gfx::TextureHandle*> aoTextures;
	gfx::TextureHandle* skyIrradianceTexture;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformViewProjBuffer;
	gfx::BufferHandle* uniformFragmentBuffer;
	gfx::BufferHandle* storageBufferTextureSetInfo;
	gfx::SamplerHandle* sampler;
	gfx::SamplerHandle* skyIrradianceSampler;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* geom;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	CubeRenderInfo* stagingMemory;
	CubeRenderInfo* currentVertex;

	struct UBOViewProj
	{
		Mat4x4 viewProj;
		Vec4 cameraPos;
	} uboViewProj;

	struct UBOFragment
	{
		Vec4 cameraPos;
		Vec4 pointLightPos;
		Vec4 pointLightColor;
		Vec4 directionalLightDir;
		Vec4 directionalLightColor;
	} uboFragment;

	struct UBOTextureSetInfo
	{
		uint8 topDiffuse;
		uint8 topNormal;
		uint8 topORM;
		uint8 topDummy;
		uint8 sideDiffuse;
		uint8 sideNormal;
		uint8 sideORM;
		uint8 sideDummy;
		uint8 botDiffuse;
		uint8 botNormal;
		uint8 botORM;
		uint8 botDummy;
	};
	UBOTextureSetInfo textureSets[256];

	std::map<std::string, uint32> textureLookup;

	const int bufferSize = 4000000;
};

CubeRenderer::CubeRenderer(gfx::DeviceHandle* device, std::string textureDirectory)
{
  data = new CubeRenderData();
  data->device = device;

  // Load up our textures
  std::vector<std::string> textureFilenames = ListFilesInDirectory(textureDirectory, true);
  for(std::string textureFilename : textureFilenames)
  {
    data->textures.push_back(gfx::TextureHandle::Create(device, textureFilename, true));
    data->textureLookup[GetFileNameFromPath(textureFilename, false)] = (uint32)(data->textures.size() - 1);
  }
  data->realTextureCount = data->textures.size();
  for(size_t i = data->textures.size(); i < 256; i++)
    data->textures.push_back(data->textures.back());

  for(int i = 0; i < 16; i++)
    data->aoTextures.push_back(gfx::TextureHandle::CreateFromData(device, gfx::Format::R8_UNORM, 3, 3, aoTextureData[i]));

  // Texture sets...
  auto& textureSets = data->textureSets;
  auto& textureLookup = data->textureLookup;
  memset(textureSets, 0, sizeof(textureSets));
  data->storageBufferTextureSetInfo = gfx::BufferHandle::Create(device, sizeof(textureSets), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  textureSets[1].topDiffuse = textureLookup["leaves.diffuse"];
  textureSets[1].topNormal = textureLookup["leaves.normal"];
  textureSets[1].topORM = textureLookup["leaves.orm"];
  textureSets[1].sideDiffuse = textureLookup["rocks.diffuse"];
  textureSets[1].sideNormal = textureLookup["rocks.normal"];
  textureSets[1].sideORM = textureLookup["rocks.orm"];
  textureSets[1].botDiffuse = textureLookup["rocks.diffuse"];
  textureSets[1].botNormal = textureLookup["rocks.normal"];
  textureSets[1].botORM = textureLookup["rocks.orm"];

  textureSets[0].topDiffuse = textureLookup["arrow"];
  textureSets[0].sideNormal = textureLookup["leaves.normal"];
  textureSets[0].topORM = textureLookup["leaves.orm"];
  textureSets[0].sideDiffuse = textureLookup["arrow"];
  textureSets[0].sideNormal = textureLookup["leaves.normal"];
  textureSets[0].sideORM = textureLookup["leaves.orm"];
  textureSets[0].botDiffuse = textureLookup["arrow"];
  textureSets[0].botNormal = textureLookup["leaves.normal"];
  textureSets[0].botORM = textureLookup["leaves.orm"];

  data->storageBufferTextureSetInfo->Update(&textureSets, 0, sizeof(textureSets));

  // Irradiance cube
  data->skyIrradianceTexture = gfx::TextureHandle::CreateCubeMap(device, "assets\\CubeMaps\\output_iem.ktx");

  // Pipeline
  data->uniformViewProjBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboViewProj), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  data->uniformFragmentBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboFragment), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);


  data->sampler = gfx::SamplerHandle::Create(device, gfx::Filter::Linear, gfx::AddressMode::ClampToEdge);

  auto& descriptorSetLayout = data->descriptorSetLayout;
  descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
  descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Geometry);
  descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Fragment);
  descriptorSetLayout->AddStorageBuffers(1, gfx::ShaderType::Fragment);
  descriptorSetLayout->AddSamplers(1, gfx::ShaderType::Fragment);
  descriptorSetLayout->AddTextures((int32)data->textures.size(), gfx::ShaderType::Fragment);
  descriptorSetLayout->AddTextures((int32)data->aoTextures.size(), gfx::ShaderType::Fragment);
  descriptorSetLayout->AddCombinedTextureSampler(gfx::ShaderType::Fragment);
  descriptorSetLayout->Finalize();

  auto& descriptorSet = data->descriptorSet;
  descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
  descriptorSet->UpdateUniformBuffer(0, data->uniformViewProjBuffer, sizeof(data->uboViewProj));
  descriptorSet->UpdateUniformBuffer(1, data->uniformFragmentBuffer, sizeof(data->uboFragment));
  descriptorSet->UpdateStorageBuffer(2, data->storageBufferTextureSetInfo, sizeof(textureSets));
  descriptorSet->UpdateSampler(3, data->sampler);
  descriptorSet->UpdateTextures(4, data->textures);
  descriptorSet->UpdateTextures(5, data->aoTextures);
  descriptorSet->UpdateCombinedTextureSampler(6, data->skyIrradianceTexture, data->sampler);

  auto& vertexInfo = data->vertexInfo;
  vertexInfo = gfx::VertexInfoHandle::Create(device);
  vertexInfo->AddVar(gfx::Format::R16_UINT); // texture set
  vertexInfo->AddVar(gfx::Format::R16_UINT); // neighbors
  vertexInfo->AddVar(gfx::Format::R32_UINT); // aoFlags1
  vertexInfo->AddVar(gfx::Format::R32_UINT); // aoFlags2
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // pos
  vertexInfo->Finalize();
  vertexInfo->CreateVertexData(data->bufferSize);

  data->vert = gfx::ShaderHandle::Create(device, "Cube.vert");
  data->geom = gfx::ShaderHandle::Create(device, "Cube.geom");
  data->frag = gfx::ShaderHandle::Create(device, "Cube.frag");

  auto& pipeline = data->pipeline;
  pipeline = gfx::PipelineHandle::Create(device);
  pipeline->AddShader(data->vert);
  pipeline->AddShader(data->geom);
  pipeline->AddShader(data->frag);

  pipeline->SetTopology(gfx::Topology::PointList);
  pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

  pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
  pipeline->SetDescriptorSetLayout(descriptorSetLayout);
  pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);
}

CubeRenderer::~CubeRenderer()
{
  for(size_t i = 0; i < data->realTextureCount; i++)
    gfx::TextureHandle::Destroy(data->textures[i]);
  for(size_t i = 0; i < data->aoTextures.size(); i++)
    gfx::TextureHandle::Destroy(data->aoTextures[i]);
  gfx::TextureHandle::Destroy(data->skyIrradianceTexture);
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformViewProjBuffer);
  gfx::BufferHandle::Destroy(data->uniformFragmentBuffer);
  gfx::BufferHandle::Destroy(data->storageBufferTextureSetInfo);
  gfx::SamplerHandle::Destroy(data->sampler);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->geom);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

int32 CubeRenderer::GetTextureIndex(std::string name)
{
	return data->textureLookup[name];
}

void CubeRenderer::UpdateViewProj(const Mat4x4& viewProj)
{
  data->uboViewProj.viewProj = viewProj;
  data->uboViewProj.viewProj.Transpose();
}

void CubeRenderer::UpdateCameraPosition(const Vec4& cameraPosition)
{
  data->uboFragment.cameraPos = cameraPosition;
  data->uboViewProj.cameraPos = cameraPosition;
}

void CubeRenderer::UpdatePointLight(const Vec4& pos, const Vec4& color)
{
  data->uboFragment.pointLightPos = pos;
  data->uboFragment.pointLightColor = color;
}

void CubeRenderer::UpdateDirectionalLight(const Vec4& dir, const Vec4& color)
{
  data->uboFragment.directionalLightDir = dir * -1.0f;
  data->uboFragment.directionalLightColor = color;
}

void CubeRenderer::RenderOne(CubeRenderInfo renderInfo)
{
  if(data->currentVertex - data->stagingMemory < data->bufferSize)
    *data->currentVertex++ = renderInfo;
  else
    __debugbreak();
}

void CubeRenderer::BeginFrame()
{
  void* temp = (void*)data->stagingMemory;
  data->vertexInfo->MapVertexData(temp, 0, data->vertexInfo->GetMaxVertices());
  data->stagingMemory = (CubeRenderInfo*)temp;
  data->currentVertex = data->stagingMemory;
}

void CubeRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  data->vertexInfo->UnmapVertexData(commandBuffer, 0, (int32)(data->currentVertex - data->stagingMemory));
  data->uniformViewProjBuffer->Update(&data->uboViewProj, 0, sizeof(data->uboViewProj));
  data->uniformFragmentBuffer->Update(&data->uboFragment, 0, sizeof(data->uboFragment));
}

void CubeRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  if(data->currentVertex == data->stagingMemory)
  	return;

  commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
  commandBuffer->Draw(data->vertexInfo, (int32)(data->currentVertex - data->stagingMemory), 0);
  commandBuffer->EndDraw();
}
