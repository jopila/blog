
#include "stdafx.h"

#include <stdexcept>

#include "Texture.h"
#include "Gfx.h"
#include "Device.h"
#include "CommandBuffer.h"
#include "Buffer.h"
#include "File.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <ddspp.h>

namespace gfx
{
	TextureHandle* TextureHandle::Create(DeviceHandle* device, std::string filename, bool generateMips) { return new Texture((Device*)device, filename, generateMips); }
	TextureHandle* TextureHandle::CreateCubeMap(DeviceHandle* device, std::string filename) { return new Texture((Device*)device, filename); }
  TextureHandle* TextureHandle::CreateFromData(DeviceHandle* device, Format format, uint32 width, uint32 height, void* data) { return new Texture((Device*)device, format, width, height, data); }
	void TextureHandle::Destroy(TextureHandle* handle) { delete (Texture*)handle; }

	Texture::Texture(Device* device, std::string filename, bool generateMips) : TextureBase(device)
	{
		if(EndsWith(filename, ".png") || EndsWith(filename, ".jpg"))
		{
			// Read png info
			int texChannels;
			stbi_uc* pixels = stbi_load(filename.c_str(), &width, &height, &texChannels, STBI_rgb_alpha);
			int32 imageSize = width * height * 4;
			int32 mipLevels = generateMips ? static_cast<uint32_t>(std::floor(std::log2(Max(width, height)))) + 1 : 1;

			if (!pixels)
			throw std::runtime_error("failed to load texture image!");

			Buffer stagingBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			stagingBuffer.Update(pixels, 0, imageSize);

			stbi_image_free(pixels);

			CreateImage(width, height, mipLevels, 1, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_2D,
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

			TransitionImageLayout(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels, 1);
			CopyBufferToImage(stagingBuffer.buffer, image, static_cast<uint32_t>(width), static_cast<uint32_t>(height));

			if(generateMips)
				GenerateMipmaps(image, VK_FORMAT_R8G8B8A8_UNORM, width, height, mipLevels);
			else
				TransitionImageLayout(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
		}
		else if(EndsWith(filename, ".ktx"))
		{
			KTX_error_code result;
			ktxTexture* kTexture;
			result = ktxTexture_CreateFromNamedFile(filename.c_str(), KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &kTexture);
			width = kTexture->baseWidth;
			height = kTexture->baseHeight;
			VkFormat format = ktxTexture_GetVkFormat(kTexture);
			int32 imageSize = (int32)ktxTexture_GetDataSize(kTexture);
			int32 mipLevels = kTexture->numLevels;

			Buffer stagingBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			stagingBuffer.Update(kTexture->pData, 0, imageSize);

			CreateImage(width, height, mipLevels, 1, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_2D,
				VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

			TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels, 1);
			CopyKtxToImage(stagingBuffer.buffer, kTexture, image, width, height, mipLevels);
			TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, mipLevels, 1);

			ktxTexture_Destroy(kTexture);
		}
		else if(EndsWith(filename, ".dds"))
		{
			std::vector<int8> bytes = ReadFile(filename);

			ddspp::Descriptor desc;
			ddspp::decode_header((uint8*)bytes.data(), desc);
			const char* initialData = bytes.data() + desc.headerSize;

			width = desc.width;
			height = desc.height;
			VkFormat format;
			switch(desc.format)
			{
			case ddspp::BC3_UNORM: format = VK_FORMAT_BC3_UNORM_BLOCK; break;
			case ddspp::BC1_UNORM: format = VK_FORMAT_BC1_RGB_UNORM_BLOCK; break;
			}
			int32 imageSize = (int32)bytes.size() - desc.headerSize;
			int32 mipLevels = desc.numMips;

			Buffer stagingBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
			stagingBuffer.Update(bytes.data(), 0, imageSize);

			CreateImage(width, height, mipLevels, 1, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_2D,
				VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

			TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels, 1);
			CopyDdsToImage(stagingBuffer.buffer, bytes, &desc, image, width, height, mipLevels);
			TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, mipLevels, 1);
		}
	}

	Texture::Texture(Device* device, std::string filename) : TextureBase(device)
	{
		KTX_error_code result;
		ktxTexture* kTexture;
		result = ktxTexture_CreateFromNamedFile(filename.c_str(), KTX_TEXTURE_CREATE_LOAD_IMAGE_DATA_BIT, &kTexture);
		width = kTexture->baseWidth;
		height = kTexture->baseHeight;
		VkFormat format = ktxTexture_GetVkFormat(kTexture);
		int32 imageSize = (int32)ktxTexture_GetDataSize(kTexture);
		int32 mipLevels = kTexture->numLevels;

		Buffer stagingBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		stagingBuffer.Update(kTexture->pData, 0, imageSize);

		CreateImage(width, height, 1, 6, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_CUBE,
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

		TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, mipLevels, 6);
		CopyKtxToCubeMap(stagingBuffer.buffer, kTexture, image, width, height, mipLevels);
		TransitionImageLayout(image, format, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, mipLevels, 6);

		ktxTexture_Destroy(kTexture);
	}

  Texture::Texture(Device* device, Format format, uint32 width, uint32 height, void* data) : TextureBase(device)
  {
    VkFormat vkFormat = GetVkFormat(format);
    int32 imageSize = width * height * GetVkFormatSize(format);
    Buffer stagingBuffer(device, imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    stagingBuffer.Update(data, 0, imageSize);

    CreateImage(width, height, 1, 1, vkFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_2D,
      VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

    TransitionImageLayout(image, vkFormat, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, 1);
    CopyBufferToImage(stagingBuffer.buffer, image, width, height);
    TransitionImageLayout(image, vkFormat, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 1, 1);
  }

	TextureBase::~TextureBase()
	{
		vkDestroyImageView(device->device, view, nullptr);
		vkFreeMemory(device->device, memory, nullptr);
		vkDestroyImage(device->device, image, nullptr);
	}

	void TextureBase::CreateImage(uint32 width, uint32 height, uint32 mipLevels, uint32 layers, VkFormat format, VkImageTiling tiling, VkImageViewType viewType, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImageAspectFlags aspect)
	{
		// Create the 'Image'
		VkImageCreateInfo imageInfo = {};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.extent.width = width;
		imageInfo.extent.height = height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = mipLevels;
		imageInfo.arrayLayers = layers;
		imageInfo.format = format;
		imageInfo.tiling = tiling;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imageInfo.usage = usage;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.flags = viewType == VK_IMAGE_VIEW_TYPE_CUBE ? VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT : 0; // Optional

		VK_ASSERT(vkCreateImage(device->device, &imageInfo, nullptr, &image), "failed to create image!");
		printf("Created vk object: %llu (VkImage)\n", (uint64)image);

		// Create the memory backing for the image
		VkMemoryRequirements memRequirements;
		vkGetImageMemoryRequirements(device->device, image, &memRequirements);

		VkMemoryAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memRequirements.size;
		allocInfo.memoryTypeIndex = FindMemoryType(device->physicalDevice, memRequirements.memoryTypeBits, properties);

		if (vkAllocateMemory(device->device, &allocInfo, nullptr, &memory) != VK_SUCCESS) {
			throw std::runtime_error("failed to allocate image memory!");
		}
		printf("Created vk object: %llu (VkDeviceMemory)\n", (uint64)memory);

		vkBindImageMemory(device->device, image, memory, 0);

		// Create the image view
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = viewType;
		viewInfo.format = format;
		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.subresourceRange.aspectMask = aspect;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = mipLevels;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = layers;

		VK_ASSERT(vkCreateImageView(device->device, &viewInfo, nullptr, &view), "failed to create texture image view!");
		printf("Created vk object: %llu (VkImageView)\n", (uint64)view);
	}


	void TextureBase::TransitionImageLayout(VkImage image, VkFormat format, VkImageAspectFlags aspect, VkImageLayout oldLayout, VkImageLayout newLayout, uint32 mipLevels, uint32 layers)
	{
		VkImageMemoryBarrier barrier = {};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.oldLayout = oldLayout;
		barrier.newLayout = newLayout;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.image = image;
		barrier.subresourceRange.aspectMask = aspect;
		barrier.subresourceRange.baseMipLevel = 0;
		barrier.subresourceRange.levelCount = mipLevels;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = layers;

		VkPipelineStageFlags sourceStage;
		VkPipelineStageFlags destinationStage;

		if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
		{
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		}
		else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
		{
			barrier.srcAccessMask = 0;
			barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

			sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
			destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
		}
		else
		{
			throw std::invalid_argument("unsupported layout transition!");
		}

		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();
		commandBuffer.PipelineBarrier(sourceStage, destinationStage, barrier);
		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();
	}

	void TextureBase::GenerateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels)
	{
		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();

		VkImageMemoryBarrier barrier{};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.image = image;
		barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		barrier.subresourceRange.baseArrayLayer = 0;
		barrier.subresourceRange.layerCount = 1;
		barrier.subresourceRange.levelCount = 1;

		int32_t mipWidth = texWidth;
		int32_t mipHeight = texHeight;

		for (uint32_t i = 1; i < mipLevels; i++) {
			barrier.subresourceRange.baseMipLevel = i - 1;
			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

			commandBuffer.PipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, barrier);

			VkImageBlit blit{};
			blit.srcOffsets[0] = {0, 0, 0};
			blit.srcOffsets[1] = {mipWidth, mipHeight, 1};
			blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.srcSubresource.mipLevel = i - 1;
			blit.srcSubresource.baseArrayLayer = 0;
			blit.srcSubresource.layerCount = 1;
			blit.dstOffsets[0] = {0, 0, 0};
			blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
			blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blit.dstSubresource.mipLevel = i;
			blit.dstSubresource.baseArrayLayer = 0;
			blit.dstSubresource.layerCount = 1;

			commandBuffer.BlitImage(image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, blit, VK_FILTER_LINEAR);

			barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

			commandBuffer.PipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, barrier);

			if (mipWidth > 1) mipWidth /= 2;
			if (mipHeight > 1) mipHeight /= 2;
		}

		barrier.subresourceRange.baseMipLevel = mipLevels - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		commandBuffer.PipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, barrier);

		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();
	}

	void Texture::CopyBufferToImage(VkBuffer buffer, VkImage image, uint32 width, uint32 height)
	{
		VkBufferImageCopy region = {};
		region.bufferOffset = 0;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;

		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.layerCount = 1;

		region.imageOffset = {0, 0, 0};
		region.imageExtent = {width, height, 1};

		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();
		commandBuffer.CopyBufferToImage(buffer, image, region);
		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();
	}

	void gfx::Texture::CopyDdsToImage(VkBuffer buffer, std::vector<int8>& bytes, ddspp::Descriptor* desc, VkImage image, uint32 width, uint32 height, uint32 mipLevels)
	{
		std::vector<VkBufferImageCopy> bufferCopyRegions;

		for(uint32 level = 0; level < mipLevels; level++)
		{
			// Calculate offset into staging buffer for the current mip level and face
			uint32 offset = ddspp::get_offset(*desc, level, 0);
			VkBufferImageCopy bufferCopyRegion = {};
			bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			bufferCopyRegion.imageSubresource.mipLevel = level;
			bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
			bufferCopyRegion.imageSubresource.layerCount = 1;
			bufferCopyRegion.imageExtent.width = width >> level;
			bufferCopyRegion.imageExtent.height = height >> level;
			bufferCopyRegion.imageExtent.depth = 1;
			bufferCopyRegion.bufferOffset = offset;
			bufferCopyRegions.push_back(bufferCopyRegion);
		}

		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();
		commandBuffer.CopyBufferToImage(buffer, image, bufferCopyRegions);
		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();
	}

	void Texture::CopyKtxToImage(VkBuffer buffer, ktxTexture* kTexture, VkImage image, uint32 width, uint32 height, uint32 mipLevels)
	{
		std::vector<VkBufferImageCopy> bufferCopyRegions;

		for(uint32 level = 0; level < mipLevels; level++)
		{
			// Calculate offset into staging buffer for the current mip level and face
			ktx_size_t offset;
			KTX_error_code ret = ktxTexture_GetImageOffset(kTexture, level, 0, 0, &offset);
			assert(ret == KTX_SUCCESS);
			VkBufferImageCopy bufferCopyRegion = {};
			bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			bufferCopyRegion.imageSubresource.mipLevel = level;
			bufferCopyRegion.imageSubresource.baseArrayLayer = 0;
			bufferCopyRegion.imageSubresource.layerCount = 1;
			bufferCopyRegion.imageExtent.width = width >> level;
			bufferCopyRegion.imageExtent.height = height >> level;
			bufferCopyRegion.imageExtent.depth = 1;
			bufferCopyRegion.bufferOffset = offset;
			bufferCopyRegions.push_back(bufferCopyRegion);
		}

		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();
		commandBuffer.CopyBufferToImage(buffer, image, bufferCopyRegions);
		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();
	}

	void Texture::CopyKtxToCubeMap(VkBuffer buffer, ktxTexture* kTexture, VkImage image, int32 width, int32 height, int32 mipLevels)
	{
		std::vector<VkBufferImageCopy> bufferCopyRegions;

		for(int32 face = 0; face < 6; face++)
		{
			for(int32 level = 0; level < mipLevels; level++)
			{
				// Calculate offset into staging buffer for the current mip level and face
				ktx_size_t offset;
				KTX_error_code ret = ktxTexture_GetImageOffset(kTexture, level, 0, face, &offset);
				assert(ret == KTX_SUCCESS);
				VkBufferImageCopy bufferCopyRegion = {};
				bufferCopyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
				bufferCopyRegion.imageSubresource.mipLevel = level;
				bufferCopyRegion.imageSubresource.baseArrayLayer = face;
				bufferCopyRegion.imageSubresource.layerCount = 1;
				bufferCopyRegion.imageExtent.width = width >> level;
				bufferCopyRegion.imageExtent.height = height >> level;
				bufferCopyRegion.imageExtent.depth = 1;
				bufferCopyRegion.bufferOffset = offset;
				bufferCopyRegions.push_back(bufferCopyRegion);
			}
		}

		CommandBuffer commandBuffer(device);
		commandBuffer.Begin();
		commandBuffer.CopyBufferToImage(buffer, image, bufferCopyRegions);
		commandBuffer.End();
		device->SubmitOutOfFrame(&commandBuffer);
		device->WaitIdle();

	}


	SamplerHandle* SamplerHandle::Create(DeviceHandle* device, Filter filter, AddressMode mode) { return new Sampler((Device*)device, GetVkFilter(filter), GetVkAddressMode(mode)); }
	void SamplerHandle::Destroy(SamplerHandle* handle) { delete (Sampler*)handle; }

	Sampler::Sampler(Device* device, VkFilter filter, VkSamplerAddressMode mode) : device(device)
	{
		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = filter;
		samplerInfo.minFilter = filter;
		samplerInfo.addressModeU = mode;
		samplerInfo.addressModeV = mode;
		samplerInfo.addressModeW = mode;
		samplerInfo.anisotropyEnable = VK_TRUE;
		samplerInfo.maxAnisotropy = 16;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 1024.0f;

		VK_ASSERT(vkCreateSampler(device->device, &samplerInfo, nullptr, &sampler), "failed to create texture sampler!");
		printf("Created vk object: %llu (VkSampler)\n", (uint64)sampler);
	}

	Sampler::~Sampler()
	{
		vkDestroySampler(device->device, sampler, nullptr);
	}

}
