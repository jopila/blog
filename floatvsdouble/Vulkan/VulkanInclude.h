#pragma once



#include <vulkan/vulkan.h>
#define TOKENPASTE(x, y) x ## y
#define TOKENPASTE2(x, y) TOKENPASTE(x, y)
#define VK_ASSERT(fn, str) VkResult TOKENPASTE2(result, __LINE__) = fn; \
if(TOKENPASTE2(result, __LINE__) != VK_SUCCESS) { \
  printf(str " (%i)\n", TOKENPASTE2(result, __LINE__)); }

#include "Types.h"

namespace gfx
{
	VkFormat GetVkFormat(Format format);
	int GetVkFormatSize(Format format);
	VkShaderStageFlags GetVkShaders(ShaderType shaders);
	VkPrimitiveTopology GetVkTopology(Topology topology);
	VkFilter GetVkFilter(Filter filter);
	VkSamplerAddressMode GetVkAddressMode(AddressMode addressMode);
}
