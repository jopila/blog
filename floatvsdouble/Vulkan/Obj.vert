
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;
layout(location = 3) in mat4 model;

layout(location = 0) out vec3 out_normal;
layout(location = 1) out vec2 out_uv;

layout(binding = 0) uniform UniformBufferObject
{
  mat4 viewProj;
} ubo;

void main()
{
	gl_Position = ubo.viewProj * model * vec4(pos, 1.0);
	mat3 modelNormal = transpose(inverse(mat3(model)));
	out_normal = modelNormal * normal;
	out_uv = uv;
}