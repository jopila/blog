#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"


namespace gfx
{

	class Device;

	class Shader final : public ShaderHandle
	{
	public:
		Shader(Device* device, std::string name);
		~Shader();

	private:
		friend class Device;
		friend class Pipeline;

		Device* device;

		VkShaderModule shaderModule;
		ShaderType shaderType;
	};

}
