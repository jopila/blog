#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"

namespace gfx
{
	class Device;
	class Semaphore;


	class CommandBuffer final : public CommandBufferHandle
	{
	public:
		CommandBuffer(Device* device);
		~CommandBuffer();

		void CopyBufferToBuffer(VkBuffer src, int srcOffset, VkBuffer dst, int dstOffset, VkDeviceSize size);
		void CopyBufferToImage(VkBuffer src, VkImage dst, VkBufferImageCopy region);
		void CommandBuffer::CopyBufferToImage(VkBuffer src, VkImage dst, std::vector<VkBufferImageCopy> regions);
		void PipelineBarrier(VkPipelineStageFlags src, VkPipelineStageFlags dst, VkImageMemoryBarrier barrier);
		void BlitImage(VkImage srcImage, VkImageLayout srcImageLayout, VkImage dstImage, VkImageLayout dstImageLayout, VkImageBlit blit, VkFilter filter);
		void BeginRenderPass(const RenderPassHandle* renderPass);
		void BeginDraw(const PipelineHandle* pipeline, const DescriptorSetHandle* descriptorSet);
		void Draw(const VertexInfoHandle* vertexInfo, int32 numVertices, int32 firstVertex = 0);
		void DrawInstanced(const VertexInfoHandle* vertexInfo, int32 numIndices, int32 numInstances, int32 firstIndex = 0, int32 firstVertex = 0, int32 instanceBufferIndex = 0);
		void SetScissorRect(int32 x, int32 y, int32 w, int32 h);
		void SetViewport(int32 x, int32 y, int32 w, int32 h, float minZ, float maxZ);
		void Clear(FloatColor4 clearColor);
		void EndDraw();
		void EndRenderPass();
		void Begin();
		void End();


	private:
		friend class Device;

		Device* device;
		VkCommandBuffer commandBuffer;
	};
}