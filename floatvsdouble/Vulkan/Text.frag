
#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_samplerless_texture_functions : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec4 color;

layout(binding = 2) uniform sampler samp;
layout(binding = 3) uniform texture2D tex;

const float pxRange = 3;

float median(float r, float g, float b)
{
    return max(min(r, g), min(max(r, g), b));
}

void main()
{
//	vec2 msdfUnit = pxRange/vec2(textureSize(texSampler, 0));
//
//	vec3 sampleR = texture(texSampler, uv - dFdx(uv)/3).rgb;
//	vec3 sampleG = texture(texSampler, uv).rgb;
//	vec3 sampleB = texture(texSampler, uv + dFdx(uv)/3).rgb;
//	vec3 sigDists;
//	sigDists.r = median(sampleR.r, sampleR.g, sampleR.b) - 0.5f;
//	sigDists.g = median(sampleG.r, sampleG.g, sampleG.b) - 0.5f;
//	sigDists.b = median(sampleB.r, sampleB.g, sampleB.b) - 0.5f;
//	sigDists *= dot(msdfUnit, 0.5f/fwidth(uv));
//	sigDists = clamp(sigDists + 0.5f, 0.0f, 1.0f);
//
//	if(sigDists.r + 0.25f < sigDists.g)
//		sigDists.g = sigDists.r + 0.25f;
//	if(sigDists.g + 0.25f < sigDists.b)
//		sigDists.b = sigDists.g + 0.25f;
//
//	if(sigDists.b + 0.25f < sigDists.g)
//	  sigDists.g = sigDists.b + 0.25f;
//	if(sigDists.g + 0.25f < sigDists.r)
//		sigDists.r = sigDists.g + 0.25f;
//
//
//	outColor.w = 1.0f;
//	outColor.rgb = sigDists + .13;// * sigDists;

//	vec3 sampleR = texture(texSampler, uv - dFdx(uv)/3).rgb;
//	vec3 sampleG = texture(texSampler, uv).rgb;
//	vec3 sampleB = texture(texSampler, uv + dFdx(uv)/3).rgb;
//	vec3 sigDists;
//	sigDists.r = median(sampleR.r, sampleR.g, sampleR.b) - 0.5f;
//	sigDists.g = median(sampleG.r, sampleG.g, sampleG.b) - 0.5f;
//	sigDists.b = median(sampleB.r, sampleB.g, sampleB.b) - 0.5f;
//	ivec2 sz = textureSize( texSampler, 0 );
//	float dx = dFdx( uv.x ) * sz.x;
//	float dy = dFdy( uv.y ) * sz.y;
//	float toPixels = max(4.0 * inversesqrt( dx * dx + dy * dy ), 1.0f);
//	vec3 opacities = clamp( sigDists * toPixels + 0.5, 0.0, 1.0 );
//
//	outColor.w = 1.0f;
//	outColor.rgb = opacities;


	vec2 msdfUnit = pxRange/vec2(textureSize(tex, 0));
	vec3 dists = texture(sampler2D(tex, samp), uv).rgb;
	float sigDist = median(dists.r, dists.g, dists.b) - 0.5f;
	sigDist *= max(dot(msdfUnit, 0.5/fwidth(uv)), 1.0f);
  float opacity = clamp(sigDist + 0.5, 0.0, 1.0);

	//outColor = color;
	outColor.w = 1.0f - opacity;
	outColor.rgb = vec3(1.0f) - color.rgb;
	//outColor.w = 1.0f;
	//outColor.rgb = vec3(1.0f) - opacity;

//	vec3 dists = texture( texSampler, uv ).rgb;
//	ivec2 sz = textureSize( texSampler, 0 );
//	float dx = dFdx( uv.x ) * sz.x;
//	float dy = dFdy( uv.y ) * sz.y;
//	float toPixels = 4.0 * inversesqrt( dx * dx + dy * dy );
//	float sigDist = median( dists.r, dists.g, dists.b ) - 0.5;
//	float opacity = clamp( sigDist * toPixels + 0.5, 0.0, 1.0 );
//
//	outColor.w = 1.0f;
//	outColor.rgb = vec3(opacity, opacity, opacity);
}