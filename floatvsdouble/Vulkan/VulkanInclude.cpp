#include "stdafx.h"
#include "VulkanInclude.h"

VkFormat gfx::GetVkFormat(Format format)
{
	switch(format)
	{
		case Format::R32_UINT:            return VK_FORMAT_R32_UINT;
		case Format::R32_SINT:						return VK_FORMAT_R32_SINT;
		case Format::R32_SFLOAT:					return VK_FORMAT_R32_SFLOAT;
		case Format::R32G32_UINT:					return VK_FORMAT_R32G32_UINT;
		case Format::R32G32_SINT:					return VK_FORMAT_R32G32_SINT;
		case Format::R32G32_SFLOAT:				return VK_FORMAT_R32G32_SFLOAT;
		case Format::R32G32B32_UINT:			return VK_FORMAT_R32G32B32_UINT;
		case Format::R32G32B32_SINT:			return VK_FORMAT_R32G32B32_SINT;
		case Format::R32G32B32_SFLOAT:		return VK_FORMAT_R32G32B32_SFLOAT;
		case Format::R32G32B32A32_UINT:		return VK_FORMAT_R32G32B32A32_UINT;
		case Format::R32G32B32A32_SINT:		return VK_FORMAT_R32G32B32A32_SINT;
		case Format::R32G32B32A32_SFLOAT:	return VK_FORMAT_R32G32B32A32_SFLOAT;

		case Format::R16_UNORM:           return VK_FORMAT_R16_UNORM;
		case Format::R16_SNORM:						return VK_FORMAT_R16_SNORM;
		case Format::R16_UINT:						return VK_FORMAT_R16_UINT;
		case Format::R16_SINT:						return VK_FORMAT_R16_SINT;
		case Format::R16_SFLOAT:					return VK_FORMAT_R16_SFLOAT;
		case Format::R16G16_UNORM:				return VK_FORMAT_R16G16_UNORM;
		case Format::R16G16_SNORM:				return VK_FORMAT_R16G16_SNORM;
		case Format::R16G16_UINT:					return VK_FORMAT_R16G16_UINT;
		case Format::R16G16_SINT:					return VK_FORMAT_R16G16_SINT;
		case Format::R16G16_SFLOAT:				return VK_FORMAT_R16G16_SFLOAT;
		case Format::R16G16B16_UNORM:			return VK_FORMAT_R16G16B16_UNORM;
		case Format::R16G16B16_SNORM:			return VK_FORMAT_R16G16B16_SNORM;
		case Format::R16G16B16_UINT:			return VK_FORMAT_R16G16B16_UINT;
		case Format::R16G16B16_SINT:			return VK_FORMAT_R16G16B16_SINT;
		case Format::R16G16B16_SFLOAT:		return VK_FORMAT_R16G16B16_SFLOAT;
		case Format::R16G16B16A16_UNORM:	return VK_FORMAT_R16G16B16A16_UNORM;
		case Format::R16G16B16A16_SNORM:	return VK_FORMAT_R16G16B16A16_SNORM;
		case Format::R16G16B16A16_UINT:		return VK_FORMAT_R16G16B16A16_UINT;
		case Format::R16G16B16A16_SINT:		return VK_FORMAT_R16G16B16A16_SINT;
		case Format::R16G16B16A16_SFLOAT:	return VK_FORMAT_R16G16B16A16_SFLOAT;

		case Format::R8_UNORM:            return VK_FORMAT_R8_UNORM;
		case Format::R8_SNORM:			      return VK_FORMAT_R8_SNORM;
		case Format::R8_UINT:				      return VK_FORMAT_R8_UINT;
		case Format::R8_SINT:				      return VK_FORMAT_R8_SINT;
		case Format::R8_SRGB:				      return VK_FORMAT_R8_SRGB;
		case Format::R8G8_UNORM:		      return VK_FORMAT_R8G8_UNORM;
		case Format::R8G8_SNORM:		      return VK_FORMAT_R8G8_SNORM;
		case Format::R8G8_UINT:			      return VK_FORMAT_R8G8_UINT;
		case Format::R8G8_SINT:			      return VK_FORMAT_R8G8_SINT;
		case Format::R8G8_SRGB:			      return VK_FORMAT_R8G8_SRGB;
		case Format::R8G8B8_UNORM:	      return VK_FORMAT_R8G8B8_UNORM;
		case Format::R8G8B8_SNORM:	      return VK_FORMAT_R8G8B8_SNORM;
		case Format::R8G8B8_UINT:		      return VK_FORMAT_R8G8B8_UINT;
		case Format::R8G8B8_SINT:		      return VK_FORMAT_R8G8B8_SINT;
		case Format::R8G8B8_SRGB:		      return VK_FORMAT_R8G8B8_SRGB;
		case Format::B8G8R8_UNORM:	      return VK_FORMAT_B8G8R8_UNORM;
		case Format::B8G8R8_SNORM:	      return VK_FORMAT_B8G8R8_SNORM;
		case Format::B8G8R8_UINT:		      return VK_FORMAT_B8G8R8_UINT;
		case Format::B8G8R8_SINT:		      return VK_FORMAT_B8G8R8_SINT;
		case Format::B8G8R8_SRGB:		      return VK_FORMAT_B8G8R8_SRGB;
		case Format::R8G8B8A8_UNORM:      return VK_FORMAT_R8G8B8A8_UNORM;
		case Format::R8G8B8A8_SNORM:      return VK_FORMAT_R8G8B8A8_SNORM;
		case Format::R8G8B8A8_UINT:	      return VK_FORMAT_R8G8B8A8_UINT;
		case Format::R8G8B8A8_SINT:	      return VK_FORMAT_R8G8B8A8_SINT;
		case Format::R8G8B8A8_SRGB:	      return VK_FORMAT_R8G8B8A8_SRGB;
		case Format::B8G8R8A8_UNORM:      return VK_FORMAT_B8G8R8A8_UNORM;
		case Format::B8G8R8A8_SNORM:      return VK_FORMAT_B8G8R8A8_SNORM;
		case Format::B8G8R8A8_UINT:	      return VK_FORMAT_B8G8R8A8_UINT;
		case Format::B8G8R8A8_SINT:	      return VK_FORMAT_B8G8R8A8_SINT;
		case Format::B8G8R8A8_SRGB:	      return VK_FORMAT_B8G8R8A8_SRGB;
			
		default: return VK_FORMAT_R8_UNORM; // =|
	}
}

int gfx::GetVkFormatSize(Format format)
{
	switch(format)
	{
 		case Format::R32_UINT:            return 4;
		case Format::R32_SINT:						return 4;
		case Format::R32_SFLOAT:					return 4;
		case Format::R32G32_UINT:					return 8;
		case Format::R32G32_SINT:					return 8;
		case Format::R32G32_SFLOAT:				return 8;
		case Format::R32G32B32_UINT:			return 12;
		case Format::R32G32B32_SINT:			return 12;
		case Format::R32G32B32_SFLOAT:		return 12;
		case Format::R32G32B32A32_UINT:		return 16;
		case Format::R32G32B32A32_SINT:		return 16;
		case Format::R32G32B32A32_SFLOAT:	return 16;

		case Format::R16_UNORM:           return 2;
		case Format::R16_SNORM:						return 2;
		case Format::R16_UINT:						return 2;
		case Format::R16_SINT:						return 2;
		case Format::R16_SFLOAT:					return 2;
		case Format::R16G16_UNORM:				return 4;
		case Format::R16G16_SNORM:				return 4;
		case Format::R16G16_UINT:					return 4;
		case Format::R16G16_SINT:					return 4;
		case Format::R16G16_SFLOAT:				return 4;
		case Format::R16G16B16_UNORM:			return 6;
		case Format::R16G16B16_SNORM:			return 6;
		case Format::R16G16B16_UINT:			return 6;
		case Format::R16G16B16_SINT:			return 6;
		case Format::R16G16B16_SFLOAT:		return 6;
		case Format::R16G16B16A16_UNORM:	return 8;
		case Format::R16G16B16A16_SNORM:	return 8;
		case Format::R16G16B16A16_UINT:		return 8;
		case Format::R16G16B16A16_SINT:		return 8;
		case Format::R16G16B16A16_SFLOAT:	return 8;

		case Format::R8_UNORM:            return 1;
		case Format::R8_SNORM:			      return 1;
		case Format::R8_UINT:				      return 1;
		case Format::R8_SINT:				      return 1;
		case Format::R8_SRGB:				      return 1;
		case Format::R8G8_UNORM:		      return 2;
		case Format::R8G8_SNORM:		      return 2;
		case Format::R8G8_UINT:			      return 2;
		case Format::R8G8_SINT:			      return 2;
		case Format::R8G8_SRGB:			      return 2;
		case Format::R8G8B8_UNORM:	      return 3;
		case Format::R8G8B8_SNORM:	      return 3;
		case Format::R8G8B8_UINT:		      return 3;
		case Format::R8G8B8_SINT:		      return 3;
		case Format::R8G8B8_SRGB:		      return 3;
		case Format::B8G8R8_UNORM:	      return 3;
		case Format::B8G8R8_SNORM:	      return 3;
		case Format::B8G8R8_UINT:		      return 3;
		case Format::B8G8R8_SINT:		      return 3;
		case Format::B8G8R8_SRGB:		      return 3;
		case Format::R8G8B8A8_UNORM:      return 4;
		case Format::R8G8B8A8_SNORM:      return 4;
		case Format::R8G8B8A8_UINT:	      return 4;
		case Format::R8G8B8A8_SINT:	      return 4;
		case Format::R8G8B8A8_SRGB:	      return 4;
		case Format::B8G8R8A8_UNORM:      return 4;
		case Format::B8G8R8A8_SNORM:      return 4;
		case Format::B8G8R8A8_UINT:	      return 4;
		case Format::B8G8R8A8_SINT:	      return 4;
		case Format::B8G8R8A8_SRGB:	      return 4;

		default: return 0;
	}
}

VkShaderStageFlags gfx::GetVkShaders(ShaderType shaders)
{
	VkShaderStageFlags result = 0;
	result |= ((int)shaders & (int)ShaderType::Vertex ? VK_SHADER_STAGE_VERTEX_BIT : 0);
	result |= ((int)shaders & (int)ShaderType::Geometry ? VK_SHADER_STAGE_GEOMETRY_BIT : 0);
	result |= ((int)shaders & (int)ShaderType::Fragment ? VK_SHADER_STAGE_FRAGMENT_BIT : 0);
	return result;
}
VkPrimitiveTopology gfx::GetVkTopology(Topology topology)
{
	switch(topology)
	{
		case Topology::PointList:     return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
		case Topology::LineList:      return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
		case Topology::LineStrip:     return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
		case Topology::TriangleList:  return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		case Topology::TriangleStrip: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
		case Topology::TriangleFan:   return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
		case Topology::LineListWithAdjacency:      return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
		case Topology::LineStripWithAdjacency:     return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
		case Topology::TriangleListWithAdjacency:  return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
		case Topology::TriangleStripWithAdjacency: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
		case Topology::PatchList:    return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
		default: __debugbreak(); return (VkPrimitiveTopology)-1;
	}
}

VkFilter gfx::GetVkFilter(Filter filter)
{
	switch(filter)
	{
		case Filter::Nearest:   return VK_FILTER_NEAREST;
		case Filter::Linear:     return VK_FILTER_LINEAR;
		case Filter::CubicImage: return VK_FILTER_CUBIC_IMG;
		default: __debugbreak(); return (VkFilter)-1;
	}
}

VkSamplerAddressMode gfx::GetVkAddressMode(AddressMode addressMode)
{
	switch(addressMode)
	{
		case AddressMode::Repeat:         return VK_SAMPLER_ADDRESS_MODE_REPEAT;
		case AddressMode::MirroredRepeat: return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
		case AddressMode::ClampToEdge:    return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
		case AddressMode::ClampToBorder:  return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
		case AddressMode::MirroredClampToEdge: return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
		default: __debugbreak(); return (VkSamplerAddressMode)-1;
	}
}
