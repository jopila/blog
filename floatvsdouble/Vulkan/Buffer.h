#pragma once

namespace gfx
{
	class Device;


	class Buffer final : public BufferHandle
	{
	public:
		Buffer(Device* device, int32 size, uint32 usage, uint32 properties);
		void Update(void* data, int32 offset, int32 size);
		void Map(void*& mappedData, int32 offset, int32 size);
		void Unmap();
		void Copy(CommandBufferHandle* commandBuffer, BufferHandle* srcBuffer, int srcOffset, BufferHandle* dstBuffer, int dstOffset, VkDeviceSize size, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore);
		~Buffer();

	private:
		friend class Device;
		friend class Texture;
		friend class DescriptorSet;
		friend class CommandBuffer;

		Device* device;
		VkBuffer buffer;
		VkDeviceMemory memory;
		CommandBuffer* copyCommandBuffer;
	};

	uint32_t FindMemoryType(VkPhysicalDevice physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties);

}

