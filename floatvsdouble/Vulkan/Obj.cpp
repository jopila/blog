
#include "stdafx.h"

#include <sstream>

#include "Obj.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "Debug.h"
#include "File.h"
#include "..\utility\Mat4x4.h"

#define mReadRaw(file, var) fread(&(var), sizeof(var), 1, file)

struct ObjRenderData
{
	gfx::DeviceHandle* device;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformViewProjBuffer;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	struct UBOViewProj
	{
		Mat4x4 viewProj;
	} uboViewProj;

	struct Vertex
	{
		float x, y, z;
		float nx, ny, nz;
		float u, v;
	};
	struct Instance
	{
		Mat4x4 model;
	};
	struct Mesh
	{
		int32 vertexCount;
		int32 indexCount;
		Vertex* vertices;
		int16* indices;
    int32 vertexLocation;
    int32 indexLocation;
    Instance* stagingMemoryInstance;
    Instance* currentInstance;
    int32 instanceBufferIndex;
	};

	std::map<std::string, Mesh> meshes;

	const int bufferSize = 4000;
};

ObjRenderer::ObjRenderer(gfx::DeviceHandle* device, std::string objDirectory)
{
  data = new ObjRenderData();
  data->device = device;

  // Pipeline
  data->uniformViewProjBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboViewProj), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  // Load up our meshes
  std::vector<std::string> objFilenames = ListFilesInDirectory(objDirectory, true);
  int32 vertexTotal = 0;
  int32 indexTotal = 0;
  for(std::string objFilename : objFilenames)
  {
    if(EndsWith(objFilename, ".mesh"))
    {
      FILE* file;
      if(!fopen_s(&file, objFilename.c_str(), "rb"))
      {
        ObjRenderData::Mesh& mesh = data->meshes[GetFileNameFromPath(objFilename, false)];
        mReadRaw(file, mesh.vertexCount);
        vertexTotal += mesh.vertexCount;
        mesh.vertices = new ObjRenderData::Vertex[mesh.vertexCount];
        fread(mesh.vertices, sizeof(ObjRenderData::Vertex), mesh.vertexCount, file);
        mReadRaw(file, mesh.indexCount);
        indexTotal += mesh.indexCount;
        mesh.indices = new int16[mesh.indexCount];
        fread(mesh.indices, sizeof(int16), mesh.indexCount, file);
      }
    }
  }

  // serialize all our meshes into one buffer
  ObjRenderData::Vertex* allVertices = new ObjRenderData::Vertex[vertexTotal];
  int16* allIndices = new int16[indexTotal];
  int32 currentVertex = 0;
  int32 currentIndex = 0;
  int32 currentInstanceBuffer = 0;
  for(auto& mesh : data->meshes)
  {
    mesh.second.instanceBufferIndex = currentInstanceBuffer++;

    mesh.second.vertexLocation = currentVertex;
    memcpy(allVertices + currentVertex, mesh.second.vertices, mesh.second.vertexCount * sizeof(mesh.second.vertices[0]));
    currentVertex += mesh.second.vertexCount;

    mesh.second.indexLocation = currentIndex;
    memcpy(allIndices + currentIndex, mesh.second.indices, mesh.second.indexCount * sizeof(mesh.second.indices[0]));
    currentIndex += mesh.second.indexCount;
  }

  auto& descriptorSetLayout = data->descriptorSetLayout;
  descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
  descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
  descriptorSetLayout->Finalize();

  auto& descriptorSet = data->descriptorSet;
  descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
  descriptorSet->UpdateUniformBuffer(0, data->uniformViewProjBuffer, sizeof(data->uboViewProj));

  auto& vertexInfo = data->vertexInfo;
  vertexInfo = gfx::VertexInfoHandle::Create(device);
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // pos
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // normal
  vertexInfo->AddVar(gfx::Format::R32G32_SFLOAT);    // uv
  vertexInfo->AddInstancedVar(gfx::Format::R32G32B32A32_SFLOAT); // model1
  vertexInfo->AddInstancedVar(gfx::Format::R32G32B32A32_SFLOAT); // model2
  vertexInfo->AddInstancedVar(gfx::Format::R32G32B32A32_SFLOAT); // model3
  vertexInfo->AddInstancedVar(gfx::Format::R32G32B32A32_SFLOAT); // model4
  vertexInfo->Finalize();

  gfx::CommandBufferHandle* commandBuffer = gfx::CommandBufferHandle::Create(device);
  vertexInfo->CreateAndUpdateVertexData(commandBuffer, vertexTotal, (void*)allVertices);
  vertexInfo->CreateInstanceData(data->bufferSize, (int32)data->meshes.size());
  vertexInfo->CreateAndUpdateIndexData(commandBuffer, indexTotal, (void*)allIndices);
  gfx::CommandBufferHandle::Destroy(commandBuffer);
  delete[] allVertices;
  delete[] allIndices;

  data->vert = gfx::ShaderHandle::Create(device, "Obj.vert");
  data->frag = gfx::ShaderHandle::Create(device, "Obj.frag");

  auto& pipeline = data->pipeline;
  pipeline = gfx::PipelineHandle::Create(device);
  pipeline->AddShader(data->vert);
  pipeline->AddShader(data->frag);

  pipeline->SetTopology(gfx::Topology::TriangleList);
  pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

  pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
  pipeline->SetDescriptorSetLayout(descriptorSetLayout);
  pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);
}

ObjRenderer::~ObjRenderer()
{
  for(auto mesh : data->meshes)
  {
    delete[] mesh.second.vertices;
    delete[] mesh.second.indices;
  }
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformViewProjBuffer);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

void ObjRenderer::UpdateViewProj(const Mat4x4& viewProj)
{
  data->uboViewProj.viewProj = viewProj;
  data->uboViewProj.viewProj.Transpose();
}

void ObjRenderer::RenderOne(ObjRenderInfo renderInfo)
{
  ObjRenderData::Mesh& mesh = data->meshes[renderInfo.obj];
  if(mesh.currentInstance - mesh.stagingMemoryInstance < data->bufferSize)
  {
    Mat4x4 mat;
    Vec4 translation = {renderInfo.x, renderInfo.y, renderInfo.z, 0.0f};
    mat = Mat4x4::MakeTranslation(translation);
    Mat4x4 matrot = Mat4x4::MakeRotation(renderInfo.q);
    mat = mat * matrot;
    mat.Transpose();
    mesh.currentInstance->model = mat;
    mesh.currentInstance++;
  }
  else
    __debugbreak();
}

void ObjRenderer::BeginFrame()
{
  for(auto& nameAndMesh : data->meshes)
  {
    ObjRenderData::Mesh& mesh = nameAndMesh.second;
    void* temp = (void*)mesh.stagingMemoryInstance;
    data->vertexInfo->MapInstanceData(temp, 0, data->vertexInfo->GetMaxInstances(), mesh.instanceBufferIndex);
    mesh.stagingMemoryInstance = (ObjRenderData::Instance*)temp;
    mesh.currentInstance = mesh.stagingMemoryInstance;
  }
}

void ObjRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  for(auto& nameAndMesh : data->meshes)
  {
    ObjRenderData::Mesh& mesh = nameAndMesh.second;
    data->vertexInfo->UnmapInstanceData(commandBuffer, 0, (int32)(mesh.currentInstance - mesh.stagingMemoryInstance), mesh.instanceBufferIndex);
  }
  data->uniformViewProjBuffer->Update(&data->uboViewProj, 0, sizeof(data->uboViewProj));
}

void ObjRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  for(auto& nameAndMesh : data->meshes)
  {
    ObjRenderData::Mesh& mesh = nameAndMesh.second;
    if(mesh.currentInstance == mesh.stagingMemoryInstance)
      continue;

    commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
    commandBuffer->DrawInstanced(data->vertexInfo, mesh.indexCount, (int)(mesh.currentInstance - mesh.stagingMemoryInstance), mesh.indexLocation, mesh.vertexLocation, mesh.instanceBufferIndex);
    commandBuffer->EndDraw();
  }
}
