#pragma once

#include "Gfx.h"

struct TextRendererHidden;

class TextRenderer : public gfx::RendererHandle
{
public:
	TextRenderer(gfx::DeviceHandle* device, std::string name);
	~TextRenderer();

	void RenderString(const char* text, float size, float x, float y, int color);
	void PushScissor(int32 x, int32 y, int32 width, int32 height);
	void PopScissor();

	float GetWidth(const char* text, float size);

private:
	friend class gfx::Gfx;
	void BeginFrame();
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
	TextRendererHidden* textRenderer;
};