
#include "stdafx.h"

#include "Gfx.h"
#include "Device.h"
#include "Synchronization.h"

namespace gfx
{
	SemaphoreHandle* SemaphoreHandle::Create(DeviceHandle* device) { return new Semaphore((Device*)device); }
	void SemaphoreHandle::Destroy(SemaphoreHandle* handle) { delete (Semaphore*)handle; }

	Semaphore::Semaphore(Device* device) : device(device)
	{

		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		VK_ASSERT(vkCreateSemaphore(device->device, &semaphoreInfo, nullptr, &semaphore), "failed to create semaphore!");
		printf("Created vk object: %llu (VkSemaphore)\n", (uint64)semaphore);
	}

	Semaphore::~Semaphore()
	{
		vkDestroySemaphore(device->device, semaphore, nullptr);
	}

	FenceHandle* FenceHandle::Create(DeviceHandle* device, bool startSignaled) { return new Fence((Device*)device, startSignaled); }
	void FenceHandle::Destroy(FenceHandle* handle) { delete (Fence*)handle; }

	Fence::Fence(Device* device, bool startSignaled) : device(device)
	{
		StackEvents ev("Fence::Fence");
		VkFenceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		createInfo.pNext = nullptr;
		createInfo.flags = startSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;

		vkCreateFence(device->device, &createInfo, nullptr, &fence);
		printf("Created vk object: %llu (VkFence)\n", (uint64)fence);
	}

	Fence::~Fence()
	{
		StackEvents ev("Fence::~Fence");
		vkDestroyFence(device->device, fence, nullptr);
	}

	bool Fence::IsSignaled()
	{
		StackEvents ev("Fence::IsSignaled");
		return vkGetFenceStatus(device->device, fence) == VK_SUCCESS;
	}

	void Fence::WaitSignaled()
	{
		StackEvents ev("Fence::WaitSignaled");
		while(vkGetFenceStatus(device->device, fence) != VK_SUCCESS)
			;
	}

	void Fence::Reset()
	{
		StackEvents ev("Fence::Reset");
		vkResetFences(device->device, 1, &fence);
	}
}