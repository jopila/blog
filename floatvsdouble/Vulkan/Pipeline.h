#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"

namespace gfx
{

	class Pipeline final : public PipelineHandle
	{
	public:
		Pipeline(Device* device);
		~Pipeline();

		void AddShader(const ShaderHandle* shader);
		void SetTopology(Topology topology);
		void SetViewport(float x, float y, float width, float height, float minZ, float maxZ);
		void SetScissor(int x, int y, uint32 width, uint32 height);
		void SetDescriptorSetLayout(DescriptorSetLayoutHandle* descriptorSetLayout);
		void Finalize(RenderPassHandle* renderPass, VertexInfoHandle* vertexInfo);

	private:
		friend class Device;
		friend class CommandBuffer;

		Device* device;

		std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		VkViewport viewport = {};
		VkRect2D scissor = {};
		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		VkDescriptorSetLayout descriptorSetLayout;

		VkPipelineLayout pipelineLayout;
		VkPipeline pipeline;
	};
}
