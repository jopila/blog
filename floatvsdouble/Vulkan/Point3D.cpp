
#include "stdafx.h"

#include "Point3D.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "Debug.h"
#include "..\utility\Mat4x4.h"

struct Point3DRenderData
{
	gfx::DeviceHandle* device;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformViewProjBuffer;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	Point3DRenderInfo* stagingMemoryVertex;
	uint16* stagingMemoryIndex;
	Point3DRenderInfo* currentVertex;
	uint16* currentIndex;

	struct UBOViewProj
	{
		Mat4x4 viewProj;
	} uboViewProj;

	struct Vertex
	{
		float x, y, z;
	};
	const std::vector<Vertex> vertices =
	{
		{-1.0f, -1.0f,  1.0f},
		{ 1.0f, -1.0f,  1.0f},
		{ 1.0f, -1.0f, -1.0f},
		{-1.0f, -1.0f, -1.0f},
		{-1.0f,  1.0f,  1.0f},
		{ 1.0f,  1.0f,  1.0f},
		{ 1.0f,  1.0f, -1.0f},
		{-1.0f,  1.0f, -1.0f},
	};

	const std::vector<uint16> indices =
	{
		// Right
		1, 2, 6,
		6, 5, 1,
		// Left
		0, 4, 7,
		7, 3, 0,
		// Top
		4, 5, 6,
		6, 7, 4,
		// Bottom
		0, 3, 2,
		2, 1, 0,
		// Back
		0, 1, 5,
		5, 4, 0,
		// Front
		3, 7, 6,
		6, 2, 3
	};

	const int bufferSize = 40000;
};

Point3DRenderer::Point3DRenderer(gfx::DeviceHandle* device)
{
  data = new Point3DRenderData();
  data->device = device;

  // Pipeline
  data->uniformViewProjBuffer = gfx::BufferHandle::Create(device, sizeof(data->uboViewProj), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

  auto& descriptorSetLayout = data->descriptorSetLayout;
  descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
  descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
  descriptorSetLayout->Finalize();

  auto& descriptorSet = data->descriptorSet;
  descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
  descriptorSet->UpdateUniformBuffer(0, data->uniformViewProjBuffer, sizeof(data->uboViewProj));

  auto& vertexInfo = data->vertexInfo;
  vertexInfo = gfx::VertexInfoHandle::Create(device);
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // pos
  vertexInfo->AddVar(gfx::Format::R32G32B32_SFLOAT); // color
  vertexInfo->Finalize();
  vertexInfo->CreateVertexData(data->bufferSize);
  vertexInfo->CreateIndexData(data->bufferSize);

  data->vert = gfx::ShaderHandle::Create(device, "Point3D.vert");
  data->frag = gfx::ShaderHandle::Create(device, "Point3D.frag");

  auto& pipeline = data->pipeline;
  pipeline = gfx::PipelineHandle::Create(device);
  pipeline->AddShader(data->vert);
  pipeline->AddShader(data->frag);

  pipeline->SetTopology(gfx::Topology::TriangleList);
  pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

  pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
  pipeline->SetDescriptorSetLayout(descriptorSetLayout);
  pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);
}

Point3DRenderer::~Point3DRenderer()
{
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformViewProjBuffer);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

void Point3DRenderer::UpdateViewProj(const Mat4x4& viewProj)
{
  data->uboViewProj.viewProj = viewProj;
  data->uboViewProj.viewProj.Transpose();
}

void Point3DRenderer::RenderOne(Point3DRenderInfo renderInfo)
{
  if(data->currentVertex - data->stagingMemoryVertex < (int32)data->vertices.size() * data->bufferSize)
  {
    int64 vertexOffset = data->currentVertex - data->stagingMemoryVertex;
    for(int i = 0; i < data->vertices.size(); i++)
    {
      Point3DRenderInfo info = renderInfo;
      info.x += data->vertices[i].x * 0.1f;
      info.y += data->vertices[i].y * 0.1f;
      info.z += data->vertices[i].z * 0.1f;
      *data->currentVertex++ = info;
    }
    for(int i = 0; i < data->indices.size(); i++)
    {
      *data->currentIndex++ = data->indices[i] + (uint16)vertexOffset;
    }
  }
  else
    __debugbreak();
}

void Point3DRenderer::BeginFrame()
{
  void* temp = (void*)data->stagingMemoryVertex;
  data->vertexInfo->MapVertexData(temp, 0, data->vertexInfo->GetMaxVertices());
  data->stagingMemoryVertex = (Point3DRenderInfo*)temp;
  data->currentVertex = data->stagingMemoryVertex;

  temp = (void*)data->stagingMemoryIndex;
  data->vertexInfo->MapIndexData(temp, 0, data->vertexInfo->GetMaxIndices());
  data->stagingMemoryIndex = (uint16*)temp;
  data->currentIndex = data->stagingMemoryIndex;
}

void Point3DRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  data->vertexInfo->UnmapVertexData(commandBuffer, 0, (int32)(data->currentVertex - data->stagingMemoryVertex));
  data->vertexInfo->UnmapIndexData(commandBuffer, 0, (int32)(data->currentIndex - data->stagingMemoryIndex));
  data->uniformViewProjBuffer->Update(&data->uboViewProj, 0, sizeof(data->uboViewProj));
}

void Point3DRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
  commandBuffer->Draw(data->vertexInfo, (int)(data->currentIndex - data->stagingMemoryIndex), 0);
  commandBuffer->EndDraw();
}
