

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in mat4 model;
layout(location = 4) in vec4 color;
layout(location = 5) in uint spriteIndex;

layout(location = 0) out mat4 outModel;
layout(location = 4) out vec4 outColor;
layout(location = 5) out vec4 outUvwh;


layout(binding = 0) uniform UniformBufferObject
{
  mat4 viewProj;
} ubo;

layout(binding = 1) buffer UniformBufferObjectBillboardAtlasInfo
{
	vec4 uvwh[256];
} uboBillboardAtlasInfo;

void main()
{
	outModel = ubo.viewProj * model;
	outUvwh = uboBillboardAtlasInfo.uvwh[spriteIndex];
	outColor = (color - 0.5f) * 2.0f;
}
