
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec4 color;

layout(binding = 2) uniform sampler samp;
layout(binding = 3) uniform texture2D tex;


void main()
{
    outColor = texture(sampler2D(tex, samp), uv) + color;
}