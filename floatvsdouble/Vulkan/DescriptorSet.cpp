
#include "stdafx.h"

#include "Gfx.h"
#include "Device.h"
#include "Buffer.h"
#include "Texture.h"
#include "DescriptorSet.h"


namespace gfx
{
	DescriptorSetLayoutHandle* DescriptorSetLayoutHandle::Create(DeviceHandle* device) { return new DescriptorSetLayout((Device*)device); }
	void DescriptorSetLayoutHandle::Destroy(DescriptorSetLayoutHandle* handle) { delete (DescriptorSetLayout*)handle; }

	DescriptorSetLayout::~DescriptorSetLayout()
	{
		vkDestroyDescriptorSetLayout(device->device, descriptorSetLayout, nullptr);
	}

	void DescriptorSetLayout::Add(VkDescriptorType type, int32 count, ShaderType shaders)
	{
		bindings.push_back(VkDescriptorSetLayoutBinding());
		VkDescriptorSetLayoutBinding& binding = bindings.back();
		binding.binding = (uint32_t)bindings.size() - 1;
		binding.descriptorType = type;
		binding.descriptorCount = count;
		binding.stageFlags = GetVkShaders(shaders);
		binding.pImmutableSamplers = nullptr; // Optional
	}

	void DescriptorSetLayout::AddUniformBuffers(int32 count, ShaderType shaders) { Add(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, count, shaders);     }
  void DescriptorSetLayout::AddStorageBuffers(int32 count, ShaderType shaders) { Add(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, count, shaders);     }
	void DescriptorSetLayout::AddTextures(int32 count, ShaderType shaders)       { Add(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, count, shaders);      }
	void DescriptorSetLayout::AddSamplers(int32 count, ShaderType shaders)       { Add(VK_DESCRIPTOR_TYPE_SAMPLER, count, shaders);            }
	void DescriptorSetLayout::AddCombinedTextureSampler(ShaderType shaders)      { Add(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, shaders); }

	void DescriptorSetLayout::Finalize()
	{
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		layoutInfo.pBindings = bindings.data();

		VK_ASSERT(vkCreateDescriptorSetLayout(device->device, &layoutInfo, nullptr, &descriptorSetLayout), "failed to create descriptor set layout!");
		printf("Created vk object: %llu (VkDescriptorSetLayout)\n", (uint64)descriptorSetLayout);
	}



	DescriptorSetHandle* DescriptorSetHandle::Create(DeviceHandle* device, DescriptorSetLayoutHandle* layout) { return new DescriptorSet((Device*)device, (DescriptorSetLayout*)layout); }
	void DescriptorSetHandle::Destroy(DescriptorSetHandle* handle) { delete (DescriptorSet*)handle; }

	DescriptorSet::DescriptorSet(Device* device, DescriptorSetLayout* layout) : device(device)
	{
		VkDescriptorSetLayout layouts[] = {layout->descriptorSetLayout};
		VkDescriptorSetAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		allocInfo.descriptorPool = device->descriptorPool;
		allocInfo.descriptorSetCount = 1;
		allocInfo.pSetLayouts = layouts;

		VK_ASSERT(vkAllocateDescriptorSets(device->device, &allocInfo, &descriptorSet), "failed to allocate descriptor set!");
	}

	DescriptorSet::~DescriptorSet()
	{
		//vkFreeDescriptorSets(device->device, device->descriptorPool, 1, &descriptorSet);
	}

	void DescriptorSet::UpdateUniformBuffer(int binding, BufferHandle* buffer, int size)
	{
		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = ((Buffer*)buffer)->buffer;
		bufferInfo.offset = 0;
		bufferInfo.range = size;


		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		writer.descriptorCount = 1;
		writer.pBufferInfo = &bufferInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}

	void DescriptorSet::UpdateStorageBuffer(int binding, BufferHandle* buffer, int size)
	{
		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = ((Buffer*)buffer)->buffer;
		bufferInfo.offset = 0;
		bufferInfo.range = size;


		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		writer.descriptorCount = 1;
		writer.pBufferInfo = &bufferInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}

	void DescriptorSet::UpdateTexture(int binding, TextureHandle* texture)
	{
		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = ((Texture*)texture)->view;

		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		writer.descriptorCount = 1;
		writer.pImageInfo = &imageInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}

	void DescriptorSet::UpdateTextures(int binding, std::vector<TextureHandle*> textures)
	{
		VkDescriptorImageInfo* imageInfo = (VkDescriptorImageInfo*)alloca(sizeof(VkDescriptorImageInfo) * textures.size());
		for(size_t i = 0; i < textures.size(); i++)
		{
			imageInfo[i].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo[i].imageView = ((Texture*)textures[i])->view;
		}

		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		writer.descriptorCount = (uint32)textures.size();
		writer.pImageInfo = imageInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}

	void DescriptorSet::UpdateSampler(int binding, SamplerHandle* sampler)
	{
		VkDescriptorImageInfo samplerInfo = {};
		samplerInfo.sampler = ((Sampler*)sampler)->sampler;

		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
		writer.descriptorCount = 1;
		writer.pImageInfo = &samplerInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}

	void DescriptorSet::UpdateCombinedTextureSampler(int binding, TextureHandle* texture, SamplerHandle* sampler)
	{
		VkDescriptorImageInfo samplerInfo = {};
		samplerInfo.sampler = ((Sampler*)sampler)->sampler;
		samplerInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		samplerInfo.imageView = ((Texture*)texture)->view;

		VkWriteDescriptorSet writer = {};
		writer.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		writer.dstSet = descriptorSet;
		writer.dstBinding = binding;
		writer.dstArrayElement = 0;
		writer.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		writer.descriptorCount = 1;
		writer.pImageInfo = &samplerInfo;

		vkUpdateDescriptorSets(device->device, 1, &writer, 0, nullptr);
	}
}
