#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"

namespace gfx
{
	class Device;


	class DescriptorSetLayout final : public DescriptorSetLayoutHandle
	{
	public:
		DescriptorSetLayout(Device* device) : device(device) {}
		~DescriptorSetLayout();

		void AddUniformBuffers(int32 count, ShaderType shaders);
		void AddStorageBuffers(int32 count, ShaderType shaders);
		void AddTextures(int32 count, ShaderType shaders);
		void AddSamplers(int32 count, ShaderType shaders);
		void AddCombinedTextureSampler(ShaderType shaders);
		void Finalize();

	private:
		friend class Device;
		friend class DescriptorSet;
		friend class Pipeline;

		void Add(VkDescriptorType type, int32 count, ShaderType shaders);

		Device* device;
		std::vector<VkDescriptorSetLayoutBinding> bindings;
		VkDescriptorSetLayout descriptorSetLayout;
	};

	class DescriptorSet final : public DescriptorSetHandle
	{
	public:
		DescriptorSet(Device* device, DescriptorSetLayout* layout);
		~DescriptorSet();

		void UpdateUniformBuffer(int binding, BufferHandle* buffer, int size);
		void UpdateStorageBuffer(int binding, BufferHandle* buffer, int size);
		void UpdateTexture(int binding, TextureHandle* texture);
		void UpdateTextures(int binding, std::vector<TextureHandle*> textures);
		void UpdateSampler(int binding, SamplerHandle* sampler);
		void UpdateCombinedTextureSampler(int binding, TextureHandle* texture, SamplerHandle* sampler);

	private:
		friend class Device;
		friend class CommandBuffer;

		Device* device;
		VkDescriptorSet descriptorSet;
	};
}
