#pragma once

#include "Gfx.h"
#include "..\utility\Vec4.h"

struct ObjRenderData;
struct Mat4x4;

struct ObjRenderInfo
{
  std::string obj;
	float x, y, z;
	Quat q;
};

class ObjRenderer : public gfx::RendererHandle
{
public:
	ObjRenderer(gfx::DeviceHandle* device, std::string objDirectory);
	~ObjRenderer();

	void UpdateViewProj(const Mat4x4& viewProj);
	void RenderOne(ObjRenderInfo renderInfo);

private:
	friend class gfx::Gfx;
	void BeginFrame();
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  ObjRenderData* data;
};
