

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(binding = 1) uniform sampler samp;
layout(binding = 2) uniform texture2D tex;


void main()
{
    outColor = texture(sampler2D(tex, samp), fragTexCoord);
}

