#pragma once

#include "Gfx.h"
#include "..\utility\Vec4.h"
struct Mat4x4;

struct BillboardRenderData;

struct BillboardRenderInfo
{
  float x, y, z;
  float w, h;
  Quat q;
  ColorOffset colorOffset;
  int32 BillboardIndex;
};

class BillboardRenderer : public gfx::RendererHandle
{
public:
  BillboardRenderer(gfx::DeviceHandle* device, std::string atlasName);
  ~BillboardRenderer();

  int32 GetBillboardIndex(std::string name);
  void GetBillboardDims(int32 index, float& width, float& height);
  void UpdateViewProj(const Mat4x4& viewProj);
  void RenderOne(BillboardRenderInfo renderInfo);

private:
  friend class gfx::Gfx;
  void BeginFrame();
  void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
  void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  BillboardRenderData* data;
};
