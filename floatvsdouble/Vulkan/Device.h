#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"
#include "ktxvulkan.h"

struct GLFWwindow;

namespace wnd
{
	class Window;
}

namespace gfx
{
	class Buffer;
	class RenderPass;
	class CommandBuffer;
	class Semaphore;
	class Fence;

	class Device final : public DeviceHandle
	{
	public:
		Device(wnd::Window* window);
		~Device();

		RenderPassHandle* getFinalRenderPass();
		bool BeginFrame(); //true if we need to re-create stuff...
		void Submit(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr, FenceHandle* fence = nullptr);
		void SubmitOutOfFrame(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr);
		bool EndFrame();
		void WaitIdle();
		int32 GetWidth() { return screenWidth; }
		int32 GetHeight() { return screenHeight; }

	private:


		friend class VertexInfo;
		friend class TextureBase;
		friend class Texture;
		friend class DepthBuffer;
		friend class Shader;
		friend class RenderPass;
		friend class Pipeline;
		friend class CommandPool;
		friend class CommandBuffer;
		friend class DescriptorSetLayout;
		friend class DescriptorSet;
		friend class Sampler;
		friend class Buffer;
		friend class Semaphore;
		friend class Fence;

		void InitQueueFamilies();
		void CreateSwapChain(VkSwapchainKHR oldSwapChain);
		void CreateImageViews();
		VkImageView CreateImageView(VkImage image, VkFormat format);
		void CreateFrameBuffers();
		void CreateSemaphores();
		void CreateDescriptorPool();
		void CreateCommandPool();
		void CreateKtxDevice();

		void HandleOutOfDateKHR();

		GLFWwindow* window;
		int32 screenWidth;
		int32 screenHeight;

		VkInstance instance;
		VkDebugReportCallbackEXT callback;
		VkPhysicalDevice physicalDevice;
		VkDevice device;
		ktxVulkanDeviceInfo ktxDevice;

		VkSurfaceKHR surface;

		VkDescriptorPool descriptorPool;
		VkCommandPool commandPool;

		VkSwapchainKHR swapChain;
		std::vector<VkImage> swapChainImages;
		VkFormat swapChainImageFormat;
		VkExtent2D swapChainExtent;
		std::vector<VkImageView> swapChainImageViews;
		std::vector<VkFramebuffer> swapChainFramebuffers;
		uint32_t currentSwapChainImageIndex;
		DepthBufferHandle* depthBuffer;

		RenderPass* finalRenderPass;

		std::vector<VkCommandBuffer> buffersToSubmit;

		VkSemaphore imageAvailableSemaphore;
		VkSemaphore renderFinishedSemaphore;

		int graphicsQueueFamily;
		VkQueue graphicsQueue;
	};
}
