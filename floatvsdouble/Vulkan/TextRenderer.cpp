
#include "stdafx.h"

#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "Synchronization.h"
#include "File.h"
#include "Debug.h"
#include "TextRenderer.h"
#include "FontInfos.h"
#include "..\common\iacaMarks.h"

#include <xmmintrin.h>
#include <algorithm>
#include <map>



struct CharInfo
{
	float xoffset;
	float yoffset;
	float w, h;
	float xadvance;
};

struct TextRendererHidden
{
	TextRendererHidden(gfx::DeviceHandle* device, std::string name);
	~TextRendererHidden();
	gfx::DeviceHandle* device;
	gfx::TextureHandle* texture;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformBuffer;
	gfx::BufferHandle* storageBufferSpriteAtlasInfo;
	gfx::SamplerHandle* sampler;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* geom;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	struct ScissorInfo
	{
		int32 x, y, w, h;
	};
	struct DrawInfo
	{
		ScissorInfo scissor;
		int32 lastVertex;
	};
	struct RenderInfo
	{
		float x, y;
		float w, h;
		unsigned char r, g, b, a;
		uint32 spriteIndex;
	};

	struct TextRendererSpriteAtlasInfo
	{
		float u, v;
		float uw, vh;
	};

	int32 charInfoMin;
	int32 charInfoMax;
	RenderInfo* stagingMemory;
	RenderInfo* currentVertex;

	std::vector<CharInfo> charInfos;

	TextRendererSpriteAtlasInfo uboSpriteAtlasInfo[256];

	std::vector<ScissorInfo> scissors;
	std::vector<DrawInfo> draws;
  FontInfo fontInfo;
};

struct TextRendererUniformBufferObject
{
	float windowWidth;
	float windowHeight;
} ubo;

TextRendererHidden::TextRendererHidden(gfx::DeviceHandle* device, std::string name) : device(device)
{
	// Texture
	texture = gfx::TextureHandle::Create(device, "..\\vulkan\\Fonts\\" + name + ".png", false);
  if(fonts.find(name) == fonts.end())
    __debugbreak(); // TODO : get a real assertion system going.
  fontInfo = fonts[name];

	// Sprite info
	int maxId = 0;
	int minId = 9999999;
	for(int i = 0; i < fontInfo.chars.size(); i++)
	{
		maxId = std::max(fontInfo.chars[i].id, maxId);
		minId = std::min(fontInfo.chars[i].id, minId);
	}
	charInfoMin = minId;
	charInfoMax = maxId;

	// Uniforms
	uniformBuffer = gfx::BufferHandle::Create(device, sizeof(TextRendererUniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	ubo.windowWidth = (float)device->GetWidth();
	ubo.windowHeight = (float)device->GetHeight();
	uniformBuffer->Update(&ubo, 0, sizeof(ubo));

	storageBufferSpriteAtlasInfo = gfx::BufferHandle::Create(device, sizeof(uboSpriteAtlasInfo), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	int width = texture->GetWidth();
	int height = texture->GetHeight();
	charInfos.resize(charInfoMax - charInfoMin + 1);
	for(int i = 0; i < fontInfo.chars.size(); i++)
	{
		FontInfo::CharInfo rInfo = fontInfo.chars[i];
		TextRendererSpriteAtlasInfo& spriteAtlasInfo = uboSpriteAtlasInfo[rInfo.id - charInfoMin];
		if(rInfo.id - charInfoMin >= 256)
			continue;
		spriteAtlasInfo.u = (float)rInfo.x / width;
		spriteAtlasInfo.v = (float)rInfo.y / height;
		spriteAtlasInfo.uw = (float)rInfo.width / width;
		spriteAtlasInfo.vh = (float)rInfo.height / height;

		CharInfo& cInfo = charInfos[rInfo.id - charInfoMin];
		cInfo.xoffset = (float)rInfo.xoffset;
		cInfo.yoffset = (float)rInfo.yoffset + (float)fontInfo.common.lineBase;
		cInfo.w = (float)rInfo.width;
		cInfo.h = (float)rInfo.height;
		cInfo.xadvance = (float)rInfo.xadvance;

	}
	storageBufferSpriteAtlasInfo->Update(&uboSpriteAtlasInfo, 0, sizeof(uboSpriteAtlasInfo));


	sampler = gfx::SamplerHandle::Create(device, gfx::Filter::Linear, gfx::AddressMode::ClampToEdge);

	descriptorSetLayout = gfx::DescriptorSetLayout::Create(device);
	descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
	descriptorSetLayout->AddStorageBuffers(1, gfx::ShaderType::Vertex);
	descriptorSetLayout->AddSamplers(1, gfx::ShaderType::Fragment);
	descriptorSetLayout->AddTextures(1, gfx::ShaderType::Fragment);
	descriptorSetLayout->Finalize();

	descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
	descriptorSet->UpdateUniformBuffer(0, uniformBuffer, sizeof(TextRendererUniformBufferObject));
	descriptorSet->UpdateStorageBuffer(1, storageBufferSpriteAtlasInfo, sizeof(uboSpriteAtlasInfo));
	descriptorSet->UpdateSampler(2, sampler);
	descriptorSet->UpdateTexture(3, texture);

	vertexInfo = gfx::VertexInfoHandle::Create(device);
	vertexInfo->AddVar(gfx::Format::R32G32_SFLOAT);
	vertexInfo->AddVar(gfx::Format::R32G32_SFLOAT);
	vertexInfo->AddVar(gfx::Format::R8G8B8A8_UNORM);
	vertexInfo->AddVar(gfx::Format::R32_UINT);
	vertexInfo->Finalize();
	vertexInfo->CreateVertexData(500000);

	vert = gfx::ShaderHandle::Create(device, "Text.vert");
	geom = gfx::ShaderHandle::Create(device, "Text.geom");
	frag = gfx::ShaderHandle::Create(device, "Text.frag");

	pipeline = gfx::PipelineHandle::Create(device);
	pipeline->AddShader(vert);
	pipeline->AddShader(geom);
	pipeline->AddShader(frag);

	pipeline->SetTopology(gfx::Topology::PointList);
	pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

	pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
	pipeline->SetDescriptorSetLayout(descriptorSetLayout);
	pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);

	device->OnScreenResize.Add([=](int32 width, int32 height)
	{
		ubo.windowWidth = (float)device->GetWidth();
		ubo.windowHeight = (float)device->GetHeight();
		uniformBuffer->Update(&ubo, 0, sizeof(ubo));
		descriptorSet->UpdateUniformBuffer(0, uniformBuffer, sizeof(TextRendererUniformBufferObject));
	});
}

TextRendererHidden::~TextRendererHidden()
{
	gfx::TextureHandle::Destroy(texture);
	gfx::DescriptorSetLayoutHandle::Destroy(descriptorSetLayout);
	gfx::DescriptorSetHandle::Destroy(descriptorSet);
	gfx::BufferHandle::Destroy(uniformBuffer);
	gfx::BufferHandle::Destroy(storageBufferSpriteAtlasInfo);
	gfx::SamplerHandle::Destroy(sampler);
	gfx::VertexInfoHandle::Destroy(vertexInfo);
	gfx::ShaderHandle::Destroy(vert);
	gfx::ShaderHandle::Destroy(geom);
	gfx::ShaderHandle::Destroy(frag);
	gfx::PipelineHandle::Destroy(pipeline);
}

TextRendererHidden* CreateTextRenderer(gfx::DeviceHandle* device, std::string name)
{
	return new TextRendererHidden(device, name);
}

void FreeTextRenderer(TextRendererHidden* textRenderer)
{
	delete textRenderer;
}


void RenderText(TextRendererHidden* textRenderer, const char* text, float size, float x, float y, int color)
{
	char prevChar = 0;
	const char* pc = &text[0];

	__m128 advance = _mm_set_ps(0.0f, 0.0f, 0.0f, 0.0f);
	__m128 xy00 = _mm_set_ps(0.0f, 0.0f, y, x);
	float ratio = size / textRenderer->fontInfo.info.size;
	__m128 rrrr = _mm_set_ps(ratio, ratio, ratio, ratio);

	TextRendererHidden::RenderInfo* destVertex = textRenderer->currentVertex;
	int32 range = textRenderer->charInfoMax - textRenderer->charInfoMin;
	std::vector<CharInfo>& charInfos = textRenderer->charInfos;

	while(*pc)
	{
		//IACA_VC64_START
		TextRendererHidden::RenderInfo* info = destVertex++;
		
		unsigned char c = (uint8)(*pc++ - textRenderer->charInfoMin);
		if(c > range)
			continue;

		*(int*)&info->r = color;

		CharInfo& cInfo = charInfos[c];
		__m128 temp = xy00;
		__m128 mm_cInfo = _mm_loadu_ps(&cInfo.xoffset);
		mm_cInfo = _mm_mul_ps(mm_cInfo, rrrr);
		temp = _mm_add_ps(temp, mm_cInfo);
		temp = _mm_add_ss(temp, advance);
		_mm_storeu_ps(&info->x, temp);

		info->spriteIndex = (uint16)(c);

		advance = _mm_add_ss(advance, _mm_mul_ss(rrrr, *(__m128*)&cInfo.xadvance));
	}
	//IACA_VC64_END
	textRenderer->currentVertex = destVertex;
}

void RenderTextBeginFrame(TextRendererHidden* textRenderer)
{
	StackEvents ev("RenderTextBeginFrame");
	void* temp = (void*)textRenderer->stagingMemory;
	textRenderer->vertexInfo->MapVertexData(temp, 0, textRenderer->vertexInfo->GetMaxVertices());
	textRenderer->stagingMemory = (TextRendererHidden::RenderInfo*)temp;
	textRenderer->currentVertex = textRenderer->stagingMemory;

	textRenderer->scissors.push_back({0, 0, textRenderer->device->GetWidth(), textRenderer->device->GetHeight()});
}

void RenderTextPushScissor(TextRendererHidden* textRenderer, int32 x, int32 y, int32 width, int32 height)
{
	textRenderer->draws.push_back({textRenderer->scissors.back(), (int32)(textRenderer->currentVertex - textRenderer->stagingMemory)});
	textRenderer->scissors.push_back({x, y, width, height});
}

void RenderTextPopScissor(TextRendererHidden* textRenderer)
{
	textRenderer->draws.push_back({textRenderer->scissors.back(), (int32)(textRenderer->currentVertex - textRenderer->stagingMemory)});
	textRenderer->scissors.pop_back();
}

float RenderTextGetWidth(TextRendererHidden* textRenderer, const char* text, float size)
{
	float result = 0.0f;
	while(*text)
	{
		result += textRenderer->charInfos[*text++ - textRenderer->charInfoMin].xadvance;
	}
	return result * size / textRenderer->fontInfo.info.size;
}

void RenderTextUpdateBuffers(TextRendererHidden* textRenderer, gfx::CommandBufferHandle* commandBuffer)
{
	StackEvents ev("RenderTextUpdateBuffers");

	RenderTextPopScissor(textRenderer);

	textRenderer->vertexInfo->UnmapVertexData(commandBuffer, 0, (int)(textRenderer->currentVertex - textRenderer->stagingMemory));
}

void RenderTextSubmitDraws(TextRendererHidden* textRenderer, gfx::CommandBufferHandle* commandBuffer)
{
	StackEvents ev("RenderTextSubmitDraws");

	if(textRenderer->currentVertex == textRenderer->stagingMemory)
		return;

	commandBuffer->BeginDraw(textRenderer->pipeline, textRenderer->descriptorSet);
	int32 prevVertexOffset = 0;
	for(int32 i = 0; i < textRenderer->draws.size(); i++)
	{
		TextRendererHidden::ScissorInfo& scissor = textRenderer->draws[i].scissor;
		commandBuffer->SetScissorRect(scissor.x, scissor.y, scissor.w, scissor.h);
		if(textRenderer->draws[i].lastVertex != prevVertexOffset)
			commandBuffer->Draw(textRenderer->vertexInfo, textRenderer->draws[i].lastVertex - prevVertexOffset, prevVertexOffset);
		prevVertexOffset = textRenderer->draws[i].lastVertex;
	}
	commandBuffer->EndDraw();
	textRenderer->draws.clear();
}



TextRenderer::TextRenderer(gfx::DeviceHandle * device, std::string name)
{
	textRenderer = CreateTextRenderer(device, name);
}

TextRenderer::~TextRenderer()
{
	FreeTextRenderer(textRenderer);
}

void TextRenderer::RenderString(const char* text, float size, float x, float y, int color)
{
	RenderText(textRenderer, text, size, x, y, color);
}

void TextRenderer::BeginFrame()
{
	RenderTextBeginFrame(textRenderer);
}

void TextRenderer::PushScissor(int32 x, int32 y, int32 width, int32 height)
{
	RenderTextPushScissor(textRenderer, x, y, width, height);
}

void TextRenderer::PopScissor()
{
	RenderTextPopScissor(textRenderer);
}

float TextRenderer::GetWidth(const char* text, float size)
{
	return RenderTextGetWidth(textRenderer, text, size);
}

void TextRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
	RenderTextUpdateBuffers(textRenderer, commandBuffer);
}

void TextRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
	RenderTextSubmitDraws(textRenderer, commandBuffer);
}

