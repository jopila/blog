
foreach ($d in get-ChildItem -Directory .\*)
{
	$command = '.\sspack.exe /pad:1 /pow2 '
    $command = $command + '/image:' + $d.name + '.png /map:' + $d.name + '.txt '
	foreach ($f in get-ChildItem -File $d\*)
	{
		$command = $command + $d.name + '\' + $f.name + ' '
	}
    echo $command
    iex $command
}

$newGuiInfoText = ''

foreach($line in [System.IO.File]::ReadLines("$d\..\GUI.txt"))
{
    echo $line
    if($line.StartsWith("border_"))
    {
        $line = $line.Substring("border_".Length)
        $newGuiInfoText += $line + "`r`n"
        $name,$nope,$x,$y,$w,$h = $line -split ' '
        #echo $name
        #echo $x
        #echo $y
        #echo $w
        #echo $h
    
        $newGuiInfoText += $name + '1 = ' + ([int]$x + 0*$w/3) + ' ' + ([int]$y + 0*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '2 = ' + ([int]$x + 1*$w/3) + ' ' + ([int]$y + 0*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '3 = ' + ([int]$x + 2*$w/3) + ' ' + ([int]$y + 0*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '4 = ' + ([int]$x + 0*$w/3) + ' ' + ([int]$y + 1*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '5 = ' + ([int]$x + 1*$w/3) + ' ' + ([int]$y + 1*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '6 = ' + ([int]$x + 2*$w/3) + ' ' + ([int]$y + 1*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '7 = ' + ([int]$x + 0*$w/3) + ' ' + ([int]$y + 2*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '8 = ' + ([int]$x + 1*$w/3) + ' ' + ([int]$y + 2*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
        $newGuiInfoText += $name + '9 = ' + ([int]$x + 2*$w/3) + ' ' + ([int]$y + 2*$h/3) + ' ' + $w/3 + ' ' + $h/3 + "`r`n"
    }
    else
    {
        $newGuiInfoText += $line + "`r`n"
    }
}

#echo $newGuiInfoText

$newGuiInfoText | Out-File 'GUI.txt' -Encoding Ascii