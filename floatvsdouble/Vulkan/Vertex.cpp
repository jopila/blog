
#include "stdafx.h"

#include "Vertex.h"
#include "Gfx.h"
#include "Buffer.h"
#include "Device.h"
#include "Synchronization.h"

namespace gfx
{
	VertexInfo::VertexInfo(Device* device)
	{
		this->device = device;

		indexBuffer = VK_NULL_HANDLE;
		instanceBuffers = nullptr;
		vertexBuffer = VK_NULL_HANDLE;
    numInstanceBuffers = 0;

		maxVertices = 0;
		maxInstances = 0;
		maxIndices = 0;
	}

	VertexInfo::~VertexInfo()
	{
		if(maxVertices)  {delete vertexBuffer;   delete vertexStagingBuffer;}
    if(maxIndices)   {delete indexBuffer;    delete indexStagingBuffer;}
		if(maxInstances)
    {
      for(int i = 0; i < numInstanceBuffers; i++)
      {
        delete instanceBuffers[i];
        delete instanceStagingBuffers[i];
      }
      delete[] instanceBuffers;
      delete[] instanceStagingBuffers;
    }
	}

	void VertexInfo::AddVar(const Format format)
	{
		VkVertexInputAttributeDescription newAttributeDescription;
		newAttributeDescription.binding = 0;
		newAttributeDescription.location = currentLocation;
		newAttributeDescription.format = GetVkFormat(format);
		newAttributeDescription.offset = currentSize;
		currentSize += GetVkFormatSize(format);
		currentLocation++;
		attributeDescriptions.push_back(newAttributeDescription);
	}

	void VertexInfo::AddInstancedVar(const Format format)
	{
		VkVertexInputAttributeDescription newAttributeDescription;
		newAttributeDescription.binding = 1;
		newAttributeDescription.location = currentLocation;
		newAttributeDescription.format = GetVkFormat(format);
		newAttributeDescription.offset = instanceCurrentSize;
		instanceCurrentSize += GetVkFormatSize(format);
		currentLocation++;
		attributeDescriptions.push_back(newAttributeDescription);
	}

	void VertexInfo::Finalize()
	{
		memset(&bindingDescription, 0, sizeof(bindingDescription));
		bindingDescription[0].binding = 0;
		bindingDescription[0].stride = currentSize;
		bindingDescription[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		bindingDescription[1].binding = 1;
		bindingDescription[1].stride = instanceCurrentSize;
		bindingDescription[1].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;

		memset(&vertexInputInfo, 0, sizeof(vertexInputInfo));
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexBindingDescriptionCount = instanceCurrentSize > 0 ? 2 : 1;
		vertexInputInfo.pVertexBindingDescriptions = bindingDescription;
		vertexInputInfo.vertexAttributeDescriptionCount = currentLocation;
		vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
	}

	void VertexInfo::CreateVertexData(int maxVertices)
	{
		this->maxVertices = maxVertices;
		vertexBuffer = new Buffer(device, currentSize * maxVertices, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		vertexStagingBuffer = new Buffer(device, currentSize * maxVertices, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	}

	void VertexInfo::CreateInstanceData(int maxInstances, int numBuffers)
	{
		this->maxInstances = maxInstances;
    this->numInstanceBuffers = numBuffers;

    instanceBuffers = new Buffer*[numBuffers];
    for(int i = 0; i < numBuffers; i++)
      instanceBuffers[i] = new Buffer(device, instanceCurrentSize * maxInstances, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    instanceStagingBuffers = new Buffer*[numBuffers];
    for(int i = 0; i < numBuffers; i++)
		  instanceStagingBuffers[i] = new Buffer(device, instanceCurrentSize * maxInstances, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	}

	void VertexInfo::CreateIndexData(int maxIndices)
	{
		this->maxIndices = maxIndices;
		indexBuffer = new Buffer(device, sizeof(uint16_t) * maxIndices, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		indexStagingBuffer = new Buffer(device, currentSize * maxIndices, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	}

	void VertexInfo::UpdateVertexData(CommandBufferHandle* commandBuffer, void * data, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("VertexInfo::UpdateVertexData");
		const int dataSize = currentSize * numVertices;
		vertexStagingBuffer->Update(data, 0, dataSize);
		vertexStagingBuffer->Copy(commandBuffer, vertexStagingBuffer, 0, vertexBuffer, currentSize * vertexOffset, dataSize, beginSemaphore, endSemaphore);
	}

	void VertexInfo::MapVertexData(void*& data, int vertexOffset, int numVertices)
	{
		StackEvents ev("VertexInfo::MapVertexData");
		vertexStagingBuffer->Map(data, currentSize * vertexOffset, currentSize * numVertices);
	}

	void VertexInfo::UnmapVertexData(CommandBufferHandle* commandBuffer, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("VertexInfo::UnmapVertexData");
		vertexStagingBuffer->Unmap();
		vertexStagingBuffer->Copy(commandBuffer, vertexStagingBuffer, 0, vertexBuffer, currentSize * vertexOffset, currentSize * numVertices, beginSemaphore, endSemaphore);
	}

	void VertexInfo::MapInstanceData(void*& data, int instanceOffset, int numInstances, int index)
	{
		StackEvents ev("VertexInfo::MapInstanceData");
		instanceStagingBuffers[index]->Map(data, instanceCurrentSize * instanceOffset, instanceCurrentSize * numInstances);
	}

	void VertexInfo::UnmapInstanceData(CommandBufferHandle* commandBuffer, int instanceOffset, int numInstances, int index, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("VertexInfo::UnmapInstanceData");
		instanceStagingBuffers[index]->Unmap();
		instanceStagingBuffers[index]->Copy(commandBuffer, instanceStagingBuffers[index], 0, instanceBuffers[index], instanceCurrentSize * instanceOffset, instanceCurrentSize * numInstances, beginSemaphore, endSemaphore);
	}

	void VertexInfo::UpdateIndexData(void * data, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		const int dataSize = sizeof(uint16_t) * numIndices;
		Buffer stagingBuffer(device, dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		indexStagingBuffer->Update(data, 0, dataSize);
		indexStagingBuffer->Copy(nullptr, indexStagingBuffer, 0, indexBuffer, currentSize * indexOffset, dataSize, beginSemaphore, endSemaphore);
	}

	void VertexInfo::MapIndexData(void *& data, int indexOffset, int numIndices)
	{
		StackEvents ev("VertexInfo::MapVertexData");
		indexStagingBuffer->Map(data, currentSize * indexOffset, currentSize * numIndices);
	}

	void VertexInfo::UnmapIndexData(CommandBufferHandle* commandBuffer, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("VertexInfo::UnmapVertexData");
		indexStagingBuffer->Unmap();
		indexStagingBuffer->Copy(commandBuffer, indexStagingBuffer, 0, indexBuffer, currentSize * indexOffset, currentSize * numIndices, beginSemaphore, endSemaphore);
	}

	void VertexInfo::CreateAndUpdateVertexData(CommandBufferHandle* commandBuffer, int numVertices, void* data)
	{
		this->maxVertices = numVertices;
		CreateAndSetBuffer(commandBuffer, data, currentSize * numVertices, vertexBuffer, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
		vertexStagingBuffer = nullptr;
	}

	void VertexInfo::CreateAndUpdateIndexData(CommandBufferHandle* commandBuffer, int numIndices, void* data)
	{
		this->maxIndices = numIndices;
		CreateAndSetBuffer(commandBuffer, data, sizeof(uint16_t) * numIndices, indexBuffer, (VkBufferUsageFlagBits)(VK_BUFFER_USAGE_INDEX_BUFFER_BIT));
		indexStagingBuffer = nullptr;
	}

	void gfx::VertexInfo::CreateAndSetBuffer(CommandBufferHandle* commandBuffer, void * data, int32 dataSize, Buffer*& buffer, VkBufferUsageFlagBits usage)
	{
		Buffer stagingBuffer(device, dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		stagingBuffer.Update(data, 0, dataSize);

		commandBuffer->Begin();
		buffer = new Buffer(device, dataSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		buffer->Copy(commandBuffer, &stagingBuffer, 0, buffer, 0, dataSize, nullptr, nullptr);
		commandBuffer->End();
		device->SubmitOutOfFrame(commandBuffer);
		device->WaitIdle();
	}

	VertexInfoHandle* VertexInfoHandle::Create(DeviceHandle* device)
	{
		return new VertexInfo((Device*)device);
	}

	void VertexInfoHandle::Destroy(VertexInfoHandle* handle)
	{
		delete (VertexInfo*)handle;
	}
}
