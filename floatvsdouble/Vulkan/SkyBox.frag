

#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_TEXTURE_CUBE_MAP_SEAMLESS : enable

layout(location = 0) in vec3 UVW;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform samplerCube samp;


void main()
{
    vec3 color = texture(samp, UVW).rgb;
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));
    outColor = vec4(color, 1.0);
}

