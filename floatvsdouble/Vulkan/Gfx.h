#pragma once

#include <functional>

#include "..\utility\Delegate.h"
#include "Types.h"
class SpriteRenderer;
class TextRenderer;

namespace wnd
{
	class Input;
	class WindowHandle
	{
	public:
		static WindowHandle* Create(int32 width, int32 height, std::string name);
		static void Destroy(WindowHandle* handle);

		virtual bool ShouldCloseWindow() = 0;
		virtual void PollEvents() = 0;
		virtual const Input& GetInput() = 0;

		virtual void SetCursorDisabled(bool disabled) = 0;

		virtual int32 GetWidth() = 0;
		virtual int32 GetHeight() = 0;
	};
}

namespace gfx
{
	class BufferHandle;
	class CommandBufferHandle;
	class DescriptorSetLayoutHandle;
	class DescriptorSetHandle;
	class DeviceHandle;
	class PipelineHandle;
	class RenderPassHandle;
	class ShaderHandle;
	class SemaphoreHandle;
	class FenceHandle;
	class TextureHandle;
	class SamplerHandle;
	class VertexInfoHandle;

	class BufferHandle
	{
	public:
		static BufferHandle* Create(DeviceHandle* device, int32 size, uint32 usage, uint32 properties);
		static void Destroy(BufferHandle* handle);

		virtual void Update(void* data, int32 offset, int32 size) = 0;
		virtual void Map(void*& mappedData, int32 offset, int32 size) = 0;
		virtual void Unmap() = 0;
		virtual void Copy(CommandBufferHandle* commandBuffer, BufferHandle* srcBuffer, int srcOffset, BufferHandle* dstBuffer, int dstOffset, uint64 size, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore) = 0;

	protected:
		BufferHandle() {}
		~BufferHandle() {}
	};

	class CommandBufferHandle
	{
	public:
		static CommandBufferHandle* Create(DeviceHandle* device);
		static void Destroy(CommandBufferHandle* handle);

		virtual void BeginRenderPass(const RenderPassHandle* renderPass) = 0;
		virtual void BeginDraw(const PipelineHandle* pipeline, const DescriptorSetHandle* descriptorSet) = 0;
		virtual void Draw(const VertexInfoHandle* vertexInfo, int32 numVertices, int32 offsetVertex = 0) = 0;
		virtual void DrawInstanced(const VertexInfoHandle* vertexInfo, int32 numIndices, int32 numInstances, int32 firstIndex = 0, int32 firstVertex = 0, int32 instanceBufferIndex = 0) = 0;
		virtual void SetScissorRect(int32 x, int32 y, int32 w, int32 h) = 0;
		virtual void Clear(FloatColor4 clearColor) = 0;
		virtual void EndDraw() = 0;
		virtual void EndRenderPass() = 0;
		virtual void Begin() = 0;
		virtual void End() = 0;

	protected:
		CommandBufferHandle() {}
		~CommandBufferHandle() {}
	};

	class DescriptorSetLayoutHandle
	{
	public:
		static DescriptorSetLayoutHandle* Create(DeviceHandle* device);
		static void Destroy(DescriptorSetLayoutHandle* handle);

		virtual void AddUniformBuffers(int count, ShaderType shaders) = 0;
		virtual void AddStorageBuffers(int count, ShaderType shaders) = 0;
		virtual void AddTextures(int count, ShaderType shaders) = 0;
		virtual void AddSamplers(int count, ShaderType shaders) = 0;
		virtual void AddCombinedTextureSampler(ShaderType shaders) = 0;
		virtual void Finalize() = 0;

	protected:
		DescriptorSetLayoutHandle() {}
		~DescriptorSetLayoutHandle() {}
	};

	class DescriptorSetHandle
	{
	public:
		static DescriptorSetHandle* Create(DeviceHandle* device, DescriptorSetLayoutHandle* layout);
		static void Destroy(DescriptorSetHandle* handle);

		virtual void UpdateUniformBuffer(int binding, BufferHandle* buffer, int size) = 0;
		virtual void UpdateStorageBuffer(int binding, BufferHandle* buffer, int size) = 0;
		virtual void UpdateTexture(int binding, TextureHandle* texture) = 0;
		virtual void UpdateTextures(int binding, std::vector<TextureHandle*> textures) = 0;
		virtual void UpdateSampler(int binding, SamplerHandle* sampler) = 0;
		virtual void UpdateCombinedTextureSampler(int binding, TextureHandle* texture, SamplerHandle* sampler) = 0;

	protected:
		DescriptorSetHandle() {}
		~DescriptorSetHandle() {}
	};

	class DeviceHandle
	{
	public:
		static DeviceHandle* Create(wnd::WindowHandle* window);
		static void Destroy(DeviceHandle* handle);

		virtual RenderPassHandle* getFinalRenderPass() = 0;
		virtual bool BeginFrame() = 0;
		virtual void Submit(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr, FenceHandle* fence = nullptr) = 0;
		virtual void SubmitOutOfFrame(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual bool EndFrame() = 0;
		virtual void WaitIdle() = 0;
		virtual int32 GetWidth() = 0;
		virtual int32 GetHeight() = 0;
		uti::Delegate<int32, int32> OnScreenResize;

	protected:
		DeviceHandle() {}
		~DeviceHandle() {}
	};

	class PipelineHandle
	{
	public:
		static PipelineHandle* Create(DeviceHandle* device);
		static void Destroy(PipelineHandle* handle);

		virtual void AddShader(const ShaderHandle* shader) = 0;
		virtual void SetTopology(Topology topology) = 0;
		virtual void SetViewport(float x, float y, float width, float height, float minZ, float maxZ) = 0;
		virtual void SetScissor(int x, int y, uint32 width, uint32 height) = 0;
		virtual void SetDescriptorSetLayout(DescriptorSetLayoutHandle* descriptorSetLayout) = 0;
		virtual void Finalize(RenderPassHandle* renderPass, VertexInfoHandle* vertexInfo) = 0;

	protected:
		PipelineHandle() {}
		~PipelineHandle() {}
	};

	class RenderPassHandle
	{
	public:
		static RenderPassHandle* Create(DeviceHandle* device, Format renderTargetFormat);
		static void Destroy(RenderPassHandle* handle);

	protected:
		RenderPassHandle() {}
		~RenderPassHandle() {}
	};

	class ShaderHandle
	{
	public:
		static ShaderHandle* Create(DeviceHandle* device, std::string name);
		static void Destroy(ShaderHandle* handle);

	protected:
		ShaderHandle() {}
		~ShaderHandle() {}
	};

	class SemaphoreHandle
	{
	public:
		static SemaphoreHandle* Create(DeviceHandle* device);
		static void Destroy(SemaphoreHandle* handle);

	protected:
		SemaphoreHandle() {}
		~SemaphoreHandle() {}
	};

	class FenceHandle
	{
	public:
		static FenceHandle* Create(DeviceHandle* device, bool startSignaled);
		static void Destroy(FenceHandle* handle);

		virtual bool IsSignaled() = 0;
		virtual void WaitSignaled() = 0;
		virtual void Reset() = 0;

	protected:
		FenceHandle() {}
		~FenceHandle() {}
	};

	class TextureHandle
	{
	public:
		static TextureHandle* Create(DeviceHandle* device, std::string filename, bool generateMips);
		static TextureHandle* CreateCubeMap(DeviceHandle* device, std::string filename);
    static TextureHandle* CreateFromData(DeviceHandle* device, Format format, uint32 width, uint32 height, void* data);
		static void Destroy(TextureHandle* handle);

		virtual int GetWidth() = 0;
		virtual int GetHeight() = 0;

	protected:
		TextureHandle() {}
		~TextureHandle() {}
	};

	class DepthBufferHandle
	{
	public:
		static DepthBufferHandle* Create(DeviceHandle* device);
		static void Destroy(DepthBufferHandle* handle);

	protected:
		DepthBufferHandle() {}
		~DepthBufferHandle() {}
	};

	class SamplerHandle
	{
	public:
		static SamplerHandle* Create(DeviceHandle* device, Filter filter, AddressMode mode);
		static void Destroy(SamplerHandle* handle);

	protected:
		SamplerHandle() {}
		~SamplerHandle() {}
	};

	class VertexInfoHandle
	{
	public:
		static VertexInfoHandle* Create(DeviceHandle* device);
		static void Destroy(VertexInfoHandle* handle);

		virtual void AddVar(const Format format) = 0;
		virtual void AddInstancedVar(const Format format) = 0;
		virtual void Finalize() = 0;
		virtual void CreateVertexData(int maxVertices) = 0;
		virtual void CreateInstanceData(int maxInstances, int numBuffers = 1) = 0;
		virtual void CreateIndexData(int maxIndices) = 0;
		virtual void UpdateVertexData(CommandBufferHandle* commandBuffer, void* data, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual void MapVertexData(void*& data, int vertexOffset, int numVertices) = 0;
		virtual void UnmapVertexData(CommandBufferHandle* commandBuffer, int vertexOffset, int numVertices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual void MapInstanceData(void*& data, int instanceOffset, int numInstances, int index = 0) = 0;
		virtual void UnmapInstanceData(CommandBufferHandle* commandBuffer, int instanceOffset, int numInstances, int index = 0, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual void UpdateIndexData(void* data, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual void MapIndexData(void*& data, int indexOffset, int numIndices) = 0;
		virtual void UnmapIndexData(CommandBufferHandle* commandBuffer, int indexOffset, int numIndices, SemaphoreHandle* beginSemaphore = nullptr, SemaphoreHandle* endSemaphore = nullptr) = 0;
		virtual void CreateAndUpdateVertexData(CommandBufferHandle* commandBuffer, int numVertices, void* data) = 0;
		virtual void CreateAndUpdateIndexData(CommandBufferHandle* commandBuffer, int numIndices, void* data) = 0;

		virtual int GetMaxVertices() = 0;
		virtual int GetMaxInstances() = 0;
		virtual int GetMaxIndices() = 0;

	protected:
		VertexInfoHandle() {}
		~VertexInfoHandle() {}
	};

	class RendererHandle
	{
	public:

	private:
		friend class Gfx;
		virtual void BeginFrame() = 0;
		virtual void UpdateBuffers(CommandBufferHandle* commandBuffer) = 0;
		virtual void SubmitDraws(CommandBufferHandle* commandBuffer) = 0;
	};

	class Gfx
	{
	public:
		Gfx(wnd::WindowHandle* window);
		~Gfx();

		void BeginFrame();
		void EndFrame();

		void RegisterRenderer(RendererHandle* renderer);
		void UnregisterRenderer(RendererHandle* renderer);

		TextRenderer* GetTextRenderer() { return textRenderer; }

		std::function<void(int32 width, int32 height)> OnScreenResize;
		int32 GetScreenWidth() { device->GetWidth(); }
		int32 GetScreenHeight() { device->GetHeight(); }

		DeviceHandle* GetDevice() { return device; }

	private:
		void CreateDescriptorSetLayout();
		void CreateDescriptorSet();
		void CreateGraphicsPipeline();

		DeviceHandle* device;

		VertexInfoHandle* vertexInfo;
		BufferHandle* uniformBuffer;

		TextureHandle* texture;
		SamplerHandle* sampler;

		CommandBufferHandle* basePassCommandBuffer;
		FenceHandle* basePassFence;

		ShaderHandle* vertShader;
		ShaderHandle* fragShader;

		TextRenderer* textRenderer;
		std::vector<RendererHandle*> renderers;

		DescriptorSetLayoutHandle* descriptorSetLayout;
		DescriptorSetHandle* descriptorSet;

		PipelineHandle* pipeline;
	};

} // namespace gfx


struct ColorOffset
{
	ColorOffset() {}
	ColorOffset(uint32 rawValue) { colorOffset = rawValue; }
	ColorOffset(float r, float g, float b, float a)
	{
		int32 ir = Min(255, int32((r * 0.5f + 0.5f) * 256));
		int32 ig = Min(255, int32((g * 0.5f + 0.5f) * 256));
		int32 ib = Min(255, int32((b * 0.5f + 0.5f) * 256));
		int32 ia = Min(255, int32((a * 0.5f + 0.5f) * 256));
		colorOffset = (ia << 24) | ((ib & 0xff) << 16) | ((ig & 0xff) << 8) | (ir & 0xff);
	}
	uint32 colorOffset;
  static ColorOffset Neutral;
  static ColorOffset Black;
};
