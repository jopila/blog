#pragma once

#include "Gfx.h"
#include "Input.h"

struct GLFWwindow;
struct GLFWcursor;

namespace gfx
{
	class Device;
}

namespace wnd
{
	class Window final : public WindowHandle
	{
	public:
		Window(int32 width, int32 height, std::string name);
		~Window();

		bool ShouldCloseWindow();

		void PollEvents();
		const Input& GetInput() { return input; }

		int32 GetWidth();
		int32 GetHeight();

		enum class CursorType
		{
			Normal,
			ResizeHorizontal,
			ResizeVertical
		};
		static void SetCursor(CursorType type);
		void SetCursorDisabled(bool disabled);

		Input input;

	private:
		friend class gfx::Device;
		static GLFWwindow* window;

		static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void CharCallback(GLFWwindow* window, unsigned int unicode);
		static void MousePosCallback(GLFWwindow* window, double xpos, double ypos);
		static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void MouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset);

		static GLFWcursor* arrowCursor;
		static GLFWcursor* resizeHorzontalCursor;
		static GLFWcursor* resizeVerticalCursor;
	};
} // namespace wnd
