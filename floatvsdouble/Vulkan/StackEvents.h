#pragma once

class StackEvents
{
public:
	StackEvents(const char* name) { Push(name); }
	~StackEvents() { Pop(); }

	static void Push(const char* name);
	static void Pop();
	static void Flush();
};

void CreateStackEventLogFile(std::string threadName);
void CloseStackEventLogFile();
