#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"
struct ktxTexture;
namespace ddspp
{
	struct Descriptor;
}

namespace gfx
{
	class Device;

	class TextureBase
	{
	public:
	protected:
		TextureBase(Device* device) : device(device) {}
		~TextureBase();
		void CreateImage(uint32 width, uint32 height, uint32 mipLevels, uint32 layers, VkFormat format, VkImageTiling tiling, VkImageViewType viewType, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImageAspectFlags aspect);
		void TransitionImageLayout(VkImage image, VkFormat format, VkImageAspectFlags aspect, VkImageLayout oldLayout, VkImageLayout newLayout, uint32 mipLevels, uint32 layers);
		void GenerateMipmaps(VkImage image, VkFormat imageFormat, int32_t texWidth, int32_t texHeight, uint32_t mipLevels);

		Device* device;

		VkImage image;
		VkDeviceMemory memory;
		VkImageView view;
	};

	class Texture final : public TextureBase, public TextureHandle
	{
	public:
		Texture(Device* device, std::string filename, bool generateMips);
		Texture(Device* device, std::string filename);
    Texture(Device* device, Format format, uint32 width, uint32 height, void* data);
		int GetWidth() { return width; }
		int GetHeight() { return height; }

	private:
		friend class Device;
		friend class DescriptorSet;

		void CopyBufferToImage(VkBuffer buffer, VkImage image, uint32 width, uint32 height);
		void CopyDdsToImage(VkBuffer buffer, std::vector<int8>& bytes, ddspp::Descriptor* desc, VkImage image, uint32 width, uint32 height, uint32 mipLevels);
		void CopyKtxToImage(VkBuffer buffer, ktxTexture* kTexture, VkImage image, uint32 width, uint32 height, uint32 mipLevels);
		void CopyKtxToCubeMap(VkBuffer buffer, ktxTexture* kTexture, VkImage image, int32 width, int32 height, int32 mipLevels);

		int width;
		int height;

	};

	class Sampler final : public SamplerHandle
	{
	public:
		Sampler(Device* device, VkFilter filter, VkSamplerAddressMode mode);
		~Sampler();

	private:
		friend class Device;
		friend class DescriptorSet;

		Device* device;
		VkSampler sampler;
	};

}
