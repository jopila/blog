
#version 450

layout(points) in;
layout(location = 0) in vec2 size[];
layout(location = 1) in vec4 color[];
layout(location = 2) in vec4 uvwh[];


layout(triangle_strip, max_vertices=4) out;
layout(location = 0) out vec2 outUv;
layout(location = 1) out vec4 outColor;

void main()
{
	vec4 pos = gl_in[0].gl_Position;
	vec2 uv = uvwh[0].xy;
	gl_PointSize = 1.0f;

	outUv = uv;
	gl_Position = pos;
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x + uvwh[0].z, uv.y);
	gl_Position = vec4(pos.x + size[0].x, pos.y, pos.z, pos.w);
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x, uv.y + uvwh[0].w);
	gl_Position = vec4(pos.x, pos.y + size[0].y, pos.z, pos.w);
	outColor = color[0];
	EmitVertex();

	outUv = vec2(uv.x + uvwh[0].z, uv.y + uvwh[0].w);
	gl_Position = vec4(pos.x + size[0].x, pos.y + size[0].y, pos.z, pos.w);
	outColor = color[0];
	EmitVertex();

  EndPrimitive();
} 

