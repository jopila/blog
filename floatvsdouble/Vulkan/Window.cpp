
#include "stdafx.h"

#define GLM_FORCE_RADIANS
#include "GLFW\glfw3.h"
#define GLFW_EXPOSE_NATIVE_WIN32
#include "GLFW\glfw3native.h"

#include "Window.h"

namespace wnd
{

	GLFWwindow* Window::window;
	GLFWcursor* Window::arrowCursor;
	GLFWcursor* Window::resizeHorzontalCursor;
	GLFWcursor* Window::resizeVerticalCursor;

	WindowHandle* WindowHandle::Create(int32 width, int32 height, std::string name) { return new Window(width, height, name); }
	void WindowHandle::Destroy(WindowHandle* handle) { delete (Window*)handle; }

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Window::Window(int32 width, int32 height, std::string name)
	{
		glfwInit();

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		window = glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr);

		glfwSetWindowUserPointer(window, this);
		glfwSetKeyCallback(window, KeyCallback);
		glfwSetCharCallback(window, CharCallback);
		glfwSetCursorPosCallback(window, MousePosCallback);
		glfwSetMouseButtonCallback(window, MouseButtonCallback);
		glfwSetScrollCallback(window, MouseScrollCallback);

		arrowCursor = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		resizeHorzontalCursor = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
		resizeVerticalCursor = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		input.mouseX = (int32)xpos;
		input.mouseY = (int32)ypos;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Window::~Window()
	{
		glfwDestroyWindow(window);

		glfwTerminate();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	bool Window::ShouldCloseWindow()
	{
		return glfwWindowShouldClose(window) != 0;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Window::PollEvents()
	{
		input.FrameUpdate();
		glfwPollEvents();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int32 Window::GetWidth()
	{
		int32 w, h;
		glfwGetWindowSize(window, &w, &h);
		return w;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int32 Window::GetHeight()
	{
		int32 w, h;
		glfwGetWindowSize(window, &w, &h);
		return h;
	}

	void Window::SetCursor(CursorType type)
	{
		switch(type)
		{
			case CursorType::Normal: glfwSetCursor(window, arrowCursor); break;
			case CursorType::ResizeHorizontal: glfwSetCursor(window, resizeHorzontalCursor); break;
			case CursorType::ResizeVertical: glfwSetCursor(window, resizeVerticalCursor); break;
		}
	}

	void Window::SetCursorDisabled(bool disabled)
	{
		glfwSetInputMode(window, GLFW_CURSOR, disabled ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}

	Keyboard FromGlfwKey(int key);
	Mouse FromGlfwMouse(int button);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Window::KeyCallback(GLFWwindow* glfwWindow, int key, int scancode, int action, int mods)
	{
		Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);

		window->input.keyStates[(int32)FromGlfwKey(key)] = action != GLFW_RELEASE;

		//if(action != GLFW_RELEASE)
		//	printf("Key pressed! %i\n", FromGlfwKey(key));
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Window::CharCallback(GLFWwindow* glfwWindow, unsigned int unicode)
	{
		Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);

		if(unicode >= 32 && unicode <= 126)
			window->input.charInput.push_back((char)unicode);

		//printf("Character pressed %i", unicode);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Window::MousePosCallback(GLFWwindow* glfwWindow, double xpos, double ypos)
	{
		Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);

		//printf("Mouse Raw! %f %f\n", xpos, ypos);
		window->input.mouseOffsetX += (int32)xpos - window->input.mouseX;
		window->input.mouseOffsetY += (int32)ypos - window->input.mouseY;
		window->input.mouseX = (int32)xpos;
		window->input.mouseY = (int32)ypos;
		//printf("Mouse Offset! %i %i\n", window->input.mouseOffsetX, window->input.mouseOffsetY);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Window::MouseButtonCallback(GLFWwindow* glfwWindow, int button, int action, int mods)
	{
		Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);

		window->input.mouseStates[(int32)FromGlfwMouse(button)] = action != GLFW_RELEASE;

		//if(action != GLFW_RELEASE)
		//	printf("Mouse button pressed! %i\n", FromGlfwMouse(button));
	}

	void Window::MouseScrollCallback(GLFWwindow * glfwWindow, double xoffset, double yoffset)
	{
		Window* window = (Window*)glfwGetWindowUserPointer(glfwWindow);
		window->input.scrollX = (int32)xoffset;
		window->input.scrollY = (int32)yoffset;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Mouse FromGlfwMouse(int button)
	{
		switch(button)
		{
			case GLFW_MOUSE_BUTTON_LEFT   : return Mouse::Left;
			case GLFW_MOUSE_BUTTON_RIGHT  : return Mouse::Right;
			case GLFW_MOUSE_BUTTON_MIDDLE : return Mouse::Middle;
			case GLFW_MOUSE_BUTTON_4      : return Mouse::Extra1;
			case GLFW_MOUSE_BUTTON_5      : return Mouse::Extra2;
			case GLFW_MOUSE_BUTTON_6      : return Mouse::Extra3;
			case GLFW_MOUSE_BUTTON_7      : return Mouse::Extra4;
			case GLFW_MOUSE_BUTTON_8      : return Mouse::Extra5;
			default                       : return Mouse::Extra5;
		}
	}


	Keyboard FromGlfwKey(int key)
	{
		switch(key)
		{
			case GLFW_KEY_UNKNOWN       : return Keyboard::Unkown;
			case GLFW_KEY_SPACE         : return Keyboard::Space;
			case GLFW_KEY_APOSTROPHE    :	return Keyboard::Apostrophe;
			case GLFW_KEY_COMMA         :	return Keyboard::Comma;
			case GLFW_KEY_MINUS         :	return Keyboard::Minus;
			case GLFW_KEY_PERIOD        :	return Keyboard::Period;
			case GLFW_KEY_SLASH         :	return Keyboard::Slash;
			case GLFW_KEY_0             :	return Keyboard::Top0;
			case GLFW_KEY_1             :	return Keyboard::Top1;
			case GLFW_KEY_2             :	return Keyboard::Top2;
			case GLFW_KEY_3             :	return Keyboard::Top3;
			case GLFW_KEY_4             :	return Keyboard::Top4;
			case GLFW_KEY_5             :	return Keyboard::Top5;
			case GLFW_KEY_6             :	return Keyboard::Top6;
			case GLFW_KEY_7             :	return Keyboard::Top7;
			case GLFW_KEY_8             :	return Keyboard::Top8;
			case GLFW_KEY_9             :	return Keyboard::Top9;
			case GLFW_KEY_SEMICOLON     :	return Keyboard::Semicolon;
			case GLFW_KEY_EQUAL         :	return Keyboard::Equal;
			case GLFW_KEY_A             :	return Keyboard::A;
			case GLFW_KEY_B             :	return Keyboard::B;
			case GLFW_KEY_C             :	return Keyboard::C;
			case GLFW_KEY_D             :	return Keyboard::D;
			case GLFW_KEY_E             :	return Keyboard::E;
			case GLFW_KEY_F             :	return Keyboard::F;
			case GLFW_KEY_G             :	return Keyboard::G;
			case GLFW_KEY_H             :	return Keyboard::H;
			case GLFW_KEY_I             :	return Keyboard::I;
			case GLFW_KEY_J             :	return Keyboard::J;
			case GLFW_KEY_K             :	return Keyboard::K;
			case GLFW_KEY_L             :	return Keyboard::L;
			case GLFW_KEY_M             :	return Keyboard::M;
			case GLFW_KEY_N             :	return Keyboard::N;
			case GLFW_KEY_O             :	return Keyboard::O;
			case GLFW_KEY_P             :	return Keyboard::P;
			case GLFW_KEY_Q             :	return Keyboard::Q;
			case GLFW_KEY_R             :	return Keyboard::R;
			case GLFW_KEY_S             :	return Keyboard::S;
			case GLFW_KEY_T             :	return Keyboard::T;
			case GLFW_KEY_U             :	return Keyboard::U;
			case GLFW_KEY_V             :	return Keyboard::V;
			case GLFW_KEY_W             :	return Keyboard::W;
			case GLFW_KEY_X             :	return Keyboard::X;
			case GLFW_KEY_Y             :	return Keyboard::Y;
			case GLFW_KEY_Z             :	return Keyboard::Z;
			case GLFW_KEY_LEFT_BRACKET  :	return Keyboard::LBracket;
			case GLFW_KEY_BACKSLASH     :	return Keyboard::Backslash;
			case GLFW_KEY_RIGHT_BRACKET :	return Keyboard::RBracket;
			case GLFW_KEY_GRAVE_ACCENT  :	return Keyboard::Grave;
			case GLFW_KEY_WORLD_1       :	return Keyboard::World1;
			case GLFW_KEY_WORLD_2       :	return Keyboard::World2;
			case GLFW_KEY_ESCAPE        :	return Keyboard::Escape;
			case GLFW_KEY_ENTER         :	return Keyboard::Enter;
			case GLFW_KEY_TAB           :	return Keyboard::Tab;
			case GLFW_KEY_BACKSPACE     :	return Keyboard::Backspace;
			case GLFW_KEY_INSERT        :	return Keyboard::Insert;
			case GLFW_KEY_DELETE        :	return Keyboard::Delete;
			case GLFW_KEY_RIGHT         :	return Keyboard::Right;
			case GLFW_KEY_LEFT          :	return Keyboard::Left;
			case GLFW_KEY_DOWN          :	return Keyboard::Down;
			case GLFW_KEY_UP            :	return Keyboard::Up;
			case GLFW_KEY_PAGE_UP       :	return Keyboard::PageUp;
			case GLFW_KEY_PAGE_DOWN     :	return Keyboard::PageDown;
			case GLFW_KEY_HOME          :	return Keyboard::Home;
			case GLFW_KEY_END           :	return Keyboard::End;
			case GLFW_KEY_CAPS_LOCK     :	return Keyboard::CapsLock;
			case GLFW_KEY_SCROLL_LOCK   :	return Keyboard::ScrollLock;
			case GLFW_KEY_NUM_LOCK      :	return Keyboard::NumLock;
			case GLFW_KEY_PRINT_SCREEN  :	return Keyboard::PrintScreen;
			case GLFW_KEY_PAUSE         :	return Keyboard::Pause;
			case GLFW_KEY_F1            :	return Keyboard::F1;
			case GLFW_KEY_F2            :	return Keyboard::F2;
			case GLFW_KEY_F3            :	return Keyboard::F3;
			case GLFW_KEY_F4            :	return Keyboard::F4;
			case GLFW_KEY_F5            :	return Keyboard::F5;
			case GLFW_KEY_F6            :	return Keyboard::F6;
			case GLFW_KEY_F7            :	return Keyboard::F7;
			case GLFW_KEY_F8            :	return Keyboard::F8;
			case GLFW_KEY_F9            :	return Keyboard::F9;
			case GLFW_KEY_F10           :	return Keyboard::F10;
			case GLFW_KEY_F11           :	return Keyboard::F11;
			case GLFW_KEY_F12           :	return Keyboard::F12;
			case GLFW_KEY_F13           :	return Keyboard::F13;
			case GLFW_KEY_F14           :	return Keyboard::F14;
			case GLFW_KEY_F15           :	return Keyboard::F15;
			case GLFW_KEY_F16           :	return Keyboard::F16;
			case GLFW_KEY_F17           :	return Keyboard::F17;
			case GLFW_KEY_F18           :	return Keyboard::F18;
			case GLFW_KEY_F19           :	return Keyboard::F19;
			case GLFW_KEY_F20           :	return Keyboard::F20;
			case GLFW_KEY_F21           :	return Keyboard::F21;
			case GLFW_KEY_F22           :	return Keyboard::F22;
			case GLFW_KEY_F23           :	return Keyboard::F23;
			case GLFW_KEY_F24           :	return Keyboard::F24;
			case GLFW_KEY_F25           :	return Keyboard::F25;
			case GLFW_KEY_KP_0          :	return Keyboard::NumPad0;
			case GLFW_KEY_KP_1          :	return Keyboard::NumPad1;
			case GLFW_KEY_KP_2          :	return Keyboard::NumPad2;
			case GLFW_KEY_KP_3          :	return Keyboard::NumPad3;
			case GLFW_KEY_KP_4          :	return Keyboard::NumPad4;
			case GLFW_KEY_KP_5          :	return Keyboard::NumPad5;
			case GLFW_KEY_KP_6          :	return Keyboard::NumPad6;
			case GLFW_KEY_KP_7          :	return Keyboard::NumPad7;
			case GLFW_KEY_KP_8          :	return Keyboard::NumPad8;
			case GLFW_KEY_KP_9          :	return Keyboard::NumPad9;
			case GLFW_KEY_KP_DECIMAL    :	return Keyboard::NumPadPeriod;
			case GLFW_KEY_KP_DIVIDE     :	return Keyboard::NumPadDivide;
			case GLFW_KEY_KP_MULTIPLY   :	return Keyboard::NumPadMultiply;
			case GLFW_KEY_KP_SUBTRACT   :	return Keyboard::NumPadSubtract;
			case GLFW_KEY_KP_ADD        :	return Keyboard::NumPadAdd;
			case GLFW_KEY_KP_ENTER      :	return Keyboard::NumPadEnter;
			case GLFW_KEY_KP_EQUAL      :	return Keyboard::NumPadEqual;
			case GLFW_KEY_LEFT_SHIFT    :	return Keyboard::LShift;
			case GLFW_KEY_LEFT_CONTROL  :	return Keyboard::LCtrl;
			case GLFW_KEY_LEFT_ALT      :	return Keyboard::LAlt;
			case GLFW_KEY_LEFT_SUPER    :	return Keyboard::LSuper;
			case GLFW_KEY_RIGHT_SHIFT   :	return Keyboard::RShift;
			case GLFW_KEY_RIGHT_CONTROL :	return Keyboard::RCtrl;
			case GLFW_KEY_RIGHT_ALT     :	return Keyboard::RAlt;
			case GLFW_KEY_RIGHT_SUPER   :	return Keyboard::RSuper;
			case GLFW_KEY_MENU          :	return Keyboard::Menu;
			default                     : return Keyboard::Unkown;
		};
	}
}
