
#include "stdafx.h"

#include <stdexcept>

#include "DepthBuffer.h"
#include "Gfx.h"
#include "Device.h"

namespace gfx
{
	DepthBufferHandle* DepthBufferHandle::Create(DeviceHandle* device) { return new DepthBuffer((Device*)device); }
	void DepthBufferHandle::Destroy(DepthBufferHandle* handle) { delete (DepthBuffer*)handle; }

	DepthBuffer::DepthBuffer(Device* device) : TextureBase(device)
	{
		CreateImage(device->GetWidth(), device->GetHeight(), 1, 1, VK_FORMAT_D32_SFLOAT, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_VIEW_TYPE_2D, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);
		TransitionImageLayout(image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1, 1);
	}
}
