#pragma once

#include "Gfx.h"

struct Point3DRenderData;
struct Mat4x4;
struct Vec4;

struct Point3DRenderInfo
{
	float x, y, z;
	float r, g, b;
};

class Point3DRenderer : public gfx::RendererHandle
{
public:
	Point3DRenderer(gfx::DeviceHandle* device);
	~Point3DRenderer();

	void UpdateViewProj(const Mat4x4& viewProj);
	void RenderOne(Point3DRenderInfo renderInfo);

private:
	friend class gfx::Gfx;
	void BeginFrame();
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  Point3DRenderData* data;
};
