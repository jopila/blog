#pragma once

#define mWriteRaw(file, var) file.WriteRaw((uint8*)&(var), sizeof(var))

class FileWriter
{
public:
	FileWriter(std::string filename);
	~FileWriter();

	void WriteRaw(uint8* data, size_t size);

private:
	class PrivateFileWriter* pfw;
};

// ex filters: {{"JayPeg", "*.jpg;*.jpeg"}, {"Bitmaps", "*.bmp"}, {"All Files", "*.*"}}
std::string OpenFileDialog(std::vector<std::pair<std::string, std::string>> filters);
std::string SaveFileDialog(std::vector<std::pair<std::string, std::string>> filters);
std::string OpenFolderDialog();

std::vector<std::string> ListFilesInDirectory(std::string directory, bool recursive);
std::string GetFileNameFromPath(std::string filePath, bool withExtension = true);

std::vector<char> ReadFile(std::string filename);
std::vector<std::string> ReadFileLines(std::string filename);

// Helper functions that happen to be commonly used for files...but really needs a better home
bool EndsWith(std::string const & value, std::string const & ending);
void Split(const std::string &s, char delim, std::vector<std::string>& result);
std::vector<std::string> Split(const std::string &s, char delim);
