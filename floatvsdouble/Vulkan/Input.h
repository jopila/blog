#pragma once

namespace wnd
{
	enum class Mouse
	{
		Left,
		Right,
		Middle,
		Extra1,
		Extra2,
		Extra3,
		Extra4,
		Extra5,
		NUM,
	};

	enum class Keyboard
	{
		Unkown,
		Space,
		Apostrophe,
		Comma,
		Minus,
		Period,
		Slash,
		Top0,
		Top1,
		Top2,
		Top3,
		Top4,
		Top5,
		Top6,
		Top7,
		Top8,
		Top9,
		Semicolon,
		Equal,
		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,
		LBracket,
		Backslash,
		RBracket,
		Grave,
		World1,
		World2,
		Escape,
		Enter,
		Tab,
		Backspace,
		Insert,
		Delete,
		Right,
		Left,
		Down,
		Up,
		PageUp,
		PageDown,
		Home,
		End,
		CapsLock,
		ScrollLock,
		NumLock,
		PrintScreen,
		Pause,
		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,
		F13,
		F14,
		F15,
		F16,
		F17,
		F18,
		F19,
		F20,
		F21,
		F22,
		F23,
		F24,
		F25,
		NumPad0,
		NumPad1,
		NumPad2,
		NumPad3,
		NumPad4,
		NumPad5,
		NumPad6,
		NumPad7,
		NumPad8,
		NumPad9,
		NumPadPeriod,
		NumPadDivide,
		NumPadMultiply,
		NumPadSubtract,
		NumPadAdd,
		NumPadEnter,
		NumPadEqual,
		LShift,
		LCtrl,
		LAlt,
		LSuper,
		RShift,
		RCtrl,
		RAlt,
		RSuper,
		Menu,
		NUM,
	};

	class Input
	{
	public:
		Input();

		void FrameUpdate();
		std::vector<char> GetCharInput() const  { return charInput; }
		bool IsKeyPressed  (Keyboard key) const { return  keyStates[(int32)key]; }
		bool IsKeyTriggered(Keyboard key) const { return  keyStates[(int32)key] && !prevKeyStates[(int32)key]; }
		bool IsKeyReleased (Keyboard key) const { return !keyStates[(int32)key] &&  prevKeyStates[(int32)key]; }
		int32 GetMouseX() const { return mouseX; }
		int32 GetMouseY() const { return mouseY; }
		int32 GetMouseOffsetX() const { return mouseOffsetX; }
		int32 GetMouseOffsetY() const { return mouseOffsetY; }
		int32 GetMouseScrollX() const { return scrollX; }
		int32 GetMouseScrollY() const { return scrollY; }
		bool IsMouseButtonPressed  (Mouse key) const { return  mouseStates[(int32)key]; }
		bool IsMouseButtonTriggered(Mouse key) const { return  mouseStates[(int32)key] && !prevMouseStates[(int32)key]; }
		bool IsMouseButtonReleased (Mouse key) const { return !mouseStates[(int32)key] &&  prevMouseStates[(int32)key]; }

	private:
		friend class Window;
		bool keyStates[(int32)Keyboard::NUM];
		bool prevKeyStates[(int32)Keyboard::NUM];
		bool mouseStates[(int32)Mouse::NUM];
		bool prevMouseStates[(int32)Mouse::NUM];
		std::vector<char> charInput;
		int32 mouseX;
		int32 mouseY;
		int32 mouseOffsetX;
		int32 mouseOffsetY;
		int32 scrollX;
		int32 scrollY;
	};

} // namespace wnd