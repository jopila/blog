#pragma once

#include "Gfx.h"

struct CubeRenderData;
struct Mat4x4;
struct Vec4;

#pragma pack(push, 2)
struct CubeRenderInfo
{
	int16 textureSet;
	int16 neighborFlags;
  int32 aoFlags1;
  int32 aoFlags2;
	float x, y, z;
};
#pragma pack(pop)

class CubeRenderer : public gfx::RendererHandle
{
public:
	CubeRenderer(gfx::DeviceHandle* device, std::string textureDirectory);
	~CubeRenderer();

	int32 GetTextureIndex(std::string name);
	void UpdateViewProj(const Mat4x4& viewProj);
	void UpdateCameraPosition(const Vec4& cameraPosition);
	void UpdatePointLight(const Vec4& pos, const Vec4& color);
	void UpdateDirectionalLight(const Vec4& dir, const Vec4& color);
	void RenderOne(CubeRenderInfo renderInfo);

private:
	friend class gfx::Gfx;
	void BeginFrame();
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  CubeRenderData* data;
};
