

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 size;
layout(location = 2) in vec4 color;
layout(location = 3) in uint spriteIndex;

layout(location = 0) out vec2 outSize;
layout(location = 1) out vec4 outColor;
layout(location = 2) out vec4 outUvwh;

layout(binding = 0) uniform UniformBufferObject
{
  vec2 screenDim;
} ubo;


layout(binding = 1) buffer UniformBufferObjectSpriteAtlasInfo
{
	vec4 uvwh[256];
} uboSpriteAtlasInfo;

void main()
{
	gl_Position = vec4(2*pos / ubo.screenDim - 1, 0, 1);
	outSize = 2*size / ubo.screenDim;
	outUvwh = uboSpriteAtlasInfo.uvwh[spriteIndex];
	outColor = (color - 0.5f) * 2.0f;
}
