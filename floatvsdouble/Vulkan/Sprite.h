#pragma once

#include "Gfx.h"

struct SpriteRenderData;

struct SpriteRenderInfo
{
	float x, y;
	float w, h;
	ColorOffset colorOffset;
	int32 spriteIndex;
};

class SpriteRenderer : public gfx::RendererHandle
{
public:
	SpriteRenderer(gfx::DeviceHandle* device, std::string atlasName);
	~SpriteRenderer();

	int32 GetSpriteIndex(std::string name);
	void GetSpriteDims(int32 index, float& width, float& height);
	void RenderOne(SpriteRenderInfo renderInfo);
	void PushScissor(int32 x, int32 y, int32 width, int32 height);
	void PopScissor();
	
private:
	friend class gfx::Gfx;
	void BeginFrame();
	void UpdateBuffers(gfx::CommandBufferHandle* commandBuffer);
	void SubmitDraws(gfx::CommandBufferHandle* commandBuffer);
  SpriteRenderData* data;
};
