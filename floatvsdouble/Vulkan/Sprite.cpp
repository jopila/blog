
#include "stdafx.h"

#include "Sprite.h"
#include "Gfx.h"
#include "DescriptorSet.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "Synchronization.h"
#include "File.h"
#include "Debug.h"
#include <map>
#include <sstream>

ColorOffset ColorOffset::Neutral(0x80808080);
ColorOffset ColorOffset::Black(0x80000000);

struct SpriteRenderData
{
	gfx::DeviceHandle* device;
	gfx::TextureHandle* texture;
	gfx::DescriptorSetLayoutHandle* descriptorSetLayout;
	gfx::DescriptorSetHandle* descriptorSet;
	gfx::BufferHandle* uniformBuffer;
	gfx::BufferHandle* storageBufferSpriteAtlasInfo;
	gfx::SamplerHandle* sampler;
	gfx::VertexInfoHandle* vertexInfo;
	gfx::ShaderHandle* vert;
	gfx::ShaderHandle* geom;
	gfx::ShaderHandle* frag;
	gfx::PipelineHandle* pipeline;

	SpriteRenderInfo* stagingMemory;
	mutable SpriteRenderInfo* currentVertex;

	struct ScissorInfo
	{
		int32 x, y, w, h;
	};
	struct DrawInfo
	{
		ScissorInfo scissor;
		int32 lastVertex;
	};
	struct SpriteRendererSpriteAtlasInfo
	{
		float u, v;
		float uw, vh;
	};

	struct SpriteInfo
	{
		float w, h;
		float u, v, uw, vh;
	};
	std::vector<SpriteInfo> spriteInfos;
	std::map<std::string, uint32> spriteLookup;

	SpriteRendererSpriteAtlasInfo uboSpriteAtlasInfo[256];

	std::vector<ScissorInfo> scissors;
	std::vector<DrawInfo> draws;

	const int bufferSize = 400000;
};

struct UniformBufferObject
{
	float windowWidth;
	float windowHeight;
} ubo;

SpriteRenderer::SpriteRenderer(gfx::DeviceHandle * device, std::string atlasName)
{
  data = new SpriteRenderData();
  data->device = device;

	// Texture
	data->texture = gfx::TextureHandle::Create(device, atlasName + ".png", false);
	int width = data->texture->GetWidth();
	int height = data->texture->GetHeight();

	// Sprite info
	std::vector<std::string> lines = ReadFileLines(atlasName + ".txt");
	for(std::string line : lines)
	{
		std::istringstream iss(line);
		std::string spriteName;
		std::string dummy;
		int x, y, w, h;
		if (!(iss >> spriteName >> dummy >> x >> y >> w >> h)) { break; } // error

    data->spriteLookup[spriteName] = (uint8)data->spriteInfos.size();
    SpriteRenderData::SpriteInfo newInfo;
		newInfo.w = (float)w;
		newInfo.h = (float)h;
		newInfo.u = (float)x / width;
		newInfo.v = (float)y / height;
		newInfo.uw = (float)w / width;
		newInfo.vh = (float)h / height;
    data->spriteInfos.push_back(newInfo);
	}

  data->storageBufferSpriteAtlasInfo = gfx::BufferHandle::Create(device, sizeof(data->uboSpriteAtlasInfo), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	for(int i = 0; i < data->spriteInfos.size(); i++)
	{
    SpriteRenderData::SpriteInfo spriteInfo = data->spriteInfos[i];
		if(i >= 256)
			__debugbreak();
    SpriteRenderData::SpriteRendererSpriteAtlasInfo& spriteAtlasInfo = data->uboSpriteAtlasInfo[i];
		spriteAtlasInfo.u = spriteInfo.u;
		spriteAtlasInfo.v = spriteInfo.v;
		spriteAtlasInfo.uw = spriteInfo.uw;
		spriteAtlasInfo.vh = spriteInfo.vh;

	}
  data->storageBufferSpriteAtlasInfo->Update(&data->uboSpriteAtlasInfo, 0, sizeof(data->uboSpriteAtlasInfo));

	// Pipeline
  data->uniformBuffer = gfx::BufferHandle::Create(device, sizeof(UniformBufferObject), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
	ubo.windowWidth = (float)device->GetWidth();
	ubo.windowHeight = (float)device->GetHeight();
  data->uniformBuffer->Update(&ubo, 0, sizeof(ubo));
  data->sampler = gfx::SamplerHandle::Create(device, gfx::Filter::Nearest, gfx::AddressMode::ClampToEdge);

  auto& descriptorSetLayout = data->descriptorSetLayout;
	descriptorSetLayout = gfx::DescriptorSetLayoutHandle::Create(device);
	descriptorSetLayout->AddUniformBuffers(1, gfx::ShaderType::Vertex);
	descriptorSetLayout->AddStorageBuffers(1, gfx::ShaderType::Vertex);
	descriptorSetLayout->AddSamplers(1, gfx::ShaderType::Fragment);
	descriptorSetLayout->AddTextures(1, gfx::ShaderType::Fragment);
	descriptorSetLayout->Finalize();

  auto& descriptorSet = data->descriptorSet;
	descriptorSet = gfx::DescriptorSetHandle::Create(device, descriptorSetLayout);
	descriptorSet->UpdateUniformBuffer(0, data->uniformBuffer, sizeof(UniformBufferObject));
	descriptorSet->UpdateStorageBuffer(1, data->storageBufferSpriteAtlasInfo, sizeof(data->uboSpriteAtlasInfo));
	descriptorSet->UpdateSampler(2, data->sampler);
	descriptorSet->UpdateTexture(3, data->texture);

  auto& vertexInfo = data->vertexInfo;
	vertexInfo = gfx::VertexInfoHandle::Create(device);
	vertexInfo->AddVar(gfx::Format::R32G32_SFLOAT);
	vertexInfo->AddVar(gfx::Format::R32G32_SFLOAT);
	vertexInfo->AddVar(gfx::Format::R8G8B8A8_UNORM);
	vertexInfo->AddVar(gfx::Format::R32_UINT);
	vertexInfo->Finalize();
	vertexInfo->CreateVertexData(data->bufferSize);

	data->vert = gfx::ShaderHandle::Create(device, "Sprite.vert");
	data->geom = gfx::ShaderHandle::Create(device, "Sprite.geom");
	data->frag = gfx::ShaderHandle::Create(device, "Sprite.frag");

  auto& pipeline = data->pipeline;
  pipeline = gfx::PipelineHandle::Create(device);
	pipeline->AddShader(data->vert);
	pipeline->AddShader(data->geom);
	pipeline->AddShader(data->frag);

	pipeline->SetTopology(gfx::Topology::PointList);
	pipeline->SetViewport(0.0f, 0.0f, (float)device->GetWidth(), (float)device->GetHeight(), 0.0f, 1.0f);

	pipeline->SetScissor(0, 0, device->GetWidth(), device->GetHeight());
	pipeline->SetDescriptorSetLayout(descriptorSetLayout);
	pipeline->Finalize(device->getFinalRenderPass(), vertexInfo);

	device->OnScreenResize.Add([=](int32 width, int32 height)
	{
		ubo.windowWidth = (float)device->GetWidth();
		ubo.windowHeight = (float)device->GetHeight();
    data->uniformBuffer->Update(&ubo, 0, sizeof(ubo));
		descriptorSet->UpdateUniformBuffer(0, data->uniformBuffer, sizeof(UniformBufferObject));
	});
}

SpriteRenderer::~SpriteRenderer()
{
  gfx::TextureHandle::Destroy(data->texture);
  gfx::DescriptorSetLayoutHandle::Destroy(data->descriptorSetLayout);
  gfx::DescriptorSetHandle::Destroy(data->descriptorSet);
  gfx::BufferHandle::Destroy(data->uniformBuffer);
  gfx::BufferHandle::Destroy(data->storageBufferSpriteAtlasInfo);
  gfx::SamplerHandle::Destroy(data->sampler);
  gfx::VertexInfoHandle::Destroy(data->vertexInfo);
  gfx::ShaderHandle::Destroy(data->vert);
  gfx::ShaderHandle::Destroy(data->geom);
  gfx::ShaderHandle::Destroy(data->frag);
  gfx::PipelineHandle::Destroy(data->pipeline);
  delete data;
}

int32 SpriteRenderer::GetSpriteIndex(std::string name)
{
	return data->spriteLookup[name];
}

void SpriteRenderer::GetSpriteDims(int32 index, float& width, float& height)
{
	width = data->spriteInfos[index].w;
	height = data->spriteInfos[index].h;
}

void SpriteRenderer::RenderOne(SpriteRenderInfo renderInfo)
{
  if(data->currentVertex - data->stagingMemory < data->bufferSize)
    *data->currentVertex++ = renderInfo;
  else
    __debugbreak();
}

void SpriteRenderer::BeginFrame()
{
  StackEvents ev("RenderSpriteBeginFrame");
  void* temp = (void*)data->stagingMemory;
  data->vertexInfo->MapVertexData(temp, 0, data->vertexInfo->GetMaxVertices());
  data->stagingMemory = (SpriteRenderInfo*)temp;
  data->currentVertex = data->stagingMemory;

  data->scissors.push_back({0, 0, data->device->GetWidth(), data->device->GetHeight()});
}

void SpriteRenderer::PushScissor(int32 x, int32 y, int32 width, int32 height)
{
  data->draws.push_back({data->scissors.back(), (int32)(data->currentVertex - data->stagingMemory)});
  data->scissors.push_back({x, y, width, height});
}

void SpriteRenderer::PopScissor()
{
  data->draws.push_back({data->scissors.back(), (int32)(data->currentVertex - data->stagingMemory)});
  data->scissors.pop_back();
}

void SpriteRenderer::UpdateBuffers(gfx::CommandBufferHandle* commandBuffer)
{
  PopScissor();

  data->vertexInfo->UnmapVertexData(commandBuffer, 0, (int32)(data->currentVertex - data->stagingMemory));
}

void SpriteRenderer::SubmitDraws(gfx::CommandBufferHandle* commandBuffer)
{
  if(data->currentVertex == data->stagingMemory)
  {
    data->draws.clear();
    return;
  }

  commandBuffer->BeginDraw(data->pipeline, data->descriptorSet);
  int32 prevVertexOffset = 0;
  for(int32 i = 0; i < data->draws.size(); i++)
  {
    SpriteRenderData::ScissorInfo& scissor = data->draws[i].scissor;
    commandBuffer->SetScissorRect(scissor.x, scissor.y, scissor.w, scissor.h);
    if(data->draws[i].lastVertex != prevVertexOffset)
      commandBuffer->Draw(data->vertexInfo, data->draws[i].lastVertex - prevVertexOffset, prevVertexOffset);
    prevVertexOffset = data->draws[i].lastVertex;
  }
  commandBuffer->EndDraw();
  data->draws.clear();
}
