
#include "stdafx.h"

#include "Gfx.h"
#include "Device.h"
#include "Pipeline.h"
#include "Shader.h"
#include "RenderPass.h"
#include "DescriptorSet.h"
#include "Vertex.h"

namespace gfx
{
	PipelineHandle* PipelineHandle::Create(DeviceHandle* device) { return new Pipeline((Device*)device); }
	void PipelineHandle::Destroy(PipelineHandle* handle) { delete (Pipeline*)handle; }

	Pipeline::Pipeline(Device* device)
	{
		this->device = device;
	}

	Pipeline::~Pipeline()
	{
		vkDestroyPipelineLayout(device->device, pipelineLayout, nullptr);
		vkDestroyPipeline(device->device, pipeline, nullptr);
	}

	void Pipeline::AddShader(const ShaderHandle* shader)
	{
		VkPipelineShaderStageCreateInfo shaderStageInfo = {};
		shaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		switch(((Shader*)shader)->shaderType)
		{
			case gfx::ShaderType::Vertex: shaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;		 break;
			case gfx::ShaderType::Fragment: shaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT; break;
			case gfx::ShaderType::Geometry: shaderStageInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT; break;
		}
		shaderStageInfo.module = ((Shader*)shader)->shaderModule;
		shaderStageInfo.pName = "main";

		shaderStages.push_back(shaderStageInfo);
	}

	void Pipeline::SetTopology(Topology topology)
	{
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssembly.topology = GetVkTopology(topology);
		inputAssembly.primitiveRestartEnable = VK_FALSE;
	}

	void Pipeline::SetViewport(float x, float y, float width, float height, float minZ, float maxZ)
	{
		viewport.x = x;
		viewport.y = y;
		viewport.width = width;
		viewport.height = height;
		viewport.minDepth = minZ;
		viewport.maxDepth = maxZ;
	}

	void Pipeline::SetScissor(int x, int y, uint32 width, uint32 height)
	{
		scissor.offset = {0, 0};
		scissor.extent = {width, height};
	}

	void Pipeline::SetDescriptorSetLayout(DescriptorSetLayoutHandle* descriptorSetLayout)
	{
		this->descriptorSetLayout = ((DescriptorSetLayout*)descriptorSetLayout)->descriptorSetLayout;
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 1;
		pipelineLayoutInfo.pSetLayouts = &this->descriptorSetLayout;
		pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
		pipelineLayoutInfo.pPushConstantRanges = 0; // Optional
	}

	void Pipeline::Finalize(RenderPassHandle* renderPass, VertexInfoHandle* vertexInfo)
	{
		// Actually pipeline stuff
		VkPipelineViewportStateCreateInfo viewportState = {};
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rasterizer = {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_NONE;
		rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;
		rasterizer.depthBiasConstantFactor = 0.0f; // Optional
		rasterizer.depthBiasClamp = 0.0f; // Optional
		rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		multisampling.minSampleShading = 1.0f; // Optional
		multisampling.pSampleMask = nullptr; // Optional
		multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
		multisampling.alphaToOneEnable = VK_FALSE; // Optional

		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_TRUE;
		colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA; // Optional
		colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA; // Optional
		colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_MAX; // Optional

		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f; // Optional
		colorBlending.blendConstants[1] = 0.0f; // Optional
		colorBlending.blendConstants[2] = 0.0f; // Optional
		colorBlending.blendConstants[3] = 0.0f; // Optional

		VK_ASSERT(vkCreatePipelineLayout(device->device, &pipelineLayoutInfo, nullptr, &pipelineLayout), "failed to create pipeline layout!");
		printf("Created vk object: %llu (VkPipelineLayout)\n", (uint64)pipelineLayout);


		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = (uint32)shaderStages.size();
		pipelineInfo.pStages = shaderStages.data();
		
		VkPipelineDynamicStateCreateInfo pipelineDynamicInfo = {};
		pipelineDynamicInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		pipelineDynamicInfo.dynamicStateCount = 2;
		VkDynamicState dynamicStates[] = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
		pipelineDynamicInfo.pDynamicStates = dynamicStates;

		VkPipelineDepthStencilStateCreateInfo depthStencil{};
		depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthStencil.depthTestEnable = VK_TRUE;
		depthStencil.depthWriteEnable = VK_TRUE;
		depthStencil.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		depthStencil.depthBoundsTestEnable = VK_FALSE;
		depthStencil.minDepthBounds = 0.0f; // Optional
		depthStencil.maxDepthBounds = 1.0f; // Optional
		depthStencil.stencilTestEnable = VK_FALSE;
		depthStencil.front = {}; // Optional
		depthStencil.back = {}; // Optional

		pipelineInfo.pVertexInputState = &((VertexInfo*)vertexInfo)->vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = &depthStencil; // Optional
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.pDynamicState = &pipelineDynamicInfo;
		pipelineInfo.layout = pipelineLayout;
		pipelineInfo.renderPass = ((RenderPass*)renderPass)->renderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
		pipelineInfo.basePipelineIndex = -1; // Optional

		VK_ASSERT(vkCreateGraphicsPipelines(device->device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline), "failed to create pipeline!");
		printf("Created vk object: %llu (VkPipeline)\n", (uint64)pipeline);
	}
}