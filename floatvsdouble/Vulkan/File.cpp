
#include "stdafx.h"

#include <fstream>
#include <sstream>
#include <ShObjIdl_core.h>
#include <locale>
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
#include <codecvt>
#include <filesystem>

#include "File.h"

class PrivateFileWriter
{
	friend class FileWriter;
	std::ofstream stream;
};

FileWriter::FileWriter(std::string filename)
{
	pfw = new PrivateFileWriter();
	pfw->stream = std::ofstream(filename, std::ios::out | std::ios::binary);
}

FileWriter::~FileWriter()
{
	pfw->stream.close();
	delete pfw;
}

void FileWriter::WriteRaw(uint8* data, size_t size)
{
	if(pfw->stream.is_open())
		pfw->stream.write((int8*)data, size);
}

std::string FileDialog(std::vector<std::pair<std::string, std::string>> filters, const IID& rclsid, const IID& riid, bool foldersOnly)
{
	CoInitialize(NULL);
	wchar_t *pszFilePath(NULL);
	// Create dialog
	IFileDialog *fileDialog(NULL);    
	HRESULT result = CoCreateInstance(rclsid, NULL, CLSCTX_ALL, riid, reinterpret_cast<void**>(&fileDialog) );

	if (!SUCCEEDED(result))
		goto end;

	const wchar_t WILDCARD[] = L"*.*";

	if(foldersOnly)
		fileDialog->SetOptions(FOS_PICKFOLDERS);

	if(filters.size() != 0)
	{
		COMDLG_FILTERSPEC* specList = new COMDLG_FILTERSPEC[filters.size()];

		for(size_t i = 0; i < filters.size(); i++)
		{
			wchar_t* friendlyName = new wchar_t[filters[i].first.length() + 1];
			mbstowcs(friendlyName, filters[i].first.c_str(), filters[i].first.length() + 1);
			specList[i].pszName = friendlyName;
			wchar_t* filterSpec = new wchar_t[filters[i].second.length() + 1];
			mbstowcs(filterSpec, filters[i].second.c_str(), filters[i].second.length() + 1);
			specList[i].pszSpec = filterSpec;
		}

		fileDialog->SetFileTypes((uint32)filters.size(), specList);

		/* free speclist */
		for(size_t i = 0; i < filters.size(); ++i)
		{
			delete[] specList[i].pszName;
			delete[] specList[i].pszSpec;
		}
		delete[] specList;
	}

	// Show the dialog.
	result = fileDialog->Show(NULL);
	if(SUCCEEDED(result))
	{
		// Get the file name
		::IShellItem *shellItem(NULL);
		result = fileDialog->GetResult(&shellItem);
		if(!SUCCEEDED(result))
			goto end;

		result = shellItem->GetDisplayName(::SIGDN_FILESYSPATH, &pszFilePath);
		if(!SUCCEEDED(result))
		{
			shellItem->Release();
			goto end;
		}

		shellItem->Release();
	}

end:
	CoUninitialize();

	// Convert the wide character string to a normal string to return it
	std::wstring string_to_convert = pszFilePath ? pszFilePath : L"";
	using convert_type = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;
	std::string converted_str = converter.to_bytes( string_to_convert );
	return converted_str;
}

std::string OpenFileDialog(std::vector<std::pair<std::string, std::string>> filters)
{
	return FileDialog(filters, CLSID_FileOpenDialog, IID_IFileOpenDialog, false);
}

std::string OpenFolderDialog()
{
	return FileDialog(std::vector<std::pair<std::string, std::string>>(), CLSID_FileOpenDialog, IID_IFileOpenDialog, true);
}

std::vector<std::string> ListFilesInDirectory(std::string directory, bool recursive)
{
	std::vector<std::string> result;

	for(const std::filesystem::path& entry : std::filesystem::directory_iterator(directory))
	{
		if(entry.has_filename())
			result.push_back(entry.string());
		else if(recursive)
			ListFilesInDirectory(entry.string(), true);
	}

	return result;
}

std::string GetFileNameFromPath(std::string filePath, bool withExtension)
{
	// Get last dot position
	size_t dotPos = filePath.rfind('.');
	size_t sepPos = filePath.rfind('\\');
	if(sepPos == std::string::npos)
		sepPos = filePath.rfind('/');
	size_t filenameSize = (withExtension || dotPos == std::string::npos) ? std::string::npos : dotPos - sepPos - 1;
	if(sepPos != std::string::npos)
	{
		return filePath.substr(sepPos + 1, filenameSize);
	}
	return filePath.substr(0, filenameSize);
}

std::string SaveFileDialog(std::vector<std::pair<std::string, std::string>> filters)
{
	return FileDialog(filters, CLSID_FileSaveDialog, IID_IFileSaveDialog, false);
}

std::vector<char> ReadFile(std::string filename)
{
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open())
		throw std::runtime_error("failed to open file!");

	size_t fileSize = (size_t) file.tellg();
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();
	return buffer;
}

std::vector<std::string> ReadFileLines(std::string filename)
{
	std::vector<std::string> result;
	std::string line;
	std::ifstream infile(filename);
	while (std::getline(infile, line))
	{
		result.push_back(line);
	}
	// Fix an issue where last line can get chopped off if empty (https://stackoverflow.com/questions/2960596/getline-sets-failbit-and-skips-last-line)
	if(line == "")
		result.push_back(line);
	return result;
}

bool EndsWith(std::string const& value, std::string const& ending)
{
	if (ending.size() > value.size())
		return false;
	return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

void Split(const std::string& s, char delim, std::vector<std::string>& result)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim))
	{
		result.push_back(item);
	}
}

std::vector<std::string> Split(const std::string& s, char delim)
{
	std::vector<std::string> elems;
	Split(s, delim, elems);
	return elems;
}
