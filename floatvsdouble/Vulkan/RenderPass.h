#pragma once

#include "Gfx.h"
#include "VulkanInclude.h"

namespace gfx
{
	class Device;

	class RenderPass final : public RenderPassHandle
	{
	public:
		RenderPass(Device* device, VkFormat renderTargetFormat);
		~RenderPass();
	private:
		friend class Device;
		friend class Pipeline;
		friend class CommandBuffer;

		Device* device;
		VkRenderPass renderPass;
	};
}