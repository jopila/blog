
#version 450
#extension GL_ARB_separate_shader_objects : enable

#include "pbr.glsl"

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 uv;
layout(location = 1) in vec4 pos;
layout(location = 2) flat in vec3 normal;
layout(location = 3) flat in vec3 tangent;
layout(location = 4) flat in uint textureSet;
layout(location = 5) flat in uint aoIndex;

layout(binding = 1) uniform UniformBufferObject
{
  vec4 camPos;
  vec4 pointLightPos;
  vec4 pointLightColor;
  vec4 directionalLightDir;
  vec4 directionalLightColor;
} ubo;

struct TextureSet
{
  uint top;
  uint side;
  uint bot;
};

layout(binding = 2) buffer UniformBufferObjectTextureSetInfo
{
	TextureSet sets[256];
} uboTextureSetInfo;

layout(binding = 3) uniform sampler samp;
layout(binding = 4) uniform texture2D tex[256];
layout(binding = 5) uniform texture2D ao[16];
layout(binding = 6) uniform samplerCube skySamp;

uint GetDiffuseTextureIndex(uint ts)
{
  if((ts & 3) == 0) return (uboTextureSetInfo.sets[ts >> 2].top >> 0) & 0xff;
  if((ts & 3) == 1) return (uboTextureSetInfo.sets[ts >> 2].side >> 0) & 0xff;
  if((ts & 3) == 2) return (uboTextureSetInfo.sets[ts >> 2].bot >> 0) & 0xff;
}

uint GetNormalTextureIndex(uint ts)
{
  if((ts & 3) == 0) return (uboTextureSetInfo.sets[ts >> 2].top >> 8) & 0xff;
  if((ts & 3) == 1) return (uboTextureSetInfo.sets[ts >> 2].side >> 8) & 0xff;
  if((ts & 3) == 2) return (uboTextureSetInfo.sets[ts >> 2].bot >> 8) & 0xff;
}

uint GetORMTextureIndex(uint ts)
{
  if((ts & 3) == 0) return uboTextureSetInfo.sets[ts >> 2].top >> 16 & 0xff;
  if((ts & 3) == 1) return uboTextureSetInfo.sets[ts >> 2].side >> 16 & 0xff;
  if((ts & 3) == 2) return uboTextureSetInfo.sets[ts >> 2].bot >> 16 & 0xff;
}


void main()
{
  vec3 bitangent = normalize(cross(normal, tangent));
  mat3 TBN = mat3(tangent, bitangent, normal);
  vec3 N = normalize(texture(sampler2D(tex[GetNormalTextureIndex(textureSet)], samp), uv).xyz);
  N = N * 2.0f - 1.0f;
  N = normalize(TBN * N);
  
  vec3 albedo = pow(texture(sampler2D(tex[GetDiffuseTextureIndex(textureSet)], samp), uv).xyz, vec3(2.2));
  float metallic = texture(sampler2D(tex[GetORMTextureIndex(textureSet)], samp), uv).b;
  float roughness = texture(sampler2D(tex[GetORMTextureIndex(textureSet)], samp), uv).g;

  vec3 V = normalize(ubo.camPos.xyz - pos.xyz);
  vec3 F0 = vec3(0.04);
  F0      = mix(F0, albedo, metallic);

  // Lighting
  vec3 Lo = vec3(0.0);
  {
    float distance    = length(ubo.pointLightPos.xyz - pos.xyz);
    float attenuation = 1.0 / (distance);
    vec3 radiance     = ubo.pointLightColor.rgb * attenuation;
    
    vec3 L = normalize(ubo.pointLightPos.xyz - pos.xyz);
    Lo += CalculateLightContribution(V, L, N, radiance, F0, albedo, roughness, metallic);

    radiance = ubo.directionalLightColor.rgb;
    L = normalize(ubo.directionalLightDir.xyz);
    Lo += CalculateLightContribution(V, L, N, radiance, F0, albedo, roughness, metallic);
  }

  // ao uv
  vec2 aouv = uv - 0.5;
  uint aoRotate = aoIndex >> 4;
  mat2 aoRotationMatrix = mat2(1, 0, 0, 1);
  if(aoRotate == 1) aoRotationMatrix = mat2(0, -1, 1, 0);
  if(aoRotate == 2) aoRotationMatrix = mat2(-1, 0, 0, -1);
  if(aoRotate == 3) aoRotationMatrix = mat2(0, 1, -1, 0);
  aouv = aoRotationMatrix * aouv + 0.5;
  aouv = aouv * 0.6666 + 0.1666;
  
  // Ambient
  vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);
  vec3 kD = 1.0 - kS;
  vec3 irradiance = texture(skySamp, N).rgb;
  vec3 diffuse = irradiance * albedo;
  vec3 ambient = (kD * diffuse);
  //vec3 ambient = vec3(0.03) * albedo;

  // Tonemap
  vec3 color = (ambient + Lo) * texture(sampler2D(ao[aoIndex & 0xf], samp), aouv).r;


  color = color / (color + vec3(1.0));
  color = pow(color, vec3(1.0/2.2));
  outColor = vec4(color, 1.0);
}
