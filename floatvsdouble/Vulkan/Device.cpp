
#include "stdafx.h"

#define GLM_FORCE_RADIANS
#define GLFW_INCLUDE_VULKAN
#include "GLFW\glfw3.h"
#undef APIENTRY
#include <iostream>
#include <array>
#include <Windows.h>
#undef max
#undef min

#include "Device.h"
#include "Buffer.h"
#include "CommandBuffer.h"
#include "RenderPass.h"
#include "DepthBuffer.h"
#include "Synchronization.h"
#include "Window.h"

namespace gfx
{

	DeviceHandle* DeviceHandle::Create(wnd::WindowHandle* window) { return new Device((wnd::Window*)window); }
	void DeviceHandle::Destroy(DeviceHandle* handle) { delete (Device*)handle; }

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData)
	{
		std::cout << "validation layer: " << msg << std::endl << std::endl;

		return VK_FALSE;
	}



	Device::Device(wnd::Window* window)
	{
		const std::vector<const char*> validationLayers =
		{
			"VK_LAYER_LUNARG_standard_validation"
		};

#ifdef NDEBUG
		const bool enableValidationLayers = false;
#else
		const bool enableValidationLayers = true;
#endif

		this->window = window->window;
		screenWidth = window->GetWidth();
		screenHeight = window->GetHeight();

		// Mostly optional app info
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "Hello Triangle";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "No Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;

		// Build up our extensions
		std::vector<const char*> extensions;
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
		for (uint32_t i = 0; i < glfwExtensionCount; i++)
			extensions.push_back(glfwExtensions[i]);
		if (enableValidationLayers)
			extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

		// createInfo basics
		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		createInfo.ppEnabledExtensionNames = extensions.data();

		// Validation
		if (enableValidationLayers)
		{
			createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else
			createInfo.enabledLayerCount = 0;

		// RELEASE THE KRAKEN!
		VK_ASSERT(vkCreateInstance(&createInfo, nullptr, &instance), "failed to create instance!");
		printf("Created vk object: %llu (VkInstance)\n", (uint64)instance);

		// Report callback
		VkDebugReportCallbackCreateInfoEXT debugCallbackCreateInfo = {};
		debugCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
		debugCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
		debugCallbackCreateInfo.pfnCallback = debugCallback;

		auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
		if (func != nullptr)
		{
			func(instance, &debugCallbackCreateInfo, nullptr, &callback);
		}

		// Select a device
		uint32_t deviceCount = 0;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
		if (deviceCount == 0)
			throw std::runtime_error("failed to find GPUs with Vulkan support!");

		std::vector<VkPhysicalDevice> devices(deviceCount);
		vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

		VkPhysicalDeviceFeatures deviceFeatures;
		for (const auto& device : devices)
		{
			VkPhysicalDeviceProperties deviceProperties;
			vkGetPhysicalDeviceProperties(device, &deviceProperties);
			vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

			uint32_t extensionCount;
			vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

			std::vector<VkExtensionProperties> availableExtensions(extensionCount);
			vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

			bool supportsSwapChain = false;
			for(VkExtensionProperties extension : availableExtensions)
				if(strcmp(extension.extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0)
					supportsSwapChain = true;

			if(deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && deviceFeatures.geometryShader && supportsSwapChain)
			{
				physicalDevice = device;
				break;
			}
		}

		if (physicalDevice == VK_NULL_HANDLE)
			throw std::runtime_error("failed to find a suitable GPU!");

		// Surface!
		VK_ASSERT(glfwCreateWindowSurface(instance, window->window, nullptr, &surface), "failed to create window surface!");

		InitQueueFamilies();

		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = graphicsQueueFamily;
		queueCreateInfo.queueCount = 1;
		float queuePriority = 1.0f;
		queueCreateInfo.pQueuePriorities = &queuePriority;

		// Create a logical device
		VkDeviceCreateInfo deviceInfo = {};
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceInfo.pQueueCreateInfos = &queueCreateInfo;
		deviceInfo.queueCreateInfoCount = 1;
		deviceInfo.pEnabledFeatures = &deviceFeatures;
		deviceInfo.enabledExtensionCount = 1;
		const char* swapChainExtName = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
		deviceInfo.ppEnabledExtensionNames = &swapChainExtName;
		deviceFeatures.samplerAnisotropy = VK_TRUE;

		if (enableValidationLayers)
		{
			deviceInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			deviceInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else
		{
			deviceInfo.enabledLayerCount = 0;
		}
		VK_ASSERT(vkCreateDevice(physicalDevice, &deviceInfo, nullptr, &device), "failed to create logical device!");
		printf("Created vk object: %llu (VkDevice)\n", (uint64)device);

		// Grab the queue we apparently made.
		vkGetDeviceQueue(device, graphicsQueueFamily, 0, &graphicsQueue);


		CreateSwapChain(VK_NULL_HANDLE);
		CreateCommandPool();
		CreateImageViews();
		finalRenderPass = new RenderPass(this, swapChainImageFormat);
		CreateFrameBuffers();
		CreateSemaphores();
		CreateDescriptorPool();
		CreateKtxDevice();

		// Print extensions available
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data());
		std::cout << "available extensions:" << std::endl;
		for (const auto& extension : availableExtensions)
			std::cout << "\t" << extension.extensionName << std::endl;

		// Print layers available
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());
		std::cout << "available layers:" << std::endl;
		for (const auto& layerProperties : availableLayers)
			std::cout << "\t" << layerProperties.layerName << std::endl;
	}

	void Device::InitQueueFamilies()
	{
		// Create a queue
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

		graphicsQueueFamily = -1;
		for (int32 i = 0; i < queueFamilies.size(); i++)
		{
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentSupport);
			if (queueFamilies[i].queueCount > 0 && queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && presentSupport)
			{
				graphicsQueueFamily = i;
				break;
			}
		}
	}

	void Device::CreateSwapChain(VkSwapchainKHR oldSwapChain)
	{
		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		uint32_t surfaceFormatCount;
		std::vector<VkSurfaceFormatKHR> surfaceFormats;
		uint32_t presentModeCount;
		std::vector<VkPresentModeKHR> presentModes;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, nullptr);
		if (surfaceFormatCount != 0)
		{
			surfaceFormats.resize(surfaceFormatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, surfaceFormats.data());
		}

		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
		if (presentModeCount != 0)
		{
			presentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes.data());
		}

		VkSurfaceFormatKHR finalSurfaceFormat;
		for(VkSurfaceFormatKHR surfaceFormat : surfaceFormats)
			if(surfaceFormat.format == VK_FORMAT_B8G8R8A8_UNORM)
				finalSurfaceFormat = surfaceFormat;
		VkPresentModeKHR presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
		VkExtent2D extent = surfaceCapabilities.currentExtent;

		VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
		swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		swapChainCreateInfo.surface = surface;
		swapChainCreateInfo.minImageCount = 3;
		swapChainCreateInfo.imageFormat = finalSurfaceFormat.format;
		swapChainCreateInfo.imageColorSpace = finalSurfaceFormat.colorSpace;
		swapChainCreateInfo.imageExtent = extent;
		swapChainCreateInfo.imageArrayLayers = 1;
		swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		swapChainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
		swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		swapChainCreateInfo.presentMode = presentMode;
		swapChainCreateInfo.clipped = VK_TRUE;
		swapChainCreateInfo.oldSwapchain = oldSwapChain;

		VK_ASSERT(vkCreateSwapchainKHR(device, &swapChainCreateInfo, nullptr, &swapChain), "failed to create swap chain!");
		printf("Created vk object: %llu (VkSwapchainKHR)\n", (uint64)swapChain);

		uint32_t imageCount;
		vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
		swapChainImages.resize(imageCount);
		VkResult result = vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());

		swapChainImageFormat = swapChainCreateInfo.imageFormat;
		swapChainExtent = swapChainCreateInfo.imageExtent;
	}


	void Device::CreateImageViews()
	{
		//Create image views for the swap chain buffers
		swapChainImageViews.resize(swapChainImages.size());
		for (int32 i = 0; i < swapChainImages.size(); i++)
		{
			swapChainImageViews[i] = CreateImageView(swapChainImages[i], swapChainImageFormat);
		}

		depthBuffer = DepthBufferHandle::Create(this);
	}

	VkImageView Device::CreateImageView(VkImage image, VkFormat format)
	{
		VkImageViewCreateInfo viewInfo = {};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = format;
		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.levelCount = 1;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.layerCount = 1;

		VkImageView newView;
		VK_ASSERT(vkCreateImageView(device, &viewInfo, nullptr, &newView), "failed to create texture image view!");
		printf("Created vk object: %llu (VkImageView)\n", (uint64)newView);

		return newView;
	}

	void Device::CreateFrameBuffers()
	{
		// Frame buffers
		swapChainFramebuffers.resize(swapChainImageViews.size());
		for (int32 i = 0; i < swapChainImageViews.size(); i++)
		{
			VkImageView viewAttachments[] = {swapChainImageViews[i], ((DepthBuffer*)depthBuffer)->view};
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = finalRenderPass->renderPass;
			framebufferInfo.attachmentCount = sizeof(viewAttachments) / sizeof(viewAttachments[0]);
			framebufferInfo.pAttachments = viewAttachments;
			framebufferInfo.width = swapChainExtent.width;
			framebufferInfo.height = swapChainExtent.height;
			framebufferInfo.layers = 1;

			VK_ASSERT(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i]), "failed to create framebuffer!");
			printf("Created vk object: %llu (VkFrameBuffer)\n", (uint64)swapChainFramebuffers[i]);
		}
	}

	void Device::CreateSemaphores()
	{
		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		VK_ASSERT(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore), "failed to create semaphore 1!");
		printf("Created vk object: %llu (VkSemaphore)\n", (uint64)imageAvailableSemaphore);
		VK_ASSERT(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore), "failed to create semaphore 2!");
		printf("Created vk object: %llu (VkSemaphore)\n", (uint64)renderFinishedSemaphore);
	}

	void Device::CreateDescriptorPool()
	{
		std::array<VkDescriptorPoolSize, 5> poolSizes = {};
		poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		poolSizes[0].descriptorCount = 100;
		poolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
		poolSizes[1].descriptorCount = 100;
		poolSizes[2].type = VK_DESCRIPTOR_TYPE_SAMPLER;
		poolSizes[2].descriptorCount = 100;
		poolSizes[3].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		poolSizes[3].descriptorCount = 1000;
		poolSizes[4].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		poolSizes[4].descriptorCount = 100;

		VkDescriptorPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
		poolInfo.pPoolSizes = poolSizes.data();
		poolInfo.maxSets = 100;

		VK_ASSERT(vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool), "failed to create descriptor pool!");
		printf("Created vk object: %llu (VkDescriptorPool)\n", (uint64)descriptorPool);
	}

	void Device::CreateCommandPool()
	{
		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = graphicsQueueFamily;
		poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional
		VK_ASSERT(vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool), "failed to create command pool!");
		printf("Created vk object: %llu (VkCommandPool)\n", (uint64)commandPool);
	}

	void Device::CreateKtxDevice()
	{
		ktxVulkanDeviceInfo_Construct(&ktxDevice, physicalDevice, device, graphicsQueue, commandPool, nullptr);
	}

	void Device::HandleOutOfDateKHR()
	{
		StackEvents ev("Device::HandleOutOfDateKHR");
		vkDeviceWaitIdle(device);
		glfwGetWindowSize(window, &screenWidth, &screenHeight);

		VkSwapchainKHR oldSwapChain = swapChain;
		CreateSwapChain(oldSwapChain);
		for (auto imageView : swapChainImageViews) vkDestroyImageView(device, imageView, nullptr);
		vkDestroySwapchainKHR(device, oldSwapChain, nullptr);
		CreateImageViews();
		for (auto framebuffer : swapChainFramebuffers) vkDestroyFramebuffer(device, framebuffer, nullptr);
		CreateFrameBuffers();
	}

	Device::~Device()
	{
		vkDeviceWaitIdle(device);

		DepthBufferHandle::Destroy(depthBuffer);

		vkDestroyDescriptorPool(device, descriptorPool, nullptr);

		delete finalRenderPass;
		
		for (auto framebuffer : swapChainFramebuffers) vkDestroyFramebuffer(device, framebuffer, nullptr);
		for (auto imageView : swapChainImageViews) vkDestroyImageView(device, imageView, nullptr);
		vkDestroySwapchainKHR(device, swapChain, nullptr);

		vkDestroyCommandPool(device, commandPool, nullptr);

		vkDestroySemaphore(device, renderFinishedSemaphore, nullptr);
		vkDestroySemaphore(device, imageAvailableSemaphore, nullptr);

		vkDestroyDevice(device, nullptr);

		auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
		if (func != nullptr)
			func(instance, callback, nullptr);

		vkDestroyInstance(instance, nullptr);
	}

	RenderPassHandle* Device::getFinalRenderPass()
	{
		return finalRenderPass;
	}

	bool Device::BeginFrame()
	{
		// Get the next back buffer
		StackEvents ev("Device::BeginFrame");
		VkResult result = vkAcquireNextImageKHR(device, swapChain, std::numeric_limits<uint64_t>::max(), imageAvailableSemaphore, VK_NULL_HANDLE, &currentSwapChainImageIndex);
		if(result == VK_ERROR_OUT_OF_DATE_KHR)
		{
			printf("Detected out of date KHR in BeginFrame\n");
			HandleOutOfDateKHR();
			return true;
		}
		VK_ASSERT(result, "Unknown error with vkAcquireNextImageKHR");
		return false;
	}

	void Device::Submit(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore, FenceHandle* fence)
	{
		StackEvents ev("Device::Submit");
		
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		VkSemaphore waitSemaphores[2] = {imageAvailableSemaphore};
		VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &((CommandBuffer*)commandBuffer)->commandBuffer;
		VkSemaphore signalSemaphores[2] = {imageAvailableSemaphore};
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		VkPipelineStageFlags stageFlags[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		if(beginSemaphore != nullptr)
		{
			submitInfo.waitSemaphoreCount++;
			waitSemaphores[1] = ((Semaphore*)beginSemaphore)->semaphore;
			submitInfo.pWaitDstStageMask = stageFlags;
		}
		if(endSemaphore != nullptr)
		{
			submitInfo.signalSemaphoreCount++;
			signalSemaphores[1] = ((Semaphore*)endSemaphore)->semaphore;
		}

		StackEvents::Push("vkQueueSubmit");
		VK_ASSERT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, fence ? ((Fence*)fence)->fence : VK_NULL_HANDLE), "failed to submit draw command buffer!");
		StackEvents::Pop();
		vkDeviceWaitIdle(device);
	}

	void Device::SubmitOutOfFrame(CommandBufferHandle* commandBuffer, SemaphoreHandle* beginSemaphore, SemaphoreHandle* endSemaphore)
	{
		StackEvents ev("Device::SubmitOutOfFrame");

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		VkSemaphore waitSemaphores[1] = {};
		VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		submitInfo.waitSemaphoreCount = 0;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &((CommandBuffer*)commandBuffer)->commandBuffer;
		VkSemaphore signalSemaphores[1] = {};
		submitInfo.signalSemaphoreCount = 0;
		submitInfo.pSignalSemaphores = signalSemaphores;

		VkPipelineStageFlags stageFlags[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		if(beginSemaphore != nullptr)
		{
			submitInfo.waitSemaphoreCount++;
			waitSemaphores[0] = ((Semaphore*)beginSemaphore)->semaphore;
			submitInfo.pWaitDstStageMask = stageFlags;
		}
		if(endSemaphore != nullptr)
		{
			submitInfo.signalSemaphoreCount++;
			signalSemaphores[0] = ((Semaphore*)endSemaphore)->semaphore;
		}

		StackEvents::Push("vkQueueSubmit");
		VK_ASSERT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE), "failed to submit out of frame command buffer");
		StackEvents::Pop();
		vkDeviceWaitIdle(device);
	}

	bool Device::EndFrame()
	{
		StackEvents ev("Device::EndFrame");

		// Submit draws
		StackEvents::Push("vkQueueSubmit");
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		VkSemaphore waitSemaphores[] = {imageAvailableSemaphore};
		VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 0;
		submitInfo.pCommandBuffers = nullptr;
		VkSemaphore signalSemaphores[] = {renderFinishedSemaphore};
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;
		VK_ASSERT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE), "failed to submit draw command buffer!");
		StackEvents::Pop();

		// Present
		StackEvents::Push("vkQueuePresentKHR");
		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &renderFinishedSemaphore;

		VkSwapchainKHR swapChains[] = {swapChain};
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapChains;
		presentInfo.pImageIndices = &currentSwapChainImageIndex;

		presentInfo.pResults = nullptr; // Optional

		VkResult result = vkQueuePresentKHR(graphicsQueue, &presentInfo);

		if(result == VK_ERROR_OUT_OF_DATE_KHR)
		{
			printf("Detected out of date KHR in EndFrame\n");
			HandleOutOfDateKHR();
			StackEvents::Pop();
			return true;
		}
		VK_ASSERT(result, "failed to present!");
		StackEvents::Pop();
		return false;
	}

	void Device::WaitIdle()
	{
		StackEvents ev("Device::WaitIdle");
		vkQueueWaitIdle(graphicsQueue);
	}
}
